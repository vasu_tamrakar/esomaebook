<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2">
            <?php include 'frontleftmenu.php'; ?>
        </div>
        <div class="col-md-10 col-lg-10">
            <div class="row">
                <div class="col-md-12">
                <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                  	<div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                      <?php
                  }
                  ?>
                  	<div class="mb-5">
                    	<!-- <h2 class="heading text-center"><strong>Change Password</strong></h2> -->
                  	</div>
                  	<div class="row justify-content-md-center">
                    		<div class="col-md-9">
    	                  		<div class="card" style="box-shadow: 0 0 3px 0px black;margin-bottom: 4rem;">
      	                  			<div class="card-body">
                                    <h2 style="margin-bottom: 41px;text-align: center;">Manage My Books</h2>
                          					<form name="cmanagemybook_Form" id="cmanagemybook_Form" method="post">
                            						<div class="row">
            	                  						<div class="col-md-12">
              		                  				<?php
                                            if(isset($managemyBooks[0]->ba_id)){
                                                $somalibooksID = $this->user_Auth->getData('books',$w=array('b_fk_of_aid' => 0,'b_category' => '5', 'b_status' => '1'), $s= 'b_id, b_title');
                                                ?>
                                                <input type="hidden" name="accessid" id="accessid" value="<?php echo $managemyBooks[0]->ba_id ; ?>">
                                                <div class="row">
                                                    <label for="txt_newpass" class="col-md-3">Change Book : </label>
                                                    <select name="selectbookId" id="selectbookId" class="form-control col-md-9" required>
                                                        <option value=""> - Select Book -</option>
                                                        <?php 
                                                        foreach ($somalibooksID as $value) {
                                                          // print_r($value); die;
                                                            if($value->b_id){
                                                        ?>
                                                            <option value="<?php echo $value->b_id; ?>" <?php if($value->b_id == $managemyBooks[0]->ba_bookid){ echo 'selected'; } ?>><?php echo (($value->b_id)?$value->b_title:''); ?></option>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <div style="margin:12px;"></div>
                                                </div>
                                                <?php
                                                
                                            }else{
                                                echo 'No more data!.';
                                            }
                                            ?>
            	                  						</div>
                                            
          		                  				</div>
                                        <?php
                                        if(isset($managemyBooks[0]->ba_id)){ ?> 
          		                  				<div class="row justify-content-md-center">
          		                  					<button type="submit" name="submit" class="btn btn-info">Save</button>
          		                  				</div>
                                        <?php } ?>
                          					</form>
                        				</div>
    	                  		</div>
  	                  	</div>	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Button to Open the Modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Open modal
</button> -->

<!-- The Modal -->
<div class="modal fade" id="myModalview">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Order Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<style type="text/css">
body {
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 102px;
height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.site-section {
    padding: 0;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
.leftsidebar {
    height: auto;
}

a.site_title:hover{
    color: #fff;
}
.navbar-nav .nav-link {
    padding: 0.3rem 1rem;
}
.fa::after {
  margin-right: 2px;
  content: ;
  content: "";
}
label.error {
    position: relative;
    margin: 0 auto;
}
@media only screen and (max-width: 992px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 167px !important;
        height: 21vh;
    }
} 
</style>