<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="fb-root"></div>

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0&appId=470017186888654&autoLogAppEvents=1"></script>
<!-- <div class="site-blocks-cover overlay" style="background-image: url(<?php echo base_url('assets/images/hero_2.jpg'); ?>);" data-aos="fade" id="home-section"> -->
<!-- Start slider section -->
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">

  <div class="clearfix">
    <?php
    if($frontSlider){ ?>
    <div id="frontSlider" class="owl-carousel">
               
      <?php
        foreach ($frontSlider as $slide) {
          ?>
          <div class="item" style='background:url(<?php echo (($slide->s_image)?base_url('uploads/sliders/'.$slide->s_image):base_url('uploads/sliders/slider_1.png')); ?>);'>
              <!-- <img src="<?php echo (($slide->s_image)?base_url('uploads/sliders/'.$slide->s_image):base_url('uploads/sliders/slider_1.png')); ?>" alt="<?php echo $slide->s_heading; ?>" class="img-responsive">
              --> 
              <div class="sliderbanner">
                  <h2 class="heading"> <?php echo $slide->s_heading; ?></h2>
                  <p> <?php echo $slide->s_subtitle; ?></p>
                  <span><?php echo $slide->s_tagline; ?> </span>
                  <div class="link"><a href="<?php echo (($slide->s_link)?$slide->s_link:base_url()); ?>">Read Now</a></div>
              </div>
          </div>
          <?php
/*
            $slide->s_id;
            $slide->s_heading;
            $slide->s_subtitle;
            $slide->s_tagline;
            $slide->s_link;
            $slide->s_image;
            $slide->s_order;
            $slide->s_status;
            $slide->s_created;
            $slide->s_modified;
            */
            } ?>
        </div>
        <?php
        }
        ?>
    </div>
</div>  
<!-- End slider section -->


<!-- Start counter section -->
<div class="record-section" id="recordsection">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
              <div class="record">
                <div class="imagebox">
                  <img src="<?php echo base_url('assets/images/books_we_have.png'); ?>" class="img-fluid">
                </div>
                <label><b><?php echo (($totalBooks)?$totalBooks:0); ?>+ </b></label>
                <label>Books we have</label>
              </div>
            </div>
            <div class="col-md-4">
              <div class="record" style="border-left: 1px solid;border-right: 1px solid;">
                <div class="imagebox">
                  <img src="<?php echo base_url('assets/images/somali_authors.png'); ?>" class="img-fluid">
                </div>
                <label><b><?php echo (($somaliAuthors)?$somaliAuthors:0); ?></b></label>
                <label>Somali Authors</label>
              </div>
            </div>

            <div class="col-md-4">
              <div class="record">
                <div class="imagebox">
                  <img src="<?php echo base_url('assets/images/total_downloads.png'); ?>" class="img-fluid">
                </div>
                <label><b><?php echo $totalDownload; ?>+ </b></label>
                <label>Total downloads</label>
              </div>
            </div>
        </div>
    </div>  
</div>
<!-- End counter section -->

<?php
if(!empty($featuredBooksID)){
?>
<section class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div><h4 class="heading">FEATURED BOOKS</h4></div>
                <div class="border-top pt-4"></div>
                <div id ="featureBooks" class="owl-carousel owl-theme">
                <?php 
                foreach ($featuredBooksID as $fbdetails) {
                    $featurebooks= $this->user_Auth->getData('books',array('b_id' => $fbdetails->b_id));
                    ?>
                    <div class="item">
                        <div class="featurebooks">
                            <?php
                            if(($featurebooks[0]->b_originalprice > 0) && ($featurebooks[0]->b_originalprice > $featurebooks[0]->b_sellingprice)){
                            ?>
                            <span class="discount"><?php $featurebookPercetage = ((($featurebooks[0]->b_originalprice - $featurebooks[0]->b_sellingprice)*100)/$featurebooks[0]->b_originalprice);echo (($featurebookPercetage > 0)?number_format($featurebookPercetage,0):''); ?><span class="percent-sign">%</span></span>
                            <?php
                            }
                            ?>
                            <a href="<?php echo site_url('books/'.$featurebooks[0]->b_id); ?>">
                                <img src="<?php echo (isset($featurebooks[0]->b_image)?base_url('uploads/books/'.$featurebooks[0]->b_image):base_url('images/books/noimage.jpg')); ?>" alt="<?php echo (isset($featurebooks[0]->b_title)?$featurebooks[0]->b_title:'Image'); ?>" class="img-fluid">
                            </a>
                            <div class="readdiv"><a href="<?php echo site_url('books/'.$featurebooks[0]->b_id); ?>">READ NOW</a></div>
                        </div>
                        <div class="describe">
                            <div class="rates">
                                <?php
                                $allAvgRating = array();
                                $avgRating = 0;
                                $allAvgRating = $this->book_model->getAvgRating($featurebooks[0]->b_id);
                                $avgRating = $allAvgRating[0]->rating;

                                ?>
                                <div class="my-rating-small" data-rating="<?php echo $avgRating;?>" ></div>
                            </div>
                            <label><?php echo (isset($featurebooks[0]->b_title)?$featurebooks[0]->b_title:''); ?></label>
                            <span>
                            <?php if (isset($featurebooks[0]->b_fk_of_aid)); 
                                $author = $this->user_Auth->getData("user_credentials", array("uc_id" => $featurebooks[0]->b_fk_of_aid));
                                echo (isset($author[0]->uc_firstname)?$author[0]->uc_firstname:'').' '.(isset($author[0]->uc_lastname)?$author[0]->uc_lastname:'');
                            ?>
                            </span>
                            <!-- Your share button code -->
                            <!-- <div class="fb-share-button" data-href="<?php echo site_url('book/bookDetails/'.$fbdetails->b_id); ?>" data-layout="button" data-size="large">
                            </div> -->

                        </div>
                        
                    </div>
                <?php
                }
                ?>
                </div>

          </div>
          
        </div>
    </div>
</section>
<?php 
}
?>
<?php if($frontcategory){ ?>
<section class="site-section border-bottom" id="services-section" style="background: #EAFCFE;">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 text-center">
                <h2 class="section-title mb-3">CATEGORIES</h2>
            </div>
        </div>
        <div class="row">
            <?php
            // print_r($frontcategory);
            $c = 1;
            foreach ($frontcategory as $categories) {
                if($c < 3){ 
                    if($c ==1) { ?>
                        <div class="col-md-3">
                    <?php 
                    } ?>
                            <div class="cart">
                                <div class="figure">
                                    <img src="<?php echo (($categories->c_image)?base_url('uploads/category/'.$categories->c_image):base_url('uploads/category/category.png')); ?>" class="img-fluid" alt="<?php echo (($categories->c_name)?$categories->c_name:""); ?>">
                                    <div class="overlay">
                                        <center><a href="<?php echo site_url('category/'.$categories->c_slug); ?>"><?php echo (($categories->c_name)?$categories->c_name:""); ?>  </a></center>
                                    </div>
                                </div>
                            </div>
                    <?php
                    if($c == 2){ ?>
                        </div>
                    <?php
                    } ?>
                <?php
                $c++;
                }else if($c > 2 && $c < 6) {
                    if($c == 3) { ?>
                        <div class="col-md-3">
                    <?php 
                    } ?>
                            <div class="cart">
                                <div class="figure">
                                    <img src="<?php echo (($categories->c_image)?base_url('uploads/category/'.$categories->c_image):base_url('uploads/category/category.png')); ?>" class="img-fluid" alt="<?php echo (($categories->c_name)?$categories->c_name:""); ?>">
                                    <div class="overlay">
                                        <center><a href="<?php echo site_url('category/'.$categories->c_slug); ?>"><?php echo (($categories->c_name)?$categories->c_name:""); ?>  </a></center>
                                    </div>
                                </div>
                            </div>
                    <?php
                    if($c == 5){ ?>
                        </div>
                    <?php
                    } 
                    $c++;
                }else if($c > 5 &&  $c < 9) { 
                    if($c == 6) { ?>
                        <div class="col-md-3">
                    <?php 
                    } ?>
                            <div class="cart">
                                <div class="figure">
                                    <img src="<?php echo (($categories->c_image)?base_url('uploads/category/'.$categories->c_image):base_url('uploads/category/category.png')); ?>" class="img-fluid" alt="<?php echo (($categories->c_name)?$categories->c_name:""); ?>">
                                    <div class="overlay">
                                        <center><a href="<?php echo site_url('category/'.$categories->c_slug); ?>"><?php echo (($categories->c_name)?$categories->c_name:""); ?>  </a></center>
                                    </div>
                                </div>
                            </div>
                    <?php
                    if($c == 8){ ?>
                        </div>
                    <?php
                    } 
                    $c++;
                }else{ 
                    if($c == 9) { ?>
                        <div class="col-md-3">
                    <?php 
                    } ?>
                            <div class="cart">
                                <div class="figure">
                                    <img src="<?php echo (($categories->c_image)?base_url('uploads/category/'.$categories->c_image):base_url('uploads/category/category.png')); ?>" class="img-fluid" alt="<?php echo (($categories->c_name)?$categories->c_name:""); ?>">
                                    <div class="overlay">
                                        <center><a href="<?php echo site_url('category/'.$categories->c_slug); ?>"><?php echo (($categories->c_name)?$categories->c_name:""); ?>  </a></center>
                                    </div>
                                </div>
                            </div>
                    <?php
                    if($c == 10){ ?>
                        </div>
                    <?php
                    } 
                    $c++;
                }

                if($c == 11){
                  $c=1;
                }
            }
            ?>
        </div>
    </div>
</section>
<?php } ?>
<?php
if(!empty($recentlyreleaseBooksID)){
?>
<section class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div><h4 class="heading">RECENTLY RELEASED BOOKS</h4></div>
                <div class="border-top pt-4"></div>
                <div id ="recentlyreleasedBooks" class="owl-carousel owl-theme">
                    <?php
                    foreach ($recentlyreleaseBooksID as $key => $rbookid) {
                        $recentDetails = $this->user_Auth->getData('books',array('b_id' => $rbookid->b_id));
                    ?>
                    <div class="item">
                        <div class="featurebooks">
                            <?php
                            if(($recentDetails[0]->b_originalprice > 0) && ($recentDetails[0]->b_originalprice > $recentDetails[0]->b_sellingprice)){
                            ?>
                            <span class="discount"><?php $recentDetailsPercetage = ((($recentDetails[0]->b_originalprice - $recentDetails[0]->b_sellingprice)*100)/$recentDetails[0]->b_originalprice);echo (($recentDetailsPercetage > 0)?number_format($recentDetailsPercetage,0):''); ?><span class="percent-sign">%</span></span>
                            <?php
                            }
                            ?>
                            <a href="<?php echo site_url('books/'.$recentDetails[0]->b_id); ?>">
                                <img src="<?php echo (isset($recentDetails[0]->b_image)?base_url('uploads/books/'.$recentDetails[0]->b_image):base_url('images/books/noimage.jpg')); ?>" alt="<?php echo (isset($recentDetails[0]->b_title)?$featurebooks[0]->b_title:'Image'); ?>" class="img-fluid">
                            </a>
                            <div class="readdiv"><a href="<?php echo site_url('books/'.$recentDetails[0]->b_id); ?>">READ NOW</a></div>
                        </div>
                        <div class="describe">
                            <div class="rates">
                            <?php
                                $allAvgRating = array();
                                $avgRating = 0;
                                $allAvgRating = $this->book_model->getAvgRating($recentDetails[0]->b_id);
                                $avgRating = $allAvgRating[0]->rating;

                                ?>
                                <div class="my-rating-small" data-rating="<?php echo $avgRating;?>" ></div>
                            </div>
                            <label><?php echo (isset($recentDetails[0]->b_title)?$recentDetails[0]->b_title:''); ?></label>
                            <span>
                            <?php if (isset($recentDetails[0]->b_fk_of_aid)); 
                                $authorR = $this->user_Auth->getData("user_credentials", array("uc_id" => $recentDetails[0]->b_fk_of_aid));
                                echo (isset($authorR[0]->uc_firstname)?$authorR[0]->uc_firstname:'').' '.(isset($authorR[0]->uc_lastname)?$authorR[0]->uc_lastname:'');
                            ?>
                            </span>
                            <!-- Your share button code -->
                            <!-- <div class="fb-share-button" data-href="<?php echo site_url('book/bookDetails/'.$rbookid->b_id); ?>" data-layout="button" data-size="large">
                            </div> -->
                        </div>
                    </div>

                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>        
    </div>
</section>
<?php
}
?>
<?php

if(!empty($mostpopularBooksID)){
?>
 <section class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div><h4 class="heading">MOST POPULAR BOOKS</h4></div>
                <div class="border-top pt-4"></div>
                <div id ="mostPopularbooks" class="owl-carousel owl-theme">
                    <?php

                    foreach ($mostpopularBooksID as $mpbookid) {
                        $popularDetails = $this->book_model->getBookDetails($mpbookid->b_id);
                        /*$popularDetails = $this->user_Auth->getData('books',array('b_id' => $mpbookid->b_id, 'b_status' => '1'));*/
                        if(isset($popularDetails)){
                    ?>
                    <div class="item">
                        <div class="featurebooks">
                            <?php
                            if(($popularDetails[0]->b_originalprice > 0) && ($popularDetails[0]->b_originalprice > $popularDetails[0]->b_sellingprice)){
                            ?>
                            <span class="discount"><?php $popularDetailsPercetage = ((($popularDetails[0]->b_originalprice - $popularDetails[0]->b_sellingprice)*100)/$popularDetails[0]->b_originalprice);echo (($popularDetailsPercetage > 0)?number_format($popularDetailsPercetage,0):''); ?><span class="percent-sign">%</span></span>
                            <?php
                            }
                            ?>
                            <a href="<?php echo site_url('books/'.$popularDetails[0]->b_id); ?>">
                                <img src="<?php echo (isset($popularDetails[0]->b_image)?base_url('uploads/books/'.$popularDetails[0]->b_image):base_url('images/books/noimage.jpg')); ?>" alt="<?php echo (isset($popularDetails[0]->b_title)?$popularDetails[0]->b_title:'Image'); ?>" class="img-fluid">
                            </a>
                            <div class="readdiv"><a href="<?php echo site_url('books/'.$popularDetails[0]->b_id); ?>">READ NOW</a></div>
                        </div>
                        <div class="describe">
                            <div class="rates">
                            <?php
                                $allAvgRating = array();
                                $avgRating = 0;
                                $allAvgRating = $this->book_model->getAvgRating($popularDetails[0]->b_id);
                                $avgRating = $allAvgRating[0]->rating;

                                ?>
                                <div class="my-rating-small" data-rating="<?php echo $avgRating;?>" ></div>
                            </div>
                            <label><?php echo (isset($popularDetails[0]->b_title)?$popularDetails[0]->b_title:''); ?></label>
                            <span>
                            <?php if (isset($popularDetails[0]->b_fk_of_aid)); 
                                $authorP = $this->user_Auth->getData("user_credentials", array("uc_id" => $popularDetails[0]->b_fk_of_aid));
                                echo (isset($authorP[0]->uc_firstname)?$authorP[0]->uc_firstname:'').' '.(isset($authorP[0]->uc_lastname)?$authorP[0]->uc_lastname:'');
                            ?>
                            </span>
                            <!-- Your share button code -->
                            <!-- <div class="fb-share-button" data-href="<?php echo site_url('book/bookDetails/'.$mpbookid->b_id); ?>" data-layout="button" data-size="large">
                            </div> -->
                        </div>
                    </div>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>        
    </div>
</section> 
<?php
}
?>
<section class="site-section bg-res" style="background-size: 100%;background-position: center center;background-image: url(./assets/images/become_a_member_bg.png);background-repeat: no-repeat;">
    <div class="container">
        <div>
          <center><h3 class="heading">Do you wanna get unlimited access...? </h3>
          <div class="link"><a href="<?php echo site_url('membership'); ?>" class="memberbtn">Become a Member</a></div></center>
        </div>        
    </div>
</section>
<?php
if(!empty($popularAuthorID)){
?>
<section class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div><h4 class="heading">POPULAR AUTHORS</h4></div>
                <div class="border-top pt-4"></div>
                <div id ="PopularAuthors" class="owl-carousel owl-theme">
                    <?php
                    foreach ($popularAuthorID as $nauthor) {
                        $popAuthDetails = $this->user_Auth->getData('user_credentials',array('uc_id' => $nauthor->uc_id));  
                    ?>
                    <div class="item">
                        <div class="popauthors">

                            <img src="<?php echo ($nauthor->uc_image)?base_url('uploads/users/'.$nauthor->uc_image):base_url('uploads/users/author.png'); ?>" alt="<?php echo (isset($nauthor->uc_firstname)?$nauthor->uc_firstname:'Image'); ?>" class="img-fluid">
                             <div class="readdiv"><a href="<?php echo site_url('authors/'.$nauthor->uc_id); ?>">READ NOW</a></div> 
                        </div>
                        <div class="describe">
                            <label style="margin-top:5px;color: #585353bd;"><?php echo (isset($nauthor->uc_firstname)?$nauthor->uc_firstname:'').' '.(isset($nauthor->uc_lastname)?$nauthor->uc_lastname:''); ?></label>
                            <span>
                            <?php echo $nauthor->totalBooks;?> Published Books
                            </span>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>        
    </div>
</section>
<?php
}
?>


    <!-- <section class="site-section bg-light" id="contact-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h3 class="section-sub-title">Services</h3>
            <h2 class="section-title mb-3">Our Services</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-7 mb-5">

            

            <form action="#" class="p-5 bg-white">
              
              <h2 class="h4 text-black mb-5">Contact Form</h2> 

              <div class="row form-group">
                <div class="col-md-6 mb-3 mb-md-0">
                  <label class="text-black" for="fname">First Name</label>
                  <input type="text" id="fname" class="form-control">
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="lname">Last Name</label>
                  <input type="text" id="lname" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label> 
                  <input type="email" id="email" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="subject">Subject</label> 
                  <input type="subject" id="subject" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="text-black" for="message">Message</label> 
                  <textarea name="message" id="message" cols="30" rows="7" class="form-control" placeholder="Write your notes or questions here..."></textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <input type="submit" value="Send Message" class="btn btn-primary btn-md text-white">
                </div>
              </div>

  
            </form>
          </div>
          <div class="col-md-5">
            
            <div class="p-4 mb-3 bg-white">
              <p class="mb-0 font-weight-bold">Address</p>
              <p class="mb-4">203 Fake St. Mountain View, San Francisco, California, USA</p>

              <p class="mb-0 font-weight-bold">Phone</p>
              <p class="mb-4"><a href="#">+1 232 3235 324</a></p>

              <p class="mb-0 font-weight-bold">Email Address</p>
              <p class="mb-0"><a href="#">youremail@domain.com</a></p>

            </div>
            
          </div>
        </div>
      </div>
    </section> -->
    <style>
#headercatmenu a.nav-link.dropdown-toggle {
    color: #54C0E1 !important;
}
.sticky-wrapper.is-sticky .site-navbar ul li a {
    color: #54C0E1 !important
}
@media only screen and (max-width: 767px){
.site-navbar .site-navigation .site-menu > li > a {
    color: #54C0E1 !important;
}
}
@media only screen and (min-width: 768px) and (max-width: 1024px) {
  .site-navbar .site-navigation .site-menu > li > a{
    color:#54C0E1 !important;

  }
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
  min-height: 600px;
  height: 22vh;
}
.sliderbanner {
  position: absolute;
  top: 31%;
  left: 20%;
  width: 34%;
}
#frontSlider img {
    height: 600px;
    width: 100%;
    display: block;
}
.sliderbanner .heading {
  font-size: 58px;
  font-weight: bold;
  color: #747374;
  font-family: inherit;
  text-transform: capitalize;
}
.sliderbanner p {
  font-size: 22px;
  color: #747474;
  margin: 5px 0px 2px 0px;
  font-weight: 400;
}
.sliderbanner span {
    font-weight: bold;
    font-size: 30px;
    color: #53BDE6;
    line-height: 71px;
    letter-spacing: 4px;
    font-family: inherit;
}
.sliderbanner .link a {
  background: #54BFE3;
  padding: 11px 64px;
  color: #ffff;
  border-radius: 7px;
  font-weight: bold;
}


.featurebooks:hover a{
  color: #ffff;
}
a:hover {
    color: #fff;
}
.featurebooks img {
    box-shadow: 0 0 20px -7px black;
}

/*--------------------------*/
.btn-read {
        background: transparent;
        border-radius: 0;
        border: 1px solid #ffffff;
        color: #fff;
        margin: -8px;
    }
  .section-box-one{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
    }
    .section-box-one figure {
        position: absolute;
        text-align: center;
        padding: 19px;
        width: 100%;
        height: 100%;
    }
    .section-box-one img{
        height: 100%;
        position: absolute;
        transition: ease-in-out .5s;
        -webkit-transition: ease-in-out .5s;
        -moz-transition: ease-in-out .5s;
    }
    .section-box-one:hover img{
        transform: translate(100%, -100%);
        -webkit-transform: translate(100%, -100%);
        -moz-transform: translate(100%, -100%);
    }

    .section-box-two{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
    }
    .section-box-two figure {
        position: absolute;
        text-align: center;
        padding: 19px;
        width: 100%;
        height: 100%;
    }
    .section-box-two img{
        height: 100%;
        position: absolute;
        transition: ease-in-out .5s;
        -webkit-transition: ease-in-out .5s;
        -moz-transition: ease-in-out .5s;
    }
    .section-box-two:hover img{
        transform: translate(-100%, -100%);
        -webkit-transform: translate(-100%, -100%);
        -moz-transform: translate(-100%, -100%);
    }

    .section-box-three{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
    }
    .section-box-three figure {
        position: absolute;
        text-align: center;
        padding: 19px;
        width: 100%;
        height: 100%;
    }
    .section-box-three img{
        height: 100%;
        position: absolute;
        transition: ease-in-out .5s;
        -webkit-transition: ease-in-out .5s;
        -moz-transition: ease-in-out .5s;
    }
    .section-box-three:hover img{
        transform: translate(-100%, 100%);
        -webkit-transform: translate(-100%, 100%);
        -moz-transform: translate(-100%, 100%);
    }

    .section-box-four{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
    }
    .section-box-four figure {
        position: absolute;
        text-align: center;
        padding: 19px;
        width: 100%;
        height: 100%;
    }
    .section-box-four img{
        height: 100%;
        position: absolute;
        transition: ease-in-out .5s;
        -webkit-transition: ease-in-out .5s;
        -moz-transition: ease-in-out .5s;
    }
    .section-box-four:hover img{
        transform: translate(-100%,0);
        -webkit-transform: translate(-100%,0);
        -moz-transform: translate(-100%,0);
    }

    .section-box-five{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
    }
    .section-box-five figure {
        position: absolute;
        text-align: center;
        padding: 19px;
        width: 100%;
        height: 100%;
    }
    .section-box-five img{
        height: 100%;
        position: absolute;
        transition: ease-in-out .5s;
        -webkit-transition: ease-in-out .5s;
        -moz-transition: ease-in-out .5s;
    }
    .section-box-five:hover img{
        transform: translate(100%,0);
        -webkit-transform: translate(100%,0);
        -moz-transform: translate(100%,0);
    }

    .section-box-six{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
    }
    .section-box-six figure {
        position: absolute;
        text-align: center;
        padding: 19px;
        width: 100%;
        height: 100%;
    }
    .section-box-six img{
        height: 100%;
        position: absolute;
        transition: ease-in-out .5s;
        -webkit-transition: ease-in-out .5s;
        -moz-transition: ease-in-out .5s;
    }
    .section-box-six:hover img{
        transform: translate(0,-100%);
        -webkit-transform: translate(0,-100%);
        -moz-transform: translate(0,-100%);
    }

    .section-box-seven{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
    }
    .section-box-seven figure {
        position: absolute;
        text-align: center;
        padding: 19px;
        width: 100%;
        height: 100%;
    }
    .section-box-seven img{
        height: 100%;
        position: absolute;
        transition: ease-in-out .5s;
        -webkit-transition: ease-in-out .5s;
        -moz-transition: ease-in-out .5s;
    }
    .section-box-seven:hover img{
        transform: scale(0);
        -webkit-transform: scale(0);
        -moz-transform: scale(0);
        opacity: 0;
    }

    .section-box-eight{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
        transform: rotateY(-180deg);
        transition: ease-in-out .5s;
    }
    .section-box-eight:hover {
        transform: rotateY(0deg);
    }
    .section-box-eight:hover img {
        transform: rotateY(-180deg);
        backface-visibility: hidden;
        transition: ease-in-out .5s;
    }
    .section-box-eight figure {
        position: absolute;
        text-align: center;
        padding: 19px;
        width: 100%;
        height: 100%;
    }
    .section-box-eight img{
        height: 100%;
        position: absolute;
    }

    .section-box-nine{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
    }
    .section-box-nine figure {
        position: absolute;
        text-align: center;
        padding: 19px;
        width: 100%;
        height: 100%;
    }
    .section-box-nine img{
        height: 100%;
        position: absolute;
        transition: ease-in-out .5s;
        -webkit-transition: ease-in-out .5s;
        -moz-transition: ease-in-out .5s;
    }
    .section-box-nine:hover img{
        transform: scale(3);
        -webkit-transform: scale(3);
        -moz-transform: scale(3);
        opacity: 0;
    }

    .section-box-ten{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
    }
    .section-box-ten figure {
        position: absolute;
        text-align: center;
        padding: 10px 19px 19px 19px;
        width: 100%;
        height: 100%;
        border-top: 1px solid #fff;
        background: rgba(0, 0, 0, 0.71);
        bottom: -83%;
        transition: ease-in-out .5s;
    }
    .section-box-ten:hover figure {
        bottom: -16px;
    }
    .section-box-ten figure h3{
        margin: 0;
        padding-bottom: 10px;
    }
    .section-box-ten img{
        height: 100%;
    }


    .section-box-eleven{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
    }
    .section-box-eleven figure {
        position: absolute;
        padding: 10px 15px;
        width: 100%;
        bottom: -25%;
        transition: ease-in-out .5s;
        background: rgba(0, 0, 0, 0.71);
        border-top: 1px solid #fff;
    }
    .section-box-eleven:hover figure {
        bottom: 0;
    }
    .section-box-eleven img{
        height: 100%;
    }

    .section-box-twelve{
        height: 250px;
        background: radial-gradient(#6e6e6e,#2f2f2f);
        background: -webkit-radial-gradient(#6e6e6e,#2f2f2f);
        background: -moz-radial-gradient(#6e6e6e,#2f2f2f);
        color: #fff;
        position: relative;
        overflow:hidden;
    }
    .section-box-twelve figure {
        position: absolute;
        text-align: center;
        padding: 10px 19px 19px 19px;
        width: 100%;
        height: 100%;
        transition: ease-in-out .5s;
        opacity: 0;
        transform: scale(2);
    }
    .section-box-twelve:hover figure {
        opacity: 1;
        transform: scale(1);
        background: rgba(0, 0, 0, 0.71);
    }
    .section-box-twelve figure a{
        color: #fff;
        font-size: 3em;
        top: 35%;
        position: relative;
    }
    .section-box-twelve img{
        height: 100%;
    }
#featureBooks .item{
  margin: 10px;
}
#mostPopularbooks .item {
    margin: 10px;
}
#featureBooks .owl-nav {
    position: absolute;
    width: 80px;
    right: 0;
    top: -80px;
}
#featureBooks .owl-nav .owl-prev {
    background-image: url(./assets/images/button_previous_2.png);
    height: 30px;
    width: 30px;
    float: left;
    display: block;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
}
#featureBooks .owl-nav .owl-next{
    background-image: url(./assets/images/button_next.png);
    height: 30px;
    width: 30px;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    display: inline-block;
}
#featureBooks .owl-nav .owl-prev.disabled {
    background-image: url(./assets/images/button_previous.png);
    height: 30px;
    width: 30px;
    float: left;
    display: block;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
}
#featureBooks .owl-next.disabled{
    background-image: url(./assets/images/button_next_1.png);
    height: 30px;
    width: 30px;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    display: inline-block;
}

#mostPopularbooks .item {
    margin: 10px;
}
#mostPopularbooks .owl-nav {
    position: absolute;
    width: 80px;
    right: 0;
    top: -80px;
}
#mostPopularbooks .owl-nav .owl-prev {
    background-image: url(./assets/images/button_previous_2.png);
    height: 30px;
    width: 30px;
    float: left;
    display: block;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
}
#mostPopularbooks .owl-nav .owl-next{
    background-image: url(./assets/images/button_next.png);
    height: 30px;
    width: 30px;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    display: inline-block;
}
#mostPopularbooks .owl-nav .owl-prev.disabled {
    background-image: url(./assets/images/button_previous.png);

}
#mostPopularbooks .owl-next.disabled{
    background-image: url(./assets/images/button_next_1.png);
}

#recentlyreleasedBooks .item {
    margin: 10px;
}
#recentlyreleasedBooks .owl-nav {
    position: absolute;
    width: 80px;
    right: 0;
    top: -80px;
}
#recentlyreleasedBooks .owl-nav .owl-prev {
    background-image: url(./assets/images/button_previous_2.png);
    height: 30px;
    width: 30px;
    float: left;
    display: block;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
}
#recentlyreleasedBooks .owl-nav .owl-next{
    background-image: url(./assets/images/button_next.png);
    height: 30px;
    width: 30px;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    display: inline-block;
}
#recentlyreleasedBooks .owl-nav .owl-prev.disabled {
    background-image: url(./assets/images/button_previous.png);

}
#recentlyreleasedBooks .owl-next.disabled{
    background-image: url(./assets/images/button_next_1.png);
}
#PopularAuthors .item {
    margin: 10px;
}
#PopularAuthors .owl-nav {
    position: absolute;
    width: 80px;
    right: 0;
    top: -80px;
}
#PopularAuthors .owl-nav .owl-prev {
    background-image: url(./assets/images/button_previous_2.png);
    height: 30px;
    width: 30px;
    float: left;
    display: block;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
}
#PopularAuthors .owl-nav .owl-next{
    background-image: url(./assets/images/button_next.png);
    height: 30px;
    width: 30px;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    display: inline-block;
}
#PopularAuthors .owl-nav .owl-prev.disabled {
    background-image: url(./assets/images/button_previous.png);

}
#PopularAuthors .owl-next.disabled{
    background-image: url(./assets/images/button_next_1.png);
}
.owl-dots {
    display: none;
}
.heading{
  text-transform: uppercase;
}

a.memberbtn {
    background: #54bfe3;
    padding: 11px;
    border: 1px solid transparent;
    border-radius: 5px;
    color: aliceblue;
    text-transform: uppercase;
}
.link {
    position: relative;
    top: 21px;
}
.cart {
    margin: 0 auto;
    display: block;
    text-align: center;
}
.overlay {

    background: #0006;
    position: relative;
    bottom: 28px;
    transition: .5s ease;

}
.overlay a {

    color: #fff !important;
    font-size: 16px;
    font-weight: bold;
    text-transform: uppercase;
    -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  text-align: center;
}

.figure:hover .figure {

    background-color: #1712124d !important;
    opacity: 0.3;


}
.figure {
    display: block;
}

@media only screen and (max-width: 600px) {
    #headercatmenu a.nav-link.dropdown-toggle {
        background: #fff;
        border-radius: 57%;
    }
    .sliderbanner {
        position: absolute !important;
        top: 40% !important;
        width: 82% !important;
        left: 9% !important;
        display: block;
    }
    .sliderbanner .link {
        position: relative;
        top: 18px;
    }
    .owl-carousel .owl-item img {
        display: block;
        max-width: 100% !important;
        width: unset;
        text-align: center;
        margin: 0 auto;
    }
    /*#featureBooks .owl-nav {
        position: absolute;
        top: 50%;
        width: 95%;
    }*/
    #featureBooks .owl-nav, #recentlyreleasedBooks .owl-nav ,#mostPopularbooks .owl-nav, #PopularAuthors .owl-nav {
        position: absolute;
        top: 50%;
        margin: 0 auto;
        width: 100%;
    }

    #featureBooks .owl-nav .owl-prev,#recentlyreleasedBooks .owl-nav .owl-prev,#mostPopularbooks .owl-nav .owl-prev, #PopularAuthors .owl-nav .owl-prev {
        background-image: url(./assets/images/button_previous_2.png);
        height: 30px;
        width: 30px;
        left: 0;
        display: block;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
    }
    #featureBooks .owl-nav .owl-next,#recentlyreleasedBooks .owl-nav .owl-next,#mostPopularbooks .owl-nav .owl-next, #PopularAuthors .owl-nav .owl-next {
        background-image: url(./assets/images/button_next.png);
        height: 30px;
        width: 30px;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        display: inline-block;
        display: block;
        /* float: right; */
        right: 0;
    }
    #featureBooks,#recentlyreleasedBooks,#mostPopularbooks,#PopularAuthors .item {
        text-align: center;
    }

    .heading {
        text-transform: uppercase;
        font-size: 20px;
    }
    .link {
        position: relative;
        top: -5px;
    }
    .bg-res {
        background-size: 100% 70% !important;
    }
    #back-to-top {
        cursor: pointer;
        position: fixed;
        bottom: 8px;
        right: 7px;
        width: 45px;
    }

}

@media only screen and (max-width: 767px){
    #headercatmenu a.nav-link.dropdown-toggle {
        background: #fff;
        border-radius: 57%;
    }
    .sliderbanner {
        position: absolute !important;
        top: 50% !important;
        width: 80% !important;
        left: 10% !important;
        display: block;
    }
    #featureBooks .item {
        margin: 0 auto;
        text-align: center;
    }
    .owl-carousel .owl-item img {
        display: block;
        max-width: 100% !important;
        width: unset;
        text-align: center;
        margin: 0 auto;
    }
    #featureBooks,#recentlyreleasedBooks,#mostPopularbooks,#PopularAuthors .item {
        text-align: center;
    }
    .heading {
        text-transform: uppercase;
        font-size: 20px;
    }
    .link {
        position: relative;
        top: 2px;
    }

}
@media only screen and (max-width: 992px){
    .heading {
        text-transform: uppercase;
        font-size: 20px;
    }
    .owl-carousel .owl-item img {
        display: block;
        max-width: 100% !important;
        text-align: center;
        width: unset;
        margin: 0 auto;
    }
    .describe {
        margin: 0 auto;
        text-align: center;
    }
}
</style>



