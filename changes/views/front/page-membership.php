<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section"></div>
<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 text-center">
                <br/>
                <br/>
                <h2 class="section-title mb-3">Membership plans</h2>
                <h3 class="section-sub-title">over 3.5 millions ebooks Read with no limit...</h3>
            </div>
        </div>
        <div class="row">
            <?php
            if(isset($membershipDetailIDs)){
                foreach ($membershipDetailIDs as $value) {
                    $membership = $this->user_Auth->getData('membershipplan', array('mp_id' => $value->mp_id));
            ?>
            <div class="col-md-4">
                <div class="plans">
                    <div class="heading">
                        <img src="./assets/images/membership_plans_forma.png" class="img-fluid">
                    </div>
                    <h1>$ <?php echo $membership[0]->mp_price; ?></h1>
                    <h6><?php echo $membership[0]->mp_validity; ?></h6>
                    <p><?php echo $membership[0]->mp_descriptions; ?></p>
                    <center><button type="button" onclick="openmembershipForm(<?php echo $membership[0]->mp_id; ?>)">Join today</button></center>
                </div>
            </div>
            <?php
                }
            ?>
            <?php
            }
            ?>
        </div>
    </div>
</section>
<?php /* Button to Open the Modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Open modal
</button> */?>

<!-- The Modal -->
<div class="modal fade" id="membershipModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center d-block">
                <h4 class="modal-title d-inline-block text-info"><strong>Membership Information</strong></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form name="memberinfoForm" id="memberinfoForm" method="post" class="was-validated">
                
                </form>
            </div>

            <?php /* Modal footer -->
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> --*/ ?>
        </div>
    </div>
</div>


<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 83px;
    height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
div.plans button:hover div.plans{
    box-shadow: 1px 2px 8px #54bfe3 !important;
}
.plans {
    background: #fff;
    padding: 0px 0px 32px 0;
    border-top-left-radius: 60px;
    width: 86%;
    margin: 15px auto;
    box-shadow: 1px 2px 8px black;
    background-image: url(./assets/images/membership_plans_forma_tag.png);
    background-repeat: no-repeat;
    background-position-y: bottom;
}
.heading {
    background-color: #55C0E1;
    padding: 36px 0px;
    text-align: center;
    border-top-left-radius: 60px;
    border-bottom-right-radius: 60px;
    margin: 0px;
}
.plans h1 {
    font-weight: bold;
    border-bottom: 1px solid gray;
    text-align: center;
    margin: 25px;
}
.plans h6 {
    font-size: 20px;
    text-align: center;
    font-family: monospace;
    font-weight: 600;
}
.plans p {
    text-align: center;
    text-decoration: underline;
    letter-spacing: 1px;
    padding: 13px;
}
.plans button {
    background-color: #656161;
    margin: 0 auto;
    border: 1px solid gray;
    color: #fff;
    font-weight: bold;
    text-transform: capitalize;
    padding: 7px 34px;
    cursor: pointer;
}
.plans button:hover{
    background-color: #54bfe3;
    color: #fff;
}
.custom-select.is-valid, .form-control.is-valid, .was-validated .custom-select:valid, .was-validated .form-control:valid {
    border-color: #08c;
}
.loader {
    background-image: url(./assets/images/green_loading_circle.gif);
    background-size: contain;
    background-position: center;
    pointer-events: none;
    background-repeat: no-repeat;
}
.form-group {
    margin-bottom: 10px;
}
label {
    display: inline-block;
    margin-bottom: -0.3rem;
}
.custom-select.is-valid:focus, .form-control.is-valid:focus, .was-validated .custom-select:valid:focus, .was-validated .form-control:valid:focus {
    border-color: #08c;
    -webkit-box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, 0.25);
    box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, 0.25);
}
.form-control {
    border-radius: 4px !important;
}

@media only screen and (max-width: 600px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 90px !important;
        height: calc(16vh);
    }
}
@media only screen and (max-width: 767px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 0px !important;
        
    }
}


@media only screen and (max-width: 992px){
   .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 0px !important;
        
    } 
}
</style>
  