<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section"></div>
<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 text-center">
                <h2 class="section-title mb-3" style="color:#54BFE3;">Author Sign-Up</h2>
            </div>
        </div>
        <div class="container" style="padding:50px;box-shadow: 0px 0px 20px -10px black;">
            <div class="col-md-12d">
                <div class="divborder">
                    <div class="row ">
                        <div class="col-md-9">

                            <h2><strong>Create your profile</strong></h2>
                            <?php 
                              $alert = $this->session->flashdata('alert');
                              if($alert){
                                  ?>
                                  <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                                    <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <?php
                              }
                              ?>

                            <form name="authorsignUp_Form" id="authorsignUp_Form" method="post" >
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">First Name</label>
                                            <input type="text" name="txt_firstname" id="txt_firstname" class="form-control" placeholder="Andrew" max-length ="150" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Last Name</label>
                                            <input type="text" name="txt_lastname" id="txt_lastname" class="form-control" placeholder="Barton" max-length ="150" required>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Email</label>
                                            <input type="email" name="txt_email" id="txt_email" class="form-control" placeholder="Andrew@gmail.com"   value="" >
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Contact Number (mobile/telephone)</label>
                                            <input type="text" name="txt_mobile" id="txt_mobile" class="form-control" placeholder="+91xxxxxxxxxx" max-length ="15" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-form-label">Address</label>
                                            <input type="text" name="txt_address" id="txt_address" class="form-control" placeholder="Your Full Address" max-length ="150" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Add Bio</label>
                                            <textarea name="txt_bio" id="txt_bio" class="form-control" placeholder="Write About Yourself" rows="5" max-length ="250"></textarea>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Create Password</label>
                                            <input type="password" name="txt_password" id="txt_password" class="form-control" placeholder="*************" max-length ="20" required>
                                        </div> 
                                    </div>
                                </div>
                               <!--  <br/>
                                <br/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="submit" name="" id="" value="Save" class="btn btn-block btn-info">
                                    </div>
                                </div> -->
                                <br/>
                                <br/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2>Payment Information<h2>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-block" style="border: 2px solid #d8d6d6;padding: 6px;background-color: #fff;" onclick="authorpaypalSubmit()"><img src="<?php echo base_url('assets/images/ppcom.png'); ?>" class="img-fluid"></button>
                                        <br/>
                                        <div id="txtpaypalEmailDiv">
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-block btn-info" onclick="authormmoneySubmit()">Mobile Money Transfer</button>
                                        <br/>
                                        <div id="txtmmoneyEmailDiv">
                                            
                                           
                                        </div>
                                    </div>
                                </div>  
                                <div class="row">
                                    <div class="form-group col-sm-4" style="margin:0 auto;"><input type="submit" value="Save" name="authorSubmit" class="btn btn-info btn-block submit"></div>
                                </div>             
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 83px;
    height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.mb-5 {
    
    margin-top: 2rem;
}
#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
div.plans button:hover div.plans{
    box-shadow: 1px 2px 8px #54bfe3 !important;
}
.plans {
    background: #fff;
    padding: 0px 0px 32px 0;
    border-top-left-radius: 60px;
    width: 86%;
    margin: 15px auto;
    box-shadow: 1px 2px 8px black;
    background-image: url(./assets/images/membership_plans_forma_tag.png);
    background-repeat: no-repeat;
    background-position-y: bottom;
}
.heading {
    background-color: #55C0E1;
    padding: 36px 0px;
    text-align: center;
    border-top-left-radius: 60px;
    border-bottom-right-radius: 60px;
    margin: 0px;
}
.plans h1 {
    font-weight: bold;
    border-bottom: 1px solid gray;
    text-align: center;
    margin: 25px;
}
.plans h6 {
    font-size: 20px;
    text-align: center;
    font-family: monospace;
    font-weight: 600;
}
.plans p {
    text-align: center;
    text-decoration: underline;
    letter-spacing: 1px;
    padding: 13px;
}
.plans button {
    background-color: #656161;
    margin: 0 auto;
    border: 1px solid gray;
    color: #fff;
    font-weight: bold;
    text-transform: capitalize;
    padding: 7px 34px;
    cursor: pointer;
}
.plans button:hover{
    background-color: #54bfe3;
    color: #fff;
}
.custom-select.is-valid, .form-control.is-valid, .was-validated .custom-select:valid, .was-validated .form-control:valid {
    border-color: #08c;
}
.loader {
    background-image: url(./assets/images/green_loading_circle.gif);
    background-size: contain;
    background-position: center;
    pointer-events: none;
    background-repeat: no-repeat;
}
.form-group {
    margin-bottom: 10px;
}
label {
    display: inline-block;
    margin-bottom: -0.3rem;
}
.custom-select.is-valid:focus, .form-control.is-valid:focus, .was-validated .custom-select:valid:focus, .was-validated .form-control:valid:focus {
    border-color: #08c;
    -webkit-box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, 0.25);
    box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, 0.25);
}
.form-control {
    border-radius: 4px !important;
}
.loader{
    background-image: url(assets/images/green_loading_circle.gif);
    background-repeat: no-repeat;
    background-position: center;
    background-size: contain;
    pointer-events: none;
    cursor: progress;
}
@media only screen and (max-width: 600px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 0px !important;
        
    }
    .mb-5 {
        margin-top: 5rem;
    }
}
@media only screen and (max-width: 767px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 0px !important;
       
    }
}
@media only screen and (min-width: 992px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 0px !important;
        
    }
}
@media only screen and (max-width: 992px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 0px !important;
        
    }
}
</style>
  