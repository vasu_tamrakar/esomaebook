<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0&appId=470017186888654&autoLogAppEvents=1"></script>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/somali_authors_bg.png'); ?>);">
    <div class="container">
        <div class="row">
        <div class="bannerheading"><h1>Somali Authors</h1></div>
        </div>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Authors</li>
          </ol>
        </nav>

    </div>
    
</div>


    <section class="site-section bg-white" id="contact-section">
      <div class="container">
        <?php 
        if(!empty($authors)){
          foreach ($authors as $author) { ?>
            <div class="row authorDiv">
  
              <div class="col-md-3">
                  <div class="authorImg">
                    <?php if($author->userData[0]->uc_image){?>
                      <img src="<?php echo base_url('uploads/users/'.$author->userData[0]->uc_image); ?>" class="img-fluid">
                  <?php }else{ ?>
                    <img src="<?php echo base_url('uploads/users/author.png'); ?>" class="img-fluid">
                  <?php } ?>
                      
                  </div>    
              </div>
              <div class="col-md-9">
                  <div class="authordetails">
                    <div class="col-md-12">
                      <h2 class="heading"><?php echo $author->userData[0]->uc_firstname.' '.$author->userData[0]->uc_lastname; ?> </h2>
                      <h6>published books <?php echo count($author->books);?> </h6>
                      <p>
                        <?php 
                          echo $author->a_description;
                        ?>
                      </p>
                    </div>
                  </div>


                  <div class="col-lg-12">
                      <div>
                          <h4 class="heading">Books of <?php echo $author->userData[0]->uc_firstname.' '.$author->userData[0]->uc_lastname; ?></h4>
                          <div class="socialicondiv">
                              <a href="<?php echo $author->a_facebook; ?>" target="_blank" class="facebook"></a>
                              <a href="<?php echo $author->a_twitter; ?>" target="_blank" class="twitter"></a>
                              <a href="<?php echo $author->a_gplush; ?>" target="_blank" class="googleplush"></a>
                              <a href="<?php echo $author->a_instagram; ?>" target="_blank" class="instagram"></a>
                          </div>
                      </div>
                      <div class="border-top pt-4"></div>
                          <div id ="author" class="authors owl-carousel owl-theme">
                              <?php 
                                if(!empty($author->books)){
                                    foreach($author->books as $book){ 
                                            $bookReviews = array();
                                            $allAvgRating = array();
                                            $avgRating = 0;
                                            $totalReview = 0;
                                            $bookReviews = $this->book_model->getBookReviews($book->b_id);
                                            if($bookReviews){
                                                $totalReview = count($bookReviews);
                                            }
                                            $allAvgRating = $this->book_model->getAvgRating($book->b_id);
                                            $avgRating = $allAvgRating[0]->rating;
                                        ?>
                                      <div class="item">
                                        <div class="featurebooks">
                                            <?php
                                            if(($book->b_originalprice > 0) && ($book->b_originalprice > $book->b_sellingprice)){
                                            ?>
                                            <span class="discount"><?php $bookPercetage = ((($book->b_originalprice - $book->b_sellingprice)*100)/$book->b_originalprice);echo (($bookPercetage > 0)?number_format($bookPercetage,0):''); ?><span class="percent-sign">%</span></span>
                                            <?php
                                            }
                                            ?>
                                            <a href="<?php echo site_url('books/'.$book->b_id); ?>">
                                                <img src="<?php echo base_url('uploads/books/'.$book->b_image); ?>" alt="<?php echo $book->b_title?>" class="img-fluid">
                                            </a>
                                            <div class="readdiv"><a href="<?php echo site_url('books/'.$book->b_id); ?>">READ NOW</a></div>
                                          </div>
                                          <div class="describe">
                                            <div class="rates">
                                              <div class="my-rating-small" data-rating="<?php echo $avgRating;?>" ></div>
                                            </div>
                                            <label><?php echo $book->b_title;?> </label>
                                            <?php //echo $book->b_published;?>
                                            <?php if($book->b_published != "0000-00-00" && $book->b_published != "1970-01-01"){?>
                                            <span><?php echo date('d F Y',strtotime($book->b_published));?></span>
                                            <?php } ?>
                                          </div>
                                          <div class="readdiv"><a href="<?php echo site_url('books/'.$book->b_id); ?>">READ NOW</a></div>
                                          <!-- Your share button code -->
                                            <!-- <div class="fb-share-button" data-href="<?php echo site_url('book/bookDetails/'.$book->b_id); ?>" data-layout="button" data-size="large">
                                            </div> -->
                                    </div>
                                <?php }

                                }else{
                                  echo "No books..";
                                }
                              ?>
                              
                          </div>

                    </div>
       
              </div>
              <div class="border-top pt-4"></div>
          </div>
        <?php  }
            }else{
                echo '<div class="col-md-12"><center><p>No Somali Authors..</p></center></div>';
            }
        ?>
        <?php if(!empty($authors)) {?>
        <div class="moreAuthor"><center><button type="button" id="moreAuthor" data="0" num="0"> More Author</button></center></div>
        </div>
        <?php } ?>
    </section>
<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 328px;
    height: calc(40vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar{
    background: #54bfe3;
}
.sticky-wrapper.is-sticky .site-navbar ul li a {
    color: #505048c4 !important;
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}


.bannerheading {
   
    margin: auto;
    display: block;
}

.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
.authorImg {
    box-shadow: 0px 1px 5px 0px black;
}
h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}
.authordetails .h6, h6 {
    font-size: 14px;
}
.authordetails p {
font-size: 14px;
margin: 1rem 0;
}
h4.heading {
    color: #757575bd;
    display: block;
    font-weight: 600;
    font-size: 23px;
    font-family: inherit;
}
.socialicondiv {
    width: 165px;
    position: absolute;
    right: 15px;
    top: -6px;
    display: block;
}
.socialicondiv a {
    background-repeat: no-repeat;
    background-size: cover;
    width: 36px;
    height: 36px;
    background-position: center;
    display: inline-block;
}
.socialicondiv a.facebook{
    background-image: url(./assets/images/Somali_authors_social_fb.png);
}
.socialicondiv a.twitter{
    background-image: url(./assets/images/Somali_authors_social_tw.png);
}
.socialicondiv a.googleplush{
    background-image: url(./assets/images/Somali_authors_social_g+.png);
}
.socialicondiv a.instagram{
    background-image: url(./assets/images/Somali_authors_social_insta.png);
}

.authors .item {
    margin: 8px;
}

/*.authors .owl-nav {
    position: absolute;
    top: 107px;
    width: auto;
}
.authors .owl-prev {
    height: 24px;
    width: 24px;
    background-image: url(./assets/images/button_previous_2.png) !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
    display: inline-block !important;
    float: left;
    margin: 0px 0px 0px -25px !important;
}
.authors .owl-next {
    height: 24px;
    width: 24px;
    background-image: url(./assets/images/button_next_2.png) !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
    margin: 0px 796px !important;
    float: right;
}
.owl-dots{
    display: none;
}*/


.authors .owl-nav {
    position: absolute;
    top: 50%;
    width: 100%;

}

.authors .owl-prev {
    height: 24px;
    width: 24px;
    background-image: url(./assets/images/button_previous_2.png) !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
    display: inline-block !important;
    left: -2em;
    margin: 0px !important;
}
.authors .owl-next {
    height: 24px;
    width: 24px;
    background-image: url(./assets/images/button_next_2.png) !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
    right: -2em;
    margin: 0px;
}
.owl-dots{
    display: none;
}
.featurebooks:hover .readdiv {
    display: block;
    position: absolute;
    background: #151414 !important;
    color: #ffff !important;
    opacity: 1 !important;
   /* margin: -134px 0px 0px 49px;*/
    font-size: small;
    font-weight: bold;
    padding: 2px 14px;
    border-radius: 4px;
    cursor: pointer;
}
.readdiv a{
    color: #fff !important;
}
button#moreAuthor {
    color: #54bfe3;
    background: #f3f3f39c;
    border: 1px solid #54bfe3;
    border-radius: 7px;
    padding: 4px 44px;
    margin-top: 50px;
    cursor: pointer;
}
button#moreAuthor:hover{
    background-color: #54bfe3;
    color: #fff;
}


/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {
 
    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 61px !important;
        font-size: 55px;
        text-align: center;
    }
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 360px !important;
        height: 22vh;
    }
    .breadcrumb {
        position: absolute;
        margin: -50px 20%;
        background: transparent;
    }
    .socialicondiv {
        width: 165px;
        position: relative !important;
        right: 15px;
        top: -6px;
        display: block;
    }
    .authordetails .col-md-12{
       padding: 0px; 
    }
    span.discount {
        right: 12px !important;
    }
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {
  
    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 60px !important;
        font-size: 55px;
        text-align: center;
    }
    .breadcrumb {
        position: absolute;
        margin: -50px 20%;
        background: transparent;
        padding: 0px;
    }
}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
 
    .breadcrumb {
        position: absolute;
        margin: -50px 30%;
        background: transparent;
    }
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {
   
} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {
    
}
</style>
  