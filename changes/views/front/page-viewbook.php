<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">




<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2">
            <?php include 'frontleftmenu.php'; ?>
        </div>
        <div class="col-md-10 col-lg-10 bl-gray">
            <div class="row">
                <div class="col-md-12">
                <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                  <div class="mb-3 mt-5">
                      <h2 class="heading text-center" style="color:#54BFE3;"><strong>My Books</strong></h2>
                  </div>

                  <table id="example" class="table table-striped  dt-responsive nowrap" style="width:100%">
                      <thead class="thead-light">
                          <tr>
                              <th>Title</th>
                              <th>Book Language</th>
                              <th>Category</th>
                              <th>Price</th>
                              <th>Published Date</th>
                              <th>Status</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php
                      if($bookeslistID){
                          foreach ($bookeslistID as $value) {
                            $bookDetail = $this->user_Auth->getData('books', array('b_id' => $value->b_id), $se='', $sh='');
                      ?>
                          <tr>
                              
                              
                              <td><?php echo $bookDetail[0]->b_title; ?></td>

                               <td><?php echo $bookDetail[0]->b_language; ?></td>

                               <td>
                                  <?php
                                  if($bookDetail[0]->b_category){
                                    $category = $this->user_Auth->getData('categories', $w=array('c_id' =>$bookDetail[0]->b_category), $se='', $sh='');
                                    echo (($category[0]->c_name)?$category[0]->c_name:'');
                                  }else{
                                    echo "";
                                  }
                                  
                                  ?>
                              </td>

                              <td><?php echo $bookDetail[0]->b_sellingprice;?></td>

                              <td><?php echo (($bookDetail[0]->b_published != "1970-01-01" && $bookDetail[0]->b_published != "0000-00-00")?date('d-M-Y', strtotime($bookDetail[0]->b_published)):''); ?></td>
                              
                             
                              <td>
                                <div class="ttdstatus">
                                <?php
                                if($bookDetail[0]->b_status === '1'){
                                  echo '<i class="fa fa-check"></i>';
                                }elseif($bookDetail[0]->b_status === '0'){
                                  echo '<i class="fa fa-times-circle"></i>';
                                }else{
                                  echo 'Pending';
                                }
                                ?>
                                </div>
                              </td>
                              <td>
                                  <a type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"> Action </a>
                                    <div class="dropdown-menu">
                                      <a class="dropdown-item" href="#" onclick="viewBookDetais(<?php echo $bookDetail[0]->b_id; ?>)">View Details</a>
                                      <a class="dropdown-item" href="<?php echo site_url('user/editbook/'.$bookDetail[0]->b_id); ?>">Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteBookDetais(<?php echo $bookDetail[0]->b_id; ?>)">Delete</a>
                                    </div>
                                
                              </td>
                          </tr>
                        <?php    
                          }
                      }
                      ?> 
                      </tbody>
                      <tfoot class="thead-light">
                          <tr>
                              <th>Title</th>
                              <th>Book Language</th>
                              <th>Category</th>
                              <th>Price</th>
                              <th>Published Date</th>
                              <th>Status</th>
                              <th>Action</th>
                          </tr>
                      </tfoot>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Button to Open the Modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Open modal
</button> -->

<!-- The Modal -->
<div class="modal" id="myModalview">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modal Heading</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<style type="text/css">
body {
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 102px;
height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.site-section {
    padding: 0;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

a.site_title:hover{
    color: #fff;
}
.navbar-nav .nav-link {
    padding: 0.3rem 1rem;
}
.fa::after {
  margin-right: 2px;
  content: ;
  content: "";
}
@media only screen and (max-width: 992px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 167px !important;
        height: 21vh;
    }
} 


</style>
  