<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">
</div>


    <section class="site-section bg-white" id="contact-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <br/>
            <br/>
            <h2 class="section-title mb-3">Forgot Password</h2>
          </div>
        </div>
        <div class="row">
            <?php 
            $alert = $this->session->flashdata('alert');
            if($alert){
                ?>
                <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                  <strong><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <?php
            }
            ?>
            <div class="col-12 col-md-12">
                <form name="signin_Form" id="signin_Form" action="<?php echo site_url('forgotPassword'); ?>" method="post">
                    <div class="form-group">
                        <label for="email">Enter email</label>
                        <input type="text" name="txt_email" id="txt_email" value="<?php echo set_value('txt_email'); ?>" class="form-control" placeholder="Enter Email Or Mobile" tabindex="-1">
                        <?php echo form_error('txt_email','<small class="form-text text-danger">','</small>'); ?>
                    </div>
                   
                  <button type="submit" class="btn btn-primary">Send</button>
                  <a href="<?php echo site_url('signIn'); ?>" class="btn btn-primary">Sign In</a>
                </form>
        </div>

      </div>
    </section>
<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 59px;
    height: calc(10vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
div.plans button:hover div.plans{
    box-shadow: 1px 2px 8px #54bfe3 !important;
}
.plans {
    background: #fff;
    padding: 0px 0px 32px 0;
    border-top-left-radius: 60px;
    width: 86%;
    margin: 15px auto;
    box-shadow: 1px 2px 8px black;
    background-image: url(./assets/images/membership_plans_forma_tag.png);
    background-repeat: no-repeat;
    background-position-y: bottom;
}
.heading {
    background-color: #55C0E1;
    padding: 36px 0px;
    text-align: center;
    border-top-left-radius: 60px;
    border-bottom-right-radius: 60px;
    margin: 0px;
}
.plans h1 {
    font-weight: bold;
    border-bottom: 1px solid gray;
    text-align: center;
    margin: 25px;
}
.plans h6 {
    font-size: 20px;
    text-align: center;
    font-family: monospace;
    font-weight: 600;
}
.plans p {
    text-align: center;
    text-decoration: underline;
    letter-spacing: 1px;
    padding: 13px;
}
.plans button {
    background-color: #656161;
    margin: 0 auto;
    border: 1px solid gray;
    color: #fff;
    font-weight: bold;
    text-transform: capitalize;
    padding: 7px 34px;
    cursor: pointer;
}
.plans button:hover{
    background-color: #54bfe3;
    color: #fff;
}
.btn.btn-primary {
    background: #54bfe3;
    border-color: #3036387d;
    color: #fff;
}
.btn.btn-primary:hover {
    background: #247b988a;
    border-color: #232729;
    color: #fff;
}

@media only screen and (max-width: 992px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 150px !important;
        
    }
}
</style>
  