<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2" >
            <?php include 'frontleftmenu.php'; ?>
        </div>
        <div class="col-sm-10">
            <div class="row">
                <div class="col-md-12">
                <?php 
                $alert = $this->session->flashdata('alert');
                if($alert){
                      ?>
                    <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                  <?php
                }
                  ?>
                    <div class="row mb-1">
                        <div class="col-12 text-center">
                            <br/>
                            <h3 class="section-sub-title">Profile Form</h3>
                        </div>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-md-8">
                            <?php if(($is_logged_in_user_info) && ($is_logged_in_user_info["u_role"] == 4)){ 
                                $aDetails = $this->user_Auth->getData('user_credentials',$w=array('uc_id' => $is_logged_in_user_info["u_id"]));
                                $amoreDetails = $this->user_Auth->getData('authors',$w=array('a_fK_of_uc_id' => $is_logged_in_user_info["u_id"]));
                            ?>
                            <div style="margin: 0 auto;width: 70px;display: block;height: 70px;">
                                <img src="<?php echo (($is_logged_in_user_info['u_image'])?base_url('uploads/users/'.$is_logged_in_user_info['u_image']):base_url('uploads/users/user.png')); ?>" class="img-fluid" style="border-radius: 50%; width:100%;height:100%;">
                            </div>
                            <form name="authorprofile_Form" id="authorprofile_Form" action="" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_firstname">First Name:</label>
                                            <input type="text" name="txt_firstname" id="txt_firstname" value="<?php echo (($is_logged_in_user_info['u_firstname'])?$is_logged_in_user_info['u_firstname']:''); ?>" class="form-control" placeholder="Enter First Name" max-length="150">
                                            <?php echo form_error('txt_firstname', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_lastname">Last Name:</label>
                                            <input type="text" name="txt_lastname" id="txt_lastname" value="<?php echo (($is_logged_in_user_info['u_lastname'])?$is_logged_in_user_info['u_lastname']:''); ?>" class="form-control" placeholder="Enter Last Name" max-length="150">
                                            <?php echo form_error('txt_lastname', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_email">Email:</label>
                                            <input type="text" name="txt_email" id="txt_email" value="<?php echo (($is_logged_in_user_info['u_email'])?$is_logged_in_user_info['u_email']:''); ?>" class="form-control" placeholder="Enter Email Address" max-length="150" disabled>
                                            <?php echo form_error('txt_email', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_mobile">Mobile:</label>
                                            <input type="text" name="txt_mobile" id="txt_mobile" value="<?php echo (($is_logged_in_user_info['u_mobile'])?$is_logged_in_user_info['u_mobile']:''); ?>" class="form-control" placeholder="+91xxxxxxxxxx" max-length="15">
                                            <?php echo form_error('txt_mobile', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_role">User Role:</label>
                                            <?php $role = $this->user_Auth->getData('user_roles',$w='',$se="ur_id,ur_name",$so='ur_id ASC');
                                            $selrole = $is_logged_in_user_info['u_role'];
                                            ?>
                                            <select name="txt_role" id="txt_role" class="form-control" disabled>
                                                <option value="" >Select the role</option>
                                                <?php 
                                                if($role){
                                                    foreach ($role as $value) {
                                                        ?>
                                                        <option value="<?php echo $value->ur_id; ?>" <?php if($value->ur_id === $selrole){ echo 'selected'; } ?>><?php echo $value->ur_name; ?></option>
                                                        <?php
                                                    }

                                                } ?>
                                            </select>
                                            <?php echo form_error('txt_role', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div> -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_image">Language:</label>
                                            <?php $aLanguage = $amoreDetails[0]->a_lang; ?>
                                            <select name="txt_language" id="txt_language" class="form-control">
                                                <option value=""> - Select the languge - </option>
                                                <option value="english" <?php if($aLanguage == 'english'){ echo 'selected'; } ?>>English</option>
                                                <option value="somali" <?php if($aLanguage == 'somali'){ echo 'selected'; } ?>>Somali</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_image">Profile Image:</label>
                                            <input type="file" name="txt_image" id="txt_image" class="form-control-file" accept="image/*"/>
                                            <small class="text-info">Image format accept only gif,jpg and png.</small>
                                        </div>
                                    </div>
                                </div>
                        
                                <!-- <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_image">Language:</label>
                                            <?php $aLanguage = $amoreDetails[0]->a_lang; ?>
                                            <select name="txt_language" id="txt_language" class="form-control">
                                                <option value=""> - Select the languge - </option>
                                                <option value="english" <?php if($aLanguage == 'english'){ echo 'selected'; } ?>>English</option>
                                                <option value="somali" <?php if($aLanguage == 'somali'){ echo 'selected'; } ?>>Somali</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_published">Published Book :</label>
                                            <input type="text" name="txt_published" id="txt_published" class="form-control" value="<?php echo $amoreDetails[0]->a_publishbooks; ?>" placeholder="1800">
                                            <?php echo form_error('txt_published','<small class="error">','</small>'); ?>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_paymmobile">Paypal Email/Mobile Money:</label><br/>
                                            <label for="txt_paymmobile1">
                                                <input type="radio" name="txt_paymmobile" id="txt_paymmobile1" class="" value="1" <?php echo (($amoreDetails[0]->a_paypal)?'checked':''); ?> onchange="changePaypAl(this);">
                                                Paypal Email
                                            </label>
                                            <label for="txt_paymmobile2">
                                                <input type="radio" name="txt_paymmobile" id="txt_paymmobile2" class="" value="2" <?php echo (($amoreDetails[0]->a_mmoney)?'checked':''); ?> onchange="changePaypAl(this);">
                                                Mobile Money
                                            </label>
                                            <?php echo form_error('txt_mmobile','<small class="error">','</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_status">User Status:</label>
                                            <label for="txt_statustrue">
                                                <input type="radio" name="txt_status" id="txt_statustrue" value="1" <?php if ($is_logged_in_user_info['u_status'] == '1'){ echo 'checked';} ?>> TRUE
                                            </label>
                                            <label for="txt_statusfalse">
                                            <input type="radio" name="txt_status" id="txt_statusfalse" value="0" <?php if ($is_logged_in_user_info['u_status'] == '0'){ echo 'checked'; } ?>> FALSE
                                            </label>
                                            <?php echo form_error('txt_status', '<small class="error">', '</small>'); ?>
                                            <!-- <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="customRadio" name="example1" value="customEx">
                                                <label class="custom-control-label" for="customRadio">Custom radio</label>
                                            </div> --> 
                                            <br/><small>Note: If you select false then your account will be happen freeze then will be contact ot administrator.</small>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div id ="paypalEmail" class="form-group">
                                            <?php
                                            if($amoreDetails[0]->a_paypal){ ?>
                                            <label for="txt_paypal">Paypal Email:</label>
                                            <input type="text" name="txt_paypal" id="txt_paypal" class="form-control" value="<?php echo $amoreDetails[0]->a_paypal; ?>" placeholder="andrew@gmail.com" required><?php
                                            }else{ ?>
                                            <label for="txt_mmoney">Mobile Money:</label>
                                            <input type="text" name="txt_mmoney" id="txt_paypal" class="form-control" value="<?php echo $amoreDetails[0]->a_mmoney; ?>" placeholder="+91xxxxxxxxxx" required>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_address">Address:</label>
                                            <input type="text" name="txt_address" id="txt_address" class="form-control" value="<?php echo $aDetails[0]->uc_address; ?>" placeholder="+91xxxxxxxxxx">
                                            <?php echo form_error('txt_address','<small class="error">','</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                if (document.getElementById('txt_paymmobile1').checked) {
                                    var textPaypal ='<label for="txt_paypal">Paypal Email:</label><input type="text" name="txt_paypal" id="txt_paypal" class="form-control" value="<?php echo $amoreDetails[0]->a_paypal; ?>" placeholder="andrew@gmail.com" required>';
                                    document.getElementById('paypalEmail').innerHTML = textPaypal;
                                }else{
                                    var textMmoney ='<label for="txt_mmoney">Mobile Money:</label><input type="text" name="txt_mmoney" id="txt_paypal" class="form-control" value="<?php echo $amoreDetails[0]->a_mmoney; ?>" placeholder="+91xxxxxxxxxx" required>';
                                    document.getElementById('paypalEmail').innerHTML = textMmoney;
                                }
                                
                                function changePaypAl(myRadio){
                                    var selecteOPt = myRadio.value;
                                    var textPaypal ='<label for="txt_paypal">Paypal Email:</label><input type="text" name="txt_paypal" id="txt_paypal" class="form-control" value="<?php echo $amoreDetails[0]->a_paypal; ?>" placeholder="andrew@gmail.com" required>';
                                    var textMmoney ='<label for="txt_mmoney">Mobile Money:</label><input type="text" name="txt_mmoney" id="txt_paypal" class="form-control" value="<?php echo $amoreDetails[0]->a_mmoney; ?>" placeholder="+91xxxxxxxxxx" required>';
                                    if(selecteOPt == 1){
                                        document.getElementById('paypalEmail').innerHTML = textPaypal;
                                    }else{
                                        document.getElementById('paypalEmail').innerHTML = textMmoney;
                                    }
                                    
                                }
                                
                                </script>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_facebook">Facebook Link:</label>
                                            <input type="text" name="txt_facebook" id="txt_facebook" class="form-control" value="<?php echo $amoreDetails[0]->a_facebook; ?>" placeholder="https://www.facebook.com/">
                                            <?php echo form_error('txt_facebook','<small class="error">','</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_twitter">Twitter Link:</label>
                                            <input type="text" name="txt_twitter" id="txt_twitter" class="form-control" value="<?php echo $amoreDetails[0]->a_twitter; ?>" placeholder="https://www.twitter.com/">
                                            <?php echo form_error('txt_twitter','<small class="error">','</small>'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_googleplus">Google+ Link:</label>
                                            <input type="text" name="txt_googleplus" id="txt_googleplus" class="form-control" value="<?php echo $amoreDetails[0]->a_gplush; ?>" placeholder="https://www.gplush.com/">
                                            <?php echo form_error('txt_googleplus','<small class="error">','</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_instagram">Instagram Link:</label>
                                            <input type="text" name="txt_instagram" id="txt_instagram" class="form-control" value="<?php echo $amoreDetails[0]->a_instagram; ?>" placeholder="https://ww.instagram.com/">
                                            <?php echo form_error('txt_instagram','<small class="error">','</small>'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txt_googleplus">Description:</label>
                                            <textarea name="txt_description" id="txt_description" class="form-control" placeholder="Something write here...." rows="4"><?php echo $amoreDetails[0]->a_description; ?></textarea>
                                            <?php echo form_error('txt_description','<small class="error">','</small>'); ?>
                                        </div>
                                    </div>
                                </div>


                                <!-- <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control"> Created Date : 
                                                <small class="text-info"><?php echo (($is_logged_in_user_info['u_created'] > 0)?date('d-M-Y H:i:s',strtotime($is_logged_in_user_info['u_created'])):''); ?>
                                                </small>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label class="form-control"> Last Modified Date : 
                                                <small class="text-info"><?php echo (($is_logged_in_user_info['u_modified'] > 0)?date('d-M-Y H:i:s',strtotime($is_logged_in_user_info['u_modified'])):''); ?>
                                                </small>
                                            </label>
                                        </div>
                                    </div>
                                </div> -->
                                <button type="submit" name="submit" class="btn btn-info">Update</button>
                            </form>
                            <?php
                            }else if(($is_logged_in_user_info) && ($is_logged_in_user_info["u_role"] == 5)){ ?>
                            <div style="margin: 0 auto;width: 70px;display: block;height: 70px;">
                                <img src="<?php echo (($is_logged_in_user_info['u_image'])?base_url('uploads/users/'.$is_logged_in_user_info['u_image']):base_url('uploads/users/user.png')); ?>" class="img-fluid" style="border-radius: 50%; width:100%;height:100%;">
                            </div>
                            <form name="userprofile_Form" id="userprofile_Form" action="" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_firstname">First Name:</label>
                                            <input type="text" name="txt_firstname" id="txt_firstname" value="<?php echo (($is_logged_in_user_info['u_firstname'])?$is_logged_in_user_info['u_firstname']:''); ?>" class="form-control" placeholder="Enter First Name" max-length="150">
                                            <?php echo form_error('txt_firstname', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_lastname">Last Name:</label>
                                            <input type="text" name="txt_lastname" id="txt_lastname" value="<?php echo (($is_logged_in_user_info['u_lastname'])?$is_logged_in_user_info['u_lastname']:''); ?>" class="form-control" placeholder="Enter Last Name" max-length="150">
                                            <?php echo form_error('txt_lastname', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_email">Email:</label>
                                            <input type="text" name="txt_email" id="txt_email" value="<?php echo (($is_logged_in_user_info['u_email'])?$is_logged_in_user_info['u_email']:''); ?>" class="form-control" placeholder="Enter Email Address" max-length="150" disabled>
                                            <?php echo form_error('txt_email', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_mobile">Mobile:</label >
                                            <input type="text" name="txt_mobile" id="txt_mobile" value="<?php echo (($is_logged_in_user_info['u_mobile'])?$is_logged_in_user_info['u_mobile']:''); ?>" class="form-control" placeholder="+91xxxxxxxxxx" max-length="15">
                                            <?php echo form_error('txt_mobile', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_image">Profile Image:</label>
                                            <input type="file" name="txt_image" id="txt_image" class="form-control-file" accept="image/*">
                                            <small class="text-info">Image format accept only gif,jpg and png.</small>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="row">

                                    <?php
                                    $uDetails = $this->user_Auth->getData('user_credentials',$w=array('uc_id' => $is_logged_in_user_info["u_id"]), $se= 'uc_address');
                                    ?>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="txt_address">Address:</label>
                                            <input type="text" name="txt_address" id="txt_address" class="form-control" value="<?php echo (($uDetails[0]->uc_address)?$uDetails[0]->uc_address:''); ?>" placeholder="Address" max-length="180">
                                        </div>
                                    </div>
                                </div>


                       
                                <button type="submit" name="submit" class="btn btn-info">Update</button>
                            </form>
                            <?php
                            }else{ ?>
                                <label class="form-control">In-valid Data.</label>
                            <?php
                            } ?>
                            <div style="padding-bottom:75px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
body {
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 102px;
height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.site-section {
    padding: 0;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
a.site_title:hover{
    color: #fff;
}
.navbar-nav .nav-link {
    padding: 0.3rem 1rem;
}
.fa::after {
  margin-right: 2px;
  content: ;
  content: "";
}

@media only screen and (max-width: 480px) {
    #search {
        width: 72% !important;
    }
}
@media only screen and (max-width: 600px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 138px !important;
        height: 20vh;
    }
} 


</style>

  