<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/video_training_bg.png'); ?>);">
    <div class="container">
        <div class="row">
            <div class="bannerheading"><h1>Video Trainning</h1></div>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Video Training</li>
            </ol>
        </nav>
    </div>
</div>
<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 text-center">
                <h2 class="section-title mb-3">Enrich your knowledge with our tutorials</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tab justify-content-center">
                    <li><a data-toggle="tab" href="#home" class="active show">All video</a></li>
                    <li><a data-toggle="tab" href="#menu2">Latest Video</a></li>
                    <li><a data-toggle="tab" href="#menu3">Most Popular</a></li>
                </ul>
            </div>
            <br/>
            <br/>

            <div class="col-md-12">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active show">
                        <div class="row">
                            <?php 
                            if(!empty($videos)){
                            foreach($videos as $video){
                                
                            ?>
                                <div class="col-md-6 videoDiv">
                                    <div class="video">
                                        <div class="imgvidoebox" data="https://player.vimeo.com/video/<?php echo $video->v_videoid; ?>?title=0&byline=0&portrait=0&autoplay=1">
                                            <img src="<?php echo base_url('uploads/videos/'.$video->v_image); ?>" class="img-fluid">
                                            <div class="playicon"></div>
                                        </div>
                                        <div class="likesharediv">
                                            <div class="date"><?php echo date('F d,Y',strtotime($video->v_created));?><div style="float:right;">12:05</div></div>
                                            <p><?php echo $video->v_title; ?></p>
                                            <div class="sharelike">
                                                <?php 
                                                    if($login_userid != ""){
                                                ?>
                                                    <div class="like">like : <a href="javascript:void(0)" onclick="addLike(<?php echo $login_userid;?>,<?php echo $video->v_id;?>)"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a></div>
                                                    &nbsp;&nbsp;
                                                <?php         
                                                    }
                                                ?>
                                                
                                                Total
                                                <?php echo $video->totalLikes; ?> <i class="fa fa-thumbs-up liked" aria-hidden="true"></i>
                                                
                                                <div class="share">Share:
                                                    <a href="http://www.facebook.com/sharer.php?u=https://simplesharebuttons.com" target="_blank" class=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                    <a href="" target="_blank" class=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                                    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://simplesharebuttons.com" target="_blank" class=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                                }else{
                                    echo '<div class="col-md-12"><center><p>No Videos..</p></center></div>';
                                }
                             ?>
                             <?php if(!empty($videos)){?>
                            <div class="col-md-12">
                                <center>
                                    <button id="moreVideo" data="" class="btn btn-info loadmore">Load More</button>
                                </center>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <!-- End Menu id 1 -->
                    <!-- Menu id 1 -->

                    <div id="menu2" class="tab-pane fade">
                        <div class="row">
                            <?php 
                            if(!empty($latestVideos)){
                            foreach($latestVideos as $video){
                                
                            ?>
                                <div class="col-md-6 latestDideoDiv">
                                    <div class="video">
                                        <div class="imgvidoebox" data="https://player.vimeo.com/video/<?php echo $video->v_videoid; ?>?title=0&byline=0&portrait=0&autoplay=1">
                                            <img src="<?php echo base_url('uploads/videos/'.$video->v_image); ?>" class="img-fluid">
                                            <div class="playicon"></div>
                                        </div>
                                        <div class="likesharediv">
                                            <div class="date"><?php echo date('F d,Y',strtotime($video->v_created));?><div style="float:right;">12:05</div></div>
                                            <p><?php echo $video->v_title; ?></p>
                                            <div class="sharelike">

                                                <?php 
                                                    if($login_userid != ""){
                                                ?>
                                                    <div class="like">like : <a href="javascript:void(0)" onclick="addLike(<?php echo $login_userid;?>,<?php echo $video->v_id;?>)"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a></div>
                                                    &nbsp;&nbsp;
                                                <?php         
                                                    }
                                                ?>
                                                
                                                Total
                                                <?php echo $video->totalLikes; ?> <i class="fa fa-thumbs-up liked" aria-hidden="true"></i>

                                                <div class="share">Share:
                                                    <a href="http://www.facebook.com/sharer.php?u=https://player.vimeo.com/video/<?php echo $video->v_videoid; ?>" target="_blank" class=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                    <a href="" target="_blank" class=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                                    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://simplesharebuttons.com" target="_blank" class=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                                }else{
                                    echo '<div class="col-md-12"><center><p>No Videos..</p></center></div>';
                                }
                             ?>
                             <?php if(!empty($latestVideos)){?>
                            <div class="col-md-12">
                                <center>
                                    <button id="latestVideo" data="" class="btn btn-info loadmore">Load More</button>
                                </center>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                    <!-- End Menu id 2 -->
                    <!-- Menu id 3 -->
                    <div id="menu3" class="tab-pane fade">
                        <div class="row">
                            <?php 
                            if(!empty($popularVideos)){
                            foreach($popularVideos as $video){
                                
                            ?>
                                <div class="col-md-6 popularVideoDiv">
                                    <div class="video">
                                        <div class="imgvidoebox" data="https://player.vimeo.com/video/<?php echo $video->v_videoid; ?>?title=0&byline=0&portrait=0&autoplay=1">
                                            <img src="<?php echo base_url('uploads/videos/'.$video->v_image); ?>" class="img-fluid">
                                            <div class="playicon"></div>
                                        </div>
                                        <div class="likesharediv">
                                            <div class="date"><?php echo date('F d,Y',strtotime($video->v_created));?><div style="float:right;">12:05</div></div>
                                            <p><?php echo $video->v_title; ?></p>
                                            <div class="sharelike">

                                                <?php 
                                                    if($login_userid != ""){
                                                ?>
                                                    <div class="like">like : <a href="javascript:void(0)" onclick="addLike(<?php echo $login_userid;?>,<?php echo $video->v_id;?>)"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a></div>
                                                    &nbsp;&nbsp;
                                                <?php         
                                                    }
                                                ?>
                                                
                                                Total
                                                <?php echo $video->totalLikes; ?> <i class="fa fa-thumbs-up liked" aria-hidden="true"></i>
                                                
                                                <div class="share">Share:
                                                    <a href="http://www.facebook.com/sharer.php?u=https://simplesharebuttons.com" target="_blank" class=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                    <a href="" target="_blank" class=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                                    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://simplesharebuttons.com" target="_blank" class=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                                }else{
                                    echo '<div class="col-md-12"><center><p>No Videos..</p></center></div>';
                                }
                             ?>
                             <?php if(!empty($popularVideos)){?>
                            <div class="col-md-12">
                                <center>
                                    <button id="popularVideo" data="" class="btn btn-info loadmore">Load More</button>
                                </center>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <!-- End Menu id 3 -->
                </div>
            </div>
         </div>
    </div>
</section>

<div class="modal fade top" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div id="videoModalData">
                    <iframe src="" width="100%" height="400px" frameborder="0" allow="autoplay;" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 360px;
    height: calc(40vh);
    background-size: cover;
    background-position:center;
}
.sticky-wrapper.is-sticky .site-navbar{
    background: #54bfe3;
}
/*.sticky-wrapper.is-sticky .site-navbar ul li a {
    color: #505048c4 !important;
}*/
.sticky-wrapper a.nav-link.dropdown-toggle {
    color: #fff !important;
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}




/*.bannerheading {
    margin: 0 auto;
    margin-top: 60px;
}*/
.bannerheading {
    display: block;
    margin: auto;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    top: 50px;
    font-size: 55px;
}
.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
.authorImg {
    box-shadow: 0px 1px 5px 0px black;
}
h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}
.nav-tab .active {

    background: #54BFE3;
    color: #fff;

}


.nav-tab li a {
    background: #EDF3F2;
    margin: 6px;
    padding: 7px 14px;
    color: #54BFE3;
    font-family: unset;
    text-transform: capitalize;
    border: 1px solid #54BFE3;
    border-radius: 4px;
    font-weight: bold;
    display: inline-block;
}
.nav.nav-tab.justify-content-center {
    margin-bottom: 60px;
}

.video{
    border: 1px solid #cccaca;
    margin-bottom: 30px;
}
.imgvidoebox {
    height: 275px;
    width: 100%;
    cursor: pointer;
}
.imgvidoebox .img-fluid {
    width: 100%;
    height: 100%;
}
.playicon {
    background-image: url(<?php echo base_url('assets/images/playvideo.png');?>);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    height: 80px;
    width: 80px;
    margin: 0 auto;
    position: relative;
    bottom: 64%;
}

.likesharediv .date {
    padding: 7px 41px;
    color: #fff;
    font-size: 13px;
    background: #54BFE3;

}
.likesharediv p {
    padding: 6px 41px;
    color: #382c2c;
    font-size: 15px;
}
.sharelike {
    display: block;
    padding: 6px 41px;
}
.sharelike a {
    color: #817979e6;
}
.like {
    display: inline-block;
}
.share {
    display: inline-block;
    float: right;
}
.share a {
    margin: 4px;
}
.like a:hover {
    color: #54BFE3;
}
.share a:hover {
    color: #54BFE3;
}

.liked{
    color: #54BFE3;
}

.center {

    text-align: center;
    margin: 0 auto;

}
.loadmore {

    background: #f2ededb3 !important;
    border-radius: 8px !important;
    color: #54bfe3 !important;
    font-weight: bold;
    font-size: 15px;
    margin-top: 20px;

}
.loadmore:hover {

    color: #fff !important;
    background: #54BFE3 !important;

}
.modal-header {
    padding: 0.5rem;
}


/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {
    .authors .owl-next {
        height: 24px;
        width: 24px;
        background-image: url(./assets/images/button_next_2.png) !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
        margin: 0px 295px !important;
        float: right;
    }
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 360px !important;
        height: 22vh;
    }
    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 65px !important;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -50px 20%;
        background: transparent;
        padding: 0rem 0rem;
    }
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {
  .authors .owl-next {
        height: 24px;
        width: 24px;
        background-image: url(./assets/images/button_next_2.png) !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
        margin: 0px 260px !important;
        float: right;
    }
    .bannerheading h1 {
        position: relative;
        color: #fff;
        /*top: 130px;*/
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -50px 30%;
        background: transparent;
        padding: 0rem 0rem;
    }
}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
}
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {

} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

}
</style>
  






