<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section"></div>
<section class="site-section bg-white" id="contact-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-lg-2">
                <?php include 'frontleftmenu.php'; ?>
            </div>
            <div class="col-md-10 col-lg-10">
                <div class="row justify-content-md-center">
                    <div class="col-md-9">

                        <div class="row mt-5">
                            <div class="col-12 text-center">
                                <h2 class="section-title mb-3" style="color:#54BFE3;">Edit My E-Book</h2>
                            </div>
                        </div>

                        <?php 
                        $alert = $this->session->flashdata('alert');
                        if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                            <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php
                        }
                        ?>
                        <div class="pt-5"></div>
                        <div class="card" style="padding:50px;box-shadow: 0px 0px 20px -10px black;">
                        <?php
                        if($bookDetails){ ?>
                            <h2><strong>Book Information</strong></h2>
                        <!-- <div class="card-header bg-info"><span style="color: #fff;">Edit Book Form</span></div> -->
                            <div class="card-body">
                                <form name="editAuthorbook_Form" id="editAuthorbook_Form" action="" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="txt_bid" id="txt_bid" value="<?php echo $bookDetails[0]->b_id; ?>">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_published">Book Language</label>
                                                <?php $selLanguage = $bookDetails[0]->b_language; ?>
                                                <select name="txt_language" id="txt_language" class="form-control">
                                                    <option value=""> - Select The Language - </option>
                                                    <option value="english" <?php if($selLanguage == 'english'){ echo 'selected'; } ?>>English</option>
                                                    <option value="somali" <?php if($selLanguage == 'somali'){ echo 'selected'; } ?>>Somali</option>
                                                </select>
                                                <?php echo form_error('txt_language', '<small class="error">', '</small>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_category">Category</label>
                                                <?php 
                                                $secat = $bookDetails[0]->b_category;
                                                $category = $this->user_Auth->getData('categories',$w = array('c_status' => '1'),$se = 'c_id,c_name',$sh='c_id ASC');
                                                if($category){ ?>
                                                <select name="txt_category" id="txt_category" class="form-control">
                                                    <option value=""> Select Category</option>
                                                    <?php
                                                    foreach ($category as $value) { ?>
                                                        <option value="<?php echo $value->c_id; ?>" <?php if($value->c_id == $secat){ echo 'selected'; } ?>><?php echo $value->c_name; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <?php
                                                } 
                                                ?>
                                                <?php echo form_error('txt_category', '<small class="error">', '</small>'); ?>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txt_title">Book Title:</label>
                                            <input type="text" name="txt_title" id="txt_title" value="<?php echo (($bookDetails[0]->b_title)?$bookDetails[0]->b_title:''); ?>" class="form-control" placeholder="Enter Book Title" max-length="150">
                                            <?php echo form_error('txt_title', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_originalprice">Book Original Price::</label>
                                            <input type="text" name="txt_originalprice" id="txt_originalprice" value="<?php echo (($bookDetails[0]->b_sellingprice)?$bookDetails[0]->b_originalprice:'')?>" class="form-control" placeholder="20.00" max-length="10" required>
                                            <?php echo form_error('txt_originalprice', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_sellingprice">Book Selling Price:</label>
                                            <input type="text" name="txt_sellingprice" id="txt_sellingprice" value="<?php echo (($bookDetails[0]->b_sellingprice)?$bookDetails[0]->b_sellingprice:'')?>" class="form-control" placeholder="20.00" max-length="10" required>
                                            <?php echo form_error('txt_sellingprice', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_publisher">Publisher:</label>
                                            <input type="text" name="txt_publisher" id="txt_publisher" value="<?php echo (($bookDetails[0]->b_publisher)?$bookDetails[0]->b_publisher:'')?>" class="form-control" placeholder="Andrew" max-length="150" required>
                                            <?php echo form_error('txt_publisher', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_publisheddate">Published Date:</label>
                                            <input type="text" name="txt_publisheddate" id="txt_publisheddate" value="<?php echo (($bookDetails[0]->b_published > 0)?date('m/d/Y',strtotime($bookDetails[0]->b_published)):''); ?>" class="form-control datepicker">
                                            <?php echo form_error('txt_publisheddate', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_isbn">ISBN:</label>
                                            <input type="text" name="txt_isbn" id="txt_isbn" value="<?php echo (($bookDetails[0]->b_isbn)?$bookDetails[0]->b_isbn:'')?>" class="form-control" placeholder="ISBN" max-length="100">
                                            <?php echo form_error('txt_isbn', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_bookpages">Book Pages:</label>
                                            <input type="text" name="txt_bookpages" id="txt_bookpages" value="<?php echo (($bookDetails[0]->b_bookpages)?$bookDetails[0]->b_bookpages:'')?>" class="form-control" placeholder="pages" max-length="10">
                                            <?php echo form_error('txt_bookpages', '<small class="error">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_image">Book Thumbnail Image:</label>
                                            <input type="file" name="txt_image" id="txt_image" class="form-control-file" accept="image/*" required>
                                            <?php echo form_error('txt_image', '<small class="error">', '</small>'); ?>
                                            <small class="text-info">Image format accept only gif,jpg and png.</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="bookFile">Book File:</label>
                                            <input type="file" name="bookFile" id="bookFile" class="form-control-file" accept="application/msword, application/vnd.ms-powerpoint,
  text/plain, application/pdf" required>
                                            <?php echo form_error('bookFile', '<small class="error">', '</small>'); ?>
                                            <small class="text-info">File format accept only pdf,docx,txt and pptx.</small>
                                        </div>
                                    </div>
                                </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="text_message">Description</label>
                                            
                                        
                                                <textarea name="text_message" id="txtEditor" rows="5" cols="10" class="form-control" placeholder="Write something..."><?php echo (($bookDetails[0]->b_description)?$bookDetails[0]->b_description:'')?></textarea>
                                                <label id="txt_description-error" class="error" for="txt_description" style="display:none;">Description field is required.</label>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" name="update" class="btn btn-info">Update</button>
                                </form>
                            </div>
                        <?php
                        }else{
                            echo '<label>Warning! Permission Access Denide!.</label>';
                        }
                        ?>
                        </div>
                    <div class="pt-5 border-bottom"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
body {
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 102px;
height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.site-section {
    padding: 0;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

a.site_title:hover{
    color: #fff;
}
.navbar-nav .nav-link {
    padding: 0.3rem 1rem;
}
.fa::after {
  margin-right: 2px;
  content: "";
}
.note-editor .btn {
    padding: 8px 18px;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
}
@media only screen and (max-width: 600px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 138px !important;
        height: 20vh;
    }
} 
</style>
