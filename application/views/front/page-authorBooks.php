<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/biogrphy_bg.png'); ?>);">
    <div class="container">
        <div class="row">
        <div class="bannerheading"><h1><?php echo $authorDetails->uc_firstname.' '.$authorDetails->uc_lastname; ?></h1></div>
        </div>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('somaliAuthors'); ?>">Author</a></li>
            <li class="breadcrumb-item active"><?php echo $authorDetails->uc_firstname.' '.$authorDetails->uc_lastname; ?></li>
          </ol>
        </nav>

    </div>
    
</div>
<section class="site-section bg-white" id="contact-section">
  <div class="container">

        <div class="row authorDiv">

          <div class="col-md-3">
              <div class="authorImg">

                <?php if($authorDetails->uc_image){?>
                      <img src="<?php echo base_url('uploads/users/'.$author->userData[0]->uc_image); ?>" class="img-fluid">
                  <?php }else{ ?>
                    <img src="<?php echo base_url('uploads/users/author.png'); ?>" class="img-fluid">
                  <?php } ?>
              </div>    
          </div>
          <div class="col-md-9">
              <div class="authordetails">
                  <h2 class="heading"><?php echo $authorDetails->uc_firstname.' '.$authorDetails->uc_lastname; ?> </h2>
                  <h6>published books <?php echo count($authorBooks);?> </h6>
                  <p>
                    <?php 
                      echo $authorUserDetails->a_description;
                    ?>
                  </p>
              </div>
   
          </div>
          <div class="border-top pt-4"></div>
      </div>

      <div class="row">
        <div class="col-md-12">

            <div style="margin:30px;"></div>

            <?php if($authorBooks){ ?>
            <div class="col-md-12">
                <div>

                    <h4 class="heading">Books of <?php echo $authorDetails->uc_firstname.' '.$authorDetails->uc_lastname; ?>
                    </h4>
                    <div class="socialicondiv">
                          <a href="<?php echo $authorUserDetails->a_facebook; ?>" target="_blank" class="facebook"></a>
                          <a href="<?php echo $authorUserDetails->a_twitter; ?>" target="_blank" class="twitter"></a>
                          <a href="<?php echo $authorUserDetails->a_gplush; ?>" target="_blank" class="googleplush"></a>
                          <a href="<?php echo $authorUserDetails->a_instagram; ?>" target="_blank" class="instagram"></a>
                      </div>
                </div>
                <div class="border-top pt-4"></div>
                <div class="filterBooksall row">
                    <?php
                    
                    foreach ($authorBooks as $book) {
                                                        
                    ?>
                    <div class="col-sm-2">
                        <div class="item">
                            <div class="featurebooks">
                                <a href="<?php echo site_url('books/'.$book->b_id); ?>">
                                    <img src="<?php echo (isset($book->b_image)?base_url('uploads/books/'.$book->b_image):base_url('images/books/noimage.jpg')); ?>" alt="<?php echo (isset($book->b_title)?$book->b_title:'Image'); ?>" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url('books/'.$book->b_id); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                <?php
                                    $allAvgRating = array();
                                    $avgRating = 0;
                                    $allAvgRating = $this->book_model->getAvgRating($book->b_id);
                                    $avgRating = $allAvgRating[0]->rating;

                                    ?>
                                    <div class="my-rating-small" data-rating="<?php echo $avgRating;?>" ></div>
                                </div>
                                <label><?php echo (isset($book->b_title)?$book->b_title:''); ?></label>
                                
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    }
                    ?>

                </div>

                
            </div>
        </div>
    </div>
  </div>
</section>

<style type="text/css">

.socialicondiv {
    width: 157px;
    position: absolute;
    right: 15px;
    top: -6px;
    display: block;
}
.socialicondiv a {
    background-repeat: no-repeat;
    background-size: cover;
    width: 36px;
    height: 36px;
    background-position: center;
    display: inline-block;
}
.socialicondiv a.facebook{
    background-image: url(../assets/images/Somali_authors_social_fb.png);
}
.socialicondiv a.twitter{
    background-image: url(../assets/images/Somali_authors_social_tw.png);
}
.socialicondiv a.googleplush{
    background-image: url(../assets/images/Somali_authors_social_g+.png);
}
.socialicondiv a.instagram{
    background-image: url(../assets/images/Somali_authors_social_insta.png);
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 328px;
    height: calc(40vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar {
    background: #54bfe3 !important;
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}


.bannerheading {
    margin: 0 auto;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    top: 130px;
    font-size: 55px;
}
.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}


.categories li, .sidelink li {
    position: relative;
    margin-bottom: 0px; 
    padding-bottom: 10px;
    border-bottom: 1px dotted #dee2e6;
    list-style: none;
}
h3.heading {
    font-weight: bold;
    font-size: 22px;
    text-transform: uppercase;
}
label {
    display: inline-block;
     margin-bottom: 0px; 
}
.sidebartitle a {
    color: grey;
}
.categories h3 {
    color: #0009;
    font-weight: bold;
    font-size: 20px;
}
.sidebar {
    background: #e4fbfb4a;
    padding: 10px;
    box-shadow: 0 0 3px 0px #00000042;
    margin-bottom: 22px;
}
.sidebar-boxx {
    margin-bottom: 1px;
    padding: 25px;
    font-size: 15px;
    width: 100%;
    background: #e4fbfb4a;
}
li span {
    float: right;
}
.describe a {
    color: gray;
}
.txt_Short {
    color: #0009;
    font-weight: bold;
    font-size: 14px;
    padding: 0px 1px;
}

#txt_Short {
    display: inline;
    width: 190px;
    border: none;
    background: #fff;
    color: gray;
}
.rating {
    margin: 4px 0px;
}
.box h5 {
    font-size: 17px;
    font-weight: bold;
    margin-top: 0.3rem;
    margin-bottom: 0.2rem;
}
.box p {
    font-size: 15px;
}
div.box a {
    color: gray;
}
.formsearch {
    background: #fff;
    border: 1px solid #c8c1c1;
    padding: 3px;
}
.sidebarheading {
    background: #fff;
    box-shadow: 0px 2px 4px #00000029;
    font-weight: bold;
    font-size: 15px;
    padding: 3px;
    margin-bottom: 18px;
}
.sidebarbox {
    padding: 10px;
    display: block;
}
.sidebarimg {
    display: block;
    width: 90px;
    height: 90px;
    border: 3px solid #8080806e;
    float: left;
    margin-right: 20px;
}

.sidebartitle {
    display: inline-block;
    margin: 11px 0px 2px 0px;
    padding: 5px;
    font-weight: bold;
    font-size: 15px;
}
.sidebartext {
    margin: 5px 0px 10px 0px;
    font-size: 15px;
}
.sidebar2{
    background: #e4fbfb4a;
}
.sidebarbox:hover {
    box-shadow: 0 3px 8px #31ceceb3;
    /*cursor: pointer;*/
    background: #fff;
}
button {
    margin: 0 auto;
    border: 0px;
    background: #fff;
    border-bottom: 1px solid;
    text-transform: capitalize;
    font-weight: 500;
    color: #00BCD4;
    cursor: pointer;
}
.commingSoonbooks:hover .readdiv{
    transition: 0.5s ease-out;
    bottom: 50%;
    position: absolute;
    background: gray;
    padding: 4px;
    left: 10%;
    right: 10%;
    text-align: center;
    font-weight: bold;
    border-radius: 4px;
    color: black;
    display: block;
}
/* Extra small devices (phones, 600px and down) */
@media only screen and (min-width: 600px) {
    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 130px;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -72px 24%;
        background: transparent;
    }
}
input[type=checkbox], input[type=radio] {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0;
    margin: 0px 9px;
}
.allcatpagination a:hover {
    background: #c8c2c1;
}
.allcatpagination a.active {
    background: #656363;
    color: #fff;
}
.firstpage {

}
.lastpage{

}
.allcatpagination a {
    background: #e7e6e2;
    color: #0c0c0c;
    padding: 6px;
    font-size: 14px;
    font-weight: bold;
    margin: 2px;
    display: -webkit-inline-box;
}
/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
}
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {


} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

}

@media only screen and (max-width: 992px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 279px !important;
        height: 22vh;
    }
}
</style>
  