<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" /> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<link rel="stylesheet" href="<?php echo base_url('assets/build/css/intlTelInput.css'); ?>">
<!-- <link rel="stylesheet" href="<?php echo base_url('assets/build/css/demo.css'); ?>"> -->
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/contact_bg.png'); ?>);">
    <div class="container">
        <div class="row">
            <div class="bannerheading"><h1>Checkout</h1></div>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active">Checkout</li>
            </ol>
        </nav>
    </div>
</div>


<div class="plan-chechout-se">
   <div class="container">
      <div class="ger-title">
         <h2>Customer Information</h2>
      </div>
      <div class="payment-bx-se">
         <div class="row">
            <div class="col-md-8">
               <div class="cont-bx-border">
                  <h3>Customer Information</h3>
                  <?php 
                    /*echo '<pre>';
                    print_r($userData);exit;*/
                  ?>
                <form role="form" id="book-checkout-form" name="payment_form" action="<?php echo site_url('checkout'); ?>" method="post" class="require-validation" data-cc-on-file="false" data-stripe-publishable-key="<?php echo $this->config->item('stripe_key') ?>" >
                    <div class="form-row">

                        <input type="hidden" name="user_id" value="<?php echo @$userData->uc_id?>" >
                        <input type="hidden" name="book_id" value="<?php echo @$bookData->b_id?>" >
                        <!--  <input type="hidden" name="stripeToken" id="stripeToken" value="" >  -->

                        <input type="hidden" id="txt_paymethod" name="txt_paymethod" value="cc">

                        <div class="form-group col-md-6">
                            <label for="txt_firstname">First Name<span> *</span></label><br>
                            <input type="text" name="txt_firstname" id="txt_firstname" value="<?php echo @$userData['u_firstname'];?>" class="form-control" placeholder="First Name" form="book-checkout-form" >
                            <?php echo form_error('txt_firstname','<small class="form-text text-danger">','</small>'); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="txt_lastname">Last Name<span> *</span></label><br>
                            <input type="text" name="txt_lastname" id="txt_lastname" value="<?php echo @$userData['u_lastname'];?>" class="form-control" placeholder="Last Name" form="book-checkout-form">
                            <?php echo form_error('txt_lastname','<small class="form-text text-danger">','</small>'); ?>
                          </div>
                    </div> 

                    <div class="form-row">
                        
                        <div class="form-group col-md-6">
                            <label for="txt_email">Email Address<span> *</span></label><br>
                            <input type="text" name="txt_email" id="txt_email" value="<?php echo @$userData['u_email'];?>" class="form-control" placeholder="Email Address" <?php echo (!empty($userData))?"readonly":""; ?>>
                            <?php echo form_error('txt_email','<small class="form-text text-danger">','</small>'); ?>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="txt_mobile2">Phone</label><br>
                            <input type="text" name="txt_phone" id="txt_mobile2" value="<?php echo @$userData['u_mobile'];?>" class="form-control" placeholder="Phone" max-length="15" >
                            <?php echo form_error('txt_phone','<small class="form-text text-danger">','</small>'); ?>
                        </div>
                    </div>

                    <?php if(empty($userData)){?>
                    <div class="row justify-content-md-center">
                        <div class="form-group col-md-12">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="txt_create_account" name="txt_create_account" value="true" >
                                <label class="custom-control-label" for="txt_create_account">Create Account</label>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                  <h3>Billing Information</h3>
                  <hr>
                  <div class="paymant-det-se">
                     <label class="control-label">Payment Method</label>
                     <ul id="paytab" class="nav nav-tabs nav-pills nav-justified">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" onclick="changePaymentType('cc')" href="#cc"><i class="fa fa-credit-card" aria-hidden="true"></i>  Credit Card</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" onclick="changePaymentType('evc')" href="#evc"><i class="fa fa-etsy" aria-hidden="true"></i>  EVC</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" onclick="changePaymentType('mpesa')" href="#mpesa"><i class="fa fa-money" aria-hidden="true"></i>  Mpesa</a>
                        </li> 
                     </ul>
                    </div>

      				<!-- Tab panes -->
                    <div class="tab-content pt-4">
                        <div class="tab-pane container active" id="cc">
                            <!-- Start tab panel stripe  -->
                                
                                
                                <div class="carda card-default credit-card-box">
                                    
                                    <div class="card-bodya">
                                        <div class='form-row row'>
                                            <div class='col-md-12 form-group required'>

                                                <label class='control-label'>Cardholder Name <span>*</span></label> <input

                                                    class='form-control' size='4' type='text' name='txt_holdername' value="<?php echo @$userData['u_firstname'].' '.@$userData['u_lastname'];?>">
                                            </div>
                                        </div>
                                        <div class='form-row row'>
                                            <div class='col-md-12 form-group card required'>
                                                <label class='control-label'>Card Number <span>*</span>
                                                  <div style="display: block;float: right;height: 33px;width: 50%;"><img src="<?php echo base_url('assets/images/allcard.png'); ?>" class="img-fluid"></div>
                                                    </label> <input

                                                        autocomplete='off' class='form-control card-number' size='20'

                                                        type='text' name='txt_cardnumber' maxlength="20">
                                            </div>
                                        </div>
                                        <div class='form-row row'>
                                            <div class='col-sm-12 col-md-4 form-group cvc required'>
                                                <label class='control-label'>CVV <span>*</span></label> <input autocomplete='off'

                                                    class='form-control card-cvc' placeholder='ex. 311' size='4'

                                                    type='text' name='txt_cvv' maxlength="4">
                                            </div>
                                            <div class='col-sm-12 col-md-4 form-group expiration required'>
                                                <label class='control-label'>Expiration Month <span>*</span></label> <select

                                                    class='form-control card-expiry-month' placeholder='Month'

                                                    name='txt_expmonth'>
                                                    <option value="">Month</option>
                                                    <option value="01">01</option>
                                                    <option value="02">02</option>
                                                    <option value="03">03</option>
                                                    <option value="04">04</option>
                                                    <option value="05">05</option>
                                                    <option value="06">06</option>
                                                    <option value="07">07</option>
                                                    <option value="08">08</option>
                                                    <option value="09">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            <div class='col-sm-12 col-md-4 form-group expiration required'>
                                                <label class='control-label'>Expiration Year <span>*</span></label> <select

                                                    class='form-control card-expiry-year'

                                                    name='txt_expyear'>
                                                    <option value="">Year</option>
                                                    <script>
                                                    
                                                    var d= new Date(); 
                                                    var x= d.getFullYear();
                                                    var y= x+50
                                                        for(;x < y ; x++){
                                                        document.write('<option value="'+x+'">'+x+'</option>');
                                                        }
                                                    </script>
                                                    </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            
                        </div>


                         <div class="tab-pane container fade" id="evc">
                            <div class="form-row">
                                <div class="form-group col-md-12 text-center">
                                    OUR CUSTOMER SERVICES IN SOMALIA HAFSA HASSAN +252-61-3148376
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane container fade" id="mpesa">
                            <div class="form-row">
                                <div class="form-group col-md-12 text-center">
                                    Dear Valued Customer please send the money through Mpesa and call us to confirm. (Mpesa Number) 0725-893-222 We are still working on to the our system automatic.
                                </div>
                            </div>
                        </div> 

                        <div class="row justify-content-md-center">
                            <div class="form-group col-md-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="txt_agree" name="txt_agree" value="true">
                                    <label class="custom-control-label" for="txt_agree">I agree to the payment terms as stated above.</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-info btn-lg btn-block" type="submit" id="submitBTN" name="paynow" value="stripe" onclick="return checkoutFormValidation();">Pay Now ($<?php echo @$bookData->b_sellingprice?>)</button>

                                <button class="btn btn-info btn-lg btn-block" type="submit" id="submitOther"  style="display:none;" name="paynow" value="stripe" onclick="return checkoutFormValidation();">Submit</button>
                            </div>
                        </div> 


                    </div>

                </form>
               </div>
            </div>

            <div class="col-md-4">
                <div class="cont-bx-border">
                  <h3>Book Details</h3>
                  <h4><?php echo @$bookData->b_title?></h4>
                  <span>Price : $<?php echo @$bookData->b_sellingprice?></span>
                </div>
            </div>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
.card{
	border:none;
}
.card-title {
display: inline;
font-weight: bold;
}
.display-table {
    display: table;
}
.display-tr {
    display: table-row;
}
.display-td {
    display: table-cell;
    vertical-align: middle;
    width: 61%;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 328px;
    height: calc(40vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar {
    background: #54bfe3;
}
/*.sticky-wrapper.is-sticky .site-navbar ul li a {
    color: #505048c4 !important;
}*/
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}


.bannerheading {
    margin: auto;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    top: 60px;
    font-size: 55px;
}
.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}
form#contact_form input {
    font-size: 14px;
}
form#contact_form {
    font-size: 14px;
    margin: 22px 0px;
}
.loadmore {
    background: #54bfe3 !important;
    border-radius: 1px !important;
    color: #fff !important;
    font-weight: bold;
    font-size: 15px;
    margin-top: 20px;
    border: 1px solid #54bfe3 !important;
    padding: 12px 80px;
}
.address {
    background: #00BCD4;
    padding: 50px;
}
.address p {
    text-align: center;
    color: #fff;
}
.address label {
    text-align: center !important;
    margin: 0 auto !important;
    color: #fff;
    font-size: large;
}
.socialicon{
  text-align: center;
}
.socialicon a {
    padding: 10px;
    background-repeat: no-repeat;
    background-size: cover;
    display: inline-block;
    width: 30px;
    height: 30px;
    margin: 4px;
}
.socialicon a:hover {
    opacity: 0.6;
}
.form-control:active, .form-control:focus {

    border-color: #00BCD4;

}
a.facebook {
    background: url(./assets/images/follow_us_fb.png);
}
a.twitter {
    background: url(./assets/images/follow_us_tw.png);
}
a.google {
    background: url(./assets/images/follow_us_g.png);
}
a.instagram {
    background: url(./assets/images/follow_us_insta.png);
}
a.linkedin {
    background: url(./assets/images/follow_us_link.png);
}

label#txt_paymethod-error ,label#txt_agree2-error{
    position: absolute;
    bottom: -80px;
    left: 0;
}
label#txt_agree2-error,label#txt_agree3-error{
    position: absolute;
    bottom: -25px;
    left: 0;
}
.custom-radio .custom-control-input:checked~.custom-control-label:before {
    background-color: #08c;
}
.custom-checkbox .custom-control-input:checked~.custom-control-label:before {
    background-color: #08c;
}
#txt_agree-error {
    display: block;
    position: absolute;
    bottom: -27px;
}

#txt_agree1-error {
    display: block;
    position: absolute;
    bottom: -27px;
}

#txt_agree2-error {
    display: block;
    position: absolute;
    bottom: -27px;
}
#paytab a.nav-link.active {
    color: #17a2b8 !important;
}
#paytab a.nav-link {
    color: #827979;
}
.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #eaeaea;
}

label {
    display: inline-block;
    margin-bottom: 0.5rem;
    color: #332727;
}
/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {

    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 60px!important;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -50px 20%;
        background: transparent;
        padding: 0px;
    }
}
.iti {
    width: 100%;
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {
    .breadcrumb {
        position: absolute;
        margin: -50px 30%;
        background: transparent;
    }
}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
}
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {

} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

}
</style>
<script src="<?php echo base_url('assets/build/js/intlTelInput.js'); ?>"></script>
  <script>
/*    var input = document.querySelector(".mobileFieldClass");
    window.intlTelInput(input, {
      allowExtensions: true,
          autoFormat: false,
          autoHideDialCode: false,
          autoPlaceholder: false,
          defaultCountry: "auto",
          ipinfoToken: "yolo",
          nationalMode: false,
          numberType: "MOBILE",
          //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
          preferredCountries: ['us', 'ca', 'in'],
          // preventInvalidNumbers: true,
          // utilsScript: "lib/libphonenumber/build/utils.js"
          preventInvalidNumbers: true,
      utilsScript: "assets/build/js/utils.js",

    });*/
  </script>

