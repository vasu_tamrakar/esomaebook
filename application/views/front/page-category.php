<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0&appId=470017186888654&autoLogAppEvents=1"></script>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/biogrphy_bg.png'); ?>);">
    <div class="container">
        <div class="row">
        <div class="bannerheading"><h1><?php echo (isset($categoryName)?$categoryName:''); ?></h1></div>
        </div>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('category'); ?>">Category</a></li>
            <li class="breadcrumb-item active"><?php echo (isset($categoryName)?$categoryName:''); ?></li>
          </ol>
        </nav>

    </div>
    
</div>
<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="sidebar">
                        <form name="short_Form" id="short_Form" type="GET" action="" class="inline-form" onchange="changecatFunction()">
                            <div class="sidebar-boxx">
                                <div class="formsearch">
                                    <?php $txtShort = $this->input->get('txtShort'); ?>
                                  <label class="txt_short">Short By:</label>
                                    <select name="txtShort" id="txt_Short" class="selectfilter">
                                        <option value="">Select Shorting</option>
                                        <option value="b_published" <?php if($txtShort =='b_published'){ echo 'selected'; } ?>>Recently Released</option>
                                        <option value="mostpopular" <?php if($txtShort =='mostpopular'){ echo 'selected'; } ?>>Most Popular</option>
                                        <option value="b_title" <?php if($txtShort =='b_title'){ echo 'selected'; } ?>>Title</option>
                                        <option value="b_fk_of_aid" <?php if($txtShort =='b_fk_of_aid'){ echo 'selected'; } ?>>Author</option>
                                    </select>
                                </div>
                            </div>
                            <?php if(isset($allCategoriesList)){ ?>
                            <div class="sidebar-boxx">
                              <div class="categories">
                                <h3>Categories</h3>
                                    <?php
                                    $catFID = $this->input->get('filterCat');
                                    
                                    if($catFID){
                                        $selcat = $catFID;
                                        
                                    }else{
                                        $catFID[] = $selectedCategory;
                                    }
                                    
                                    foreach ($allCategoriesList as $category) {
                                        ?>
                                        <li><label><input type="checkbox" name="filterCat[]" slue="<?php echo $category->c_slug; ?>" value="<?php echo $category->c_id; ?>" <?php if( in_array($category->c_id, $catFID) ){ echo 'checked'; } ?>><?php echo $category->c_name; ?></label><span>(<?php $languageSet = (($this->session->userdata('get_Language'))?$this->session->userdata('get_Language'):'english'); $countCat = $this->user_Auth->countAllwhere('books',array("b_category" => $category->c_id,'b_language' => $languageSet, "b_status" => '1')); print_r($countCat); ?>)</span></li>
                                    <?php  
                                    }
                                    ?>
                                    <!-- <button type="submit">asa</button> -->
                                    
                              </div>
                            </div>
                            <?php
                            } ?>
                        </form>
                    </div>
                    <?php if($allpopAuthorList){ ?>
                    <div class="sidebar2">
                        <div class="sidebarheading">Popular Authors</div>
                        <?php
                        foreach ($allpopAuthorList as $popAuthor) { ?>
                            <div class="box">
                                <div class="sidebarbox">
                                    <div class="sidebarimg"><img src="<?php echo (($popAuthor['a_image'])?base_url('uploads/users/'.$popAuthor["a_image"]):base_url('uploads/users/authors.png')); ?>" style="width:100%; height:100%;"></div>
                                    <div class="sidebartitle"><a href="<?php echo site_url('authorDetails/'.base64_encode($popAuthor['a_id'])); ?>"><?php echo (isset($popAuthor['a_firstname'])?$popAuthor['a_firstname']:'').' '.(isset($popAuthor['a_lastname'])?$popAuthor['a_lastname']:''); ?></a></div>
                                    <div class="sidebartext"><?php echo (isset($popAuthor['a_publishbooks'])?$popAuthor['a_publishbooks']:0); ?> Published books</div>
                                </div>
                            </div>
                            <?php //echo $allpopAuthorList->a_id; ?>
                            <?php //echo $allpopAuthorList->a_firstname; ?>
                            <?php //echo $allpopAuthorList->a_lastname; ?>
                            <?php //echo $allpopAuthorList->a_email; ?>
                            
                            
                        <?php
                        }
                        ?>
                    </div>
                    <?php
                    } ?>
                </div>

                <div class="col-md-8">
                    
                    <div class="row">
                    <?php if($mostPopularBooks){ ?>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div>
                            <div class="float-right">Showing 1-8 of <?php print_r(count($mostPopularBooks)); ?> results</div>
                            <h3 class="heading">Most Popular</h3>
                        </div>
                        <div class="border-top pt-4"></div>
                        <div class="Mostppular row">
                            <?php
                            $c=0;
                            foreach($mostPopularBooks as $pupular){
                                $mostPopulardetails = $this->user_Auth->getData('books', array('b_id' => $pupular->pd_bookid));
                                if(($mostPopulardetails) && ($c < 8)){ $c++;
                            ?>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="box">
                                    <?php
                                    if(($mostPopulardetails[0]->b_originalprice > 0) && ($mostPopulardetails[0]->b_originalprice > $mostPopulardetails[0]->b_sellingprice)){
                                    ?>
                                    <span class="discount"><?php $mostPopulardetailsPercetage = ((($mostPopulardetails[0]->b_originalprice - $mostPopulardetails[0]->b_sellingprice)*100)/$mostPopulardetails[0]->b_originalprice);echo (($mostPopulardetailsPercetage > 0)?number_format($mostPopulardetailsPercetage,0):''); ?><span class="percent-sign">%</span></span>
                                    <?php
                                    }
                                    ?>
                                    <img src="<?php echo ( ($mostPopulardetails[0]->b_image)?base_url('uploads/books/'.$mostPopulardetails[0]->b_image):base_url('uploads/books/books.png')); ?>" class="img-fluid">
                                    
                                    <div class="rating">
                                        <?php
                                        for($r= 1; $r <= 5; $r++){
                                            if($r <= $mostPopulardetails[0]->b_rating){
                                                ?>
                                                <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                                <?php
                                            }else{
                                                ?>
                                                <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                                <?php
                                            }
                                        } ?>
                                    </div>
                                    <h5><a href="<?php echo site_url('book/bookDetails/'.$mostPopulardetails[0]->b_id); ?>"><?php echo (($mostPopulardetails[0]->b_title)?$mostPopulardetails[0]->b_title:''); ?></a></h5>
                                    <p><?php $author = $this->user_Auth->getData('user_credentials',array('uc_id'=>$mostPopulardetails[0]->b_fk_of_aid), $s='uc_firstname,uc_lastname'); echo (isset($author[0]->uc_firstname)?$author[0]->uc_firstname:'').' '.(isset($author[0]->uc_lastname)?$author[0]->uc_lastname:''); ?></p>
                                    <!-- Your share button code -->
                                    <!-- <div class="fb-share-button" data-href="<?php echo site_url('book/bookDetails/'.$mostPopulardetails[0]->b_id); ?>" data-layout="button" data-size="large">
                                    </div> -->
                                </div>
                            </div>
                            <?php
                                }
                            }?><?php /*
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            */?>
                            <?php if(count($mostPopularBooks) >= 8){ ?>
                            <div class="col-sm-12 col-md-12 col-lg-12 text-center" id="mostpopularBook" cate="<?php echo (isset($selectedCategory)?$selectedCategory:0); ?>" data="0">
                                <button type="button" onclick="loadmorePopularCategorybook(<?php echo $selectedCategory; ?>,0)">see All</button>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <div style="margin:30px;"></div>
                    <?php if($allbooksidByCategory){ ?>
                    <div class="col-md-12">
                        <div>
                            <div class="float-right">Showing <span class="updatePN">1-8</span> of <?php $total = (isset($allbooksidByCategory)?count($allbooksidByCategory):0); echo $total; ?> results</div>
                            <h3 class="heading">All about Business & Investing</h3>
                        </div>
                        <div class="border-top pt-4"></div>
                        <div class="filterBooksall row">
                            <?php
                            $page = 1;
                            foreach ($allbooksidByCategory as $allbooks) {
                                //echo "- ".$allbooks->b_id;
                                $allbdetails = $this->user_Auth->getData('books', array('b_id' => $allbooks->b_id));
                                if( $page <= 8 ){ 
                                    $page++;
                                
                            ?>
                            <div class="col-sm-3">
                                <div class="box">
                                    <?php
                                    if(($allbdetails[0]->b_originalprice > 0) && ($allbdetails[0]->b_originalprice > $allbdetails[0]->b_sellingprice)){
                                    ?>
                                    <span class="discount"><?php $allbdetailsPercetage = ((($allbdetails[0]->b_originalprice - $allbdetails[0]->b_sellingprice)*100)/$allbdetails[0]->b_originalprice);echo (($allbdetailsPercetage > 0)?number_format($allbdetailsPercetage,0):''); ?><span class="percent-sign">%</span></span>
                                    <?php
                                    }
                                    ?>
                                    <img src="<?php echo ( ($allbdetails[0]->b_image)?base_url('uploads/books/'.$allbdetails[0]->b_image):base_url('uploads/books/books.png')); ?>" class="img-fluid">
                                    <div class="rating">
                                        <div class="rating">
                                        <?php
                                        for($r= 1; $r <= 5; $r++){
                                            if($r <= $allbdetails[0]->b_rating){
                                                ?>
                                                <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                                <?php
                                            }else{
                                                ?>
                                                <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                                <?php
                                            }
                                        } ?>
                                    </div>
                                    </div>
                                    <h5><a href="<?php echo site_url('book/bookDetails/'.$allbdetails[0]->b_id); ?>"><?php echo (isset($allbdetails[0]->b_title)?$allbdetails[0]->b_title:''); ?></a></h5>
                                    <p>
                                        <?php 
                                        if(isset($allbdetails[0]->b_id)){
                                            $oneAuthor = $this->user_Auth->getData('user_credentials',array('uc_id' => $allbdetails[0]->b_fk_of_aid), $se='uc_firstname, uc_lastname');
                                            echo (isset($oneAuthor[0]->uc_firstname)?$oneAuthor[0]->uc_firstname:'').' '.(isset($oneAuhor[0]->uc_lastname)?$oneAuthor[0]->uc_lastname:'');
                                        }
                                         ?>
                                    </p>
                                    <!-- Your share button code -->
                                    <!-- <div class="fb-share-button" data-href="<?php echo site_url('book/bookDetails/'.$allbooks->b_id); ?>" data-layout="button" data-size="large">
                                    </div> -->
                                </div>
                            </div>
                            <?php
                                }
                            }
                            ?>
                            <?php /*
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>*/?>
                        </div>
                            <?php if($total > 8){ ?>
                            <div class="col-sm-12 text-center allcatpagination" cate="<?php echo (isset($selectedCategory)?$selectedCategory:0); ?>" data="0">
                                <?php $numbr =ceil($total/8);
                                if($numbr > 0){
                                    ?>
                                    <a href="javascript:void(0)" onclick="pageLoadData(<?php echo '1,'.$total; ?>)" class="firstpage plink"> < </a>
                                    <?php
                                }
                                $c=1;
                                for($x=1; $x <= $numbr; $x++) {
                                    ?>
                                    <a href="javascript:void(0)" onclick="pageLoadData(<?php echo $x.','.$total; ?>)" class="plink <?php if($x==1){ echo 'active'; } ?>"><?php echo $x; ?></a>
                                    <?php
                                } 
                                if($numbr > 1){
                                    ?>
                                    <a href="javascript:void(0)" onclick="pageLoadData(<?php echo $numbr.','.$total; ?>)" class="lastpage plink"> > </a>
                                    <?php
                                }
                                 ?>
                            </div>
                            <?php } ?>
                        
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="site-section bg-res" style="background-size: 100%;background-position: center center;background-image: url(<?php echo base_url('assets/images/become_a_member_bg.png'); ?>);background-repeat: no-repeat;">
    <div class="container">
        <div>
          <center><h3 class="heading">Do you wanna get unlimited access...? </h3>
            
          <div class="link"><a href="<?php echo site_url('membership'); ?>" class="memberbtn">Become a Menber</a></div></center>
        </div>        
    </div>
</section>

<section style="margin: 70px 0px;">
    <div class="container">
    <?php if($comingsoonbookbycategory){ ?>
        <div class="col-lg-12">
            <div>
                
                <h3 class="heading">Comming Soon</h3>
            </div>
            <div class="border-top pt-4"></div>
            <div id ="commingSoon" class="commingSoon owl-carousel owl-theme">
            <?php
            foreach ($comingsoonbookbycategory as $commingsoon) {
                
                $commingsoonDet = $this->user_Auth->getData('books', array('b_id'=> $commingsoon->b_id));
                ?>
                <div class="item">
                <div class="commingSoonbooks">
                    <?php
                    if(($commingsoonDet[0]->b_originalprice > 0) && ($commingsoonDet[0]->b_originalprice > $commingsoonDet[0]->b_sellingprice)){
                    ?>
                    <span class="discount"><?php $commingsoonDetPercetage = ((($commingsoonDet[0]->b_originalprice - $commingsoonDet[0]->b_sellingprice)*100)/$commingsoonDet[0]->b_originalprice);echo (($commingsoonDetPercetage > 0)?number_format($commingsoonDetPercetage,0):''); ?><span class="percent-sign">%</span></span>
                    <?php
                    }
                    ?>
                    <a href="<?php echo site_url('book/bookDetails/'.$commingsoon->b_id); ?>">
                        <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                    </a>
                    <div class="readdiv"><a href="<?php echo site_url('book/bookDetails/'.$commingsoon->b_id); ?>">READ NOW</a></div>
                </div>
                <div class="describe">
                    <div class="rates">
                        <?php
                        for($r= 1; $r <= 5; $r++){
                            if($r <= $commingsoonDet[0]->b_rating){
                                ?>
                                <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                <?php
                            }else{
                                ?>
                                <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                <?php
                            }
                        } ?>
                    </div>
                    <label><a href="<?php echo site_url('book/bookDetails/'.$commingsoon->b_id); ?>"><?php echo (isset($commingsoonDet[0]->b_title)?$commingsoonDet[0]->b_title:''); ?></a></label>
                    <span><?php echo (isset($commingsoonDet[0]->b_published)?date('d M Y',strtotime($commingsoonDet[0]->b_published)):''); ?></span>
                    <!-- Your share button code -->
                    <!-- <div class="fb-share-button" data-href="<?php echo site_url('book/bookDetails/'.$commingsoon->b_id); ?>" data-layout="button" data-size="large">
                    </div> -->
                </div>
            </div>
                <?php
            }
            ?>
                <?php /*
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>*/?>
                
                
            </div>
        </div>
        <?php } ?>
    </div>
  </section>

<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 360px;
    height: calc(40vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar {
    background: #54bfe3 !important;
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}


.bannerheading {
    display: block;
    margin: auto;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    top: 50px;
    font-size: 55px;
    font-family: unset;
}
.breadcrumb {
    position: absolute;
    margin: -50px 25%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}


.categories li, .sidelink li {
    position: relative;
    margin-bottom: 0px; 
    padding-bottom: 10px;
    border-bottom: 1px dotted #dee2e6;
    list-style: none;
    overflow: auto;
}
h3.heading {
    font-weight: bold;
    font-size: 22px;
    text-transform: uppercase;
}
label {
    display: inline-block;
     margin-bottom: 0px; 
}
.sidebartitle a {
    color: grey;
}
.categories h3 {
    color: #0009;
    font-weight: bold;
    font-size: 20px;
}
.sidebar {
    background: #e4fbfb4a;
    padding: 10px;
    box-shadow: 0 0 3px 0px #00000042;
    margin-bottom: 22px;
}
.sidebar-boxx {
    margin-bottom: 1px;
    padding: 25px;
    font-size: 15px;
    width: 100%;
    background: #e4fbfb4a;
}
li span {
    float: right;
}
.describe a {
    color: gray;
}
.txt_Short {
    color: #0009;
    font-weight: bold;
    font-size: 14px;
    padding: 0px 1px;
}

#txt_Short {
    display: inline;
    width: 190px;
    border: none;
    background: #fff;
    color: gray;
}
.rating {
    margin: 4px 0px;
}
.box h5 {
    font-size: 17px;
    font-weight: bold;
    margin-top: 0.3rem;
    margin-bottom: 0.2rem;
}
.box p {
    font-size: 15px;
}
div.box a {
    color: gray;
}
.formsearch {
    background: #fff;
    border: 1px solid #c8c1c1;
    padding: 3px;
}
.sidebarheading {
    background: #fff;
    box-shadow: 0px 2px 4px #00000029;
    font-weight: bold;
    font-size: 15px;
    padding: 3px;
    margin-bottom: 18px;
}
.sidebarbox {
    padding: 10px;
    display: block;
}
.sidebarimg {
    display: block;
    width: 90px;
    height: 90px;
    border: 3px solid #8080806e;
    float: left;
    margin-right: 20px;
}

.sidebartitle {
    display: inline-block;
    margin: 11px 0px 2px 0px;
    padding: 5px;
    font-weight: bold;
    font-size: 15px;
}
.sidebartext {
    margin: 5px 0px 10px 0px;
    font-size: 15px;
}
.sidebar2{
    background: #e4fbfb4a;
}
.sidebarbox:hover {
    box-shadow: 0 3px 8px #31ceceb3;
    /*cursor: pointer;*/
    background: #fff;
}
button {
    margin: 0 auto;
    border: 0px;
    background: #fff;
    border-bottom: 1px solid;
    text-transform: capitalize;
    font-weight: 500;
    color: #00BCD4;
    cursor: pointer;
}
.commingSoonbooks:hover .readdiv{
    transition: 0.5s ease-out;
    bottom: 50%;
    position: absolute;
    background: gray;
    padding: 4px;
    left: 10%;
    right: 10%;
    text-align: center;
    font-weight: bold;
    border-radius: 4px;
    color: black;
    display: block;
}

input[type=checkbox], input[type=radio] {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0;
    margin: 0px 9px;
}
.allcatpagination a:hover {
    background: #c8c2c1;
}
.allcatpagination a.active {
    background: #656363;
    color: #fff;
}
.firstpage {

}
.lastpage{

}
.allcatpagination a {
    background: #e7e6e2;
    color: #0c0c0c;
    padding: 6px;
    font-size: 14px;
    font-weight: bold;
    margin: 2px;
    display: -webkit-inline-box;
}
/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {
    .bannerheading h1 {
        position: relative !important;
        color: #fff;
        top: 55px !important;
        font-size: 38px !important;
        text-align: center !important;
        /* font-family: unset !important; */
    }
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 360px !important;
        height: 22vh;
    }
    
}
/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (max-width: 768px) {
    .breadcrumb {
       position: absolute;
        margin: -50px 25%;
        background: transparent;
         padding: 0px !important;
    }
}
 
/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

}
@media only screen and (min-width: 769px) and (max-width: 1024px) {
    .breadcrumb {
       position: absolute;
        margin: -50px 30%;
        background: transparent;
        
    }
}
@media only screen and (max-width: 992px){
    
}
</style>
  