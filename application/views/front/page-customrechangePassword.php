<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2">
            <?php include 'frontleftmenu.php'; ?>
        </div>
        <div class="col-md-10 col-lg-10">
            <div class="row">
                <div class="col-md-12">
                <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                  	<div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                      <?php
                  }
                  ?>
                  	<div class="mb-5">
                    	<!-- <h2 class="heading text-center"><strong>Change Password</strong></h2> -->
                  	</div>
                  	<div class="row justify-content-md-center">
                    		<div class="col-md-9">
    	                  		<div class="card" style="box-shadow: 0 0 3px 0px black;">
      	                  			<div class="card-body">
                                    <h2 style="margin-bottom:20px;">Change Password</h2>
                          					<form name="changePassword_Form" id="changePassword_Form" method="post">
                            						<div class="row">
            	                  						<div class="col-md-6">
              		                  						<div class="form-group">
                		                  							<label for="txt_newpass">New Password</label>
                		                  							<input type="password" name="txt_newpass" id="txt_newpass" class="form-control" placeholder="New password" max-length="25">
              		                  						</div>
            	                  						</div>
            	                  						<div class="col-md-6">
              		                  						<div class="form-group">
                		                  							<label for="txt_cnfnewpass">Confirm New Password</label>
                		                  							<input type="password" name="txt_cnfnewpass" id="txt_cnfnewpass" class="form-control" placeholder="Confirm New password" max-length="25">
              		                  						</div>
            		                  					</div>
          		                  				</div>
          		                  				<div class="row justify-content-md-center">
          		                  					<button type="submit" name="submit" class="btn btn-info">Save</button>
          		                  				</div>
                          					</form>
                        				</div>
    	                  		</div>
  	                  	</div>	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Button to Open the Modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Open modal
</button> -->

<!-- The Modal -->
<div class="modal fade" id="myModalview">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Order Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<style type="text/css">
body {
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 102px;
height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.site-section {
    padding: 0;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

a.site_title:hover{
    color: #fff;
}
.navbar-nav .nav-link {
    padding: 0.3rem 1rem;
}
.fa::after {
  margin-right: 2px;
  content: ;
  content: "";
}
@media only screen and (max-width: 480px) {
    #search{
        width: 71% !important;
    }
}
@media only screen and (max-width: 600px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 138px !important;
        height: 20vh;
    }
}
</style>