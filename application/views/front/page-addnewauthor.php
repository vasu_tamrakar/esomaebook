<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2">
            <?php include 'frontleftmenu.php'; ?>
        </div>
        <div class="col-md-10 col-lg-10">
            <div class="row">
                <div class="col-md-12">
	                <div class="mb-5"></div>
	                <div class="col-md-12">
	                	<div class="card">
                            <?php 
                            if($authorAdded){ ?>
                               <div class="alert alert-info alert-dismissible fade show" role="alert">
                                    <strong style="text-transform: capitalize;">Info!</strong> You have added author. <a href="<?php echo site_url('user/editAuthor/'.$authorAdded["u_id"]); ?>"><?php echo $authorAdded[0]->a_name; ?></a>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                </div> 
                            <?php
                            }else{
                            ?>

		              		<div class="card-header bg-info"><span style="color: #fff;">Add new author form</span></div>
		              		<div class="card-body">
		              			<form name="addauthor_Form" id="addauthor_Form" action="" method="post" enctype="multipart/form-data">
				                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
        				                        <label for="txt_authorname">Author Name:</label>
        				                        <input type="text" name="txt_authorname" id="txt_authorname" value="<?php echo set_value('txt_authorname'); ?>" class="form-control" placeholder="Enter Author Name" max-length="150">
        				                        <?php echo form_error('txt_authorname', '<small class="error">', '</small>'); ?>
        				                    </div>
        				                </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
        				                        <label for="txt_published">Published Year:</label>
        				                        <input type="text" name="txt_published" id="txt_published" value="<?php echo set_value('txt_published'); ?>" class="form-control" placeholder="Enter year" max-length="4">
        				                        <?php echo form_error('txt_published', '<small class="error">', '</small>'); ?>
        				                    </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
        				                    <div class="form-group">
        				                        <label for="txt_description">Description :</label>
        				                        <textarea name="txt_description" id="txt_description" class="form-control" placeholder="Enter Descriptions..." rows="5"><?php echo set_value('txt_description'); ?></textarea>
        				                        <?php echo form_error('txt_description', '<small class="error">', '</small>'); ?>
        				                    </div>
                                        </div>
                                        <div class="col-md-6">
        				                    <div class="form-group">
        				                        <label for="txt_image">Profile Image :</label>
        				                        <input type="file" name="txt_image" id="txt_image" class="form-control-file" accept="image/*">
        				                        <small class="text-info">Image format accept only gif,jpg and png.</small>
        				                    </div>
			                            </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_facebook">Facebook Link :</label>
                                                <input type="text" name="txt_facebook" id="txt_facebook" class="form-control" placeholder="https://www.faceboook.com/zzxsdsd..." value="<?php echo set_value('txt_facebook'); ?>" max-lenght="200">
                                                <?php echo form_error('txt_facebook', '<small class="error">', '</small>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_twitter">Twitter Link :</label>
                                                <input type="text" name="txt_twitter" id="txt_twitter" class="form-control" placeholder="https://www.twitter.in/zzxsdsd..." value="<?php echo set_value('txt_twitter'); ?>" max-lenght="200">
                                                <?php echo form_error('txt_twitter', '<small class="error">', '</small>'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_instagram">Instagram Link :</label>
                                                <input type="text" name="txt_instagram" id="txt_instagram" class="form-control" placeholder="https://www.instagram.com/zzxsdsd..." value="<?php echo set_value('txt_instagram'); ?>" max-lenght="200">
                                                <?php echo form_error('txt_instagram', '<small class="error">', '</small>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_gplush">Google Plus Link :</label>
                                                <input type="text" name="txt_gplush" id="txt_gplush" class="form-control" placeholder="https://www.gplush.in/zzxsdsd..." value="<?php echo set_value('txt_gplush'); ?>" max-lenght="200">
                                                <?php echo form_error('txt_gplush', '<small class="error">', '</small>'); ?>
                                            </div>
                                        </div>
                                    </div>
				                    
			                         <button type="submit" class="btn btn-info">Save</button>
				                </form>
		              		</div>
		              		<?php
                            }
                            ?>
		              	</div>
	                </div>
	              	
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
body {
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 102px;
height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.site-section {
    padding: 0;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
.card-header {
	padding: 0.2rem 1.25rem;
}
a.site_title:hover{
    color: #fff;
}
.fa::after {
  margin-right: 2px;
  content: ;
  content: "";
}
@media only screen and (max-width: 992px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 167px !important;
        height: 21vh;
    }
} 


</style>
  