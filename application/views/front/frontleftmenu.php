<div class="leftsidebar">
    <div class="clearfix"></div>
    <?php
    if($is_logged_in_user_info){ ?>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="<?php echo (isset($is_logged_in_user_info['u_image'])?base_url('uploads/users/').$is_logged_in_user_info['u_image']:base_url('uploads/users/').$is_logged_in_user_info['u_image']); ?>" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h5><?php echo (isset($is_logged_in_user_info['u_firstname'])?$is_logged_in_user_info['u_firstname']:''). ' '.(isset($is_logged_in_user_info['u_lastname'])?$is_logged_in_user_info['u_lastname']:''); ?></h5>
            </div>
        </div>
      <!-- /menu profile quick info -->
    <?php
    }
    ?>
    <div id="leftsidemenu">
        <button type="button" id="lfttogbtn" class="btn btn-info" data-toggle="collapse" data-target="#leftSidedemo"> Menu  <i class="fa fa-bars"></i></button>
        <ul class="leftsidemenu navbar-nav collapse" id="leftSidedemo">
            <?php /*<li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('user'); ?>"><i class="fa fa-home"></i> Dashboard</a>
            </li>*/ ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('user/profile/'.$is_logged_in_user_info['u_id']); ?>"><i class="fa fa-user"></i> Profile</a>
            </li>
            <?php
            if($is_logged_in_user_info['u_role'] == 5){
                if($customerAdded){
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('user/myOrder'); ?>"><i class="fa fa-th-list" aria-hidden="true"></i>
</i> My Order</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('user/manageMybooks'); ?>"><i class="fa fa-edit" aria-hidden="true"></i>
</i> Manage My Books</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('user/customerchangePassword'); ?>"><i class="fa fa-unlock-alt" aria-hidden="true"></i>
 Change  Password</a>
                    </li>
                    <?php
                }

            } 
            if($is_logged_in_user_info['u_role'] == 4){
                if(!$authorAdded){ ?>
            
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('user/addAuthor'); ?>"><i class="fa fa-edit"></i> Add New Author</a>
            </li>
              <?php
                }else{
              ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('user/addbook'); ?>"><i class="fa fa-book"></i> Add New Books</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('user/viewBooks'); ?>"><i class="fa fa-list-alt" aria-hidden="true"></i>
 View Books</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('user/viewmyOrder'); ?>"><i class="fa fa-th-list" aria-hidden="true"></i>
</i> View My Order</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('user/authorchangePassword'); ?>"><i class="fa fa-unlock-alt" aria-hidden="true"></i>
 Change Password</a>
            </li>
            <?php
                }
            } ?>
        </ul>
    </div>
</div>
<style>
@media only screen and (max-width: 600px){
    #leftsidemenu button.btn.btn-info {
        width: 100%;
    }
}
</style>