<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section"></div>
<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 text-center">
                <h2 class="section-title mb-3" style="color:#54BFE3;">Sell Your E-Book</h2>
            </div>
        </div>
        <div class="row" style="border:1px solid gray;padding:17px;box-shadow: 0px 0px 20px -10px black;">
            <div class="col-md-12">
                <div class="divborder">
                    <div class="row">
                        <div class="col-md-9">
                            <h2><strong>Create Your Profile</strong></h2>
                            <form name="" id="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">First Name</label>
                                            <input type="text" name="" id="" class="form-control" placeholder="Andrew">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Last Name</label>
                                            <input type="text" name="" id="" class="form-control" placeholder="Barton">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Email</label>
                                            <input type="email" name="" id="" class="form-control" placeholder="Andrew@gmail.com">
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Contact Number (mobile/telephone)</label>
                                            <input type="text" name="" id="" class="form-control" placeholder="worton">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-form-label">Address</label>
                                            <input type="text" name="" id="" class="form-control" placeholder="Your Full Address">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Add Bio</label>
                                            <textarea name="" id="" class="form-control" placeholder="Write About Yourself" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Create Password</label>
                                            <input type="password" name="" id="" class="form-control" placeholder="*************">
                                        </div> 
                                    </div>
                                </div>
                                <br/>
                                <br/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="submit" name="" id="" value="Save" class="btn btn-block btn-info">
                                    </div>
                                </div>
                                <br/>
                                <br/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2>Payment Information<h2>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-block" style="border: 2px solid #d8d6d6;padding: 6px;background-color: #fff;"><img src="<?php echo base_url('assets/images/ppcom.png'); ?>" class="img-fluid"></button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-block btn-info">Mobile Money Transfer</button>
                                    </div>
                                </div>               
                            </form>
                        </div>
                        <div class="col-md-3">
                            <h3>Profile Picture</h3>
                            <img src="<?php echo base_url('assets/images/Somali_authors_3.png'); ?>" class="img-fluid" style="border: 5px solid #fff;box-shadow: 0 0 9px 0px #dee2e6;">
                            <input type="file" name="" id="" style="visibility: hidden;">
                            <label style="text-decoration: underline;">Upload Profile Picture</label>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <br/></br/>

        <div class="row" style="border:1px solid gray;padding:17px;box-shadow: 0px 0px 20px -10px black;">
            <div class="col-md-12">
                <div class="divborder">
                    <div class="row">
                        <div class="col-md-9">
                            <h2><strong>Book Information</strong></h2>
                            <form name="" id="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Book Name</label>
                                            <input type="text" name="" id="" class="form-control" placeholder="Book Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Book Title</label>
                                            <input type="text" name="" id="" class="form-control" placeholder="Book Title">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Sub Title</label>
                                            <input type="text" name="" id="" class="form-control" placeholder="Sub Title">
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Selling Price</label>
                                            <input type="text" name="" id="" class="form-control" placeholder="50">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-form-label">Description</label>
                                            <textarea name="" id="" class="form-control" placeholder="Write About Yourself" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                                 

                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="submit" name="" id="" value="UPLOAD" class="btn btn-block btn-info">
                                    </div>
                                </div>
                                <br>
                                              
                            </form>
                        </div>
                        <div class="col-md-3">
                            <br/>
                            <br/>
                            <label>Book Image</label>
                            <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid" style="border: 5px solid #fff;box-shadow: 0 0 9px 0px #dee2e6;">
                            <input type="file" name="" id="" style="visibility: hidden;">
                            <label style="text-decoration: underline;">Upload Image</label>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php /* Button to Open the Modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Open modal
</button> */?>

<!-- The Modal -->
<div class="modal fade" id="membershipModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center d-block">
                <h4 class="modal-title d-inline-block"><strong>Membership Information</strong></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form name="memberinfoForm" id="memberinfoForm" method="post" class="was-validated">
                
                </form>
            </div>

            <?php /* Modal footer -->
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> --*/ ?>
        </div>
    </div>
</div>


<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 83px;
    height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
div.plans button:hover div.plans{
    box-shadow: 1px 2px 8px #54bfe3 !important;
}
.plans {
    background: #fff;
    padding: 0px 0px 32px 0;
    border-top-left-radius: 60px;
    width: 86%;
    margin: 15px auto;
    box-shadow: 1px 2px 8px black;
    background-image: url(./assets/images/membership_plans_forma_tag.png);
    background-repeat: no-repeat;
    background-position-y: bottom;
}
.heading {
    background-color: #55C0E1;
    padding: 36px 0px;
    text-align: center;
    border-top-left-radius: 60px;
    border-bottom-right-radius: 60px;
    margin: 0px;
}
.plans h1 {
    font-weight: bold;
    border-bottom: 1px solid gray;
    text-align: center;
    margin: 25px;
}
.plans h6 {
    font-size: 20px;
    text-align: center;
    font-family: monospace;
    font-weight: 600;
}
.plans p {
    text-align: center;
    text-decoration: underline;
    letter-spacing: 1px;
    padding: 13px;
}
.plans button {
    background-color: #656161;
    margin: 0 auto;
    border: 1px solid gray;
    color: #fff;
    font-weight: bold;
    text-transform: capitalize;
    padding: 7px 34px;
    cursor: pointer;
}
.plans button:hover{
    background-color: #54bfe3;
    color: #fff;
}
.custom-select.is-valid, .form-control.is-valid, .was-validated .custom-select:valid, .was-validated .form-control:valid {
    border-color: #08c;
}
.loader {
    background-image: url(./assets/images/green_loading_circle.gif);
    background-size: contain;
    background-position: center;
    pointer-events: none;
    background-repeat: no-repeat;
}
.form-group {
    margin-bottom: 10px;
}
label {
    display: inline-block;
    margin-bottom: -0.3rem;
}
.custom-select.is-valid:focus, .form-control.is-valid:focus, .was-validated .custom-select:valid:focus, .was-validated .form-control:valid:focus {
    border-color: #08c;
    -webkit-box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, 0.25);
    box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, 0.25);
}
.form-control {
    border-radius: 4px !important;
}

</style>
  