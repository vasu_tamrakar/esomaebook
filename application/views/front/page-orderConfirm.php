<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">
</div>
<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="row">
        
        <div class="col-md-12">
            <?php 
            $alert = $this->session->flashdata('alert');
            if($alert){
            ?>
            <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                <strong><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php
            }
            ?>
            <div class="jumbotron text-center">
                <h1 class="text-success"> Thank You!</h1>
                <label>Your order is completed.</label>
                <?php 
                    if($txnDetails[0]->payment_mode == 'evc'){
                        echo '<p>Please complete your payment to get book access. <br>OUR CUSTOMER SERVICES IN SOMALIA HAFSA HASSAN +252-61-3148376</p>';
                    }else if($txnDetails[0]->payment_mode == 'mpesa'){
                        echo '<p>Please complete your payment to get book access. <br>Send the money thorugh Mpesa  and call us to confirm.<br>(Mpesa Number) 0725-893-222</p>';
                    }
                ?>
            </div>
             <?php
             if(isset($txnDetails)){
                ?>
                <div class="card">
                    <div class="card-header bg-success text-primary"> <center>Payment Bill Details</center></div>
                        <div class="card-body">
                        
                    
                            <table id="outprint" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                      <td>Payment Status</td>
                                      <td><?php echo $txnDetails[0]->payment_status; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Transaction ID</td>
                                        <td><?php echo $txnDetails[0]->transaction_id; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Book Name</td>
                                        <td>
                                        <?php 
                                        $bookDetails = $this->book_model->getBookDetails($txnDetails[0]->book_id);
                                        echo $bookDetails[0]->b_title; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Book Price</td>
                                      <td>$ <?php echo $bookDetails[0]->b_sellingprice; ?></td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                    </div>
                    
                </div>
                <center><button type="button" name="printMe" id="printMe" class="btn btn-info">Print</button></center>
                <?php
            }
            ?>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 59px;
    height: calc(10vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
div.plans button:hover div.plans{
    box-shadow: 1px 2px 8px #54bfe3 !important;
}
.plans {
    background: #fff;
    padding: 0px 0px 32px 0;
    border-top-left-radius: 60px;
    width: 86%;
    margin: 15px auto;
    box-shadow: 1px 2px 8px black;
    background-image: url(./assets/images/membership_plans_forma_tag.png);
    background-repeat: no-repeat;
    background-position-y: bottom;
}
.heading {
    background-color: #55C0E1;
    padding: 36px 0px;
    text-align: center;
    border-top-left-radius: 60px;
    border-bottom-right-radius: 60px;
    margin: 0px;
}
.plans h1 {
    font-weight: bold;
    border-bottom: 1px solid gray;
    text-align: center;
    margin: 25px;
}
.plans h6 {
    font-size: 20px;
    text-align: center;
    font-family: monospace;
    font-weight: 600;
}
.plans p {
    text-align: center;
    text-decoration: underline;
    letter-spacing: 1px;
    padding: 13px;
}
.plans button {
    background-color: #656161;
    margin: 0 auto;
    border: 1px solid gray;
    color: #fff;
    font-weight: bold;
    text-transform: capitalize;
    padding: 7px 34px;
    cursor: pointer;
}
.plans button:hover{
    background-color: #54bfe3;
    color: #fff;
}
.btn.btn-primary {
    background: #54bfe3;
    border-color: #3036387d;
    color: #fff;
}
.btn.btn-primary:hover {
    background: #247b988a;
    border-color: #232729;
    color: #fff;
}

</style>
