<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/contact_bg.png'); ?>);">
    <div class="container">
        <div class="row">
        <div class="bannerheading"><h1>Contact</h1></div>
        </div>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Contact</li>
          </ol>
        </nav>

    </div>
    
</div>


<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="col-md-12">
            <div class="heading"><h1>Leave us a message</h1></div>
            <div class="row">
                <div class="col-md-8">
                    <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                    <form name="contact_form" id="contact_form" action="<?php echo site_url('contact'); ;?>" method="post">
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="txt_firstname">First Name</label>
                          <input type="text" name="txt_firstname" id="txt_firstname" value="" class="form-control" placeholder="First Name">
                          <?php echo form_error('txt_firstname','<small class="error">','</small>'); ?>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="txt_lastname">Last Name</label>
                          <input type="text" name="txt_lastname" id="txt_lastname" value="" class="form-control" placeholder="Last Name">
                          <?php echo form_error('txt_lastname','<small class="error">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                          <label for="txt_email">Email</label>
                          <input type="text" name="txt_email" id="txt_email" value="" class="form-control" placeholder="Email">
                          <?php echo form_error('txt_email','<small class="error">','</small>'); ?>
                      </div>
                      <div class="form-group">
                          <label for="subject">Subject</label>
                          <input type="text" name="txt_subject" id="txt_subject" value="" class="form-control" placeholder="Subject">
                          <?php echo form_error('txt_subject','<small class="error">','</small>'); ?>
                      </div>
                      <div class="form-group">
                          <label for="mgs">Message</label>
                          <textarea name="txt_messsage" id="txt_messsage" value="" class="form-control" palceholder="write your message" rows="5"></textarea>
                          <?php echo form_error('txt_messsage','<small class="error">','</small>'); ?>
                      </div>
                      <button type="submit" class="btn btn-primary loadmore">SEND</button>
                    </form>
                </div>

                <div class="col-md-4">
                    <div class="address">
                        <p><b>Phone</b></p><p><label> +249 90 255 2006</label></p>
                        <div class="border-top pt-4"></div>
                        <p><b>Email</b></p><p><label> Info@esomabooks.com</label></p>
                        <div class="border-top pt-4"></div>
                        <p><b>Info</b></p><p><label> Info@esomabooks.com</label></p>
                        <div class="border-top pt-4"></div>
                        <p><b>Follow us</b></p>
                        
                        <div class="socialicon">
                            <a href="" target="_blank" class="facebook"></a>
                            <a href="" target="_blank" class="twitter"></a>
                            <a href="" target="_blank" class="instagram"></a>
                            <a href="" target="_blank" class="linkedin"></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-xl-12 pt-5">
                    <div class="">
                        <img src="<?php echo site_url('assets/images/get_book.png'); ?>" class="img-fluid">
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 328px ;
    height: calc(25vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar{
    background: #54bfe3;
}
/*.sticky-wrapper.is-sticky .site-navbar ul li a {
    color: #505048c4 !important;
}*/
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}

/*#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}*/


.bannerheading {
    margin: auto;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    top: 35px;
    font-size: 55px;
}
.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}
form#contact_form input {
    font-size: 14px;
}
form#contact_form {
    font-size: 14px;
    margin: 22px 0px;
}
.loadmore {
    background: #54bfe3 !important;
    border-radius: 1px !important;
    color: #fff !important;
    font-weight: bold;
    font-size: 15px;
    margin-top: 20px;
    border: 1px solid #54bfe3 !important;
    padding: 12px 80px;
}
.address {
    background: #00BCD4;
    padding: 50px;
}
.address p {
    text-align: center;
    color: #fff;
}
.address label {
    text-align: center !important;
    margin: 0 auto !important;
    color: #fff;
    font-size: large;
}
.socialicon{
  text-align: center;
}
.socialicon a {
    padding: 10px;
    background-repeat: no-repeat;
    background-size: cover;
    display: inline-block;
    width: 30px;
    height: 30px;
    margin: 4px;
}
.socialicon a:hover {
    opacity: 0.6;
}
.form-control:active, .form-control:focus {

    border-color: #00BCD4;

}
a.facebook {
    background: url(./assets/images/follow_us_fb.png);
}
a.twitter {
    background: url(./assets/images/follow_us_tw.png);
}
a.google {
    background: url(./assets/images/follow_us_g.png);
}
a.instagram {
    background: url(./assets/images/follow_us_insta.png);
}
a.linkedin {
    background: url(./assets/images/follow_us_link.png);
}

/* Extra small devices (phones, 600px and down) */

@media only screen and (min-width: 900px) and (max-width: 1024px) {

    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 60px;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -50px 40% !important;
        background: transparent;
        padding: 0px;
    }
}
@media only screen and (max-width: 600px) {

    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 32px !important;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -50px 20%;
        background: transparent;
        padding: 0px;
    }
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 360px !important;
        height: calc(40vh);
        background-size: cover;
        background-position: center;
    }
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {
    .breadcrumb {
        position: absolute;
        margin: -50px 30%;
        background: transparent;
        padding: 0px;
    }
}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
}
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (max-width: 992px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 282px !important;
        height: 22vh;
    }

} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

}
</style>
  