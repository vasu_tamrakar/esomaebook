<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Order Details</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>View Order<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                          <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="x_content">
                        <br />
                        <div class="col-md-8 col-md-offset-2">
                        <?php if($bookOrder){ ?>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <label class="control-label">Order ID</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookOrder->id; ?> </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Order Book</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookOrder->b_title; ?></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Order Amount</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookOrder->total_amt; ?></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Customer Info</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookOrder->uc_firstname,' '.$bookOrder->uc_lastname; ?></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Payment Status</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">
                                    <?php 
                                        echo $bookOrder->payment_status;
                                    ?>
                                        
                                    </label>
                                </div>
                            </div>
                            
                            
                            <br><br>    
                            <div class="row">
                                  <div class="col-md-12">
                                      <center>
                                        
                                          <a href="<?php echo site_url('dashboard/manageBookReviews'); ?>" class="btn btn-warning">Back</a>
                                          <?php if($bookOrder->payment_status == 'pending'){?>
                                          <button type="button" onclick="orderStatusChange(<?php echo $bookOrder->id; ?>,'completed')" class="btn btn-primary">Change Status to Complete</button>
                                          <?php }else if($bookOrder->payment_status == 'completed'){ ?>
                                          <button type="button" onclick="orderStatusChange(<?php echo $bookOrder->id; ?>,'pending')" class="btn btn-info">Change Status to Pending</button>
                                        <?php }?>
                                          <button type="button" onclick="deleteOrder(<?php echo $bookOrder->id; ?>)" class="btn btn-info">Delete</button>
                                      </center>
                                  </div>
                            </div>
                    
                        <?php
                        }else{
                        ?><label class="control-label"> Invalid Order Details Request.</label><?php
                        } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->