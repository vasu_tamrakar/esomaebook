 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Datatables -->
<link href="<?php echo base_url('themes/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
              <h3>All Members</h3>
            </div>
        </div>
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>All Members</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                        <?php 
                          $alert = $this->session->flashdata('alert');
                          if($alert){
                              ?>
                              <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                                <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                        <?php
                        }
                        ?>

                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>User</th>
                                    <th>User Email</th>
                                    <th>Plan</th>
                                    <th>Payment Type</th>
                                    <th>Payment Status</th>
                                    <th>Order Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php
                            if(!empty($allMembers)) { ?>
                      
                            <tbody>
                              <?php 
                                foreach ($allMembers as $member) {
                                  echo '<tr>';
                                  echo '<td>'.$member->pd_id.'</td>';
                                  echo '<td>'.$member->uc_firstname.' '.$member->uc_lastname.'</td>';
                                  echo '<td>'.$member->uc_email.'</td>';
                                  echo '<td>'.$member->mp_name.'<br>( $'.$member->mp_price.'-'.$member->mp_validity.' )</td>';
                                  
                                  echo '<td>';
                                  if($member->pd_payby == 'cc'){
                                    echo 'Credit Card';
                                  }else if($member->pd_payby == 'evc'){
                                    echo 'EVC';
                                  }else if($member->pd_payby == 'mpesa'){
                                    echo 'Mpesa';
                                  }
                                  echo '</td>';

                                  echo '<td>'.$member->pd_status.'</td>';
                                
                                  echo '<td>'.date('m-d-Y',strtotime($member->pd_created)).'</td>';

                                  echo '<td>';
                                  ?>

                                    <div class="action-menu">
                                           <a title="View Details" class="btn btn-info" href="<?php echo site_url('dashboard/viewMember/'.$member->pd_id); ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                          
                                          <!-- <a title="Delete" class="btn btn-info" href="javascript:void(0)" onclick="deleteMember(<?php echo $member->pd_id; ?>)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>  -->
                                      </div>
                                      <div class="clearfix"></div>
                                  <?php
                                  echo '</td>';
                                  echo '</tr>';
                                }
                              ?>
                            </tbody>
                            <?php 
                            }else{ ?>
                            
                            No Members..
                            <?php
                            } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>