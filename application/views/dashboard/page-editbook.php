<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main"> 
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Edit Books</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Edit Books Form<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                    ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                            <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php
                    }
                    ?>
                    <div class="x_content">
                        <br />
                        <?php if($bookDetails){ ?>
                        <?php if($bookDetails[0]->b_file){ ?>
                        <div style="width:100px; margin: 0 auto 10px auto; ">
                            <a href="<?php echo base_url('uploads/books/'.$bookDetails[0]->b_file); ?>" target="_blank"><img src="<?php echo (($bookDetails[0]->b_image)?base_url('uploads/books/'.$bookDetails[0]->b_image):base_url('uploads/books/book.png')); ?>" class="img-responsive" >
                            </a>
                        </div>
                        <?php } ?>
                    
                        <form name="editBook_Form" id="editBook_Form" action="<?php echo site_url('dashboard/editbookDetails/'.$bookDetails[0]->b_id); ?>" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left needs-validation" novalidate="novalidate">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_language">Book Language<span class="required"> *</span>
                                        </label>
                                        <input type="hidden" name="txt_id" id="txt_id" value="<?php echo (($bookDetails[0]->b_id)?$bookDetails[0]->b_id:""); ?>">
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                           <?php $textLanguage = $bookDetails[0]->b_language; ?>
                                            <select name="txt_language" id="txt_language" class="form-control">
                                                <option value=""> -Select The Language- </option>
                                                <option value="english" <?php if($textLanguage == 'english'){ echo 'selected'; } ?>>English</option>
                                                <option value="somali" <?php if($textLanguage == 'somali'){ echo 'selected'; } ?>>Somali</option>
                                            </select>
                                           <?php echo form_error('txt_language','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_title">Book Title<span class="required"> *</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_title" id="txt_title" value="<?php echo (($bookDetails[0]->b_title)?$bookDetails[0]->b_title:""); ?>" placeholder="Book Title" class="form-control col-md-7 col-xs-12" max-length="180" required>
                                            <?php echo form_error('txt_title','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_publisheddate">Publish Date<span class="required"> *</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_publisheddate" data-provide="datepicker" id="txt_publisheddate" data-date-format="mm/dd/yyyy" value="<?php echo (($bookDetails[0]->b_published > 0)?date('m/d/Y',strtotime($bookDetails[0]->b_published)):""); ?>" class="datepicker form-control col-md-7 col-xs-12" required>
                                            <?php echo form_error('txt_publisheddate','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_category">Category </label>
                                        <?php $selcat = (isset($bookDetails[0]->b_category)?$bookDetails[0]->b_category:''); ?>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                        <?php $category = $this->user_Auth->getData('categories', $w = array('c_status' => '1'), $se='c_id,c_name', $sh='c_id DESC');
                                        if($category){ ?>
                                            <select name="txt_category" id="txt_category" class="form-control col-md-7 col-xs-12">
                                                <option value="">Select The Category</option>
                                                <?php foreach ($category as $value) { ?>
                                                    <option value="<?php echo $value->c_id; ?>" <?php if($value->c_id == $selcat){echo 'selected';} ?>> <?php echo $value->c_name; ?> </option>
                                                <?php } ?>
                                            </select>
                                            <?php echo form_error('txt_category','<span class="text-danger">','</span>'); ?>
                                        <?php } ?>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_image">Book Thumbnail Image<span class="required"> *</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="file" name="txt_image" id="txt_image" class="form-control col-md-7 col-xs-12" accept="image/*">
                                            <span class="text-danger">Accept file only .png,jpeg,gif.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bookFile">Book File<span class="required"> *</span></label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="file" name="bookFile" id="bookFile" accept="application/msword, application/vnd.ms-powerpoint,
      text/plain, application/pdf" class="form-control col-md-7 col-xs-12">
                                            <span class="text-danger">Accept file only .docx,ppt,pdf.text</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_originalprice">Book Original Price<span class="required"> *</span></label>
                                        <?php $txt_originalprice = (isset($bookDetails[0]->b_originalprice)?$bookDetails[0]->b_originalprice:''); ?>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_originalprice" id="txt_originalprice" value ="<?php echo $txt_originalprice; ?>" class="form-control col-md-7 col-xs-12" max-length="10" placeholder="20">
                                        <?php echo form_error('txt_originalprice','<span class="text-danger">','</span>'); ?>
                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_sellingprice">Book Selling Price<span class="required"> *</span></label>
                                        <?php $txt_sellingprice = (isset($bookDetails[0]->b_sellingprice)?$bookDetails[0]->b_sellingprice:''); ?>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_sellingprice" id="txt_sellingprice" value ="<?php echo $txt_sellingprice; ?>" class="form-control col-md-7 col-xs-12" max-length="10" placeholder="20">
                                        <?php echo form_error('txt_sellingprice','<span class="text-danger">','</span>'); ?>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_authorid">Author<span class="required"> *</span></label>
                                        <?php $selauthor = (isset($bookDetails[0]->b_price)?$bookDetails[0]->b_fk_of_aid:''); ?>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                        <?php $auhtorsids = $this->user_Auth->getData('user_credentials', $w = array('uc_role' => '4', 'uc_status' => '1'), $se='uc_id,uc_firstname,uc_lastname,uc_email', $sh='uc_id DESC');
                                        if($auhtorsids){ ?>
                                            <select name="txt_authorid" id="txt_authorid" class="form-control">
                                                <option value="">Select The Author</option>
                                                <?php foreach ($auhtorsids as $authordata) { ?>
                                                  <option value="<?php echo $authordata->uc_id;?>" <?php if($authordata->uc_id === $selauthor){echo 'selected';}?>> <?php echo $authordata->uc_id.' - '.$authordata->uc_email; ?> </option>
                                                <?php } ?>
                                            </select>
                                        <?php echo form_error('txt_authorid','<span class="text-danger">','</span>'); ?>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_statusa">Status<span class="required"> *</span></label>
                                        <?php $selstatus = (isset($bookDetails[0]->b_status)?$bookDetails[0]->b_status:''); ?>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                        <label for="txt_status1">
                                            <input type="radio" id="txt_status1" name="txt_status" value="1" <?php if($selstatus == '1'){ echo 'checked'; }?>>True
                                        </label>
                                        <label for="txt_status2">
                                            <input type="radio" id="txt_status2" name="txt_status" value="0" <?php if($selstatus == '0'){ echo 'checked'; }?>>False
                                        </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_publisher">Publisher</label>
                                        
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_publisher" id="txt_publisher" value ="<?php echo (($bookDetails[0]->b_publisher)?$bookDetails[0]->b_publisher:""); ?>" class="form-control col-md-7 col-xs-12" placeholder="Publisher">
                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_bookpages">Book Pages</label>
                                        
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_bookpages" id="txt_bookpages" value ="<?php echo (($bookDetails[0]->b_bookpages)?$bookDetails[0]->b_bookpages:""); ?>" class="form-control col-md-7 col-xs-12"  placeholder="Book Pages">                                        
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_isbn">ISBN</label>
                                        
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_isbn" id="txt_isbn" value ="<?php echo (($bookDetails[0]->b_isbn)?$bookDetails[0]->b_isbn:""); ?>" class="form-control col-md-7 col-xs-12"  placeholder="ISBN">
                                            <span class="text-info">If you do not enter ISBN, automatic ISBN will be generated.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_description">Book Descriptions.<span class="required"> *</span></label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="textDescription" style="display:none;"><?php echo (isset($bookDetails[0]->b_description)?$bookDetails[0]->b_description:''); ?></div>
                                    <textarea name="txt_message" id="txtEditor"></textarea>
                                    <label id="txt_description-error" class="error" for="txt_description" style="display:none;">Description field is required.</label>
                                </div>
                                
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Update</button>
                                    <a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary" type="button">Cancel</a>
                                </div>
                            </div>

                        </form>
                        <?php
                        }else{
                        ?>
                        <label class="control-label">Invalid Books </label>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>           
    </div>
</div>
<!-- /page content -->