 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Datatables -->
<link href="<?php echo base_url('themes/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
              <h3>Orders Report</h3>
            </div>
        </div>
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Orders Report</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                        <?php 
                          $alert = $this->session->flashdata('alert');
                          if($alert){
                              ?>
                              <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                                <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                        <?php
                        }
                        ?>

                        <?php 

                              echo form_label('Search Options:');
                          ?>
                          <form id="order_report" method="post" class="mb-3">
                          <div class="row">

                            <div class="col-sm-6">
                              <div class="col-sm-12">
                                  
                                  <div class="col-sm-6">
                                    <label>Author</label>
                                  <?php 
                                     $options = $authors;
                                     $aselected =  @$postData['order_report_author'];
                                     $extra = 'id="order_report_author" class="form-control col-sm-6" ';
                                     echo form_dropdown('order_report_author', $options,$aselected,$extra);

                                    
                                  ?>
                                  </div>
                                   <div class="col-sm-6">
                                    <label>Payment Status</label>
                                    <?php 
                                     $options = array('all'=>'Payment Status All','completed'=>'Completed','pending'=>'Pending');
                                     $pselected =  @$postData['report_payment_status'];
                                     $extra = 'id="report_payment_status" class="form-control col-sm-6"';
                                     echo form_dropdown('report_payment_status', $options,$pselected,$extra);

                                  ?>
                                  </div>

                                  
                                  
                              </div>

                              <?php 
                               /* echo '<pre>';
                                print_r($postData);*/
                              ?>
                              <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <label>Start Date</label>
                                    <input type="text" name="report_start_date" data-provide="datepicker" id="report_start_date" data-date-format="mm/dd/yyyy" value="<?php echo @$postData['report_start_date'];?>" class="datepicker form-control col-md-6" >
                                  </div>
                                  <div class="col-sm-6">
                                    <label>End Date</label>
                                    <input type="text" name="report_end_date" data-provide="datepicker" id="report_end_date" data-date-format="mm/dd/yyyy" value="<?php echo @$postData['report_end_date'];?>" class="datepicker form-control col-md-6" >
                                  </div>
                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="col-sm-6">
                                <label>Pending Total Amount : </label>
                                <span><?php echo number_format($pendingTotal, 2);?></span>
                              </div>
                              <div class="col-sm-6">
                                <label>Completed Total Amount : </label>
                                <span><?php echo number_format($completedTotal, 2);?></span>
                              </div>
                          </div>
                        </div>  

                        <div class="row">
                            <div class="col-sm-2">
                                <input type="submit" class="btn btn-info" value="Show Order Report" name="viewReport">
                            </div>
                        </div>
                      </form>
                      <hr>

                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <th>Book</th>
                                    <th>Book Price</th>
                                    <th>User</th>
                                    <th>Payment Method</th>
                                    <th>Payment Status</th>
                                    <th>Order Date</th>
                                </tr>
                            </thead>
                            <?php
                            if(!empty($allOrders)) { ?>
                      
                            <tbody>
                              <?php 
                                foreach ($allOrders as $order) {
                                  echo '<tr>';
                                  echo '<td>'.$order->id.'</td>';
                                  echo '<td>'.$order->b_title.'</td>';
                                  echo '<td>'.$order->b_price.'</td>';
                                  echo '<td>'.$order->uc_firstname.' '.$order->uc_lastname.'</td>';
                                  
                                  echo '<td>';
                                  if($order->payment_mode == 'cc'){
                                    echo 'Credit Card';
                                  }else if($order->payment_mode == 'evc'){
                                    echo 'EVC';
                                  }else if($order->payment_mode == 'mpesa'){
                                    echo 'Mpesa';
                                  }
                                  echo '</td>';
                                  echo '<td>'.$order->payment_status.'</td>';
                                  echo '<td>'.date('m-d-Y',strtotime($order->created_at)).'</td>';
                                  
                                  echo '</tr>';
                                }
                              ?>
                            </tbody>
                            <?php 
                            }else{ ?>
                            
                            No Orders..
                            <?php
                            } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>