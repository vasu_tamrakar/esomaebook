 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Datatables -->
    <link href="<?php echo base_url('themes/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- page content -->
<div class="right_col" role="main" style="background-image: url('themes/images/cubes.png');">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Home page slider setting</h3>
      </div>

      <!-- <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
      </div> -->
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Home page slider <small>Sliders</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <?php 
              $alert = $this->session->flashdata('alert');
              if($alert){
                  ?>
                  <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                    <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <?php
              }
              ?>
  
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Subtitle</th>
                  <th>Tagline</th>
                  <th>Status</th>
                  <th>Slider Image</th>
                  <th>Created</th>
                  <th>OrderNum</th>
                  <th>Action</th>
                </tr>
              </thead>
              <?php
              if($sliderSlide) { ?>
              
              <tbody>
                <?php
                  foreach ($sliderSlide as $sID) {
                    

                    
                  $slide = $this->user_Auth->getData('sliders', $w=array('s_id' => $sID->s_id),$se='',$sh='');
                   ?>
                <tr>
                  <td><?php echo $slide[0]->s_id; ?></td>
                  <td><?php echo (($slide[0]->s_heading)?$slide[0]->s_heading:""); ?></td>
                  <td><?php echo (isset($slide[0]->s_subtitle)?$slide[0]->s_subtitle:''); ?> </td>
                  <td><?php echo (isset($slide[0]->s_tagline)?$slide[0]->s_tagline:''); ?> </td>
                  <td><?php echo (($slide[0]->s_status == '1')?"TRUE":"FALSE"); ?> </td>
                  <td>
                    <div style="width:100px;">
                      <img src="<?php echo (isset($slide[0]->s_image)?base_url('uploads/sliders/'.$slide[0]->s_image):base_url('uploads/sliders/slide.png')); ?>" class="img-responsive">
                    </div>
                  </td>
                  <td><?php echo (($slide[0]->s_created > 0)?date('d-M-Y H:i:s',strtotime($slide[0]->s_created)):""); ?> </td>
                  <td><?php echo (($slide[0]->s_order > 0)?$slide[0]->s_order:""); ?> </td>
                  <td>
                      <div class="action-menu">
                          <button title="Edit" class="btn btn-info" onclick="editSlidemodel(<?php echo $slide[0]->s_id; ?>)"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>
                          <a title="Delete" data-title="Goto twitter?" class="btn btn-info" href="javascript:void(0)" onclick="deleteslideDetail(<?php echo $slide[0]->s_id; ?>)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                      </div>
                      <div class="clearfix"></div>
                    
                    <?php //echo $author[0]->a_id; ?>
                  </td>
                </tr>
                <?php
                
                  }
                ?>
              </tbody>
              <?php 
              } ?>
            </table>
  
            <p>
                <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Add new slide </a>
            </p>
            <div class="collapse" id="collapseExample">
                <div class="card card-body">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <form name="addHomeslide_Form" id="addHomeslide_Form" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left needs-validation" novalidate>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_heading">Title <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="txt_heading" id="txt_heading" value="<?php echo set_value('txt_heading'); ?>" placeholder="Title" class="form-control col-md-7 col-xs-12" max-length="150">
                                <?php echo form_error('txt_heading','<span class="text-danger">','</span>'); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_subtitle">Subtitle <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="txt_subtitle" id="txt_subtitle" value="<?php echo set_value('txt_subtitle'); ?>" placeholder="Subtitle" class="form-control col-md-7 col-xs-12" max-length="200">
                                <?php echo form_error('txt_subtitle','<span class="text-danger">','</span>'); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_tagline">Tagline <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="txt_tagline" id="txt_tagline" value="<?php echo set_value('txt_tagline'); ?>" placeholder="Tagline" class="form-control col-md-7 col-xs-12" max-length="100">
                                <?php echo form_error('txt_tagline','<span class="text-danger">','</span>'); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_link">Read More Link <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                               <input type="text" name="txt_link" id="txt_link" value="<?php echo set_value('txt_link'); ?>" placeholder="https://www.example.lcom" class="form-control col-md-7 col-xs-12" max-length="200">
                                <?php echo form_error('txt_link','<span class="text-danger">','</span>'); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_image">Slider Image <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                               <input type="file" name="txt_image" id="txt_image" class="form-control col-md-7 col-xs-12" accept="image/*">
                               <small class="text-info">Image format are accept only png,jpeg,jpg. and size 1528x600 px. </small>
                                <?php echo form_error('txt_image','<span class="text-danger">','</span>'); ?>
                              </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                                <a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary" type="button">Cancel</a>
                              </div>
                            </div>

                          </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->


<div id="edit_Slide" class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title text-center text-info" id="myModalLabel2">Edit Slide</h4>
      </div>
      <div class="modal-body">
          <form name="editHomeslide_Form" id="editHomeslide_Form" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left needs-validation" novalidate>
          </form>
        
      </div>
<?php /*      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
*/ ?>
    </div>
  </div>
</div>
<style>
.homeshimg{
  margin: 0px auto; 
  height: 150px;
  width: 150px;
  display: block;
}
</style>