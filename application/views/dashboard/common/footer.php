<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- footer content -->
        <footer>
          <div class="pull-right">
            Theme Admin Template by <a href="https://www.impetrosys.com">Impetrosys</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url('themes/vendors/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url('themes/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('themes/vendors/fastclick/lib/fastclick.js'); ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url('themes/vendors/nprogress/nprogress.js'); ?>"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url('themes/vendors/Chart.js/dist/Chart.min.js'); ?>"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo base_url('themes/vendors/jquery-sparkline/dist/jquery.sparkline.min.js'); ?>"></script>
    <!-- Flot -->
    <script src="<?php echo base_url('themes/vendors/Flot/jquery.flot.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/Flot/jquery.flot.pie.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/Flot/jquery.flot.time.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/Flot/jquery.flot.stack.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/Flot/jquery.flot.resize.js'); ?>"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url('themes/vendors/flot.orderbars/js/jquery.flot.orderBars.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/flot-spline/js/jquery.flot.spline.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/flot.curvedlines/curvedLines.js'); ?>"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url('themes/vendors/DateJS/build/date.js'); ?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url('themes/vendors/moment/min/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
    <?php $page = $this->uri->segment(2, 0);
    if($page == ('viewallAuthors' || 'viewallCategory')){
    ?>
    <!-- Datatables -->
    <script src="<?php echo base_url('themes/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
    <script src="<?php echo base_url('themes/vendors/datatables.net-scroller/js/dataTables.scroller.min.js'); ?>"></script>
    <?php } ?>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('themes/build/js/custom.min.js'); ?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('themes/build/js/jquery-confirm.min.css'); ?>">
<script src="<?php echo base_url('themes/build/js/jquery-confirm.min.js'); ?>"></script>
<script src="<?php echo base_url('themes/build/js/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('themes/build/js/additional-methods.min.js'); ?>"></script>
<script src="<?php echo base_url('themes/build/js/editor.js'); ?>"></script>
<script src="<?php echo base_url('themes/build/js/main.js'); ?>"></script>
   
  </body>
</html>
