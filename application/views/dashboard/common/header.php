<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('themes/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('themes/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('themes/vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url('themes/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('themes/build/css/custom.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/build/css/editor.css'); ?>" rel="stylesheet">
    <script type="text/javascript">
    const SITEURL = '<?php echo site_url(); ?>';
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url(); ?>" class="site_title"><img src="<?php echo base_url('assets/images/logo.png'); ?>" class="img-responsive"></a>
            </div>

            <div class="clearfix"></div>
            <?php
/*            if($is_logged_in_user_info){ ?>

        	
            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo (isset($is_logged_in_user_info['u_image'])?base_url('uploads/users/').$is_logged_in_user_info['u_image']:base_url('uploads/users/').$is_logged_in_user_info['u_image']); ?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo (isset($is_logged_in_user_info['u_firstname'])?$is_logged_in_user_info['u_firstname']:''). ' '.(isset($is_logged_in_user_info['u_lastname'])?$is_logged_in_user_info['u_lastname']:''); ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <?php }*/ ?>

            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-home"></i> Dashboard</a>
                  </li>
                  <li><a><i class="fa fa-arrows-alt" aria-hidden="true"></i> Category <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('dashboard/viewallCategory'); ?>">View All Categories</a></li>
                      <li><a href="<?php echo base_url('dashboard/addnewCategory'); ?>">Add New Category</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Authors <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('dashboard/viewallAuthors'); ?>">View All Authors</a></li>
                      <li><a href="<?php echo base_url('dashboard/addnewAuthor'); ?>">Add New Author</a></li>
                    </ul>
                  </li>

                   <li><a><i class="fa fa-users"></i> Users <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('dashboard/viewallUsers'); ?>">View All Users</a></li>
                      <li><a href="<?php echo base_url('dashboard/addnewUser'); ?>">Add New User</a></li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-book"></i> Books <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('dashboard/viewallBooks'); ?>">View All Books</a></li>
                      <li><a href="<?php echo base_url('dashboard/addnewBook'); ?>">Add New Books</a></li>
                    </ul>
                  </li>

                  <!-- Books chapter navigation start -->
                  <li><a><i class="fa fa-book"></i> Book Chapters <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('dashboard/viewallBooksChapter'); ?>">View All Book Chapters</a></li>
                      <li><a href="<?php echo base_url('dashboard/addnewBookChapter'); ?>">Add New Book Chapter</a></li>
                    </ul>
                  </li>
                  <!-- Books chapter navigation end -->

                  <!-- Books reviews navigation start -->
                  <li><a><i class="fa fa-book"></i>Manage Book Reviews <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('dashboard/manageBookReviews'); ?>">Manage Book Reviews</a></li>
                    </ul>
                  </li>
                  <!-- Books reviews navigation end -->

                  <!-- Books orders navigation start -->
                  <li><a><i class="fa fa-book"></i>Manage Book Orders <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('dashboard/manageBookOrders'); ?>">Manage Book Orders</a></li>
                    </ul>
                  </li>
                  <!-- Books orders navigation end -->

                   <li><a href="<?php echo base_url('dashboard/orderReports'); ?>"><i class="fa fa-book"></i>Order Reports</a></li>

                  <li><a><i class="fa fa-users"></i> Membership Users <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('dashboard/viewAllMembers'); ?>">View All Members</a></li>
                    </ul>
                  </li>

                 
                  <li><a><i class="fa fa-object-group"></i> Membership Plan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('dashboard/viewallPlans'); ?>">View All Plan</a></li>
                      <li><a href="<?php echo base_url('dashboard/addnewPlan'); ?>">Add New Plan</a></li>
                    </ul>
                  </li>
                  <li><a href="<?php echo base_url('dashboard/homeSlider'); ?>"><i class="fa fa-clone"></i>Home Slider Setting</a>
                  </li>
                  <li><a><i class="fa fa-object-group"></i> Video <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('dashboard/viewallVideos'); ?>">View All Video</a></li>
                      <li><a href="<?php echo base_url('dashboard/addnewVideo'); ?>">Add New Video</a></li>
                    </ul>
                  </li>
                  <li><a href="<?php echo base_url('dashboard/changePassoword'); ?>"><i class="fa fa-lock"></i>Change Your Password</a>
                  </li>
                </ul>
              </div>
              <?php /*!-- <div class="menu_section">
                <h3>Live On</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">E-commerce</a></li>
                      <li><a href="projects.html">Projects</a></li>
                      <li><a href="project_detail.html">Project Detail</a></li>
                      <li><a href="contacts.html">Contacts</a></li>
                      <li><a href="profile.html">Profile</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="page_403.html">403 Error</a></li>
                      <li><a href="page_404.html">404 Error</a></li>
                      <li><a href="page_500.html">500 Error</a></li>
                      <li><a href="plain_page.html">Plain Page</a></li>
                      <li><a href="login.html">Login Page</a></li>
                      <li><a href="pricing_tables.html">Pricing Tables</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="#level1_1">Level One</a>
                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>
                  </li>                  
                  <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
                </ul>
              </div> --*/ ?>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="SignOut" href="<?php echo site_url('signOut'); ?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <?php if(isset($is_logged_in_user_info)) { ?>
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url('uploads/users/'.$is_logged_in_user_info["u_image"]); ?>" alt="userImg"><?php echo $is_logged_in_user_info['u_firstname']. ' '.$is_logged_in_user_info['u_lastname']; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo site_url('dashboard/profile/'.$login_userid); ?>"> Profile</a></li>
                    <li><a href="<?php echo site_url('signOut'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
                <?php } ?>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="<?php echo base_url('themes/images/img.jpg'); ?>" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="<?php echo base_url('themes/images/img.jpg'); ?>" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="<?php echo base_url('themes/images/img.jpg'); ?>" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="<?php echo base_url('themes/images/img.jpg'); ?>" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->