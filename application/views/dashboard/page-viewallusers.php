 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Datatables -->
    <link href="<?php echo base_url('themes/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Veiw All Users</h3>
      </div>

      <!-- <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
      </div> -->
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>View All<small>Users</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <?php 
              $alert = $this->session->flashdata('alert');
              if($alert){
                  ?>
                  <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                    <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <?php
              }
              ?>
  
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <!-- <th>Role</th> -->
                  <th>Profile Image</th>
                  <th>Status</th>
                  <th>Active</th>
                  <th>Created Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <?php
              if($allUsersids) { ?>
              
              <tbody>
                <?php
                  foreach ($allUsersids as $uID) {
                    if($uID->uc_id != $this->session->userdata('is_logged_in_user')){

                    
                  $user = $this->user_Auth->getData('user_credentials', $w=array('uc_id' => $uID->uc_id),$se='',$sh='');
                   ?>
                <tr>
                  <td><?php echo $user[0]->uc_id; ?></td>
                  <td><?php echo (($user[0]->uc_firstname)?$user[0]->uc_firstname:"").' '.(($user[0]->uc_lastname)?$user[0]->uc_lastname:""); ?></td>
                  <td><?php echo (isset($user[0]->uc_email)?$user[0]->uc_email:''); ?> </td>
                  <td><?php echo (isset($user[0]->uc_mobile)?$user[0]->uc_mobile:''); ?> </td>
                  <!-- <td>
                    <?php
                        if(isset($user[0]->uc_role)){
                            $roleUser = $this->user_Auth->getData('user_roles',$wh=array("ur_id" => $user[0]->uc_role), $se='ur_id,ur_name',$so='');
                            echo $roleUser[0]->ur_name;
                        }
                    ?>
                  </td> -->
                  <td>
                      <div style="width:50px;">
                          <a href="<?php echo (($user[0]->uc_image)?base_url('uploads/users/'.$user[0]->uc_image):base_url('uploads/users/user.png')); ?>"><img src="<?php echo (($user[0]->uc_image)?base_url('uploads/users/'.$user[0]->uc_image):base_url('uploads/users/user.png')); ?>" class="img-responsive"></a>
                      </div>
                  </td>
                  <td><div style="text-align: center; font-size: 20px;"><?php echo (($user[0]->uc_status == '1')?"<i class='fa fa-check'></i>":"<i class='fa fa-times-circle'></i>"); ?> </div></td>
                  <td><div style="text-align: center; font-size: 20px;"><?php echo (($user[0]->uc_active == '1')?"<i class='fa fa-check'></i>":"<i class='fa fa-times-circle'></i>"); ?> </div></td>
                  <td><?php echo (($user[0]->uc_created > 0)?date('d-M-Y H:i:s',strtotime($user[0]->uc_created)):""); ?> </td>
                  <td>

                      
                      <div class="action-menu">
                          <a title="View Details" class="btn btn-info" href="<?php echo site_url('dashboard/userDetails/'.$uID->uc_id); ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                          <a title="Edit" class="btn btn-info" href="<?php echo site_url('dashboard/edituserDetails/'.$uID->uc_id); ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                          <a title="Delete" data-title="Goto twitter?" class="btn btn-info" href="javascript:void(0)" onclick="deleteUserDetail(<?php echo $uID->uc_id; ?>)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                      </div>
                      <div class="clearfix"></div>
                    
                    <?php //echo $author[0]->a_id; ?>
                  </td>
                </tr>
                <?php
                }
                  }
                ?>
              </tbody>
              <?php 
              }else{ ?>
              
              No More Data..
              <?php
              } ?>
            </table>
  
  
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
