 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Datatables -->
<link href="<?php echo base_url('themes/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
              <h3>Book Orders</h3>
            </div>
        </div>
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Book Orders</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                        <?php 
                          $alert = $this->session->flashdata('alert');
                          if($alert){
                              ?>
                              <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                                <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                        <?php
                        }
                        ?>

                          <div class="row">
                            <div class="form-group">
                                <?php 
                                    $attributes = array(
                                            'class' => 'control-label col-md-2 col-sm-2 col-xs-12',
                                    );

                                    echo form_label('Search Options:', 'book_review', $attributes);
                                ?>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                <?php 
                                   $options = $authors;

                                   $extra = 'id="order_author" class="form-control col-sm-3" ';
                                   echo form_dropdown('order_author', $options,$filter,$extra);

                                  
                                ?>
                                </div>
                                 <div class="col-md-3 col-sm-3 col-xs-12">
                                  <?php 
                                   $options = array('all'=>'Payment Status All','completed'=>'Completed','pending'=>'Pending');

                                   $extra = 'id="order_payment_status" class="form-control col-sm-3"';
                                   echo form_dropdown('order_payment_status', $options,$filter1,$extra);

                                ?>
                                </div>
                               <!-- <div class="col-md-3 col-sm-3 col-xs-12">
                                  <?php 
                                   $options = $authors;

                                   $extra = 'id="order_author" class="form-control col-sm-3"';
                                   echo form_dropdown('order_author', $options,'',$extra);

                                ?>
                                </div> -->
                            </div>
                        </div>  

                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <th>Book</th>
                                    <th>Book Price</th>
                                    <th>User</th>
                                    <th>Payment Method</th>
                                    <th>Payment Status</th>
                                    <th>Order Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php
                            if(!empty($allOrders)) { ?>
                      
                            <tbody>
                              <?php 
                                foreach ($allOrders as $order) {
                                  echo '<tr>';
                                  echo '<td>'.$order->id.'</td>';
                                  echo '<td>'.$order->b_title.'</td>';
                                  echo '<td>'.$order->b_sellingprice.'</td>';
                                  echo '<td>'.$order->uc_firstname.' '.$order->uc_lastname.'</td>';
                                  
                                  echo '<td>';
                                  if($order->payment_mode == 'cc'){
                                    echo 'Credit Card';
                                  }else if($order->payment_mode == 'evc'){
                                    echo 'EVC';
                                  }else if($order->payment_mode == 'mpesa'){
                                    echo 'Mpesa';
                                  }
                                  echo '</td>';
                                  echo '<td>'.$order->payment_status.'</td>';
                                  echo '<td>'.date('m-d-Y',strtotime($order->created_at)).'</td>';
                                  echo '<td>';
                                  ?>

                                    <div class="action-menu">
                                           <a title="View Details" class="btn btn-info" href="<?php echo site_url('dashboard/viewOrder/'.$order->id); ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                          
                                          <a title="Delete" data-title="Goto twitter?" class="btn btn-info" href="javascript:void(0)" onclick="deletebookReview(<?php echo $order->id; ?>)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
                                      </div>
                                      <div class="clearfix"></div>
                                  <?php
                                  echo '</td>';
                                  echo '</tr>';
                                }
                              ?>
                            </tbody>
                            <?php 
                            }else{ ?>
                            
                            No Orders..
                            <?php
                            } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>