<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Add New Video</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Add New Video Form<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                           
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                            <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="x_content">
                    <br />
                        <form name="addnewvideo_Form" id="addnewvideo_Form" action="<?php echo site_url('dashboard/addnewVideo'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left needs-validation" novalidate>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_language2">Select Language <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <select id="txt_language2" name="txt_language2" class="form-control col-md-7 col-xs-12">
                                                <option value=""> - Select The Language - </option>
                                                <option value="english">English</option>
                                                <option value="somali">Somali</option>
                                            </select>
                                            <?php echo form_error('txt_language2','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_category">Select Category <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <select id="txt_category" name="txt_category" class="form-control col-md-7 col-xs-12">
                                                <option value=""> - Select The Category - </option>
                                                <option value="Testimonial Videos">Testimonial Videos</option>
                                                <option value="Promotional Videos">Promotional Videos</option>
                                                <option value="Training and Tools Videos">Training and Tools Videos</option>
                                                <option value="Premium Training Videos">Premium Training Videos</option>
                                            </select>
                                            <?php echo form_error('txt_category','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_title">Video Title <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" id="txt_title" name="txt_title" value ="<?php echo set_value('txt_title'); ?>" placeholder="Video Title" class="form-control col-md-7 col-xs-12" max-length="200">
                                            <?php echo form_error('txt_title','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_videoid">Video Id <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" id="txt_videoid" name="txt_videoid" placeholder="xsdfsxsx12" class="form-control col-md-7 col-xs-12" max-length="180">
                                            <?php echo form_error('txt_videoid','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_video">Premium Video <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <label for="txt_premium">
                                              <input type="checkbox" id="txt_premium" name="txt_premium" class="form-control col-md-7 col-xs-12"/> Is Video Free For All
                                          </label>
                                          <?php echo form_error('txt_premium','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_video">Video Thumbnail <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="file" id="txt_image" name="txt_image" class="form-control col-md-7 col-xs-12" required accept="image/*" />
                                            <small>minimum image size 287x162 </small>
                                          <?php echo form_error('txt_premium','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_video">Description <span class="required">*</span>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <textarea name="txt_message" id="txtEditor"></textarea>
                                    <label id="txt_message-error" class="error" for="txt_message" style="display:none;">Description is required.</label>
                                </div>

                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary" type="button">Cancel</a> 
                              </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <!-- /page content -->
