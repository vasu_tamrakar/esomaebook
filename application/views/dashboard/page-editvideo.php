<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Edit Video</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Edit Video Form<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                            <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="x_content">
                    <br />
                    <?php 
                    if($videoDetails){
                    ?>
                    <div class="col-md-6">
                        <div style="width:300px;height:300px;"><img src="<?php echo (($videoDetails[0]->v_image)?base_url('uploads/videos/'.$videoDetails[0]->v_image):base_url('uploads/videos/Video-Icon-PNG-Image.png')); ?>" class="img-responsive"></div>
                    </div>
                    
                        
                    <?php
                    if($videoDetails[0]->v_videoid){
                        ?><div class="col-md-6">
                        <iframe src="https://player.vimeo.com/video/<?php echo $videoDetails[0]->v_videoid; ?>" width="300" height="300" allow="autoplay" frameborder="0" allow="; fullscreen" allowfullscreen></iframe>
                        </div>
                        <?php
                    }
                        
                    ?>
                        <form name="editvideo_Form" id="editvideo_Form" action="<?php echo site_url('dashboard/editVideo'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left needs-validation" novalidate>
                          <input type="hidden" id="txt_id" name="txt_id" value ="<?php echo $videoDetails[0]->v_id; ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_language2">Select Lanaguage <span class="required">*</span>
                                        </label>

                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <select id="txt_language2" name="txt_language2" class="form-control col-md-7 col-xs-12">
                                                <option value=""> - Select The Language - </option>
                                                <option value="english" <?php if($videoDetails[0]->v_language == 'english'){ echo 'selected'; } ?>>English</option>
                                                <option value="somali" <?php if($videoDetails[0]->v_language == 'somali'){ echo 'selected'; } ?>>Somali</option>
                                            </select>
                                            <?php echo form_error('txt_language2','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_category">Select Category <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <select id="txt_category" name="txt_category" class="form-control col-md-7 col-xs-12">
                                                <option value=""> - Select The Category - </option>
                                                <option value="Testimonial Videos" <?php if($videoDetails[0]->v_category == 'Testimonial Videos'){ echo 'selected'; } ?>>Testimonial Videos</option>
                                                <option value="Promotional Videos" <?php if($videoDetails[0]->v_category == 'Promotional Videos'){ echo 'selected'; } ?>>Promotional Videos</option>
                                                <option value="Training and Tools Videos" <?php if($videoDetails[0]->v_category == 'Training and Tools Videos'){ echo 'selected'; } ?>>Training and Tools Videos</option>
                                                <option value="Premium Training Videos" <?php if($videoDetails[0]->v_category == 'Premium Training Videos'){ echo 'selected'; } ?>>Premium Training Videos</option>
                                            </select>
                                            <?php echo form_error('txt_category','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_title">Video Title <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" id="txt_title" name="txt_title" value ="<?php echo $videoDetails[0]->v_title; ?>" placeholder="Video Title" class="form-control col-md-7 col-xs-12" max-length="200">
                                            <?php echo form_error('txt_title','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_videoid">Video Id <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" id="txt_videoid" name="txt_videoid" value ="<?php echo $videoDetails[0]->v_videoid; ?>" placeholder="xsdfsxsx12" class="form-control col-md-7 col-xs-12" max-length="180">
                                            <?php echo form_error('txt_videoid','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_premium1">Premium Video <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <label for="txt_premium">
                                              <input type="checkbox" id="txt_premium" name="txt_premium" value="1" class="form-control col-md-7 col-xs-12"/ <?php if($videoDetails[0]->v_premium == 'true'){ echo 'checked'; } ?>> Is Video Free For All
                                          </label>
                                          <?php echo form_error('txt_premium','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_video">Video Thumbnail <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="file" id="txt_image" name="txt_image" class="form-control col-md-7 col-xs-12" accept="image/*" />
                                            <small>minimum image size 287x162 </small>
                                          <?php echo form_error('txt_premium','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_status1">Video Status
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <label for="txt_status">
                                              <input type="checkbox" id="txt_status" name="txt_status" value="1" class="form-control col-md-7 col-xs-12"/ <?php if($videoDetails[0]->v_status == '1'){ echo 'checked'; } ?>>Video status
                                          </label>
                                          <?php echo form_error('txt_status','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_video">Description <span class="required">*</span>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <textarea name="txt_message" id="txtEditor"></textarea>
                                    <div id="textDescription" style="display:none;"><?php echo (isset($videoDetails[0]->v_description)?$videoDetails[0]->v_description:''); ?> </div>
                                    <label id="txt_message-error" class="error" for="txt_message" style="display:none;">Description is required.</label>
                                </div>

                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Update</button>
                                    <a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary" type="button">Cancel</a> 
                              </div>
                            </div>
                        </form>
                    <?php
                    }else{
                    ?>
                    <?php
                    }
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
