<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>View Video Details</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>View Video<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                          <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="x_content">
                        <br />
                        <div class="col-md-8 col-md-offset-2">
                        <?php if($videoDetails){ ?>
                            <div class="row bg-danger">
                                <div class="col-md-6 ">
                                    <label class="control-label">Video ID</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $videoDetails[0]->v_id; ?> </label>
                                </div>
                            </div>
                            <div class="row bg">
                                <div class="col-md-6">
                                    <label class="control-label">Video Language</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $videoDetails[0]->v_language; ?> </label>
                                </div>
                            </div>
                            <div class="row bg-danger">
                                <div class="col-md-6">
                                    <label class="control-label">Video Title</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $videoDetails[0]->v_title; ?></label>
                                </div>
                            </div>
                            <div class="row bg">
                                <div class="col-md-6">
                                    <label class="control-label">Video Category</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $videoDetails[0]->v_category; ?></label>
                                </div>
                            </div>
                            <div class="row bg-danger">
                                <div class="col-md-6">
                                    <label class="control-label">Video Premium</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $videoDetails[0]->v_premium; ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Video Description</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $videoDetails[0]->v_description; ?></label>
                                </div>
                            </div>
                            <div class="row bg-danger">
                                <div class="col-md-6">
                                    <label class="control-label">Video Category</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">
                                    </label>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Video Picture</label>
                                </div>
                                <div class="col-md-6">
                                    <div style="width: 100px;">
                                      <a target="_blank" href="<?php echo base_url('uploads/videos/'.$videoDetails[0]->v_image);?>" title=""><img src="<?php echo (($videoDetails[0]->v_image)?base_url('uploads/videos/'.$videoDetails[0]->v_image):base_url('uploads/videos/video.png')); ?>" class="img-responsive"></a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row bg-danger">
                                <div class="col-md-6">
                                    <label class="control-label">Video Status</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo (($videoDetails[0]->v_status)?"TRUE":"FALSE"); ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Video Created</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo (($videoDetails[0]->v_created > 0)?date('M d Y H:i:s',strtotime($videoDetails[0]->v_created)):""); ?></label>
                                </div>
                            </div>
                            <div class="row bg-danger">
                                <div class="col-md-6">
                                    <label class="control-label">Video Modified</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo (($videoDetails[0]->v_modified > 0)?date('M d Y H:i:s',strtotime($videoDetails[0]->v_modified)):""); ?> </label>
                                </div>
                            </div>
                            <?php if($videoDetails[0]->v_videoid){ ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Video</label>
                                </div>
                                <div class="col-md-6">
                                    <?php echo (($videoDetails[0]->v_videoid)?$videoDetails[0]->v_videoid:""); ?>
                                    <iframe src="https://player.vimeo.com/video/<?php echo (($videoDetails[0]->v_videoid > 0)?$videoDetails[0]->v_videoid:""); ?>" width="100%" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="row">
                                  <div class="col-md-12">
                                      <center>
                                          <a href="<?php echo site_url('dashboard/editvideoDetails/'.$videoDetails[0]->v_id); ?>" class="btn btn-success">Edit</a>
                                          <a href="<?php echo site_url('dashboard/viewallVideos'); ?>" class="btn btn-warning">Back</a>
                                          <a href="<?php echo site_url('dashboard/addnewVideo'); ?>" class="btn btn-primary">Add New Video</a>
                                          <button type="button" onclick="deleteVideoDetail(<?php echo $videoDetails[0]->v_id; ?>)" class="btn btn-info">Delete</button>
                                      </center>
                                  </div>
                            </div>
                    
                        <?php
                        }else{
                        ?><label class="control-label"> Invalid Video.</label><?php
                        } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->