<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Author extends CI_Controller {
	protected $selectedLanguage;
	public function __construct (){
		parent :: __construct();
		$this->load->model('user_Auth');
		$this->load->model('book_model');
		if($this->session->userdata('get_language')){
			$this->selectedLanguage = $this->session->userdata('get_language');
		}else{
			$this->selectedLanguage ='english';
		}

	}
	public function index()
	{
		$data["title"] = 'Author Page | '.SITENAME;
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-archivecategory',$data);
		$this->load->view('front/common/footer',$data);
	}
	public function authorBooks($id="")
	{
		$data["title"] = 'Author Page | '.SITENAME;
		if($id == ""){
			redirect(base_url());exit;
		}
		$authorData = $this->user_Auth->getData('user_credentials',$W=array('uc_id' => $id));
		$authorUserData = $this->user_Auth->getData('authors',$W=array('a_fK_of_uc_id' => $id));

		$data['authorDetails'] = array();
		if($authorData){
			$data['authorDetails'] =$authorData[0];
			$data['authorUserDetails'] = $authorUserData[0];
		}

		$authorBooks = $this->book_model->getBooksByAuthorID($id);
		$data['authorBooks'] = array();
		if($authorBooks){
			$data['authorBooks'] = $authorBooks;
		}
/*
		echo '<pre>';
		print_r($authorData);
		print_r($authorBooks);
		exit;*/

		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-authorBooks',$data);
		$this->load->view('front/common/footer',$data);
	}
	public function categories($catname, $cat){
		
		$catname =urldecode($catname);
		$cat =urldecode($cat);
		$data["title"] = $cat.' Book Page | '.SITENAME;
		$data["categoryName"] = $cat;
		$data["bookName"] = $cat;
		
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-singlebook',$data);
		$this->load->view('front/common/footer',$data);
	}
	
}
