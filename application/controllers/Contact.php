<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if($this->session->userdata('is_logged_in_user')){
			$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');	
		}
		if($this->session->userdata('is_logged_in_user_info')){
			$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');	
		}
		$this->load->model('user_Auth');
		
		
	}
	public function index()
	{
		$data["title"] = 'Contact Page | '.SITENAME;
		$post = $this->input->post();


		if($post){
			//echo '<pre>';
			//print_r($post);exit;
			$this->form_validation->set_rules('txt_firstname','First Name','required');
			 $this->form_validation->set_rules('txt_lastname','Last Name','required');
			 $this->form_validation->set_rules('txt_email','Email','required');
			$this->form_validation->set_rules('txt_subject','Subject','required');
			$this->form_validation->set_rules('txt_messsage','Message','required');

			
			if($this->form_validation->run() === TRUE){
				//exit('yes');
				$formData["first_name"] = $post["txt_firstname"];
				$formData["last_name"] = $post["txt_lastname"];
				$formData["email"] = $post["txt_email"];
				$formData["subject"] = $post["txt_subject"];
				$formData["message"] = $post["txt_messsage"];
				$formData["created_at"] = date('Y-m-d');

				$instID =  $this->user_Auth->insert('contact_us',$formData);

				$sendMail = "";
				if($instID){
					$sendMail =	$this->sendNotification($formData);
					$this->session->set_flashdata('alert','success');
					if(!$sendMail)
					$this->session->set_flashdata('message', 'Thank you for contacting us. Something wrong with email sending.');
					else
					$this->session->set_flashdata('message', 'Thank you for contacting us.');
						
					redirect('contact'); exit();
				}else{
					$this->session->set_flashdata('alert', 'error');
					$this->session->set_flashdata('message', 'Something wrong while connecting to us!..');
					redirect('contact'); exit();
				}
				
			}else{
				$this->load->view('front/common/header',$data);
				$this->load->view('front/page-contact',$data);
				$this->load->view('front/common/footer',$data);	
			}
		}else{
			$this->load->view('front/common/header',$data);
			$this->load->view('front/page-contact',$data);
			$this->load->view('front/common/footer',$data);
		}
	}

	public function sendNotification($formData){

		$html = '<!DOCTYPE html>
			<html lang="en">
			    <head>
			        <title>'.SITENAME.'</title>
			        <meta charset="utf-8">
			        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
			        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
			        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
			        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
			        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
			        <style>
			        </style>
			    </head>
			    <body>
			        <div class="container">
			            <div class="row">
			                <div style="background: #54bfe3;width: 100%;">
			                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
			                </div>
			                <div style="margin: 30px;display: block; width:100%">
			                    <h3>Hello Admin,</h3>
			                    <p>Contact request recieved from:</p>';
	                        	$html .= '<br>
	                        	<strong>Name :</strong> '.$formData['first_name'].' '.$formData['last_name'].'<br>
	                        	<strong>Email :</strong> '.$formData['email'].'<br>
	                        	<strong>Subject :</strong> '.$formData['subject'].'<br>
	                        	<strong>Message :</strong> '.$formData['message'].'<br>
	                        	</p>
			                </div>
			                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
			                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
			                </div>
			            </div>
			        </div>
			    </body>
			</html>';

		$config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		$this->email->to('paresh3779@gmail.com', SITENAME);
		$this->email->from('pareshnagar87@gmail.com',SITENAME);
		$this->email->subject(SITENAME ." Contact Form");
		$this->email->message($html);
		$sendMail = $this->email->send();
		if($sendMail){
            return 1;
        }else{
            echo $this->email->print_debugger();
        }
        
	}
}