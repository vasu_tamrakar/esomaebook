<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if($this->session->userdata('is_logged_in_user')){
			$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');	
		}
		if($this->session->userdata('is_logged_in_user_info')){
			$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');	
		}
		$this->load->model('user_Auth');
		$this->load->model('book_model');
		
		
	}
	public function index()
	{
		$data["title"] = 'Home | '.SITENAME;
		$data["frontSlider"] = $this->user_Auth->getData('sliders',$W=array('s_status' => '1'), $se='',$sh='s_order ASC');

		$data["totalBooks"] = $this->user_Auth->countAllwhere('books',$W=array('b_status' => '1'));
		
		$data["somaliBooks"] = $this->user_Auth->countAllwhere('books',$W=array('b_language' => 'somali','b_status' => '1'));

		$data["somaliAuthors"] = $this->user_Auth->countAllwhere('user_credentials',$W=array('uc_role' => '4','uc_status' => '1','uc_active' => '1'));

		$data["totalDownload"] = $this->user_Auth->get_Totaldownloadcount();

		$data["frontcategory"] = $this->user_Auth->getData('categories', $w = array('c_status' => '1'), $s='', $sh = 'c_id ASC');

		$data["featuredBooksID"] = $this->user_Auth->getData('books', $w = array('b_status' => '1'), $s='b_id', $sh = 'b_id ASC');

		$data["recentlyreleaseBooksID"] = $this->user_Auth->getData('books', $w = array('b_status' => '1'), $s='b_id', $sh = 'b_published DESC');

		$mostpopularBooksIDs = $this->user_Auth->getData('book_orders', $w = '', $se= 'book_id as b_id, count(`book_id`) c', $sh = 'c desc', $grp ='book_id');



		$data["mostpopularBooksID"] = array();
		if($mostpopularBooksIDs){
			foreach ($mostpopularBooksIDs as $mostPopularBook) {
				$popularBookDetails = $this->book_model->getBookDetails($mostPopularBook->b_id);
				$mostPopularBook->bookDetails = $popularBookDetails[0];
			}
		}
		
		$data["mostpopularBooksID"] = $mostpopularBooksIDs;
	
		$data["popularAuthorID"] = $this->user_Auth->mostpupularAuthorsid();
		if($data["popularAuthorID"]){
			foreach ($data["popularAuthorID"] as $popularAuthor) {
				$authorBooks = $this->book_model->getBooksByAuthorID($popularAuthor->uc_id);
				$popularAuthor->totalBooks = count($authorBooks);
			}
		}
		
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-front',$data);
		$this->load->view('front/common/footer',$data);
	}
	public function emailSubscribe(){
		$post = $this->input->post();
		if($post){
			$emailAddress = $post["edata"];
			if(!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^",$emailAddress)){
				echo json_encode(array('status' => 100, 'message' => 'Please enter Valid email adddress!.'));
			}else{
				//$instID = $this->user_Auth->emailSubscribe($emailAddress);
				$t = 1;
				if($t){
				// if($instID['status'] == 200){

					$html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block;">
					                    <h6>Hi '.$emailAddress.',</h6>
						                <p>Thansk for email subscribe.</p>
						                <small>For unsubscribe email click <a href="'.site_url('subscribe?&D=ffsf&ssdem='.base64_encode($emailAddress)).'&ll=xzxdfasdf">here</a> </small>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
			        $config['protocol'] 	= 'smtp';
			        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
			        $config['smtp_port'] 	= '465';
			        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
			        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
			        $config['mailtype'] 	= 'html';
			        $config['charset'] 		= 'utf-8';
			        $config['newline'] 		= "\r\n";
			        $config['wordwrap'] 	= TRUE;
					$this->email->initialize($config);
					$this->email->set_mailtype("html");
					$this->email->set_newline("\r\n");


					$subject = SITENAME.' Email Subscriptions';
					$this->email->to($emailAddress);
			        $this->email->from('pareshnagar87@gmail.com',SITENAME);
			        $this->email->subject($subject);
			        $this->email->message($html);
			        $sended =$this->email->send();
			        if($sended){
			        	echo json_encode(array('status' => 200, 'message' => $instID['message']));
			        }else{
			    		echo json_encode(array('status' => 200, 'message' => $instID['message']." But email sending failed!."));
			    	}



					
				}else{
					echo json_encode(array('status' => 100, 'message' => $instID['message']));		
				}
			}
		}else{
			echo json_encode(array('status' => 100, 'message' => 'In-valid Input!.'));
		}
		die();
	}

	public function unsubscribeMail(){
		$data['title'] = 'Unsubscribe Page | '.SITENAME; 
		$get = $this->input->get();
		if($get["ssdem"]){
			$email = base64_decode($get["ssdem"]);
			$formData["es_status"] = '0';
			$formData["es_modified"] = date('Y-m-d H:i:s');
			$updated = $this->user_Auth->update('emailsubscriber', $formData, array("es_email" => $email));
			if($updated){
				$this->session->set_flashdata('alert', 'success');
				$this->session->set_flashdata('message', $email.' address successfully unsubscribed!.');
				redirect('thanks'); exit();
			}else{
				$this->session->set_flashdata('alert', 'error');
				$this->session->set_flashdata('message', 'Process failed!.');
				redirect('thanks'); exit();
			}
		}else{
			$this->session->set_flashdata('alert', 'error');
			$this->session->set_flashdata('message', 'Process failed!.');
			redirect('thanks'); exit();
		}
	}
}
