<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->library('upload');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
	}
	public function index()
	{
		$data["title"] = 'Add New Plan Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
		}else{
			$post = $this->input->post();
			if($post){
				// [txt_name]
				// [txt_price]
				// [txt_valididty]
				// [txt_descriptions] 
				$this->form_validation->set_rules('txt_name','Plan Name','required');
				$this->form_validation->set_rules('txt_price','Plan Price','required');
				$this->form_validation->set_rules('txt_valididty','Plan Validity','required');
				$this->form_validation->set_rules('txt_descriptions','Plan Descriptions','required');
				if($this->form_validation->run() === TRUE){
					

					$formData["mp_name"] = $post["txt_name"];
					$formData["mp_price"] = $post["txt_price"];
					$formData["mp_validity"] = $post["txt_valididty"];
					$formData["mp_descriptions"] = $post["txt_descriptions"];
					$formData["mp_created"] = date("Y-m-d H:i:s");
					$formData["mp_status"] = '1';
					
						
					$instID =  $this->user_Auth->insert('membershipplan',$formData);
					if($instID){
						$this->session->set_flashdata('alert','success');
						$this->session->set_flashdata('message','Plan Successfully Added.');
						redirect('dashboard/addnewPlan'); exit();	
					}else{
						$this->session->set_flashdata('alert','success');
						$this->session->set_flashdata('message','Plan added failed.');
						redirect('dashboard/addnewPlan'); exit();
					}
					
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-addnewplan',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-addnewplan',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
			 
		}
		
	}
	

    /*----------------------------------------------
		viewallPlans()
    ----------------------------------------------*/
    public function viewallPlans(){
    	$data["title"] = 'View All Plans Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		$data['allplanids'] = $this->user_Auth->getData('membershipplan',$w='' ,$se='mp_id', $short='mp_id DESC');
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-viewallplans',$data);
		$this->load->view('dashboard/common/footer',$data);
    }


	/*----------------------------------------------
		userDetails($aid)
    ----------------------------------------------*/
    public function userDetails($uid){
    	$data["title"] = 'View User Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($uid){
			$data['userDetails'] = $this->user_Auth->getData('user_credentials',$w=array('uc_id' => $uid) ,$se='', $short='');
	    	$this->load->view('dashboard/common/header',$data);
			$this->load->view('dashboard/page-viewusersdetails',$data);
			$this->load->view('dashboard/common/footer',$data);
		}
		
    }

    /*----------------------------------------------
		editplanDetails($aid)
    ----------------------------------------------*/
    public function editplanDetails($pid){
    	$data["title"] = 'Edit Plan Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($pid){
			$data['planDetails'] = $this->user_Auth->getData('membershipplan',$w=array('mp_id' => $pid) ,$se='', $short='');
			$post = $this->input->post();
			if($post){
				$this->form_validation->set_rules('txt_name','Plan Name','required');
				$this->form_validation->set_rules('txt_price','Plan Price','required');
				$this->form_validation->set_rules('txt_valididty','Plan Validity','required');
				$this->form_validation->set_rules('txt_descriptions','Plan Descriptions','required');
				if($this->form_validation->run() === TRUE){

					$formData["mp_name"] = $post["txt_name"];
					$formData["mp_price"] = $post["txt_price"];
					$formData["mp_validity"] = $post["txt_valididty"];
					$formData["mp_descriptions"] = $post["txt_descriptions"];
					if($post["txt_created"]){
						$formData["mp_created"] = date('Y-m-d H:i:s',strtotime($post["txt_created"]));	
					}
					$formData["mp_status"] = (($post["txt_status"] == 1)?'1':'0');
					$formData['mp_modified'] = date('Y-m-d H:i:s');
					$resultUpdate = $this->user_Auth->update('membershipplan', $formData, $filter=array('mp_id' => $pid) );
					if($resultUpdate){
						$this->session->set_flashdata('alert', "success");
						$this->session->set_flashdata('message', 'Plan successfully updated..');
						redirect('dashboard/editplanDetails/'.$pid);
						exit();
					}else{
						$this->session->set_flashdata('alert' ,'error');
						$this->session->set_flashdata('message' ,'Update Process Failed.');
						$this->load->view('dashboard/common/header', $data);
						$this->load->view('dashboard/page-editplan', $data);
						$this->load->view('dashboard/common/footer', $data);
						exit();
					}
			
				}else{
					$this->load->view('dashboard/common/header', $data);
					$this->load->view('dashboard/page-editplan', $data);
					$this->load->view('dashboard/common/footer', $data);
				}
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-editplan',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
	    	
		}
		
    }
    /*-----------------------------------------
		deleteplanDetail()
    -----------------------------------------*/
    public function deleteplanDetail(){
    	$post = $this->input->post();
    	if($post){
    		$pId = $post["delid"];
    		$delete = $this->user_Auth->delete('membershipplan', array('mp_id' => $pId));
    		if($delete){
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/viewallPlans'), 'message' => 'Plan Delete Successfully Done.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Process Process Failed.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid operation.'));
    	}
    }
    
}
