<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->model('membership_model');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denied!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}

	}
	public function index()
	{
		$data["title"] = 'Manage All Members | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;

		$data['allMembers'] = $this->membership_model->getMembers();
		
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-viewallMembers',$data);
		$this->load->view('dashboard/common/footer',$data);
		
	}

	public function viewMember($id){
    	$data["title"] = 'Member Details | '.SITENAME;
    	$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		
		if($id){
			$data['member'] = $this->membership_model->getMembers($id);
			$data['member'] = $data['member'][0];
	    	$this->load->view('dashboard/common/header',$data);
			$this->load->view('dashboard/page-viewMemberDetails',$data);
			$this->load->view('dashboard/common/footer',$data);
		}else{
			redirect('dashboard/viewAllMembers'); exit();
		}
		
    }

	

    /*-----------------------------------------
		approvebookReview()
    -----------------------------------------*/
    public function paymentStatusChange(){
    	$post = $this->input->post();
    	if($post){
     		$id = $post["id"];

			$formData["pd_status"] = $post['status'];
			$planDetails = $this->user_Auth->getData('paymentdetails',$w=array('pd_id'=>$id),$se='pd_planid');
			$planID = $planDetails[0]->pd_planid;

            $datetimeNow = date('Y-m-d H:i:s');
            $formData["pd_approvedate"] = $datetimeNow;
            
            if($post['status'] == "completed"){
            	if($planID == 1){
	            	$next_month = date("Y-m-d H:i:s", strtotime("$datetimeNow +1 month"));
	            	$formData["pd_expiredate"] = $next_month;
	            }else if($planID == 2){
	            	$new_date = strtotime('+ 1 year', strtotime($datetimeNow));
					$oneyear = date('Y-m-d H:i:s', $new_date);
	            	$formData["pd_expiredate"] = $oneyear;
	            }else{
	            	$new_date = strtotime('+ 3 year', strtotime($datetimeNow));
					$oneyear = date('Y-m-d H:i:s', $new_date);
	            	$formData["pd_expiredate"] = $oneyear;
	            }	
            }else{
            	$formData["pd_approvedate"]='';
            	$formData["pd_expiredate"]='';
            }
            


            

			$updated = $this->user_Auth->update('paymentdetails',$formData,array('pd_id'=>$id));

    		if($updated){
    			$this->session->set_flashdata('alert' ,'success');
    			$this->session->set_flashdata('message' ,'Member Approved Successfully.');
    			if($post['status'] == "completed"){

    				$member = $this->membership_model->getMembers($id);
    				$this->user_Auth->update("user_credentials", array("uc_status" => '1',"uc_active" => '1'),array("uc_id" => $member[0]->uc_id));

    				$this->sendApprovalNotification($member);

    				echo json_encode(array('status' => 'success', 'data' => site_url('dashboard/viewAllMembers'), 'message' => 'Member Approved Successfully.'));
    		}else if($post['status'] == "pending"){
    			$this->session->set_flashdata('alert' ,'success');
    			$this->session->set_flashdata('message' ,'Member Declined Successfully.');
    			echo json_encode(array('status' => 'success', 'data' => site_url('dashboard/viewAllMembers'), 'message' => 'Member Declined Successfully.'));
    		}
				
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Something Wrong with Member Updation.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid request.'));
    	}
    }

    public function sendApprovalNotification($user = array()){
        $html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block;">
					                    <h3>Hi '.$user[0]->uc_firstname.' '.$user[0]->uc_lastname.',</h3>
					                    
                                    	<p>Your account is aproved by admin. You can login click  <a href="'.site_url('signIn').'">here</a></p>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

        
        $this->email->to($user[0]->uc_email);
        $this->email->from('pareshnagar87@gmail.com',SITENAME);

        $this->email->subject('Account Approval Notification');
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
            return 1;
        }else{
            $this->email->print_debugger();
        }
    }
    
    
}