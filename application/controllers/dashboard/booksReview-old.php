<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BooksReview extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->model('BookReview_model');
		$this->load->model('Book_model');
		$this->load->library('upload');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denied!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}

	}
	public function index($filter = "")
	{
		$data["title"] = 'Manage Book Reviews | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;

		$books = $this->user_Auth->getData('books',array() ,$se='', $short='');
		$booksArray = array();
		$booksArray[''] = "All Books";
		foreach($books as $book){
			$booksArray[$book->b_id] = $book->b_title;
		}
		$data['books'] = $booksArray;
		
		$data['filtered'] = "";
		$data['bookReviews'] = array(); 
		if($filter != ''){
			$data['filtered'] = $filter;
			$bookReviews = $this->BookReview_model->getReviewsByBookID($filter);
		}else{
			$bookReviews = $this->BookReview_model->getBookReviews();
		}
		if($bookReviews){
			$data['bookReviews'] = $bookReviews;
		}
		
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-viewallbookreviews',$data);
		$this->load->view('dashboard/common/footer',$data);
		
	}

	/*----------------------------------------------
		reviewDetails($aid)
    ----------------------------------------------*/
    public function reviewDetails($id){
    	$data["title"] = 'Book Review Details | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($id){
			$data['bookReviews'] = array(); 
			$bookReviews = $this->BookReview_model->getBookReviews($id);
			if($bookReviews){
				$data['bookReviews'] = $bookReviews;
			}

	    	$this->load->view('dashboard/common/header',$data);
			$this->load->view('dashboard/page-viewReviewDetails',$data);
			$this->load->view('dashboard/common/footer',$data);
		}else{
			redirect('dashboard/manageBookReviews'); exit();
		}
		
    }
	

    /*-----------------------------------------
		deletebookChapter()
    -----------------------------------------*/
    public function deletebookReview(){
    	$post = $this->input->post();
    	if($post){
    		$id = $post["delid"];
    		$delete = $this->user_Auth->delete('book_reviews', array('id' => $id));
    		if($delete){
    			$this->session->set_flashdata('alert' ,'success');
				$this->session->set_flashdata('message' ,'Book Review Deleted Successfully.');
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/manageBookReviews'), 'message' => 'Book Review Deleted Successfully.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Something Wrong with Deletion.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid request.'));
    	}
    }

    /*-----------------------------------------
		approvebookReview()
    -----------------------------------------*/
    public function approvebookReview(){
    	$post = $this->input->post();
    	if($post){
    		$id = $post["id"];

			$formData["approved"] = '1';

			$updated = $this->user_Auth->update('book_reviews',$formData,array('id'=>$id));

    		if($updated){
    			$this->session->set_flashdata('alert' ,'success');
				$this->session->set_flashdata('message' ,'Book Review Approved Successfully.');
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/manageBookReviews'), 'message' => 'Book Review Approved Successfully.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Something Wrong with Book Approval.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid request.'));
    	}
    }
    
    
}