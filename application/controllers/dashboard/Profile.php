<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
	}

	/*-------------------------------------------
		
	-------------------------------------------*/
	public function index($uid)
	{

		$data['title'] = 'Profile Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($uid == $this->is_logged_in_user){
			$post = $this->input->post();
			if($post){
				$this->form_validation->set_rules('txt_firstname','First Name','required');
				$this->form_validation->set_rules('txt_lastname','Last Name','required');
				$this->form_validation->set_rules('txt_email','Email','required');
				$this->form_validation->set_rules('txt_role','User Role','required');

				if($this->form_validation->run() ===TRUE){
					$form_data['uc_firstname'] = $post["txt_firstname"];
					$form_data['uc_lastname'] = $post["txt_lastname"];
					$form_data['uc_email'] = $post["txt_email"];
					$form_data['uc_mobile'] = $post["txt_mobile"];
					if($post["txt_mobile"]){
						$form_data['uc_mobile'] = $post["txt_mobile"];	
					}
					
					if($post["txt_address"]){
						$form_data['uc_address'] = $post["txt_address"];
					}
					$form_data['uc_role'] = $post["txt_role"];
					$form_data['uc_modified'] = date('Y-m-d H:i:s');
					$result = $this->user_Auth->exist_emailMobile($post["txt_email"], $post["txt_mobile"], $this->is_logged_in_user);
					if($result["status"] == 'success'){

						if($_FILES['txt_img']['name']){
							$Imgres = $this->do_upload('txt_img');
							
							if(isset($Imgres['upload_data'])){
								$form_data['uc_image'] = $Imgres['upload_data']['file_name'];
								
							}else{
								$this->session->set_flashdata('alert' ,'error');
								$this->session->set_flashdata('message' ,$Imgres['error']);
								$this->load->view('dashboard/common/header', $data);
								$this->load->view('dashboard/page-profile', $data);
								$this->load->view('dashboard/common/footer', $data);
								exit();
							}
						}
						$resultUpdate = $this->user_Auth->update('user_credentials', $form_data, $filter=array('uc_id' => $this->is_logged_in_user) );
						if($resultUpdate){
							$user = $this->user_Auth->getData('user_credentials', $where = array('uc_id' => $this->is_logged_in_user),$se='',$s='');

							$udata["u_id"] = $user[0]->uc_id;
							$udata["u_firstname"] = $user[0]->uc_firstname;
							$udata["u_lastname"] = $user[0]->uc_lastname;
							$udata["u_email"] = $user[0]->uc_email;
							$udata["u_mobile"] = $user[0]->uc_mobile;
							$udata["u_image"] = (($user[0]->uc_image)?$user[0]->uc_image:'user.png');
							$udata["u_role"] = $user[0]->uc_role;
							$udata["u_status"] = $user[0]->uc_status;
							$udata["u_created"] = $user[0]->uc_created;
							$udata["u_modified"] = (($user[0]->uc_modified >0)?$user[0]->uc_modified:'');
							$this->session->set_userdata('is_logged_in_user', $user[0]->uc_id);
							$this->session->set_userdata('is_logged_in_user_info', $udata);
							$this->session->set_flashdata('alert', "success");
							$this->session->set_flashdata('message', 'Profile successfully updated..');
							redirect('dashboard/profile/'.$user[0]->uc_id);
							exit();
						}else{
							$this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,'Update Process Failed.');
							$this->load->view('dashboard/common/header', $data);
							$this->load->view('dashboard/page-profile', $data);
							$this->load->view('dashboard/common/footer', $data);
							exit();
						}
						

					}else{
						$this->session->set_flashdata('alert' ,'error');
						$this->session->set_flashdata('message' ,$result["message"]);
						$this->load->view('dashboard/common/header', $data);
						$this->load->view('dashboard/page-profile', $data);
						$this->load->view('dashboard/common/footer', $data);
					}

				}else{
					$this->load->view('dashboard/common/header', $data);
					$this->load->view('dashboard/page-profile', $data);
					$this->load->view('dashboard/common/footer', $data);
				}
			}else{
				$this->load->view('dashboard/common/header', $data);
				$this->load->view('dashboard/page-profile', $data);
				$this->load->view('dashboard/common/footer', $data);
			}
		}else{
			$this->session->sess_destroy();
			$this->session->unset_userdata('is_logged_in_user');
			$this->session->unset_userdata('is_logged_in_user_info');
			$this->session->set_flashdata('alert' ,'error');
			$this->session->set_flashdata('message' ,'Access Denide please singin again.');
			redirect('signIn'); exit();
		}
		
	}


	/*-------------------------------------------
		do_upload(name,folder);
	-------------------------------------------*/

	public function do_upload($fileName)
    {
        $config['upload_path']          = './uploads/users/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2048;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($fileName))
        {
                return array('error' => $this->upload->display_errors());

        }
        else
        {
                return array('upload_data' => $this->upload->data());

        }
    }

    /*-------------------------------------------
		changePassoword();
	-------------------------------------------*/

	public function changePassoword()
    {
        $data['title'] = 'Change Password Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		$this->load->view('dashboard/common/header', $data);
		$this->load->view('dashboard/page-changepassword', $data);
		$this->load->view('dashboard/common/footer', $data);
    }

    /*-------------------------------------------
		changePasswordbyajax();
	-------------------------------------------*/

	public function changePasswordbyajax()
    {
       	$post = $this->input->post();
       	if($post['txt_newpass']){
       		
       		$pass = $post['txt_newpass'];
       		if($post['txt_newpass'] !== $post['txt_cnfnewpass']){
       			echo json_encode(array('status' => 300, 'message' => 'Please enter the same value again.'));	
       			die;
       		}
       		$dataf =array('uc_password' => md5($pass),'uc_modified' => date('Y-m-d H:i:s'));
       		$res =  $this->user_Auth->update('user_credentials',$dataf,$w=array('uc_id' => $this->is_logged_in_user));
       		
       		if($res){
       			echo json_encode(array('status' => 200, 'message' => 'Successfully Done.'));
       		}else{
       			echo json_encode(array('status' => 100, 'message' => 'Update Process Failed.'));
       		}		
       	}else{
       		echo json_encode(array('status' => 100, 'message' => 'In-valid Method!.'));
       	}
       	die();
    }
}
