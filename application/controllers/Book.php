<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book extends CI_Controller {

	public function __construct (){
		parent :: __construct();
		$this->load->model('user_Auth');
		$this->load->model('book_model');
	}
	/*------------------------------------
		Dashboard index 
	------------------------------------*/
	public function index()
	{
		$data['title'] = 'Books |'.SITENAME;
		
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-singlebook', $data);
		$this->load->view('front/common/footer', $data);
	}

	public function bookDetails($id = ''){
		$data['title'] = 'Book Details | '.SITENAME;

		$data['bookReview'] = array();
		if($this->session->userdata('is_logged_in_user')){
			$data['login_userid'] = $this->session->userdata('is_logged_in_user');
			$data['bookReview'] = $this->book_model->getBookReviews($id,$data['login_userid']);
		}else{
			$data['login_userid'] = "";
		}

		$data['bookReviews'] = array();
		$bookReviews = $this->book_model->getBookReviews($id);
		if($bookReviews){
			$data['bookReviews'] = $bookReviews;
		}

		$totalRating = $this->book_model->getTotalRating($id);
		$data['totalRating'] = $totalRating[0]->totalRating;

		$avgRating = $this->book_model->getAvgRating($id);
		$data['avgRating'] = $avgRating[0]->rating;

		$book = $this->book_model->getBookDetails($id);

		if($book[0]->b_fk_of_aid != "" && $book[0]->b_fk_of_aid!='0'){
			$booksOfAuthor = $this->book_model->getBooksByAuthorID($book[0]->b_fk_of_aid);
			$bookChapters = $this->book_model->getBookChapters($id);
		
			$booksOfSameCategory = $this->book_model->getBooksByCategoryID($book[0]->b_category);

			$data['book'] = $book[0];

			$data['bookChapters'] = array();
			if($bookChapters){
				$data['bookChapters'] = $bookChapters;
			}

			$data['totalBooksOfAuthor'] = 0;
			$data['booksOfAuthor'] = array();
			if($booksOfAuthor){
				$data['booksOfAuthor'] = $booksOfAuthor;
				$data['totalBooksOfAuthor'] = count($booksOfAuthor);
			}

			$data['booksOfSameCategory'] = array();
			if($booksOfSameCategory){
				$data['booksOfSameCategory'] = $booksOfSameCategory;
			} 

			$bookPurchaseCount = $this->book_model->getDownloadInfo($id);
			$data['downloads'] = $bookPurchaseCount;
			$this->load->view('front/common/header', $data);
			$this->load->view('front/page-singlebook', $data);
			$this->load->view('front/common/footer', $data);
		}else{
			$bookChapters = $this->book_model->getBookChapters($id);
			$booksOfSameCategory = $this->book_model->getBooksByCategoryID($book[0]->b_category);

			$data['book'] = $book[0];

			$data['bookChapters'] = array();
			if($bookChapters){
				$data['bookChapters'] = $bookChapters;
			}

			$data['booksOfSameCategory'] = array();
			if($booksOfSameCategory){
				$data['booksOfSameCategory'] = $booksOfSameCategory;
			} 
			$this->load->view('front/common/header', $data);
			$this->load->view('front/page-singleMembBook', $data);
			$this->load->view('front/common/footer', $data);
		}
		
	}

	public function purchaseBook(){

		$data['title'] = 'Checkout | '.SITENAME;

		$post = $this->input->post();
		/*if(empty($post)){
			redirect(base_url());  exit();
		}*/
		$bookId = "";
		if(!empty($post)){
			$bookId = $post['book_id'];
		}

		$sessionBookId = $this->session->userdata('purchase_book_id');
		/*echo $bookId;exit;*/
		if($bookId){
		
			if(($sessionBookId != $bookId)){
				$this->session->set_userdata('purchase_book_id', $bookId);
			}
		}else{
		
			if($this->session->userdata('purchase_book_id')){
				$bookId = $this->session->userdata('purchase_book_id');
			}else{
				/*echo $bookId;exit;*/
				redirect(base_url('somaliAuthors')); exit();
			}
		}
	

		$data['userData'] = array();
		if($this->session->userdata('is_logged_in_user')){
			$data['userData'] = $this->session->userdata('is_logged_in_user_info');
		}

		$data['bookData'] = array();
		if($bookId){
			$bookData = $this->user_Auth->getData('books',$where=array('b_id' =>$bookId));
			if($bookData){
				$data['bookData'] = $bookData[0];
			}
		}

		if(isset($post['paynow'])){
			/*echo '<pre>';
			print_r($post);exit;*/
			$this->form_validation->set_rules('txt_firstname','First Name','required');
			$this->form_validation->set_rules('txt_lastname','Last Name','required');
			$this->form_validation->set_rules('txt_email','Email Address','required|valid_email');
			//if(isset($post["txt_phone"]) && ($post["txt_phone"])){
				$this->form_validation->set_rules('txt_phone','Mobile','required');
			//}
			if($post['txt_paymethod'] == 'cc'){
				$this->form_validation->set_rules('txt_paymethod','Payment Method','required');
				$this->form_validation->set_rules('txt_holdername','Card Holder Name','required');
				$this->form_validation->set_rules('txt_cardnumber','Card Number','required');
				$this->form_validation->set_rules('txt_cvv','CVV','required');
				$this->form_validation->set_rules('txt_expmonth','Expiration Month','required');
				$this->form_validation->set_rules('txt_expyear','Expiration Year','required');	
			}
			
			$this->form_validation->set_rules('txt_agree','I agree to condition','required');

			if($this->form_validation->run() === TRUE){
				$resExistemail =  $this->user_Auth->existEmail($post["txt_email"]);
				$userData = array();
				if(empty($resExistemail)){
					$formData["uc_firstname"] = $post["txt_firstname"];
					$formData["uc_lastname"] = $post["txt_lastname"];
					$formData["uc_email"] = $post["txt_email"];
					//if(isset($post["txt_phone"]) && ($post["txt_phone"])){
						$formData["uc_mobile"] = $post["txt_phone"];
					//}

					$formData["uc_role"] = 5;
					$formData["uc_created"] = date("Y-m-d H:i:s");
					$formData["uc_status"] = '0';
					$formData["uc_active"] = '0';

					$instID =  $this->user_Auth->insert('user_credentials',$formData);

					if($instID){

						$user = $this->user_Auth->getData('user_credentials',$where=array('uc_id' =>$instID),$sel='',$sort ='');
						$userData = $user[0];
					}
					
				}else{

					$formData["uc_firstname"] = $post["txt_firstname"];
					$formData["uc_lastname"] = $post["txt_lastname"];
					
					//if(isset($post["txt_phone"]) && ($post["txt_phone"])){
						$formData["uc_mobile"] = $post["txt_phone"];
					//}

					$this->user_Auth->update("user_credentials", $formData,array("uc_id" => $resExistemail[0]->uc_id));
					$userData = $resExistemail[0];
				}
				if($post['txt_paymethod'] == 'cc'){
					$this->stripePayment($post,$userData);
				}else{

					if(isset($post['txt_create_account'])){

	        			$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
					    $password = array(); 
					    $alphaLength = strlen($alphabet) - 1; 
					    for ($i = 0; $i < 8; $i++) {
					        $n = rand(0, $alphaLength);
					        $password[] = $alphabet[$n];
					    }
			
					    $userData->password = implode($password);
					    $formData["uc_password"]	= md5(implode($password)); 
	            		$formData["uc_active"] = 1;
	            		$formData["uc_status"] = 1;

						$this->user_Auth->update("user_credentials", $formData,array("uc_id" => $userData->uc_id));
						$this->signup_activated($userData->uc_email,$sub='', $userData);
					}

					$txnID = $this->txngenreate();

					$paymentDetails["user_id"] = $userData->uc_id;
		            $paymentDetails["book_id"] = $data['bookData']->b_id;
		            $paymentDetails["author_id"] = $data['bookData']->b_fk_of_aid;
		            $paymentDetails["qty"] = 1;
		            $paymentDetails["transaction_id"] = $txnID;
		            $paymentDetails["subtotal"] = $data['bookData']->b_sellingprice;
		            $paymentDetails["total_amt"] = $data['bookData']->b_sellingprice;
		            if($post['txt_paymethod'] == 'evc'){
		            	$paymentDetails["payment_mode"] = 'evc';
		            }else if($post['txt_paymethod'] == 'mpesa'){
		            	$paymentDetails["payment_mode"] = 'mpesa';
		            }
		            
		            $paymentDetails["payment_status"] = 'pending';
		            $paymentDetails["author_payment_status"] = 'pending';
		            $paymentDetails["created_at"] = date('Y-m-d H;i:s');
		            $paymentDetails["updated_at"] = date('Y-m-d H;i:s');
		            
		            $orderInstId = $this->user_Auth->insert("book_orders", $paymentDetails);
		            
		            if($orderInstId){
		            	$this->sendOrderRequestMail($userData,$data['bookData'],$post['txt_paymethod']);

			            $this->session->set_flashdata('alert','success');
		                $this->session->set_flashdata('message','Transaction Successfully completed.');
		                redirect("order-confirm/".base64_encode($txnID));
		                exit();
		            }else{
		            	$this->user_Auth->delete("user_credentials", array('uc_id'=> $user[0]->uc_id,'uc_active'=>0,'uc_status'=>0));
			            $this->session->set_flashdata('alert','error');
			            $this->session->set_flashdata('message',"Book purchase request failed.");
			            redirect("failure");
			            exit();
		            }	
		            
				}
				
				/*echo '<pre>';
				print_r($resExistemail);exit;*/
			}
		}

		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-checkout', $data);
		$this->load->view('front/common/footer', $data);
	}

	/*--------------------------------------------------
		
	---------------------------------------------------*/
	public function stripePayment($post,$user){

		$bookDetails = $this->user_Auth->getData('books', array("b_id" => $post['book_id']));	

		$cardHolder     = $post["txt_holdername"];
        $cardNumber     = $post["txt_cardnumber"];
        $cvvNumber 		= $post["txt_cvv"];
        $cardexpMonth   = $post["txt_expmonth"];
        $cardexpyear    = $post["txt_expyear"];
    
        require_once('application/libraries/stripe-php/init.php');

        \Stripe\Stripe::setApiKey($this->config->item('stripe_secret'));


        $response = \Stripe\Token::create(array(
            "card" => array(
              
                "number" => $cardNumber,
                "exp_month" =>$cardexpMonth,
                "exp_year" => $cardexpyear,
                "cvc" => $cvvNumber
            )
        ));

        if($response->error){
        	$this->session->set_flashdata('alert','error');
                $this->session->set_flashdata('message',$response->error->message);
                redirect("failure");
                exit();
        }


        $charge =  \Stripe\Charge::create ([
                "amount"        => 100 * ($bookDetails[0]->b_sellingprice),
                "currency"      => "usd",
                "source"        => $response->id,
                "description"   => $bookDetails[0]->b_title,
                "metadata"      =>  array('bookID' => $bookDetails[0]->b_id, "customerID" => $user->uc_id)
        ]);
        $chargeJson = $charge->jsonSerialize(); 

        if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
            $transactionID      = $chargeJson['balance_transaction']; 
            $paidAmount         = $chargeJson['amount']; 
            $paidCurrency       = $chargeJson['currency']; 
            $payment_status     = $chargeJson['status'];
            $description        = $chargeJson["description"];
            $metadata           = $chargeJson["metadata"];
            $application        = $chargeJson["application"];
            $chargeId           = $chargeJson["id"];

            $paymentDetails["user_id"] = $metadata["customerID"];
            $paymentDetails["book_id"] = $metadata["bookID"];
            $paymentDetails["author_id"] = $bookDetails[0]->b_fk_of_aid;
            $paymentDetails["qty"] = 1;
            $paymentDetails["subtotal"] = ($paidAmount/100);
            $paymentDetails["total_amt"] = ($paidAmount/100);
            $paymentDetails["transaction_id"] = $transactionID;
            $paymentDetails["payment_mode"] = 'cc';
            $paymentDetails["payment_status"] = 'completed';
            $paymentDetails["author_payment_status"] = 'pending';
            $paymentDetails["created_at"] = date('Y-m-d H;i:s');
            $paymentDetails["updated_at"] = date('Y-m-d H;i:s');
            
            
            $this->user_Auth->insert("book_orders", $paymentDetails);
            

            if($payment_status == 'succeeded'){
            	
            	if(isset($post['txt_create_account'])){

        			$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
				    $password = array(); 
				    $alphaLength = strlen($alphabet) - 1; 
				    for ($i = 0; $i < 8; $i++) {
				        $n = rand(0, $alphaLength);
				        $password[] = $alphabet[$n];
				    }
		
				    $user->password = implode($password);
				    $formData["uc_password"]	= md5(implode($password)); 
            		$formData["uc_active"] = 1;
            		$formData["uc_status"] = 1;
				

					$this->user_Auth->update("user_credentials", $formData,array("uc_id" => $user->uc_id));

					$this->signup_activated($user->uc_email,$sub='', $user);
				}
				$this->order_confirm_email($user,$bookDetails[0],$transactionID);

                $this->session->set_flashdata('alert','success');
                $this->session->set_flashdata('message','Transaction Successfully completed.');
                redirect("order-confirm/".base64_encode($transactionID));
                exit();
            }else{
                $this->user_Auth->delete("user_credentials", array('uc_id'=> $user[0]->uc_id,'uc_active'=>0,'uc_status'=>0));
                $this->session->set_flashdata('alert','error');
                $this->session->set_flashdata('message',$charege);
                redirect("failure");
                exit();
            }

        }else{
        	$this->user_Auth->delete("user_credentials", array('uc_id'=> $user[0]->uc_id,'uc_active'=>0,'uc_status'=>0));
            $this->session->set_flashdata('alert','error');
            $this->session->set_flashdata('message',$charege);
            redirect("failure");
            exit();
        }
	}

	public function sendOrderRequestMail($user=array(),$book=array(),$method=""){
		$html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block; width:100%">
					                    <h3>Hi '.$user->uc_firstname.' '.$user->uc_lastname.',</h3>
					                    <p>Thanks for book purchase request.</p>
                                    	<p>Please make payment to get book.<p>';
                                    	if($method == 'evc'){
                                    		$html .= '<p>OUR CUSTOMER SERVICES IN SOMALIA HAFSA HASSAN +252-61-3148376</p>';
                                    	}else if($method == 'mpesa'){
                                    		$html .= '<p>Send the money thorugh Mpesa  and call us to confirm.<br>(Mpesa Number) 0725-893-222</p>';
                                    	}
                                    	
                                    	$html .= '<p>Your Order Request Details :<br>
                                    	<strong>Book Name :</strong> '.$book->b_title.'<br>
                                    	<strong>Book Price :</strong> '.$book->b_sellingprice.'<br>
                                    	</p>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

        
        $this->email->to($user->uc_email);
        $this->email->from('pareshnagar87@gmail.com',SITENAME);

        $this->email->subject('Order Completed');
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
            return 1;
        }else{
            $this->email->print_debugger();
        }
	}

	public function order_confirm_email($user, $book, $txnID){
        $html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block; width:100%">
					                    <h3>Hi '.$user->uc_firstname.' '.$user->uc_lastname.',</h3>
					                    <p>Thanks for book purchase.</p>
                                    	<p>Your order is completed.<br>
                                    	Your Order Details :<br>
                                    	<strong>Book Name :</strong> '.$book->b_title.'<br>
                                    	<strong>Book Price :</strong> '.$book->b_sellingprice.'<br>
                                    	<strong>Transaction ID :</strong> '.$txnID.'<br></p>

                                    	<p>Click <a href="'.site_url('signIn').'">Sign In</a> for login.</p>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

        
        $this->email->to($user->uc_email);
        $this->email->from('pareshnagar87@gmail.com',SITENAME);

        $this->email->subject('Order Completed');
        $this->email->message($html);
        // $this->email->attach(base_url('uploads/books/'.$book->b_file));
        $sended =$this->email->send();
        if($sended){
            return 1;
        }else{
            $this->email->print_debugger();
        }
    }

	public function signup_activated($to, $subject=false, $data){
    	//echo '<pre>';
    	//print_r($data);exit;
        $html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block; width:100%">
					                    <h3>Hi '.$data->uc_firstname.' '.$data->uc_lastname.',</h3>
					                    <p>Thanks for registration on '.SITENAME.'.</p>
                                    	<p>Your account is activated now.<br>
                                    	Your login details :<br>
                                    	<strong>Username :</strong> '.$data->uc_email.'<br>
                                    	<strong>Password :</strong> '.$data->password.'<br>

                                    	Click <a href="'.site_url('signIn').'">Sign In</a> for login.</p>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

        
        $this->email->to($to);
        $this->email->from('pareshnagar87@gmail.com',SITENAME);

        $this->email->subject( (($subject)?$subject:'Signup Confirmation') );
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
            return 1;
        }else{
            $this->email->print_debugger();
        }
    }

	function orderConfirm($txnid){
		$data["title"] = 'Order Completed | '.SITENAME;
		$txnid = base64_decode($txnid);

		if($txnid){
			$data["txnDetails"] = $this->user_Auth->getData("book_orders",array("transaction_id" => $txnid));
		}
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-orderConfirm', $data);
		$this->load->view('front/common/footer', $data);
	}

	public function adduserReview(){
		$post = $this->input->post();
		$formData['user_id'] = $post['user_id'];
		$formData['book_id'] = $post['book_id'];
		$formData['rating']   = $post['rate'];
		$formData['review']   = $post['review'];
		$formData['approved']   = '0';
		$formData['created_at']   = date("Y-m-d H:i:s");
		$formData['updated_at']   = date("Y-m-d H:i:s");

		$userBookReview = $this->user_Auth->getData('book_reviews',array('user_id' => $post['user_id'],'book_id'=>$post['book_id']));

		if($userBookReview){
			echo json_encode(array('status' => 'Success','message'=>'You have already rated this book.'));
		}else{

			$instID = $this->user_Auth->insert('book_reviews',$formData);
			$user = $this->user_Auth->getData('user_credentials',$where=array('uc_id' =>$post['user_id']));
			$book = $this->user_Auth->getData('books',$where=array('b_id' =>$post['book_id']));

			if($instID){
				
				//$emailsend = $this->thanksMailtoUser($user[0]->uc_email,'',$user,$book);
				$emailsend = $this->thanksMailtoUser('paresh3779@gmail.com','',$user,$book);
				$authorEmailSend = $this->review_approval('paresh3779@gmail.com','',$user,$book,$instID);
				echo json_encode(array('status' => 'Success','message'=>"Thank you for rating the book. Your Review will be displayed after approval.",'book_id'=>$post['book_id']));
			}else{
				echo json_encode(array('status' => 'Fail','message'=>"Something wrong with book rating."));
			}
		}
		
		die();
	}

	/*------------------------------------------------
    	Thanks mail to user for review & rating
    ------------------------------------------------*/
    public function thanksMailtoUser($to, $subject=false, $data, $book=array()){
		$url = site_url('signIn/activated/?time='.base64_encode($data[0]->uc_id).'&slot='.base64_encode($data[0]->uc_email));
		$html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>E Book</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block; width:100%">
					                    <h3>Hi '.$data[0]->uc_firstname.' '.$data[0]->uc_lastname.',</h3>
					                	<p>Thanks for submitting review & rating for <strong>'.$book[0]->b_title.'</strong>.<br> Your review will be displayed after approval. </p>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		$this->email->to($to);
        $this->email->from('pareshnagar87@gmail.com', SITENAME);

        $this->email->subject( (($subject)?$subject:SITENAME.': Thanks For Book Review & Rating') );
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
        	return 1;
        }else{
    		$this->email->print_debugger();
    	}
	}


	/*------------------------------------------------
    	Thanks mail to user for review & rating
    ------------------------------------------------*/
    public function review_approval($to, $subject=false, $data, $book=array(),$instID){
		$url = site_url('dashboard/reviewDetails/'.$instID);
		$html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>E Book</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block; width:100%">
					                    <h3>Hello Admin,</h3>
					                	<p>'.$data[0]->uc_firstname.' '.$data[0]->uc_lastname.' has submitted review & rating for <strong>'.$book[0]->b_title.'</strong>.<br> To approve review click on <a href="'.$url.'">Approve Review</a>. </p>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		$this->email->to($to);
        $this->email->from('pareshnagar87@gmail.com', SITENAME);

        $this->email->subject( (($subject)?$subject:SITENAME.':Request for Book Review & Rating') );
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
        	return 1;
        }else{
    		$this->email->print_debugger();
    	}
	}



	/*---------------------------------------------
		catLoademoreajax()
	---------------------------------------------*/
	public function catLoademoreajax(){
		$post = $this->input->post();
		
		if(($post['pageUrls']) && ($post['pageid']) && ($post['total'])){
			$num=8;
			$pageno= $post['pageid'];
			$total= $post['total'];
			$start = (($pageno <= 1)?0: $num*($pageno-1));
			$end = ((($pageno*$num) >= $num)?$total:($pageno*$num));
			$show = $start.'-'.$end;
			$url = $post['pageUrls'];
			$parts = parse_url($url);
			if(isset($parts["query"])){
				parse_str($parts['query'], $query);
				if(!empty($query["txtShort"])){
					$short = $query["txtShort"];
				}else{
					$short ='';
				}
				if(!empty($query["filterCat"])){
					$catID = $query["filterCat"];
				}else{
					$catID ='';
				}
				if(!empty($query["filterCat"])){
					$catID = $query["filterCat"];
				}
				$resData = $this->user_Auth->loadeMorebycatandshort($catID,$short,$post['pageid'],$post['total']);
				if($resData){
					$htmlData='';
					foreach ($resData as $value) {
						$booksDetails = $this->user_Auth->getData('books',array('b_id' => $value->b_id));
						$htmlData .= '<div class="col-sm-3"><div class="box"><img src="'.( ($booksDetails[0]->b_image)?base_url('uploads/books/'.$booksDetails[0]->b_image):base_url('uploads/books/books.png')).'" class="img-fluid">
                                <div class="rating"><div class="rating">';
                                    for($r= 1; $r <= 5; $r++){
                                        if($r <= $booksDetails[0]->b_rating){
                                            
                                            $htmlData .= '<img src="'.base_url('assets/images/rating_a.png').'" class="img-fluid">';
                                            
                                        }else{
                                            
                                            $htmlData .= '<img src="'.base_url('assets/images/rating_b.png').'" class="img-fluid">';
                                            
                                        }
                                    }
            					$htmlData .= '</div>
                                </div>
                                <h5><a href="'.site_url('book/bookDetails/'.$booksDetails[0]->b_id).'">'.(isset($booksDetails[0]->b_title)?$booksDetails[0]->b_title:'').'</a></h5>
                                <p>';
                                    if(isset($booksDetails[0]->b_id)){
                                        $oneAuthor = $this->user_Auth->getData('user_credentials',array('uc_id' => $booksDetails[0]->b_fk_of_aid), $se='uc_firstname, uc_lastname');
                                        $htmlData .= (isset($oneAuthor[0]->uc_firstname)?$oneAuthor[0]->uc_firstname:'').' '.(isset($oneAuhor[0]->uc_lastname)?$oneAuthor[0]->uc_lastname:'');
                                    }
                            	$htmlData .= '</p></div></div>';
						
					}
					echo json_encode(array('status' => 200 ,'data' => $htmlData, 'span' => $show, 'message' =>'Successfully Done.')); die();
				}
			}else{
				$url = $url;
			    $link_array = explode('/',$url);
			    $catslug = end($link_array);
			 	$catID = $this->user_Auth->getData('categories', array('c_slug' => $catslug,'c_status' => '1'),$se='c_id'); 
			 	$catID = $catID[0]->c_id;
			 	if($catID){
			 	
	 				$resData = $this->user_Auth->loadeMorebycatandshort($catID,$short,$post['pageid'],$post['total']);
					if($resData){
						$htmlData='';
						foreach ($resData as $value) {
							$booksDetails = $this->user_Auth->getData('books',array('b_id' => $value->b_id));
							$htmlData .= '<div class="col-sm-3"><div class="box"><img src="'.( ($booksDetails[0]->b_image)?base_url('uploads/books/'.$booksDetails[0]->b_image):base_url('uploads/books/books.png')).'" class="img-fluid">
	                                <div class="rating"><div class="rating">';
	                                    for($r= 1; $r <= 5; $r++){
	                                        if($r <= $booksDetails[0]->b_rating){
	                                            
	                                            $htmlData .= '<img src="'.base_url('assets/images/rating_a.png').'" class="img-fluid">';
	                                            
	                                        }else{
	                                            
	                                            $htmlData .= '<img src="'.base_url('assets/images/rating_b.png').'" class="img-fluid">';
	                                            
	                                        }
	                                    }
	            					$htmlData .= '</div>
	                                </div>
	                                <h5><a href="'.site_url('book/bookDetails/'.$booksDetails[0]->b_id).'">'.(isset($booksDetails[0]->b_title)?$booksDetails[0]->b_title:'').'</a></h5>
	                                <p>';
	                                    if(isset($booksDetails[0]->b_id)){
	                                        $oneAuthor = $this->user_Auth->getData('user_credentials',array('uc_id' => $booksDetails[0]->b_fk_of_aid), $se='uc_firstname, uc_lastname');
	                                        $htmlData .= (isset($oneAuthor[0]->uc_firstname)?$oneAuthor[0]->uc_firstname:'').' '.(isset($oneAuhor[0]->uc_lastname)?$oneAuthor[0]->uc_lastname:'');
	                                    }
	                            	$htmlData .= '</p></div></div>';
							
						}
						echo json_encode(array('status' => 200 ,'data' => $htmlData, 'span' => $show, 'message' =>'Successfully Done.')); die();
					}
			 		
			 	}
			}
			
		}else{
			echo json_encode(array('status' =>100, 'message' => 'Invalid operation.'));
		}
		die();
	}

	public function txngenreate(){
		$mt = explode(' ',microtime());
		$round = date('dmY').$mt[1];
		return $round;
	}
	/*--------------------------------------------
		referToFriend()
	--------------------------------------------*/
	public function referToFriend(){
		$post = $this->input->post();
		if($post){
			$bidId = $post["txtId"];
			$txtsenderName = $post["txtsenderName"];
			$txtsenderEmail = $post["txtsenderEmail"];
			$txtsenderMag = $post["txtsenderMag"];
			$txtreceiverName = $post["txtreceiverName"];
			$txtreceiverEmail = $post["txtreceiverEmail"];
			$book = $this->user_Auth->getData('books', $w=array('b_id' => $bidId, 'b_status' => '1'));
			$bookAauthor = $this->user_Auth->getData('user_credentials', $w=array('uc_id' => $book[0]->b_fk_of_aid),$se="uc_id,uc_firstname,uc_lastname,uc_email");
			$bookCategory= $this->user_Auth->getData('categories', $w=array('c_id' => $book[0]->b_category), $se='c_id,c_name');
			// $bookDetails["b_fk_of_aid"];
			// $bookDetails["b_fk_of_uid"]
			// $bookDetails["b_language"]
			// $bookDetails["b_title"]
			// $bookDetails["b_slug"]
			// $bookDetails["b_published"]
			// $bookDetails["b_originalprice"]
			// $bookDetails["b_published"];
			// $bookDetails["b_sellingprice"];
			// $bookDetails["b_publisher"]
			// $bookDetails["b_bookpages"];
			// $bookDetails["b_isbn"];
			// $bookDetails["b_rating"];
			// $bookDetails["b_category"];
			// $bookDetails["b_image"];
			// $bookDetails["b_description"];
			// $bookDetails["b_file"];
			// $bookDetails["b_downloads"];
			// $bookDetails["b_status"];
			// $bookDetails["b_created"];
			// $bookDetails["b_modified"];

			$html = '<p>Hi '.$txtreceiverName.',</p><p>This refered by '.$txtsenderName.'</p><p>'.$txtsenderMag.'</p><div calss="container"><table id="booksdetails" class="table table-striped" border="2px">
    			<thead>
    				<th>Name</th>
    				<th>Details</th>
    			</thead>
    			<tbody>
    				<tr>
    					<td> Book Name</td><td> '.$book[0]->b_title.'</td>
    				</tr>
    					<td> Book Author Name</td><td> '.(isset($bookAauthor[0]->uc_firstname)?$bookAauthor[0]->uc_firstname:'').' '.(isset($bookAauthor[0]->uc_lastname)?$bookAauthor[0]->uc_lastname:'').'</td>
    				<tr>
    					<td> Book Language</td><td style="text-transform: capitalize;"> '.$book[0]->b_language.'</td>
    				</tr>
    				<tr>
    					<td> Original Price</td><td> '.(($book[0]->b_originalprice)?$book[0]->b_originalprice:'').'</td>
    				</tr>
    				<tr>
    					<td> Selling Price</td><td> '.(($book[0]->b_sellingprice)?$book[0]->b_sellingprice:'').'</td>
    				</tr>
    				<tr>
    					<td> Discount</td><td> ';
    					if(($book[0]->b_originalprice > 0) && ($book[0]->b_originalprice >=  $book[0]->b_sellingprice)){
    						$per = ((($book[0]->b_originalprice -  $book[0]->b_sellingprice)*100)/$book[0]->b_originalprice);
    						$html.= (($per > 0)?number_format($per, 2).'%':'0%');
    					}else{
    						$html.= '0%';
    					}
    					$html.='</td></tr>
    				<tr>
    					<td> Publisher</td><td> '.(($book[0]->b_publisher)?$book[0]->b_publisher:'').'</td>
    				</tr>
    				<tr>
    					<td> Book Rating</td><td> '.$book[0]->b_rating.'</td>
    				</tr>
    				<tr>
    					<td> Book Category </td><td> '.(isset($bookCategory[0]->c_name)?$bookCategory[0]->c_name:'').'</td>
    				</tr>
    				<tr>
    					<td> Book Status</td><td> ';
    					
    					if($book[0]->b_status == 1){
    						$html.= 'True';
    					}else if($book[0]->b_status == 2){
    						$html.= 'Pending';
    					}else{
    						$html.= 'False';
    					}
    					$html.= '</td></tr>
    				<tr>
    					<td> Published Books</td><td> '.(($book[0]->b_published)?$book[0]->b_published:'').'</td>
    				</tr>
    				<tr>
    					<td> Pages</td><td> '.(($book[0]->b_bookpages)?$book[0]->b_bookpages:'').'</td>
    				</tr>
    				<tr>
    					<td> ISBN</td><td> '.(($book[0]->b_isbn)?$book[0]->b_isbn:'').'</td>
    				</tr>
    				<tr>
    					<td> Descriptions</td><td> '.(($book[0]->b_description)?$book[0]->b_description:'').'</td>
    				</tr>
    				<tr>
    					<td> Book Created</td><td> '.(($book[0]->b_created >0)?date('d-M-Y H:i:s',strtotime($book[0]->b_created)):'').'</td>
    				</tr>
    				<tr>
    					<td> Book Modified </td><td> '.(($book[0]->b_modified >0)?date('d-M-Y H:i:s',strtotime($book[0]->b_modified)):'').'</td>
    				</tr>
    				<tr>
    					<td> Book Picture</td><td colspan="3"> <div style="width:150px;"><a target="_blank" href="'.site_url("book/bookDetails/".$book[0]->b_id).'"><img src="'.base_url("uploads/books/".$book[0]->b_image).'" class="img-fluid"></a></div></td>
    				</tr>
    			</tbody>
    			</table></div>';

    			$config['protocol'] 	= 'smtp';
		        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
		        $config['smtp_port'] 	= '465';
		        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
		        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
		        $config['mailtype'] 	= 'html';
		        $config['charset'] 		= 'utf-8';
		        $config['newline'] 		= "\r\n";
		        $config['wordwrap'] 	= TRUE;
				$this->email->initialize($config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");

		        $toemail = $txtreceiverEmail;
		        
		        $subject = SITENAME.' Refered by '.$txtsenderName;
		        $this->email->to($toemail);
		        $this->email->from('pareshnagar87@gmail.com',SITENAME);

		        $this->email->subject($subject);
		        $this->email->message($html);
    			$sendM = $this->email->send();
    			if($sendM){
    				echo json_encode(array('status' => 200, 'message' => 'Successfully Done.'));
    			}else{
    				echo json_encode(array('status' => 100, 'message' => 'Process failed please do to try after sometime.'));
    			}

		}else{
			echo json_encode(array('status' => 100, 'message' => 'In-valid Method!.'));
		}
		die();
	}
	
}