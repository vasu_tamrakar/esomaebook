<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VideoTraining extends CI_Controller {
	 public function __construct (){
	 	parent :: __construct();
	 	
		$this->load->model('user_Auth');
		$this->load->model('VideoTraining_model');
	 }
	public function index()
	{
		if($this->session->userdata('is_logged_in_user')){
			$data['login_userid'] = $this->session->userdata('is_logged_in_user');
		}else{
			$data['login_userid'] = "";
		}

		$data['videos'] = $this->user_Auth->getData('videos',array('v_category' => 'Training and Tools Videos'),'','v_id');
		foreach($data['videos'] as $video){
			$videoLikes = $this->VideoTraining_model->getVideoLikes($video->v_id);
			$video->totalLikes = $videoLikes[0]->totalLikes;
		}

		$data['latestVideos'] = $this->user_Auth->getData('videos',array('v_category' => 'Training and Tools Videos'),'','v_id desc');
		foreach($data['latestVideos'] as $video){
			$videoLikes = $this->VideoTraining_model->getVideoLikes($video->v_id);
			$video->totalLikes = $videoLikes[0]->totalLikes;
		}

		$mostLiked = array();
		$data['popularVideos'] = $this->user_Auth->getData('videos',array('v_category' => 'Training and Tools Videos'),'','v_id desc');
		foreach($data['popularVideos'] as $video){
			$videoLikes = $this->VideoTraining_model->getVideoLikes($video->v_id);
			$video->totalLikes = $videoLikes[0]->totalLikes;
			$mostLiked[$video->v_id] = $videoLikes[0]->totalLikes;
		}

		array_multisort($mostLiked, SORT_DESC, $data['popularVideos']);

		/*echo '<pre>';
		print_r($data['popularVideos']);exit;*/

		$data["title"] = 'Video Training Page | '.SITENAME;
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-videotraining',$data);
		$this->load->view('front/common/footer',$data);

	}

	public function addLike(){

		$post = $this->input->post();
		$formData['user_id'] = $post['user_id'];
		$formData['video_id'] = $post['video_id'];
		$formData['video_like']   = 1;

		$userVideoLike = $this->user_Auth->getData('video_likes',array('user_id' => $post['user_id'],'video_id'=>$post['video_id']));

		if($userVideoLike){
			echo json_encode(array('status' => 'Success','message'=>'You have already liked this video.'));
		}else{
			$instID = $this->user_Auth->insert('video_likes',$formData);
			if($instID){
				echo json_encode(array('status' => 'Success','message'=>"Thank you for like our video."));
			}else{
				echo json_encode(array('status' => 'Fail','message'=>"Something wrong with video like."));
			}
		}
		
		die();
	}
}
