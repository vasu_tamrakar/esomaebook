<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SomaliAuthors extends CI_Controller {
	public function __construct (){
		parent :: __construct();
		$this->load->model('user_Auth');
		$this->load->model('book_model');
	}
	public function index()
	{
		$data["title"] = 'Somali Authors page | '.SITENAME;
		$data['authors'] = $this->user_Auth->getData('authors',array('a_lang'=>'somali','a_status'=>1));

		foreach ($data['authors'] as $author) {
			$author->userData = $this->user_Auth->getData('user_credentials',array('uc_id' => $author->a_fK_of_uc_id),'','');
			$author->books = $this->user_Auth->getData('books',array('b_fk_of_aid' => $author->a_id,'b_status'=>'1'));
		}

		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-somaliauthors',$data);
		$this->load->view('front/common/footer',$data);
	}
}
