<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VideoTraining_model extends CI_Model {
	
	public function getVideoLikes($video_id){
		$this->db->select('count(*) as totalLikes');
		$this->db->from('video_likes');
		$this->db->where('video_id', $video_id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return false;
		}
	}
	
}
	
?>