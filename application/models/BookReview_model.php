<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BookReview_model extends CI_Model {
	/*----------------------------------
		insert($tablename,$data);

	----------------------------------*/
	public function insert($tname,$data){
		$this->db->insert($tname,$data);
		$id = $this->db->insert_id();
		if($id > 0){
			return $id; 
		}else{
			return FALSE;
		}
	}
	/*----------------------------------
		update($tablename,$data,$filter);

	----------------------------------*/
	public function update($tname,$data,$filter){
		//print_r($filter);
		$this->db->where($filter);
		$query = $this->db->update($tname,$data);
			
		if($this->db->affected_rows() >= 0){
			return TRUE; 
		}else{
			return FALSE;
		}
	}
	/*----------------------------------
		delete($tablename,$filter);

	----------------------------------*/
	public function delete($tname,$filter){
		$this->db->where($filter);
		$query = $this->db->delete($tname);
		if($this->db->affected_rows() > 0){
			return TRUE; 
		}else{
			return FALSE;
		}
	}

	public function getBookReviews($id=""){

		$this->db->select('*');
		$this->db->from('book_reviews');
		$this->db->join('user_credentials', 'user_credentials.uc_id = book_reviews.user_id','left');
		$this->db->join('books', 'books.b_id = book_reviews.book_id','left');

		if($id!=""){
			$this->db->where(array('id'=>$id));
		}		
		$this->db->order_by('id desc');	
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return FALSE;
		}
		
	}

	public function getReviewsByBookID($book_id){
		$this->db->select('*');
		$this->db->from('book_reviews');
		$this->db->join('user_credentials', 'user_credentials.uc_id = book_reviews.user_id','left');
		$this->db->join('books', 'books.b_id = book_reviews.book_id','left');

		if($book_id!=""){
			$this->db->where(array('book_id'=>$book_id));
		}
		$this->db->order_by('id desc');	
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return FALSE;
		}
	}
	
}
	
?>