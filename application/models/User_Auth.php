<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Auth extends CI_Model {
	/*----------------------------------
		insert($tablename,$data);

	----------------------------------*/
	public function insert($tname,$data){
		$this->db->insert($tname,$data);
		$id = $this->db->insert_id();
		if($id > 0){
			return $id; 
		}else{
			return FALSE;
		}
	}
	/*----------------------------------
		update($tablename,$data,$filter);

	----------------------------------*/
	public function update($tname,$data,$filter){
		//print_r($filter);
		$this->db->where($filter);
		$query = $this->db->update($tname,$data);
			
		if($this->db->affected_rows() >= 0){
			return TRUE; 
		}else{
			return FALSE;
		}
	}
	/*----------------------------------
		delete($tablename,$filter);

	----------------------------------*/
	public function delete($tname,$filter){
		$this->db->where($filter);
		$query = $this->db->delete($tname);
		if($this->db->affected_rows() > 0){
			return TRUE; 
		}else{
			return FALSE;
		}
	}

	/*----------------------------------
		getData($tablename,$filter,$selected,$order);

	----------------------------------*/
	public function getData($tname,$filter=false,$selected=false,$order=false,$groupby=false, $start=false,$end=false){
		if($selected){
			$this->db->select($selected);	
		}
		if($filter){
			$this->db->where($filter);	
		}
		if($order){
			$this->db->order_by($order);	
		}
		if($groupby){
			$this->db->group_by($groupby);
		}
		if($start === 0){

			$this->db->limit(2);
		}else{
			if($end){
				$this->db->limit($start, $end);
			}
		}
		
		$query = $this->db->get($tname);
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return array();
		}
	}

	/*----------------------------------
		existEmail($email);

	----------------------------------*/
	public function existEmail($email){
		$query = $this->db->get_where('user_credentials',array('uc_email' =>$email));
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return FALSE;
		}
	}
	/*----------------------------------
		existMobile($mobile);

	----------------------------------*/
	public function existMobile($mobile){
		$query = $this->db->get_where('user_credentials',array('uc_mobile' =>$mobile));
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return FALSE;
		}
	}

	/*----------------------------------
		checkAuthSignIn($userlogin,$password);

	----------------------------------*/
	public function checkAuthSignIn($userlogin,$password){
		$queryEmail = $this->db->get_where('user_credentials',array('uc_email' =>$userlogin));
		$queryMobile = $this->db->get_where('user_credentials',array('uc_mobile' =>$userlogin));
		if($queryEmail->num_rows() > 0){
			$queryRes = $this->db->get_where('user_credentials',array('uc_email' => $userlogin, 'uc_password' => md5($password)));
			if($queryRes->num_rows() > 0){
				if(($queryRes->result()[0]->uc_status == '1') && ($queryRes->result()[0]->uc_active == '1')){
					return array('status'=>'Success', 'data' => $queryRes->result()[0], 'message' => $userlogin.' scccessfully signIn.');
				}
				if(($queryRes->result()[0]->uc_status == '0') && ($queryRes->result()[0]->uc_active == '0')){
					return array('status'=>'Invalid', 'message' => 'Account is dectivated.');
				}
				if(($queryRes->result()[0]->uc_status == '1') && ($queryRes->result()[0]->uc_active == '0')){
					return array('status'=>'Invalid', 'message' => 'Account is dectivated.');
				}
				if($queryRes->result()[0]->uc_status == '0'){
					return array('status'=>'Invalid', 'message' => 'Please contact to administrator.');
				}
				
				
			}else{
				return array('status'=>'Invalid', 'message' => 'In-Valid credentials.');
			}
			
		}elseif($queryMobile->num_rows() > 0){
			$queryResmob = $this->db->get_where('user_credentials',array('uc_mobile' =>$userlogin, 'uc_password' =>md5($password)));
			if($queryResmob->num_rows() > 0){

				if(($queryResmob->result()[0]->is_status == '1') && ($queryResmob->result()[0]->is_active == '1')){
					return array('status'=>'Success', 'data' => $queryResmob->result()[0],'message' => $userlogin.' scccessfully signIn.');
				}
				if(($queryResmob->result()[0]->is_status == '0') && ($queryResmob->result()[0]->is_active == '0')){
					return array('status'=>'Invalid', 'message' => 'Account is dectivated.');
				}
				if(($queryResmob->result()[0]->is_status == '1') && ($queryResmob->result()[0]->is_active == '0')){
					return array('status'=>'Invalid', 'message' => 'Account is dectivated.');
				}
				if($queryResmob->result()[0]->is_status == '0'){
					
					return array('status'=>'Invalid', 'message' => 'Please contact to administrator.');
				}

			}else{
				return array('status'=>'Invalid','message' => 'In-Valid credentials.');
			}
		}else{
			return array("status"=> "Signup","message" => $userlogin. " Not registered. click <a href='".site_url('signUp')."'>here </a>for register.");
		}
	}
	/*----------------------------------
		ForgotPassword($email);

	----------------------------------*/
	public function ForgotPassword($email){

		$num = rand(0,9999);
		$this->db->where('uc_email',$email);
		$result = $this->db->update('user_credentials', array('uc_secrete' => $num, 'uc_password' => md5('.lzxkc;;'),'uc_status' => '0'));
		
		if($this->db->affected_rows() > 0){
			return $num;
		}else{
			return FALSE;
		}
	}

	/*----------------------------------
		exist_emailMobile($email,$mobile, $id);

	----------------------------------*/
	public function exist_emailMobile($email,$mobile=false,$uid){
		$this->db->where(array('uc_email' => $email));
		$this->db->where_not_in('uc_id',$uid);
		$emailEx = $this->db->get('user_credentials');
		if($emailEx->num_rows > 0){
			return array('status'=>'error', 'message'=>'Email Already registered.Please change the email address.');
		}
		if($mobile){
			$this->db->where(array('uc_mobile' => $mobile));
			$this->db->where_not_in('uc_id',$uid);
			$mobileEx = $this->db->get('user_credentials');
			if($mobileEx->num_rows > 0){
				return array('status'=>'error', 'message'=>'Mobile Number Already registered.Please change the mobile number.');
			}
		}
		return array('status'=>'success', 'message'=>'not');
		
	}
	
	/*-------------------------------------
		activeAccoount(id,email)
	-------------------------------------*/
	public function activeAccoount($uid,$email){
		$this->db->where(array('uc_id' => $uid,'uc_email' => $email));
		$this->db->update('user_credentials', array('uc_status' => '1', 'uc_active' => '1'));
		if($this->db->affected_rows() > 0 ){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	/*-------------------------------------------
		countAll(tname,$where)
	-------------------------------------------*/
	public function countAll($name, $where=false){
		if($where){
			$this->db->where($where);	
		}
		return $this->db->count_all($name);
	
	}
	/*-------------------------------------------
		countAllwhere(tname,$where)
	-------------------------------------------*/
	public function countAllwhere($name, $where=false){
		if($where){
			$this->db->where($where);	
		}
		return  $this->db->count_all_results($name);
	
	}
	/*----------------------------------
		get_authorByuid(userId)
	----------------------------------*/
	public function get_authorByuid($userId){
		$query = $this->db->get_where('authors',array("a_fK_of_uc_id" => $userId));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	/*----------------------------------
		get_Totaldownloadcount($tname, $select)
	----------------------------------*/
	public function get_Totaldownloadcount(){
		$this->db->count_all_results('book_orders');
		$this->db->from('book_orders');
		$this->db->where('payment_status', 'completed');
		return $this->db->count_all_results();
	}
	/*------------------------------------------
		emailSubscribe($em)
	------------------------------------------*/
	public function emailSubscribe($em){
		
		$existE = $this->db->get_where('emailsubscriber', array('es_email' => $em));

		if($existE->num_rows() > 0){
			$dataemail= $existE->result();
			if($dataemail[0]->es_status === '0'){
				$this->db->where(array('es_email' => $em));
				$this->db->update('emailsubscriber',array('es_status' => '1', 'es_modified'=> date('Y-m-d H:i:s')));
				// $qry = $this->db->insert_id();
				if($this->db->affected_rows()){
					return array('status' => 200, 'message' => $em." address successfully subscribe!.");
				}
			}else{
				return array('status' => 100, 'message' => $em." address already subscribed!.");
			}
		}else{
			$this->db->insert('emailsubscriber',array('es_email' =>$em, 'es_status' => '1', 'es_created'=> date('Y-m-d H:i:s')));
			$qry = $this->db->insert_id();
			if($qry > 0){
				return array('status' => 200, 'message' => $em." address successfully subscribe!.");
			}else{
				return array('status' => 100, 'message' => 'Process failed.');
			}
		}
		die();
	}
	/*-----------------------------------------
		mostpopularBooksID()
	-----------------------------------------
	public function mostpopularBooksID(){
		$mostpopularBooksID = 
		if($bIDS){
			$this->db->select('b_id');
			$this->db->where('b_status', '1');
			$this->db->where_in('b_id',$bIDS);
			$qurery = $this->db->get('books');
			if($qurery->num_rows() > 0 ){
				return $qurery->result();
			}else{
				return FALSE;
			}	
		}else{
			return FALSE;
		}
	}*/

	/*-----------------------------------------
		mostpopularBooksIDbyfiltercategory($catid)
	-----------------------------------------*/
	public function mostpopularBooksIDbyfilteronecategory($catid=false, $short=false){
		$filterLanguage = (($this->session->userdata('get_Language'))?$this->session->userdata('get_Language'):'english');
		
		if(is_array($catid)){

			$this->db->select('b_id');
			$this->db->where('b_status', '1');
			$this->db->where('b_language', $filterLanguage);
			if($short){
				$short = (($short=='mostpopular')?'b_id':$short);
				$this->db->order_by("$short ASC");	
			}else{
				$this->db->order_by("b_id ASC");	
			}
			
			$this->db->where_in('b_category', $catid);
			$query1	 = $this->db->get('books');
			// print_r($this->db->last_query()); die;
			if($query1->num_rows() > 0){
				$bids =$query1->result();

				foreach ($bids as $bookids) {
					$iddata[] = $bookids->b_id;
				}
				$this->db->select('pd_bookid');
				$this->db->where_in('pd_bookid',$iddata);
				$queryR = $this->db->get('paymentdetails');
				// print_r($this->db->last_query());
				if($queryR->num_rows() > 0){
					return $queryR->result();
				}else{
					return FALSE;
				}
			}else{
				$bids =FALSE;
			}
		}else{

			// $bids = $this->getData('books', $w = array('b_category' => $catid,'b_status' => '1'), $se= 'b_id', $sh = 'b_id ASC');
			// $this->getData('paymentDetails', $w = '', $se= 'pd_bookid, count(`pd_bookid`) c', $sh = 'c desc', $grp ='pd_bookid');

			// $this->db->select('books.b_id,paymentDetails.pd_bookid, count(paymentDetails.`pd_bookid`) c');
			// $this->db->where(array('b_category' => $catid, 'b_language' => $filterLanguage,'b_status' => '1'));
			// $this->db->order_by('b_id,c  DESC');
			// $this->db->from('books')
			// $this->db->group_by('pd_bookid');
			// $this->db->join('paymentDetails', 'books.b_id = paymentDetails.pd_bookid', 'left');
			// $this->db->get();

			$this->db->select('b_id');
			$this->db->where(array('b_category' => $catid, 'b_language' => $filterLanguage,'b_status' => '1'));
			$this->db->order_by('b_id ASC');
			$query2 = $this->db->get('books');
			if($query2->num_rows() > 0){
				$bids = $query2->result();
			}else{
				$bids = FALSE;
			}
		}
		
		if($bids){
			foreach ($bids as $bookids) {
				$iddata[] = $bookids->b_id;
			}
			$this->db->select('pd_bookid');
			$this->db->where_in('pd_bookid',$iddata);
			$queryR = $this->db->get('paymentdetails');
			if($queryR->num_rows() > 0){
				return $queryR->result();
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}


	/*----------------------------------------
		get_textfreesearch(text)
	-----------------------------------------*/
	public function get_textfreesearch($sText){

		$this->db->select('uc_id');
		
		$this->db->like('uc_firstname', $sText);
		$this->db->or_like('uc_lastname', $sText);
		$this->db->where(array('uc_role', 4, 'uc_status', '1', 'uc_active', '1'));
		$query = $this->db->get('user_credentials');
		if($query->num_rows() > 0){
			$searchdata["authors"] = $query->result();
		}
		$this->db->select('b_id');
		$this->db->where('b_status', '1');
		$this->db->like('b_title', $sText);
		$query2 = $this->db->get('books');

		if($query2->num_rows() > 0){
			$searchdata["books"] = $query2->result();
		}
		if(empty($searchdata)){
			return FALSE;
		}else{
			return $searchdata;
		}
	}
	/*-------------------------------------
		getPupularAuthor()
	-------------------------------------*/
	public function getPupularAuthor($catID=false){
		$selectedLanguage = (($this->session->userdata('get_language'))?$this->session->userdata('get_language'):'english');
		$bids = $this->getData('books',$w = array('b_category' => $catID, 'b_language' => $selectedLanguage, 'b_status' => '1'), $s='b_id', $sh= 'b_id ASC');
		
		if($bids){
			$newids=array();
			foreach ($bids as $value) {
				$newids[] = $value->b_id;
			}
			$this->db->select('pd_bookid');
			$this->db->where_in('pd_bookid',$newids);
			$this->db->group_by('pd_bookid');
			$queryR = $this->db->get('paymentdetails');
			if($queryR->num_rows() > 0){
				foreach ($queryR->result() as $idvalue) {
					$boID[] = $idvalue->pd_bookid;
				}
				$this->db->select('b_fk_of_aid');
				$this->db->group_by('b_fk_of_aid');
				$this->db->where_in('b_id', $boID);
				$authorid = $this->db->get('books');
				$authorid = $authorid->result();
				if($authorid){
					foreach ($authorid as $avalue) {
						$authorIDS[] = $avalue->b_fk_of_aid;
					}
				}
				if(isset($authorIDS) && ($authorIDS)){
					$this->db->select('uc_id, uc_firstname, uc_lastname, uc_email, uc_image, uc_role');
					$this->db->where(array('uc_role' => 4, 'uc_status' => '1'));
					$this->db->where_in('uc_id', $authorIDS);
					$user = $this->db->get('user_credentials');
					// print_r($this->db->last_query()); die;
					if($user->num_rows() > 0){
						foreach ($user->result() as $author) {
							$published = $this->getData('authors',$w = array('a_fK_of_uc_id' => $author->uc_id), $se = 'a_publishbooks',$sh='');
							$data[] = array(
								'a_id'=>$author->uc_id,
								'a_firstname'=>$author->uc_firstname,
								'a_lastname'=>$author->uc_lastname,
								'a_email'=>$author->uc_email,
								'a_image'=>$author->uc_image,
								'a_role'=>$author->uc_role,
								'a_publishbooks'=>(isset($published[0]->a_publishbooks)?$published[0]->a_publishbooks:0),
								);
						}
						return $data;
					}
				}else{
					return FALSE;
				}
			}else{
				$bookIDS = FALSE;
			}
		}else{
			return FALSE;
		}
		
	}
	/*-----------------------------------
		allbooksidByCategory(array('b_category' => $categoryData[0]->c_id, 'b_status' => '1'), $se='b_id', $se='b_id DESC',$g='', $st = 0, $end = 2)
	-----------------------------------*/
	public function allbooksidByCategory($filter,$sel=false, $short=false,$ggrp=false, $start = false, $end = false){
		$d='';
		foreach ($filter as $key => $value) {
			$d.= "`".$key."` = ".$value." AND ";
		}
		if($sel){
			$sel = $sel;
		}else{
			$sel ='*';
		}
		$short = (($short)?$short.' AND ':'');
		$ggrp = (($ggrp)?$ggrp.' AND ':'');
		$ggrp = (($start)?$start.' AND ':'');
		$ggrp = (($end)?$end.' AND ':'');
		$query = $this->db->query('SELECT '.$sel.' FROM books WHERE '.$d.' ORDER BY '.$short.' LIMIT '.$start.','.$end);
		print_r($query); die;
	}

	/*-------------------------------------------------
		getDatabycategoryFilter()
	-------------------------------------------------*/
	public function getDatabycategoryFilter($name,$where=false,$select =false, $short=false,$groupby=''){
		$selectedLanguage = (($this->session->userdata('get_language'))?$this->session->userdata('get_language'):'english');
		if(is_array($where)){
			$this->db->select($select);
			$this->db->where_in('b_category',$where);
			$this->db->where('b_language',$selectedLanguage);
			$this->db->where('b_status','1');
			if($short){
				$short = (($short == 'mostpopular')?'b_id':$short);
				$this->db->order_by("$short ASC");	
			}
			
			$queryG = $this->db->get('books');
			if($queryG){
				return $queryG->result();
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
		
	}


	/*-----------------------------------------------------------------------------------
		loadeMorebycatandshort($catID=false,$short=false,$pageid=false,$total=false)
	-----------------------------------------------------------------------------------*/

	public function loadeMorebycatandshort($catID=false,$short=false,$pageid=false,$total=false){
		$selectedLanguage = (($this->session->userdata('get_language'))?$this->session->userdata('get_language'):'english');
		if(is_array($catID)){
			$this->db->select('b_id');
			$this->db->where_in('b_category',$catID);
			$this->db->where('b_language',$selectedLanguage);
			$this->db->where('b_status','1');
			if($short){
				$short = (($short == 'mostpopular')?'b_id':$short);
				$this->db->order_by("$short ASC");	
			}
			if(($pageid) && ($total)){
				$num =8;
				$start = (($pageid <= 1)?0: $num*($pageid-1));
				$end = $num;
				$this->db->limit($end, $start);	
			}
			// echo "stri".$start;
			// echo " emd ".$end;
			$queryG = $this->db->get('books');
			// echo $this->db->last_query(); die;
			if($queryG){
				return $queryG->result();
			}else{
				return FALSE;
			}
		}else{
			/* todo for Single */
			$this->db->select('b_id');
			$this->db->where('b_category',$catID);
			$this->db->where('b_language',$selectedLanguage);
			$this->db->where('b_status','1');
			if($short){
				$short = (($short == 'mostpupular')?'b_id':$short);
				$this->db->order_by("$short ASC");	
			}else{
				$this->db->order_by("b_id ASC");	
			}
			if(($pageid) && ($total)){
				$num =8;
				$start = (($pageid <= 1)?0: $num*($pageid-1));
				$end = $num;
				$this->db->limit($end, $start);	
			}

			$queryG = $this->db->get('books');
			// echo $this->db->last_query(); die;
			if($queryG){
				return $queryG->result();
			}else{
				return FALSE;
			}
		}
	}
	/*------------------------------------------------------------
		comingsoonbookbycategoryFilter()
	------------------------------------------------------------*/
	public function comingsoonbookbycategoryFilter($tname,$where=false,$select=false,$short=false,$groupby=false){
		if(is_array($where)){
			$this->db->select($select);
			$this->db->where_in('b_category',$where);
			$this->db->where('b_status', '1');
			if($short){
				$short = (($short == 'mostpopular')?'b_id':$short);
				$this->db->order_by("$short ASC");	
			}
			$que = $this->db->get($tname);
			if($que->num_rows() > 0){
				return $que->result();
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	/*----------------------------------------------------
		mostpopauthorrefbybookid($idsdd)
	----------------------------------------------------*/
	public function mostpopauthorrefbybookid($bookid){
		if($bookid){

			if(is_array($bookid)){
				foreach ($bookid as $bvalue) {
					// print_r($bvalue->pd_bookid); die;
					$newIDs[] = $bvalue->pd_bookid;
				}
				$this->db->select('b_fk_of_aid');
				$this->db->where_in('b_id',$newIDs);
				$this->db->group_by('b_fk_of_aid');
				$aids =$this->db->get('books');

				if($aids->num_rows() > 0){
					foreach ($aids->result() as $key => $value) {
						$newIDs[] = $value->b_fk_of_aid;
					}
					// print_r($newIDs);
					$this->db->select('uc_id, uc_firstname, uc_lastname, uc_email, uc_image, uc_role');
					$this->db->where(array('uc_role' => 4, 'uc_status' => '1'));
					$this->db->where_in('uc_id', $newIDs);
					$user = $this->db->get('user_credentials');
					// print_r($this->db->last_query()); die;
					if($user->num_rows() > 0){
						foreach ($user->result() as $author) {
							$published = $this->getData('authors',$w = array('a_fK_of_uc_id' => $author->uc_id), $se = 'a_publishbooks',$sh='');
							$data[] = array(
								'a_id'=>$author->uc_id,
								'a_firstname'=>$author->uc_firstname,
								'a_lastname'=>$author->uc_lastname,
								'a_email'=>$author->uc_email,
								'a_image'=>$author->uc_image,
								'a_role'=>$author->uc_role,
								'a_publishbooks'=>(isset($published[0]->a_publishbooks)?$published[0]->a_publishbooks:0),
								);
						}
						// print_r($data); die;
						return $data;
					}
				}else{
					return FALSE;
				}
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}

	/*---------------------------------
		mostpupularAuthorsid()
	---------------------------------*/
	public function mostpupularAuthorsid_old(){
		$bIB = $this->getData('paymentdetails', $w = '', $se= 'pd_bookid, count(`pd_bookid`) c', $sh = 'c desc', $grp ='pd_bookid');
		$newArr =array();
		foreach ($bIB as $key => $value) {
			$newArr[] = $value->pd_bookid;
		}
		if($newArr){
			$this->db->select('b_fk_of_aid as a_id');
			$this->db->where_in('b_id', $newArr);
			$this->db->where(array('b_status' => '1'));
			$this->db->group_by('b_fk_of_aid');
			$query = $this->db->get('books');
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}


	public function mostpupularAuthorsid(){
		$this->db->select('*, count(author_id) as totalCnt');
		$this->db->from('book_orders');
		
		$this->db->join('user_credentials', 'user_credentials.uc_id = book_orders.author_id','left');
		//$this->db->join('books', 'books.b_fk_of_aid = book_orders.author_id','left');
		/*$this->db->join('authors', 'authors.a_fK_of_uc_id = book_orders.author_id','left');*/
		$this->db->group_by('book_orders.author_id');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return false;
		}
	}
	/*----------------------------------------
		existUpdateunique($name,$compare=array(), $notin=array())
	---------------------------------------*/
	public function existUpdateunique($tname,$compare, $notin){
		foreach ($notin as $key => $value) {
			$kval= $key;
		}
		$this->db->select($kval);
		$this->db->where($compare);
		$this->db->where_not_in($kval,$notin[$kval]);
		$query = $this->db->get($tname);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}
}
?>