<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership_model extends CI_Model {
	/*----------------------------------
		insert($tablename,$data);

	----------------------------------*/
	public function getMembers($id=""){
		$this->db->select('*');
		$this->db->from('paymentdetails');
		$this->db->join('user_credentials', 'user_credentials.uc_id = paymentdetails.pd_userid','left');
		$this->db->join('membershipplan', 'membershipplan.mp_id = paymentdetails.pd_planid','left');
		if($id){
			$this->db->where(array('pd_id'=>$id));
		}
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return array();
		}
	}
	

	public function getBookDetails($id=""){
		if($id == ""){
			return FALSE;
		}else{
			$this->db->select('*');
			$this->db->from('books');
			$this->db->join('user_credentials', 'user_credentials.uc_id = books.b_fk_of_aid','left');
			$this->db->join('authors', 'authors.a_fK_of_uc_id = user_credentials.uc_id','left');
			$this->db->join('categories', 'categories.c_id = books.b_category','left');
			
			$this->db->where(array('b_id'=>$id));

			$query = $this->db->get();
			if($query->num_rows() > 0){
				return $query->result(); 
			}else{
				return FALSE;
			}
		}
		
	}

	public function getBookChapters($book_id=""){
		if($book_id == ""){
			return false;
		}else{
			$this->db->select('*');
			$this->db->from('book_chapters');
			$this->db->where(array('book_id'=>$book_id));
			$query = $this->db->get();
			if($query->num_rows() > 0){
				return $query->result(); 
			}else{
				return false;
			}
		}
		
	}

	public function getBooksByAuthorID($author_id=""){
		if($author_id == ""){
			return false;
		}else{
			$this->db->select('*');
			$this->db->from('books');
			$this->db->where(array('b_fk_of_aid'=>$author_id));
			$this->db->where(array('b_status'=>'1'));
			$query = $this->db->get();
			if($query->num_rows() > 0){
				return $query->result(); 
			}else{
				return false;
			}
		}
	}

	public function getBooksByCategoryID($cat_id=""){
		if($cat_id == ""){
			return false;
		}else{
			$this->db->select('*');
			$this->db->from('books');
			$this->db->where(array('b_category'=>$cat_id));
			$this->db->where(array('b_status'=>'1'));
			$query = $this->db->get();
			if($query->num_rows() > 0){
				return $query->result(); 
			}else{
				return false;
			}
		}
	}

	public function getBookReviews($id="",$user_id=""){
		$this->db->select('*');
		$this->db->from('book_reviews');
		$this->db->join('user_credentials', 'user_credentials.uc_id = book_reviews.user_id','left');
		$whereArr['book_id'] = $id;
		
		if($user_id != ""){
			$whereArr['user_id'] = $user_id;
		}else{
			$whereArr['approved'] = '1';
		}
		$this->db->where($whereArr);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return false;
		}
	}

	public function getTotalRating($id=""){
		$this->db->select('count(*) as totalRating');
		$this->db->from('book_reviews');
		$this->db->where('book_id', $id);
		$this->db->where('approved', '1');
		$this->db->where('rating !=', "");
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return false;
		}
	}

	public function getAvgRating($id=""){
		$this->db->select_avg('rating');
		$this->db->from('book_reviews');
		$this->db->where('book_id', $id);
		$this->db->where('approved', '1');
		$this->db->where('rating !=', "");
		//$this->db->where($whereArr);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return false;
		}
	}

	public function getDownloadInfo($book_id){
		$this->db->count_all_results('book_orders');
		$this->db->from('book_orders');
		$this->db->where('book_id', $book_id);
		$this->db->where('payment_status', 'completed');
		return $this->db->count_all_results();
	}

	public function getAllBookOrders($where = array()){
		$this->db->select('*');
		$this->db->from('book_orders');
		$this->db->join('books', 'books.b_id = book_orders.book_id','left');
		$this->db->join('user_credentials', 'user_credentials.uc_id = book_orders.user_id','left');
		$this->db->join('authors', 'authors.a_fK_of_uc_id = book_orders.author_id','left');
		if(!empty($where)){
			$this->db->where($where);
		}
		$this->db->order_by('book_orders.created_at','desc');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return false;
		}
		
	}

	public function getBookOrder($id=""){
		$this->db->select('*');
		$this->db->from('book_orders');
		$this->db->join('books', 'books.b_id = book_orders.book_id','left');
		$this->db->join('user_credentials', 'user_credentials.uc_id = book_orders.user_id','left');
		$this->db->join('authors', 'authors.a_fK_of_uc_id = book_orders.author_id','left');
		//if(!empty($where)){
		$this->db->where('id',$id);
		//}
		//$this->db->order_by('book_orders.created_at','desc');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return false;
		}
		
	}

	public function getOrderReport($where = array(),$startDate="",$endDate=""){
		$this->db->select('*');
		$this->db->from('book_orders');
		$this->db->join('books', 'books.b_id = book_orders.book_id','left');
		$this->db->join('user_credentials', 'user_credentials.uc_id = book_orders.user_id','left');
		$this->db->join('authors', 'authors.a_fK_of_uc_id = book_orders.author_id','left');
		if(!empty($where)){
			$this->db->where($where);
		}
		$this->db->where('book_orders.created_at BETWEEN "'. date('Y-m-d', strtotime($startDate)). '" and "'. date('Y-m-d', strtotime($endDate)).'"');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return false;
		}
	}

	
}
	
?>