-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 15, 2019 at 03:54 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebook_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `a_id` bigint(20) NOT NULL,
  `a_name` varchar(255) NOT NULL,
  `a_lang` varchar(255) DEFAULT NULL,
  `a_fK_of_uc_id` bigint(20) NOT NULL,
  `a_publishbooks` bigint(20) NOT NULL,
  `a_description` text NOT NULL,
  `a_paypal` varchar(255) NOT NULL,
  `a_mmoney` varchar(255) NOT NULL,
  `a_facebook` varchar(255) NOT NULL,
  `a_twitter` varchar(255) NOT NULL,
  `a_gplush` varchar(255) NOT NULL,
  `a_instagram` varchar(255) NOT NULL,
  `a_status` enum('1','0') NOT NULL,
  `a_created` datetime NOT NULL,
  `a_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`a_id`, `a_name`, `a_lang`, `a_fK_of_uc_id`, `a_publishbooks`, `a_description`, `a_paypal`, `a_mmoney`, `a_facebook`, `a_twitter`, `a_gplush`, `a_instagram`, `a_status`, `a_created`, `a_modified`) VALUES
(1, '', 'somali', 1, 0, 'Myself Paresh Nagar and I am selling ebooks.', 'paresh3779@yahoo.co.in', '', '', '', '', '', '1', '0000-00-00 00:00:00', '2019-08-08 12:43:58'),
(2, '', 'somali', 9, 0, 'I am ali Hussein, i wrote Three books, i novel and two about real things. ', '', '', '', '', '', '', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `b_id` bigint(20) NOT NULL,
  `b_fk_of_aid` bigint(20) NOT NULL,
  `b_fk_of_uid` bigint(20) NOT NULL,
  `b_language` enum('somali','english') NOT NULL,
  `b_title` varchar(255) NOT NULL,
  `b_slug` varchar(255) DEFAULT NULL,
  `b_published` date NOT NULL,
  `b_originalprice` double(8,2) NOT NULL,
  `b_sellingprice` double(8,2) NOT NULL,
  `b_publisher` varchar(255) NOT NULL,
  `b_bookpages` varchar(255) NOT NULL,
  `b_isbn` varchar(255) NOT NULL,
  `b_rating` int(5) NOT NULL,
  `b_category` int(11) NOT NULL,
  `b_image` text NOT NULL,
  `b_description` text NOT NULL,
  `b_file` text NOT NULL,
  `b_downloads` bigint(20) NOT NULL,
  `b_status` enum('0','1','2') NOT NULL,
  `b_created` datetime NOT NULL,
  `b_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`b_id`, `b_fk_of_aid`, `b_fk_of_uid`, `b_language`, `b_title`, `b_slug`, `b_published`, `b_originalprice`, `b_sellingprice`, `b_publisher`, `b_bookpages`, `b_isbn`, `b_rating`, `b_category`, `b_image`, `b_description`, `b_file`, `b_downloads`, `b_status`, `b_created`, `b_modified`) VALUES
(1, 1, 1, 'somali', 'War & Peace', 'war-peace', '0000-00-00', 30.00, 30.00, '', '', '5RHFRqRT', 0, 0, 'book_62.png', 'War & Peace', 'demoform18.pdf', 0, '1', '2019-08-06 15:36:18', '0000-00-00 00:00:00'),
(2, 1, 1, 'somali', 'Gear', 'gear', '1970-01-01', 55.00, 47.00, '', '77', 'pqrysRqV', 0, 1, 'book_13.png', 'Gear ', 'pdf-sample41.pdf', 0, '1', '2019-08-06 17:18:30', '2019-08-08 13:49:55'),
(4, 1, 1, 'somali', 'People of the Raven', 'people-of-the-raven', '0000-00-00', 30.00, 15.00, '', '', '0rWQL82s', 0, 3, 'book_55.png', 'People of the Raven', 'demoform110.pdf', 0, '1', '2019-08-06 17:26:27', '2019-08-06 17:42:48'),
(5, 0, 2, 'english', 'Screen', 'screen', '2019-08-08', 0.00, 0.00, '', '30', 'fLSB5Xdq', 0, 5, 'book_411.png', 'Screen', 'book1.pdf', 0, '1', '2019-08-08 08:56:05', '0000-00-00 00:00:00'),
(6, 0, 2, 'english', 'Life of a worm', 'life-of-a-worm', '2019-08-08', 0.00, 0.00, '', '45', 'QWKiBe4S', 0, 10, 'book_211.png', 'Life of a worm', 'book2.pdf', 0, '1', '2019-08-08 08:56:57', '0000-00-00 00:00:00'),
(7, 0, 2, 'somali', 'People of the Raven', 'people-of-the-raven', '2019-08-08', 0.00, 0.00, '', '78', 'QQ9U9oAY', 0, 5, '4f5428927a8575d1b06dbee94e5e3d6a1.png', 'People of the Raven', 'book3.pdf', 0, '1', '2019-08-08 09:01:36', '0000-00-00 00:00:00'),
(8, 1, 1, 'somali', 'Text editor check', 'text-editor-check', '2019-04-01', 50.00, 48.00, 'Anand', '5', 'qAsTRObt', 0, 0, 'book_31.png', 'This is text editor fied is resquired field so I am filling this field.<blockquote>sad sadf sadfsadjsd ak jljklsadf sadf sadf asdf sadf sadfsad asdf asdf asdf dasdf</blockquote>', 'book4.pdf', 0, '1', '2019-08-17 09:52:13', '2019-08-17 09:58:51'),
(9, 1, 1, 'somali', 'ffffffffffffffffffff', 'ffffffffffffffffffff', '2019-07-31', 21.00, 20.00, 'asdasdf', '4', 'sAHIoazB', 0, 2, 'book_23.png', '<p>sadf asdfa sdfasdf<b>asd faasd fasdsdf sadfasd asdf asdf<u>df asdff asdf asdfvvvvvvvv</u></b></p>', 'pdf-sample11.pdf', 0, '2', '2019-08-17 14:01:05', '2019-08-17 14:52:28'),
(10, 1, 1, 'somali', 'TESteset', 'testeset', '2019-06-29', 10.00, 9.00, '33333', '2', 'ZgdNzf2w', 0, 2, 'book_43.png', '<p>dfsgsdfg</p><p>asdfdsfgsdfgasdfasdf</p><pre>dsfgsdfdasfasdfasdfasdfasf</pre><blockquote class=\"blockquote\">fgasdfasdfasdfasdfdsafsa</blockquote>', 'book(1).pdf', 0, '1', '2019-08-17 14:18:22', '2019-08-17 14:24:17'),
(11, 1, 1, 'somali', 'asdfasdf', 'asdfasdf', '2019-04-13', 22.00, 20.00, 'asdsasdsa', '22', 'GbTtnt3d', 0, 10, 'book_63.png', '<p><br></p>', 'Dcmt_1910201815399291071.pdf', 0, '2', '2019-08-17 14:34:44', '0000-00-00 00:00:00'),
(12, 1, 1, 'somali', 'safdasdfas', 'safdasdfas', '0000-00-00', 20.00, 15.00, '', '', '8OzZ7JqK', 0, 0, 'book_32.png', '<p>sad</p>', 'pdf-sample9.pdf', 0, '2', '2019-08-17 14:41:18', '0000-00-00 00:00:00'),
(13, 1, 1, 'somali', 'sadf asdf asdfdsa', 'sadf-asdf-asdfdsa', '0000-00-00', 14.00, 10.00, 'safasdfasdfas', '1', 'nWQznqGf', 0, 0, 'book_33.png', '<p>adsa sad sa ASD AsdaS DASd asdzcx asdasd asdas dasd sad</p>', 'demoform16.pdf', 0, '2', '2019-08-17 14:48:40', '0000-00-00 00:00:00'),
(14, 1, 1, 'somali', 'dfgsdfgsdfgsdfgdsfgsdfgdsfg', 'dfgsdfgsdfgsdfgdsfgsdfgdsfg', '0000-00-00', 0.00, 0.00, '', '', '9wyi8NNu', 0, 0, 'book_24.png', '<p><br></p>', 'demoform17.pdf', 0, '2', '2019-08-17 15:22:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `book_access`
--

CREATE TABLE `book_access` (
  `ba_id` bigint(20) NOT NULL,
  `ba_userid` bigint(20) NOT NULL,
  `ba_bookid` bigint(20) NOT NULL,
  `ba_created` datetime NOT NULL,
  `ba_changedate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `book_access`
--

INSERT INTO `book_access` (`ba_id`, `ba_userid`, `ba_bookid`, `ba_created`, `ba_changedate`) VALUES
(5, 8, 7, '2019-08-09 16:37:22', '2019-08-09 16:37:22'),
(6, 1, 5, '2019-08-11 02:53:05', '2019-08-11 02:53:05');

-- --------------------------------------------------------

--
-- Table structure for table `book_chapters`
--

CREATE TABLE `book_chapters` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `book_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `book_orders`
--

CREATE TABLE `book_orders` (
  `id` bigint(20) NOT NULL,
  `book_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `author_id` bigint(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `total_amt` double NOT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `payment_mode` enum('cc','evc','mpesa') NOT NULL,
  `payment_status` enum('completed','pending','process','declined') NOT NULL,
  `author_payment_status` enum('pending','completed') NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_orders`
--

INSERT INTO `book_orders` (`id`, `book_id`, `user_id`, `author_id`, `qty`, `subtotal`, `tax`, `total_amt`, `transaction_id`, `payment_mode`, `payment_status`, `author_payment_status`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, 1, 30, NULL, 30, '060820191565106682', 'evc', 'completed', 'pending', '2019-08-06', '2019-08-06'),
(2, 2, 5, 1, 1, 47, NULL, 47, 'txn_1F5E3fB7viNOPLYhi5gSk1nx', 'cc', 'completed', 'pending', '2019-08-08', '2019-08-08'),
(3, 1, 10, 1, 1, 30, NULL, 30, '150820191565896992', 'evc', 'pending', 'pending', '2019-08-15', '2019-08-15');

-- --------------------------------------------------------

--
-- Table structure for table `book_reviews`
--

CREATE TABLE `book_reviews` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `book_id` bigint(20) NOT NULL,
  `rating` float NOT NULL,
  `review` text NOT NULL,
  `approved` enum('0','1') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_reviews`
--

INSERT INTO `book_reviews` (`id`, `user_id`, `book_id`, `rating`, `review`, `approved`, `created_at`, `updated_at`) VALUES
(1, 1, 8, 2.5, 'This is good.', '0', '2019-08-17 10:47:47', '2019-08-17 10:47:47');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `c_id` bigint(20) NOT NULL,
  `c_name` varchar(250) NOT NULL,
  `c_slug` varchar(255) DEFAULT NULL,
  `c_image` varchar(255) NOT NULL,
  `c_description` text NOT NULL,
  `c_status` enum('1','0') NOT NULL,
  `c_created` datetime NOT NULL,
  `c_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`c_id`, `c_name`, `c_slug`, `c_image`, `c_description`, `c_status`, `c_created`, `c_modified`) VALUES
(1, 'Business & Investing', 'business-investing', 'categories1.png', '  ', '1', '2019-06-24 00:00:00', '2019-07-23 08:46:13'),
(2, 'Arts & Photography', 'arts-photography', 'categories51.png', 'Arts & Photography', '1', '2019-06-24 00:01:00', '2019-07-23 08:46:59'),
(3, 'Cooking,Food & Wine', 'cooking-food-wine', 'categories3.png', ' Cooking,Food & Wine', '1', '2019-06-24 00:02:00', '2019-07-23 08:47:09'),
(4, 'Computers & Internet', 'computers-internet', 'categories4.png', 'Computers & Internet', '1', '2019-06-24 00:03:00', '2019-07-23 08:47:22'),
(5, 'Entertainment', 'entertainment', 'categories21.png', 'Entertainment', '1', '2019-06-24 00:04:00', '2019-07-23 08:47:34'),
(6, 'Business Management', 'business-management', 'categories6.png', 'Business Management', '1', '2019-06-24 05:00:00', '2019-07-23 08:47:49'),
(7, 'Health, Mind & Body', 'health-mind-body', 'categories7.png', 'Health, Mind & Body', '1', '2019-06-24 00:06:00', '2019-07-23 08:48:00'),
(8, 'Personal Development', 'personal-development', 'categories8.png', 'Personal Development', '1', '2019-06-24 00:07:00', '2019-07-23 08:48:10'),
(9, 'Biographies & Memories', 'biographies-memories', 'categories_9.png', 'Biographies & Memories', '1', '2019-06-24 00:08:00', '2019-07-23 08:48:30'),
(10, 'Soft Skills', 'soft-skills', 'categories10.png', 'Soft Skills', '1', '2019-06-24 00:09:00', '2019-07-23 08:48:51');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` bigint(20) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `first_name`, `last_name`, `email`, `subject`, `message`, `created_at`) VALUES
(1, 'Paresh', 'Nagar', 'paresh3779@yahoo.co.in', 'Test Subject', 'Test Message', '2019-08-05'),
(2, 'Paresh', 'Nagar', 'paresh3779@yahoo.co.in', 'Test Subject', 'This is test message', '2019-08-05'),
(3, 'Paresh', 'nagar', 'paresh3779@yahoo.co.in', 'Test Subject', 'fhtfh', '2019-08-05'),
(4, 'Paresh', 'Nagar', 'paresh3779@yahoo.co.in', 'dfgd', 'fgdg', '2019-08-05'),
(5, 'Paresh', 'Nagar', 'paresh3779@yahoo.co.in', 'dfgd', 'fgdg', '2019-08-05'),
(6, 'Paresh', 'Nagar', 'paresh3779@yahoo.co.in', 'dfgd', 'fgdg', '2019-08-05');

-- --------------------------------------------------------

--
-- Table structure for table `emailsubscriber`
--

CREATE TABLE `emailsubscriber` (
  `es_id` bigint(20) NOT NULL,
  `es_firstname` varchar(255) NOT NULL,
  `es_lastname` varchar(255) NOT NULL,
  `es_email` varchar(150) NOT NULL,
  `es_planid` int(2) NOT NULL,
  `es_status` enum('1','0') NOT NULL,
  `es_created` datetime NOT NULL,
  `es_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `emailsubscriber`
--

INSERT INTO `emailsubscriber` (`es_id`, `es_firstname`, `es_lastname`, `es_email`, `es_planid`, `es_status`, `es_created`, `es_modified`) VALUES
(6, 'paresh', 'nagar', 'paresh3779@gmail.com', 1, '1', '2019-08-09 16:36:56', '0000-00-00 00:00:00'),
(7, 'paresh', 'nagar', 'paresh3779@yahoo.co.in', 1, '1', '2019-08-11 02:44:11', '0000-00-00 00:00:00'),
(8, 'aax', 'xx', 'aa@aa.1sd', 1, '1', '2019-08-22 09:44:57', '0000-00-00 00:00:00'),
(9, 'sdsdfs', 'dfddfsdfsdf', 'sdf@ASdf.df', 1, '1', '2019-08-22 12:56:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `membershipplan`
--

CREATE TABLE `membershipplan` (
  `mp_id` bigint(20) NOT NULL,
  `mp_name` varchar(200) NOT NULL,
  `mp_price` decimal(8,0) NOT NULL,
  `mp_validity` varchar(250) NOT NULL,
  `mp_descriptions` text NOT NULL,
  `mp_created` datetime NOT NULL,
  `mp_modified` datetime NOT NULL,
  `mp_status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `membershipplan`
--

INSERT INTO `membershipplan` (`mp_id`, `mp_name`, `mp_price`, `mp_validity`, `mp_descriptions`, `mp_created`, `mp_modified`, `mp_status`) VALUES
(1, 'Pletinum', '30', 'Monthly Access', 'Repeate predefined chunks discovered the undoubtable sourece going to use a passage of him discovered the undoubtable source repeat predefined chunks.', '2019-06-26 15:09:12', '2019-07-23 07:48:45', '1'),
(2, 'Silver Plan', '50', 'Yearly Access', 'Repeate predefined chunks discovered the undoubtable sourece going to use a passage of him discovered the undoubtable source repeat predefined chunks.', '2019-06-27 07:37:05', '2019-07-17 08:09:49', '1'),
(3, 'Golden Plans', '100', 'Lifetime Accessds', 'Repeate predefined chunks discovered the undoubtable sourece going to use a passage of him discovered the undoubtable source repeat predefined chunks.', '2019-06-27 07:37:44', '2019-07-20 13:53:24', '1');

-- --------------------------------------------------------

--
-- Table structure for table `option_table`
--

CREATE TABLE `option_table` (
  `ot_id` bigint(20) NOT NULL,
  `ot_keyname` varchar(190) NOT NULL,
  `ot_value` text NOT NULL,
  `ot_created` datetime NOT NULL,
  `ot_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `paymentdetails`
--

CREATE TABLE `paymentdetails` (
  `pd_id` bigint(20) NOT NULL,
  `pd_txnid` text NOT NULL,
  `pd_planid` bigint(20) NOT NULL,
  `pd_bookid` bigint(20) NOT NULL,
  `pd_planprice` decimal(8,0) NOT NULL,
  `pd_userid` bigint(20) NOT NULL,
  `pd_currency` varchar(20) NOT NULL,
  `pd_payby` enum('cc','evc','mpesa') NOT NULL,
  `pd_status` varchar(50) NOT NULL,
  `pd_approvedate` datetime NOT NULL,
  `pd_expiredate` datetime NOT NULL,
  `pd_cardholder` varchar(255) NOT NULL,
  `pd_cardnumber` varchar(20) NOT NULL,
  `pd_cvvnumber` varchar(4) NOT NULL,
  `pd_cartexpmonth` int(2) NOT NULL,
  `pd_cardexpyear` int(5) NOT NULL,
  `pd_chargeid` varchar(255) NOT NULL,
  `pd_created` datetime NOT NULL,
  `pd_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paymentdetails`
--

INSERT INTO `paymentdetails` (`pd_id`, `pd_txnid`, `pd_planid`, `pd_bookid`, `pd_planprice`, `pd_userid`, `pd_currency`, `pd_payby`, `pd_status`, `pd_approvedate`, `pd_expiredate`, `pd_cardholder`, `pd_cardnumber`, `pd_cvvnumber`, `pd_cartexpmonth`, `pd_cardexpyear`, `pd_chargeid`, `pd_created`, `pd_modified`) VALUES
(5, '090820191565361442', 2, 7, '50', 8, 'usd', 'evc', 'completed', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', 0, 0, '', '2019-08-09 16:37:22', '0000-00-00 00:00:00'),
(6, '110820191565509985', 1, 5, '30', 1, 'usd', 'evc', 'pending', '2019-08-01 00:00:00', '2019-08-30 00:00:00', '', '', '', 0, 0, '', '2019-08-11 02:53:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `s_id` bigint(20) NOT NULL,
  `s_heading` varchar(255) NOT NULL,
  `s_subtitle` varchar(255) NOT NULL,
  `s_tagline` varchar(255) NOT NULL,
  `s_link` varchar(255) NOT NULL,
  `s_image` text NOT NULL,
  `s_order` int(5) NOT NULL,
  `s_status` enum('1','0') NOT NULL,
  `s_created` datetime NOT NULL,
  `s_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`s_id`, `s_heading`, `s_subtitle`, `s_tagline`, `s_link`, `s_image`, `s_order`, `s_status`, `s_created`, `s_modified`) VALUES
(1, 'The Best BookStores Online', 'Ower 3.5 Milion ebook read with on limits...', 'E-SOMA. BUUG WALWA', '', 'slider_1.png', 2, '1', '0000-00-00 00:00:00', '2019-07-21 15:09:03'),
(3, 'The Best BookStores Online', 'Ower 3.5 Milion ebook read with on limits...', 'E-SOMA. BUUG WALWA', 'http://localhost/ebook/dashboard/vvv/1', 'slider_11.png', 1, '1', '2019-06-27 15:17:52', '2019-07-17 13:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `user_credentials`
--

CREATE TABLE `user_credentials` (
  `uc_id` bigint(20) NOT NULL,
  `uc_authid` varchar(150) NOT NULL,
  `uc_firstname` varchar(150) NOT NULL,
  `uc_lastname` varchar(150) NOT NULL,
  `uc_email` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `uc_mobile` varchar(15) NOT NULL,
  `uc_password` varchar(255) NOT NULL,
  `uc_image` varchar(255) NOT NULL,
  `uc_role` int(2) NOT NULL,
  `uc_secrete` varchar(40) NOT NULL,
  `uc_address` text NOT NULL,
  `uc_created` datetime NOT NULL,
  `uc_modified` datetime NOT NULL,
  `uc_status` enum('1','0') NOT NULL,
  `uc_active` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_credentials`
--

INSERT INTO `user_credentials` (`uc_id`, `uc_authid`, `uc_firstname`, `uc_lastname`, `uc_email`, `uc_mobile`, `uc_password`, `uc_image`, `uc_role`, `uc_secrete`, `uc_address`, `uc_created`, `uc_modified`, `uc_status`, `uc_active`) VALUES
(1, '', 'paresh', 'nagar', 'vasutamrakar@yahoo.in', '787878787878', 'e10adc3949ba59abbe56e057f20f883e', '', 4, '', 'hjfhfh', '2019-08-05 14:48:08', '2019-08-08 17:16:27', '1', '1'),
(2, '', 'Admin', 'Admin', 'vasu.impetrosys@gmail.com', '', '81dc9bdb52d04dc20036dbd8313ed055', '', 1, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', '1'),
(8, '', 'paresh', 'nagar', 'vasu1tamra@gmail.com', '787878787878', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', 'test address', '2019-08-09 16:37:22', '0000-00-00 00:00:00', '1', '1'),
(9, '', 'Ali', 'Hussein ', 'Salebandahir5@gmail.com', '+252615666066', '0a688f93f3f2ec791b710dbba84e399c', '', 4, '', 'Mogadishu, Banadir Somalia ', '2019-08-11 09:47:40', '2019-08-12 06:15:34', '1', '1'),
(10, '', 'Idris ', 'Abdi ', 'Ahahha@gmail.com', '6672045626', '0f35b6f8aa53b7858b0ef5de4bb63ac5', '', 5, '', '', '2019-08-15 14:23:10', '0000-00-00 00:00:00', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `ur_id` int(11) NOT NULL,
  `ur_name` varchar(250) NOT NULL,
  `uc_created` datetime NOT NULL,
  `uc_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `v_id` bigint(20) NOT NULL,
  `v_language` enum('english','somali') NOT NULL,
  `v_category` enum('Testimonial Videos','Promotional Videos','Training and Tools Videos','Premium Training Videos') NOT NULL,
  `v_title` varchar(255) NOT NULL,
  `v_slug` varchar(255) DEFAULT NULL,
  `v_premium` enum('true','false') NOT NULL,
  `v_description` text NOT NULL,
  `v_image` varchar(255) NOT NULL,
  `v_videoid` varchar(255) NOT NULL,
  `v_like` bigint(20) NOT NULL,
  `v_status` enum('1','0') NOT NULL,
  `v_created` datetime NOT NULL,
  `v_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `video_likes`
--

CREATE TABLE `video_likes` (
  `id` bigint(20) NOT NULL,
  `video_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `video_like` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `book_access`
--
ALTER TABLE `book_access`
  ADD PRIMARY KEY (`ba_id`);

--
-- Indexes for table `book_chapters`
--
ALTER TABLE `book_chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_orders`
--
ALTER TABLE `book_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_reviews`
--
ALTER TABLE `book_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emailsubscriber`
--
ALTER TABLE `emailsubscriber`
  ADD PRIMARY KEY (`es_id`),
  ADD UNIQUE KEY `esmail` (`es_email`);

--
-- Indexes for table `membershipplan`
--
ALTER TABLE `membershipplan`
  ADD PRIMARY KEY (`mp_id`);

--
-- Indexes for table `option_table`
--
ALTER TABLE `option_table`
  ADD PRIMARY KEY (`ot_id`),
  ADD UNIQUE KEY `otkeyname` (`ot_keyname`);

--
-- Indexes for table `paymentdetails`
--
ALTER TABLE `paymentdetails`
  ADD PRIMARY KEY (`pd_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `user_credentials`
--
ALTER TABLE `user_credentials`
  ADD PRIMARY KEY (`uc_id`),
  ADD UNIQUE KEY `ucemail` (`uc_email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`ur_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`v_id`);

--
-- Indexes for table `video_likes`
--
ALTER TABLE `video_likes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `a_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `b_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `book_access`
--
ALTER TABLE `book_access`
  MODIFY `ba_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `book_chapters`
--
ALTER TABLE `book_chapters`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_orders`
--
ALTER TABLE `book_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `book_reviews`
--
ALTER TABLE `book_reviews`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `c_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `emailsubscriber`
--
ALTER TABLE `emailsubscriber`
  MODIFY `es_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `membershipplan`
--
ALTER TABLE `membershipplan`
  MODIFY `mp_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `option_table`
--
ALTER TABLE `option_table`
  MODIFY `ot_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paymentdetails`
--
ALTER TABLE `paymentdetails`
  MODIFY `pd_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `s_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_credentials`
--
ALTER TABLE `user_credentials`
  MODIFY `uc_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `ur_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `v_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `video_likes`
--
ALTER TABLE `video_likes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
