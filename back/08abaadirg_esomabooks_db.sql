-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 07, 2019 at 11:57 PM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abaadirg_esomabooks_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `a_id` bigint(20) NOT NULL,
  `a_name` varchar(255) NOT NULL,
  `a_lang` varchar(255) DEFAULT NULL,
  `a_fK_of_uc_id` bigint(20) NOT NULL,
  `a_publishbooks` bigint(20) NOT NULL,
  `a_description` text NOT NULL,
  `a_paypal` varchar(255) NOT NULL,
  `a_mmoney` varchar(255) NOT NULL,
  `a_facebook` varchar(255) NOT NULL,
  `a_twitter` varchar(255) NOT NULL,
  `a_gplush` varchar(255) NOT NULL,
  `a_instagram` varchar(255) NOT NULL,
  `a_status` enum('1','0') NOT NULL,
  `a_created` datetime NOT NULL,
  `a_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`a_id`, `a_name`, `a_lang`, `a_fK_of_uc_id`, `a_publishbooks`, `a_description`, `a_paypal`, `a_mmoney`, `a_facebook`, `a_twitter`, `a_gplush`, `a_instagram`, `a_status`, `a_created`, `a_modified`) VALUES
(2, '', 'somali', 5, 1970, '<p style=\"margin-top: 1rem; margin-bottom: 1rem; font-size: 14px; color: rgb(128, 128, 128); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.</p><p style=\"margin-top: 1rem; margin-bottom: 1rem; font-size: 14px; color: rgb(128, 128, 128); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.</p>   ', '', '', 'https://www.facebook.com/', 'https://twitter.com', 'https://www.google.com/', 'https://www.instagram.com/', '1', '2019-07-21 15:55:54', '2019-07-23 09:16:45'),
(3, '', 'somali', 6, 1570, '<p style=\"margin-top: 1rem; margin-bottom: 1rem; font-size: 14px; color: rgb(128, 128, 128); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.</p><p style=\"margin-top: 1rem; margin-bottom: 1rem; font-size: 14px; color: rgb(128, 128, 128); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.</p>  ', '', '', 'https://www.facebook.com/', 'https://twitter.com', 'https://www.google.com/', 'https://www.instagram.com/', '1', '2019-07-21 15:58:20', '2019-07-23 09:17:02'),
(4, '', 'somali', 7, 1970, '<p style=\"margin-top: 1rem; margin-bottom: 1rem; font-size: 14px; color: rgb(128, 128, 128); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.</p><p style=\"margin-top: 1rem; margin-bottom: 1rem; font-size: 14px; color: rgb(128, 128, 128); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.</p>  ', '', '', 'https://www.facebook.com/', 'https://twitter.com', 'https://www.google.com/', 'https://www.instagram.com/', '1', '2019-07-21 16:02:45', '2019-07-23 09:27:03'),
(5, '', 'somali', 10, 0, 'My Full Bio', '', '7878787878', '', '', '', '', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `b_id` bigint(20) NOT NULL,
  `b_fk_of_aid` bigint(20) NOT NULL,
  `b_fk_of_uid` bigint(20) NOT NULL,
  `b_language` enum('somali','english') NOT NULL,
  `b_title` varchar(255) NOT NULL,
  `b_slug` varchar(255) DEFAULT NULL,
  `b_published` date NOT NULL,
  `b_price` double(8,2) NOT NULL,
  `b_publisher` varchar(255) NOT NULL,
  `b_bookpages` varchar(255) NOT NULL,
  `b_isbn` varchar(255) NOT NULL,
  `b_rating` int(5) NOT NULL,
  `b_category` int(11) NOT NULL,
  `b_image` text NOT NULL,
  `b_description` text NOT NULL,
  `b_file` text NOT NULL,
  `b_downloads` bigint(20) NOT NULL,
  `b_status` enum('0','1','2') NOT NULL,
  `b_created` datetime NOT NULL,
  `b_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`b_id`, `b_fk_of_aid`, `b_fk_of_uid`, `b_language`, `b_title`, `b_slug`, `b_published`, `b_price`, `b_publisher`, `b_bookpages`, `b_isbn`, `b_rating`, `b_category`, `b_image`, `b_description`, `b_file`, `b_downloads`, `b_status`, `b_created`, `b_modified`) VALUES
(10, 7, 1, 'english', 'Gear', 'gear', '2015-05-24', 23.00, '', '', '', 4, 2, 'book_1.png', 'Gear', 'book.pdf', 0, '1', '2019-06-25 13:18:04', '2019-07-24 07:22:45'),
(11, 7, 1, 'english', 'War & Peace', 'war-peace', '2018-04-24', 20.00, '', '', '', 1, 6, 'book_4.png', 'War &amp; Peace', 'pdf-sample3.pdf', 0, '1', '2019-06-25 13:18:53', '2019-07-24 07:23:05'),
(12, 7, 1, 'english', 'War & Peace', 'war-peace', '2001-05-30', 45.00, '', '', '', 4, 7, '9cf582c45558e41f4c3f0ea859899f58.png', '<span style=\"font-family: &quot;Helvetica Neue&quot;, Roboto, Arial, &quot;Droid Sans&quot;, sans-serif; white-space: nowrap;\">War &amp; Peace</span>', '8dd95ecb22e109e1c50c77159cdfd36d.pdf', 0, '1', '2019-06-25 13:22:22', '2019-07-24 07:28:30'),
(15, 6, 1, 'somali', 'War & Peace', 'war-peace', '2019-07-10', 30.00, '', '', '', 3, 10, 'book_6.png', '<span style=\"font-family: &quot;Helvetica Neue&quot;, Roboto, Arial, &quot;Droid Sans&quot;, sans-serif; white-space: nowrap; background-color: rgb(249, 249, 249);\">War &amp; Peace</span>', 'Mail_A_Ticket2.pdf', 0, '1', '2019-07-08 15:54:18', '2019-07-24 07:28:58'),
(16, 6, 1, 'somali', 'People Of the Raven', 'people-of-the-raven', '2004-07-14', 30.00, '', '', '', 3, 10, 'book_51.png', 'People Of the Raven', 'demoform12.pdf', 0, '1', '2019-07-12 13:52:48', '2019-07-24 07:29:25'),
(18, 6, 1, 'english', 'Screen flat Design', 'screen-flat-design', '2019-07-09', 30.00, '', '', '', 4, 10, 'book_41.png', 'Screen flat Design', 'pdf-sample(2).pdf', 0, '1', '2019-07-12 15:45:17', '2019-07-24 07:29:35'),
(19, 5, 1, 'english', 'People of the Raven', 'people-of-the-raven', '2002-05-02', 40.00, '', '', '', 3, 6, 'book_52.png', 'People of the Raven', 'demoform13.pdf', 0, '1', '2019-07-15 07:10:30', '2019-07-24 07:29:45'),
(20, 5, 1, 'english', 'People of the Raven', 'people-of-the-raven', '1991-02-02', 50.00, '', '', '', 0, 6, 'book_53.png', 'People of the Raven', 'demoform14.pdf', 0, '1', '2019-07-19 08:31:52', '2019-07-24 07:32:41'),
(21, 10, 10, 'english', 'New Book ', 'new-book', '2010-07-14', 30.49, 'James', '78', 'WdB6a8T7', 0, 1, 'book_22.png', 'This is amazing book for beginners', 'demoform151.pdf', 0, '1', '2019-08-04 08:35:08', '2019-08-04 08:37:35');

-- --------------------------------------------------------

--
-- Table structure for table `book_access`
--

CREATE TABLE `book_access` (
  `ba_id` bigint(20) NOT NULL,
  `ba_userid` bigint(20) NOT NULL,
  `ba_bookid` bigint(20) NOT NULL,
  `ba_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `book_access`
--

INSERT INTO `book_access` (`ba_id`, `ba_userid`, `ba_bookid`, `ba_created`) VALUES
(1, 35, 14, '0000-00-00 00:00:00'),
(2, 36, 14, '0000-00-00 00:00:00'),
(3, 37, 12, '0000-00-00 00:00:00'),
(4, 38, 14, '0000-00-00 00:00:00'),
(5, 39, 15, '0000-00-00 00:00:00'),
(6, 43, 14, '0000-00-00 00:00:00'),
(7, 45, 17, '0000-00-00 00:00:00'),
(8, 53, 17, '0000-00-00 00:00:00'),
(9, 60, 17, '0000-00-00 00:00:00'),
(10, 65, 17, '0000-00-00 00:00:00'),
(11, 67, 17, '0000-00-00 00:00:00'),
(12, 69, 16, '0000-00-00 00:00:00'),
(13, 70, 17, '0000-00-00 00:00:00'),
(14, 71, 17, '0000-00-00 00:00:00'),
(15, 72, 17, '0000-00-00 00:00:00'),
(16, 73, 16, '0000-00-00 00:00:00'),
(17, 74, 16, '0000-00-00 00:00:00'),
(18, 75, 17, '0000-00-00 00:00:00'),
(19, 76, 17, '0000-00-00 00:00:00'),
(20, 77, 17, '0000-00-00 00:00:00'),
(21, 78, 17, '0000-00-00 00:00:00'),
(22, 79, 17, '0000-00-00 00:00:00'),
(23, 80, 17, '2019-07-17 10:13:51'),
(24, 81, 16, '2019-07-17 10:15:49'),
(25, 82, 17, '2019-07-17 10:18:15'),
(26, 3, 17, '2019-07-17 15:21:15'),
(27, 4, 17, '2019-07-17 09:22:27'),
(28, 8, 16, '2019-07-24 07:57:11'),
(29, 11, 16, '2019-08-06 11:08:53');

-- --------------------------------------------------------

--
-- Table structure for table `book_chapters`
--

CREATE TABLE `book_chapters` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `book_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_chapters`
--

INSERT INTO `book_chapters` (`id`, `title`, `description`, `book_id`) VALUES
(3, 'Chapter 1', '<span style=\"color: rgb(128, 128, 128); font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; font-size: 14px;\">W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.</span>', 19),
(4, 'Chapter 2', '<span style=\"color: rgb(128, 128, 128); font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; font-size: 14px;\">W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.</span>', 19);

-- --------------------------------------------------------

--
-- Table structure for table `book_orders`
--

CREATE TABLE `book_orders` (
  `id` bigint(20) NOT NULL,
  `book_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `author_id` bigint(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `total_amt` double NOT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `payment_status` enum('completed','pending','process','declined') NOT NULL,
  `author_payment_status` enum('pending','completed') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `book_reviews`
--

CREATE TABLE `book_reviews` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `book_id` bigint(20) NOT NULL,
  `rating` float NOT NULL,
  `review` text NOT NULL,
  `approved` enum('0','1') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_reviews`
--

INSERT INTO `book_reviews` (`id`, `user_id`, `book_id`, `rating`, `review`, `approved`, `created_at`, `updated_at`) VALUES
(5, 1, 19, 4, 'Very Helpful Book.', '1', '2019-07-26 08:23:11', '2019-07-26 08:23:11'),
(6, 1, 16, 3.5, 'Very Helpful Book...', '1', '2019-07-28 01:11:45', '2019-07-28 01:11:45');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `c_id` bigint(20) NOT NULL,
  `c_name` varchar(250) NOT NULL,
  `c_slug` varchar(255) DEFAULT NULL,
  `c_image` varchar(255) NOT NULL,
  `c_description` text NOT NULL,
  `c_status` enum('1','0') NOT NULL,
  `c_created` datetime NOT NULL,
  `c_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`c_id`, `c_name`, `c_slug`, `c_image`, `c_description`, `c_status`, `c_created`, `c_modified`) VALUES
(1, 'Business & Investing', 'business-investing', 'categories1.png', '  ', '1', '2019-06-24 00:00:00', '2019-07-23 08:46:13'),
(2, 'Arts & Photography', 'arts-photography', 'categories51.png', 'Arts & Photography', '1', '2019-06-24 00:01:00', '2019-07-23 08:46:59'),
(3, 'Cooking,Food & Wine', 'cooking-food-wine', 'categories3.png', ' Cooking,Food & Wine', '1', '2019-06-24 00:02:00', '2019-07-23 08:47:09'),
(4, 'Computers & Internet', 'computers-internet', 'categories4.png', 'Computers & Internet', '1', '2019-06-24 00:03:00', '2019-07-23 08:47:22'),
(5, 'Entertainment', 'entertainment', 'categories21.png', 'Entertainment', '1', '2019-06-24 00:04:00', '2019-07-23 08:47:34'),
(6, 'Business Management', 'business-management', 'categories6.png', 'Business Management', '1', '2019-06-24 05:00:00', '2019-07-23 08:47:49'),
(7, 'Health, Mind & Body', 'health-mind-body', 'categories7.png', 'Health, Mind & Body', '1', '2019-06-24 00:06:00', '2019-07-23 08:48:00'),
(8, 'Personal Development', 'personal-development', 'categories8.png', 'Personal Development', '1', '2019-06-24 00:07:00', '2019-07-23 08:48:10'),
(9, 'Biographies & Memories', 'biographies-memories', 'categories_9.png', 'Biographies & Memories', '1', '2019-06-24 00:08:00', '2019-07-23 08:48:30'),
(10, 'Soft Skills', 'soft-skills', 'categories10.png', 'Soft Skills', '1', '2019-06-24 00:09:00', '2019-07-23 08:48:51');

-- --------------------------------------------------------

--
-- Table structure for table `emailSubscriber`
--

CREATE TABLE `emailSubscriber` (
  `es_id` bigint(20) NOT NULL,
  `es_firstname` varchar(255) NOT NULL,
  `es_lastname` varchar(255) NOT NULL,
  `es_email` varchar(150) NOT NULL,
  `es_planid` int(2) NOT NULL,
  `es_status` enum('1','0') NOT NULL,
  `es_created` datetime NOT NULL,
  `es_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `emailSubscriber`
--

INSERT INTO `emailSubscriber` (`es_id`, `es_firstname`, `es_lastname`, `es_email`, `es_planid`, `es_status`, `es_created`, `es_modified`) VALUES
(1, 'user', 'lastname', 'user@gmail.com', 1, '1', '2019-07-17 15:20:23', '0000-00-00 00:00:00'),
(2, 'vas', 'tam', 'vasu1tamra@gmail.com', 1, '1', '2019-07-17 09:21:22', '0000-00-00 00:00:00'),
(3, 'paresh', 'nagar', 'paresh3779@gmail.com', 1, '1', '2019-07-23 16:57:57', '0000-00-00 00:00:00'),
(4, 'Hdjeb ', 'Eheheheh', 'Snsnsn@gmail.com', 2, '1', '2019-07-28 19:45:34', '0000-00-00 00:00:00'),
(5, 'Ali', 'Hussein ', 'Takaroow55@gmail.com', 1, '1', '2019-07-31 15:29:13', '0000-00-00 00:00:00'),
(6, 'Ali', 'Hussein ', 'Salebandahir5@gmail.com', 1, '1', '2019-08-03 07:27:51', '0000-00-00 00:00:00'),
(7, 'vikram', 'test', 'vikram@yopmail.com', 1, '1', '2019-08-06 05:25:54', '0000-00-00 00:00:00'),
(8, 'Idris ', 'Andi ', 'Whahah@gmail.com', 1, '1', '2019-08-07 16:08:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `membershipplan`
--

CREATE TABLE `membershipplan` (
  `mp_id` bigint(20) NOT NULL,
  `mp_name` varchar(200) NOT NULL,
  `mp_price` decimal(8,0) NOT NULL,
  `mp_validity` varchar(250) NOT NULL,
  `mp_descriptions` text NOT NULL,
  `mp_created` datetime NOT NULL,
  `mp_modified` datetime NOT NULL,
  `mp_status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `membershipplan`
--

INSERT INTO `membershipplan` (`mp_id`, `mp_name`, `mp_price`, `mp_validity`, `mp_descriptions`, `mp_created`, `mp_modified`, `mp_status`) VALUES
(1, 'Pletinum', '30', 'Monthly Access', 'Repeate predefined chunks discovered the undoubtable sourece going to use a passage of him discovered the undoubtable source repeat predefined chunks.', '2019-06-26 15:09:12', '2019-07-23 07:48:45', '1'),
(2, 'Silver Plan', '50', 'Yearly Access', 'Repeate predefined chunks discovered the undoubtable sourece going to use a passage of him discovered the undoubtable source repeat predefined chunks.', '2019-06-27 07:37:05', '2019-07-17 08:09:49', '1'),
(3, 'Golden Plans', '100', 'Lifetime Accessds', 'Repeate predefined chunks discovered the undoubtable sourece going to use a passage of him discovered the undoubtable source repeat predefined chunks.', '2019-06-27 07:37:44', '2019-07-20 13:53:24', '1');

-- --------------------------------------------------------

--
-- Table structure for table `option_table`
--

CREATE TABLE `option_table` (
  `ot_id` bigint(20) NOT NULL,
  `ot_keyname` varchar(190) NOT NULL,
  `ot_value` text NOT NULL,
  `ot_created` datetime NOT NULL,
  `ot_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `paymentDetails`
--

CREATE TABLE `paymentDetails` (
  `pd_id` bigint(20) NOT NULL,
  `pd_txnid` text NOT NULL,
  `pd_planid` bigint(20) NOT NULL,
  `pd_bookid` bigint(20) NOT NULL,
  `pd_planprice` decimal(8,0) NOT NULL,
  `pd_userid` bigint(20) NOT NULL,
  `pd_currency` varchar(20) NOT NULL,
  `pd_payby` enum('cc','evc','mpesa') NOT NULL,
  `pd_status` varchar(50) NOT NULL,
  `pd_cardholder` varchar(255) NOT NULL,
  `pd_cardnumber` varchar(20) NOT NULL,
  `pd_cvvnumber` varchar(4) NOT NULL,
  `pd_cartexpmonth` int(2) NOT NULL,
  `pd_cardexpyear` int(5) NOT NULL,
  `pd_chargeid` varchar(255) NOT NULL,
  `pd_created` datetime NOT NULL,
  `pd_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paymentDetails`
--

INSERT INTO `paymentDetails` (`pd_id`, `pd_txnid`, `pd_planid`, `pd_bookid`, `pd_planprice`, `pd_userid`, `pd_currency`, `pd_payby`, `pd_status`, `pd_cardholder`, `pd_cardnumber`, `pd_cvvnumber`, `pd_cartexpmonth`, `pd_cardexpyear`, `pd_chargeid`, `pd_created`, `pd_modified`) VALUES
(1, 'txn_1ExD4IB7viNOPLYhIq34Focp', 1, 0, '30', 3, 'usd', 'cc', 'succeeded', 'user lastname', '4242424242424242', '123', 12, 2024, 'ch_1ExD4IB7viNOPLYh9kDyEvm0', '2019-07-17 15:21:15', '0000-00-00 00:00:00'),
(2, '170720191563373347', 1, 0, '30', 4, 'usd', 'evc', 'waiting', '', '', '', 0, 0, '', '2019-07-17 09:22:27', '0000-00-00 00:00:00'),
(3, '240720191563947831', 3, 0, '100', 8, 'usd', 'mpesa', 'waiting', '', '', '', 0, 0, '', '2019-07-24 07:57:11', '0000-00-00 00:00:00'),
(4, '060820191565107733', 1, 16, '30', 11, 'usd', 'evc', 'waiting', '', '', '', 0, 0, '', '2019-08-06 11:08:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `s_id` bigint(20) NOT NULL,
  `s_heading` varchar(255) NOT NULL,
  `s_subtitle` varchar(255) NOT NULL,
  `s_tagline` varchar(255) NOT NULL,
  `s_link` varchar(255) NOT NULL,
  `s_image` text NOT NULL,
  `s_order` int(5) NOT NULL,
  `s_status` enum('1','0') NOT NULL,
  `s_created` datetime NOT NULL,
  `s_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`s_id`, `s_heading`, `s_subtitle`, `s_tagline`, `s_link`, `s_image`, `s_order`, `s_status`, `s_created`, `s_modified`) VALUES
(1, 'The Best BookStores Online', 'Ower 3.5 Milion ebook read with on limits...', 'E-SOMA. BUUG WALWA', '', 'slider_1.png', 2, '1', '0000-00-00 00:00:00', '2019-07-21 15:09:03'),
(3, 'The Best BookStores Online', 'Ower 3.5 Milion ebook read with on limits...', 'E-SOMA. BUUG WALWA', 'http://localhost/ebook/dashboard/vvv/1', 'slider_11.png', 1, '1', '2019-06-27 15:17:52', '2019-07-17 13:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `user_credentials`
--

CREATE TABLE `user_credentials` (
  `uc_id` bigint(20) NOT NULL,
  `uc_authid` varchar(150) NOT NULL,
  `uc_firstname` varchar(150) NOT NULL,
  `uc_lastname` varchar(150) NOT NULL,
  `uc_email` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `uc_mobile` varchar(15) NOT NULL,
  `uc_password` varchar(255) NOT NULL,
  `uc_image` varchar(255) NOT NULL,
  `uc_role` int(2) NOT NULL,
  `uc_secrete` varchar(40) NOT NULL,
  `uc_address` text NOT NULL,
  `uc_created` datetime NOT NULL,
  `uc_modified` datetime NOT NULL,
  `uc_status` enum('1','0') NOT NULL,
  `uc_active` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_credentials`
--

INSERT INTO `user_credentials` (`uc_id`, `uc_authid`, `uc_firstname`, `uc_lastname`, `uc_email`, `uc_mobile`, `uc_password`, `uc_image`, `uc_role`, `uc_secrete`, `uc_address`, `uc_created`, `uc_modified`, `uc_status`, `uc_active`) VALUES
(1, '', 'Vasu', 'Tamrakar', 'vasu.impetrosys@gmail.com', '+917000667679', '81dc9bdb52d04dc20036dbd8313ed055', '', 1, '', 'Indore India.', '2019-07-17 00:00:00', '0000-00-00 00:00:00', '1', '1'),
(3, '', 'user', 'lastname', 'user@gmail.com', '+919854854768', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', '', '2019-07-17 15:21:09', '0000-00-00 00:00:00', '0', '0'),
(4, '', 'vas', 'tam', 'vasu1tamra@gmail.com', '+9170008787', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', 'we In indore.', '2019-07-17 09:22:27', '0000-00-00 00:00:00', '1', '1'),
(5, '', 'Jamila ', 'Gordon', 'JamilaTest@gmail.com', '232332332', '81dc9bdb52d04dc20036dbd8313ed055', 'jamila.png', 4, '', '', '2019-07-21 15:55:54', '0000-00-00 00:00:00', '1', '1'),
(6, '', 'Almal', 'Aden', 'almanTest@gmail.com', '534543535', 'c9340e945d936c859c9f724d60677333', 'alman.png', 4, '', '', '2019-07-21 15:58:20', '0000-00-00 00:00:00', '1', '1'),
(7, '', 'Edmonton', 'Edmonton', 'EdmontonTest@gmail.com', '454335', '2dd8ce6c45d669696a37fb7a0e5d57c6', 'Edmonton.png', 4, '', '', '2019-07-21 16:02:45', '0000-00-00 00:00:00', '1', '1'),
(8, '', 'paresh', 'nagar', 'paresh3779@gmail.com', 'dfsfsdfsfds', 'adb1d9cd025762e3f30fe720f9363a48', '', 5, '', 'sdfsf', '2019-07-24 07:57:11', '0000-00-00 00:00:00', '0', '0'),
(10, '', 'Paresh', 'Nagar', 'paresh3779@yahoo.co.in', '7878787878', 'e10adc3949ba59abbe56e057f20f883e', '', 4, '', 'Full Address', '2019-08-04 08:30:30', '2019-08-04 08:31:56', '1', '1'),
(11, '', 'Ali', 'Hussein ', 'Takaroow55@gmail.com', '+252615666066', '0a688f93f3f2ec791b710dbba84e399c', '', 5, '', 'Km4', '2019-08-06 11:08:53', '0000-00-00 00:00:00', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `ur_id` int(11) NOT NULL,
  `ur_name` varchar(250) NOT NULL,
  `uc_created` datetime NOT NULL,
  `uc_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`ur_id`, `ur_name`, `uc_created`, `uc_modified`) VALUES
(1, 'Administrator', '2019-06-20 05:14:00', '0000-00-00 00:00:00'),
(2, 'Manager', '2019-06-20 05:15:00', '0000-00-00 00:00:00'),
(3, 'Contributor', '2019-06-20 05:38:00', '0000-00-00 00:00:00'),
(4, 'Author', '2019-06-20 05:40:00', '0000-00-00 00:00:00'),
(5, 'Customer', '2019-07-03 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `v_id` bigint(20) NOT NULL,
  `v_language` enum('english','somali') NOT NULL,
  `v_category` enum('Testimonial Videos','Promotional Videos','Training and Tools Videos','Premium Training Videos') NOT NULL,
  `v_title` varchar(255) NOT NULL,
  `v_slug` varchar(255) DEFAULT NULL,
  `v_premium` enum('true','false') NOT NULL,
  `v_description` text NOT NULL,
  `v_image` varchar(255) NOT NULL,
  `v_videoid` varchar(255) NOT NULL,
  `v_like` bigint(20) NOT NULL,
  `v_status` enum('1','0') NOT NULL,
  `v_created` datetime NOT NULL,
  `v_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`v_id`, `v_language`, `v_category`, `v_title`, `v_slug`, `v_premium`, `v_description`, `v_image`, `v_videoid`, `v_like`, `v_status`, `v_created`, `v_modified`) VALUES
(2, 'somali', 'Testimonial Videos', 'Test second video', 'test-second-video', 'true', 'Test second video<br>     ', '', '59488722', 0, '1', '2019-07-18 12:15:37', '2019-07-24 08:29:04'),
(6, 'english', 'Training and Tools Videos', 'Conclusion A recap of the course', 'conclusion-a-recap-of-the-course', 'true', 'Conclusion A recap of the course   ', '798331255.jpg', '346380058', 0, '1', '2019-07-21 07:36:14', '2019-07-24 08:29:34'),
(7, 'english', 'Training and Tools Videos', 'Conclusion A recap of the course 2', 'conclusion-a-recap-of-the-course-2', 'true', 'Conclusion A recap of the course 2  ', '7983312551.jpg', '346380058', 0, '1', '2019-07-21 13:38:17', '2019-07-24 08:29:44'),
(8, 'english', 'Training and Tools Videos', 'Conclusion A recap of the course 3', 'conclusion-a-recap-of-the-course-3', 'true', 'Conclusion A recap of the course 3 ', '7983312552.jpg', '346380058', 0, '1', '2019-07-21 13:38:54', '2019-07-24 08:29:55'),
(9, 'somali', 'Premium Training Videos', 'My Video', 'my-video', 'true', 'My Video', '7983312553.jpg', '346380058', 0, '1', '2019-07-24 08:33:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `video_likes`
--

CREATE TABLE `video_likes` (
  `id` bigint(20) NOT NULL,
  `video_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `video_like` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video_likes`
--

INSERT INTO `video_likes` (`id`, `video_id`, `user_id`, `video_like`) VALUES
(3, 6, 1, '1'),
(4, 7, 1, '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `book_access`
--
ALTER TABLE `book_access`
  ADD PRIMARY KEY (`ba_id`);

--
-- Indexes for table `book_chapters`
--
ALTER TABLE `book_chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_orders`
--
ALTER TABLE `book_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_reviews`
--
ALTER TABLE `book_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `emailSubscriber`
--
ALTER TABLE `emailSubscriber`
  ADD PRIMARY KEY (`es_id`),
  ADD UNIQUE KEY `esmail` (`es_email`);

--
-- Indexes for table `membershipplan`
--
ALTER TABLE `membershipplan`
  ADD PRIMARY KEY (`mp_id`);

--
-- Indexes for table `option_table`
--
ALTER TABLE `option_table`
  ADD PRIMARY KEY (`ot_id`),
  ADD UNIQUE KEY `otkeyname` (`ot_keyname`);

--
-- Indexes for table `paymentDetails`
--
ALTER TABLE `paymentDetails`
  ADD PRIMARY KEY (`pd_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `user_credentials`
--
ALTER TABLE `user_credentials`
  ADD PRIMARY KEY (`uc_id`),
  ADD UNIQUE KEY `ucemail` (`uc_email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`ur_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`v_id`);

--
-- Indexes for table `video_likes`
--
ALTER TABLE `video_likes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `a_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `b_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `book_access`
--
ALTER TABLE `book_access`
  MODIFY `ba_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `book_chapters`
--
ALTER TABLE `book_chapters`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `book_orders`
--
ALTER TABLE `book_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_reviews`
--
ALTER TABLE `book_reviews`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `c_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `emailSubscriber`
--
ALTER TABLE `emailSubscriber`
  MODIFY `es_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `membershipplan`
--
ALTER TABLE `membershipplan`
  MODIFY `mp_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `option_table`
--
ALTER TABLE `option_table`
  MODIFY `ot_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paymentDetails`
--
ALTER TABLE `paymentDetails`
  MODIFY `pd_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `s_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_credentials`
--
ALTER TABLE `user_credentials`
  MODIFY `uc_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `ur_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `v_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `video_likes`
--
ALTER TABLE `video_likes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
