 AOS.init({
 	duration: 800,
 	easing: 'slide',
 	once: true
 });

jQuery(document).ready(function($) {

	"use strict";

	

	var siteMenuClone = function() {

		$('.js-clone-nav').each(function() {
			var $this = $(this);
			$this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
		});


		setTimeout(function() {
			
			var counter = 0;
      $('.site-mobile-menu .has-children').each(function(){
        var $this = $(this);
        
        $this.prepend('<span class="arrow-collapse collapsed">');

        $this.find('.arrow-collapse').attr({
          'data-toggle' : 'collapse',
          'data-target' : '#collapseItem' + counter,
        });

        $this.find('> ul').attr({
          'class' : 'collapse',
          'id' : 'collapseItem' + counter,
        });

        counter++;

      });

    }, 1000);

		$('body').on('click', '.arrow-collapse', function(e) {
      var $this = $(this);
      if ( $this.closest('li').find('.collapse').hasClass('show') ) {
        $this.removeClass('active');
      } else {
        $this.addClass('active');
      }
      e.preventDefault();  
      
    });

		$(window).resize(function() {
			var $this = $(this),
				w = $this.width();

			if ( w > 768 ) {
				if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
			}
		})

		$('body').on('click', '.js-menu-toggle', function(e) {
			var $this = $(this);
			e.preventDefault();

			if ( $('body').hasClass('offcanvas-menu') ) {
				$('body').removeClass('offcanvas-menu');
				$this.removeClass('active');
			} else {
				$('body').addClass('offcanvas-menu');
				$this.addClass('active');
			}
		}) 

		// click outisde offcanvas
		$(document).mouseup(function(e) {
	    var container = $(".site-mobile-menu");
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	      if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
	    }
		});
	}; 
	siteMenuClone();


	var sitePlusMinus = function() {
		$('.js-btn-minus').on('click', function(e){
			e.preventDefault();
			if ( $(this).closest('.input-group').find('.form-control').val() != 0  ) {
				$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) - 1);
			} else {
				$(this).closest('.input-group').find('.form-control').val(parseInt(0));
			}
		});
		$('.js-btn-plus').on('click', function(e){
			e.preventDefault();
			$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) + 1);
		});
	};
	// sitePlusMinus();


	var siteSliderRange = function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 300 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
	};
	// siteSliderRange();


	
	var siteCarousel = function () {
		if ( $('.nonloop-block-13').length > 0 ) {
			$('.nonloop-block-13').owlCarousel({
		    center: false,
		    items: 1,
		    loop: true,
				stagePadding: 0,
		    margin: 0,
		    autoplay: true,
		    dots: true,
		    nav: true,
				navText: ['<span class="img_back">', '<span class="img_forward">'],
		    responsive:{
	        600:{
	        	margin: 0,
	        	nav: true,
	          items: 2
	        },
	        1000:{
	        	margin: 0,
	        	stagePadding: 0,
	        	nav: true,
	          items: 3
	        },
	        1200:{
	        	margin: 0,
	        	stagePadding: 0,
	        	nav: true,
	          items: 6
	        }
		    }
			});
		}

		$('.slide-one-item').owlCarousel({
	    center: false,
	    items: 1,
	    loop: true,
			stagePadding: 0,
	    margin: 0,
	    autoplay: true,
	    pauseOnHover: false,
	    nav: true,
	    navText: ['<span class="icon-keyboard_arrow_left">', '<span class="icon-keyboard_arrow_right">']
	  });

	  $('.slide-one-item-alt').owlCarousel({
	    center: false,
	    items: 1,
	    loop: true,
			stagePadding: 0,
			smartSpeed: 700,
	    margin: 0,
	    autoplay: true,
	    pauseOnHover: false,
      responsive:{
    600:{
      margin: 0,
      items: 1
    },
    1000:{
      margin: 0,
      stagePadding: 0,
      items: 1
    },
    1200:{
      margin: 0,
      stagePadding: 0,
      items: 1
    }
    }
	  });

	  
	  

	  $('.custom-next').click(function(e) {
	  	e.preventDefault();
	  	$('.slide-one-item-alt').trigger('next.owl.carousel');
	  });
	  $('.custom-prev').click(function(e) {
	  	e.preventDefault();
	  	$('.slide-one-item-alt').trigger('prev.owl.carousel');
	  });
	  
	};
	siteCarousel();

	var siteStellar = function() {
		$(window).stellar({
	    responsive: false,
	    parallaxBackgrounds: true,
	    parallaxElements: true,
	    horizontalScrolling: false,
	    hideDistantElements: false,
	    scrollProperty: 'scroll'
	  });
	};
	siteStellar();

	var siteCountDown = function() {

		$('#date-countdown').countdown('2020/10/10', function(event) {
		  var $this = $(this).html(event.strftime(''
		    + '<span class="countdown-block"><span class="label">%w</span> weeks </span>'
		    + '<span class="countdown-block"><span class="label">%d</span> days </span>'
		    + '<span class="countdown-block"><span class="label">%H</span> hr </span>'
		    + '<span class="countdown-block"><span class="label">%M</span> min </span>'
		    + '<span class="countdown-block"><span class="label">%S</span> sec</span>'));
		});
				
	};
	siteCountDown();

	var siteDatePicker = function() {

		if ( $('.datepicker').length > 0 ) {
			$('.datepicker').datepicker();
		}

	};
	siteDatePicker();

	var siteSticky = function() {
		$(".js-sticky-header").sticky({topSpacing:0});
	};
	siteSticky();

	// navigation
  var OnePageNavigation = function() {
    var navToggler = $('.site-menu-toggle');
   	$("body").on("click", ".main-menu li a[href^='#'], .smoothscroll[href^='#'], .site-mobile-menu .site-nav-wrap li a", function(e) {
      e.preventDefault();

      var hash = this.hash;

      $('html, body').animate({
        'scrollTop': $(hash).offset().top
      }, 600, 'easeInOutCirc', function(){
        window.location.hash = hash;
      });

    });
  };
  OnePageNavigation();

  var siteScroll = function() {

  	

  	$(window).scroll(function() {

  		var st = $(this).scrollTop();

  		if (st > 100) {
  			$('.js-sticky-header').addClass('shrink');
  		} else {
  			$('.js-sticky-header').removeClass('shrink');
  		}

  	}) 

  };
  siteScroll();

});

 $('#featureBooks').owlCarousel({
    center: false,
    items: 1,
    loop: false,
		stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
		navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
    	margin: 0,
    	nav: true,
      items: 2
    },
    1000:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 3
    },
    1200:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 6
    }
    }
	});
 $('#recentlyreleasedBooks').owlCarousel({
    center: false,
    items: 1,
    loop: false,
		stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
		navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
    	margin: 0,
    	nav: true,
      items: 2
    },
    1000:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 3
    },
    1200:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 6
    }
    }
	});
 $('#mostPopularbooks').owlCarousel({
    center: false,
    items: 1,
    loop: false,
		stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
		navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
    	margin: 0,
    	nav: true,
      items: 2
    },
    1000:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 3
    },
    1200:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 6
    }
    }
	});
$('#PopularAuthors').owlCarousel({
    center: false,
    items: 1,
    loop: false,
		stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
		navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
    	margin: 0,
    	nav: true,
      items: 2
    },
    1000:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 3
    },
    1200:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 6
    }
    }
	});
$('.authors').owlCarousel({
    center: false,
    items: 1,
    loop: false,
    stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
    navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
      margin: 0,
      nav: true,
      items: 2
    },
    1000:{
      margin: 0,
      stagePadding: 0,
      nav: true,
      items: 2
    },
    1200:{
      margin: 0,
      stagePadding: 0,
      nav: true,
      items: 4
    }
    }
  });
$('.commingSoon').owlCarousel({
    center: false,
    items: 1,
    loop: false,
    stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
    navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
      margin: 0,
      nav: true,
      items: 2
    },
    1000:{
      margin: 0,
      stagePadding: 0,
      nav: true,
      items: 4
    },
    1200:{
      margin: 0,
      stagePadding: 0,
      nav: true,
      items: 6
    }
    }
  });
$('#frontSlider').owlCarousel({
    center: false,
    items: 1,
    loop: false,
    stagePadding: 0,
    margin: 0,
    autoplay: true,
    responsive:{
    600:{
      margin: 0,
      items: 1
    },
    1000:{
      margin: 0,
      stagePadding: 0,
      items: 1
    },
    1200:{
      margin: 0,
      stagePadding: 0,
      items: 1
    }
    }
  });

$(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        

});

$(document).ready(function () {         
$(function(){
    var current_page_URL = location.href;

    $( ".leftsidemenu a" ).each(function() {

        if ($(this).attr("href") !== "#") {

            var target_URL = $(this).prop("href");
                // console.log(target_URL+"  = =  "+current_page_URL);
                if (target_URL == current_page_URL) {
                    $('a').parents('li, ul').removeClass('active');
                    $(this).parent('li').addClass('active');
                    return false;
                }
        }
    }); }); });


$('.datepicker').datepicker();



function viewBookDetais(bid){
    $.ajax({
        type: 'POST',
        url: SITEURL+'profile/viewBookDetails',
        data: {'view_ID': bid},
        dataType: 'json',
        success:function(responce){
          if(responce.status == 'Success'){
            //$.alert(responce.message);
              $("#myModalview").modal("show");
              
              $("#myModalview .modal-title").html("View Book Details");
              $("#myModalview .modal-body").html(responce.data);
          }else{
            $.alert(responce.message);
          }
        }
      });
}
function deleteBookDetais(bid){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
              $.ajax({
                type: 'POST',
                url: SITEURL+'profile/deleteBookDetails',
                data: {'del_ID': bid},
                dataType: 'json',
                success:function(responce){
                  if(responce.status == 200){
                    $.alert(responce.message);
                    setTimeout(function(){ window.location.reload(true); },3000);
                  }else{
                    $.alert(responce.message);
                  }
                }
              });
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}

 $(document).ready(function() {
  if($('#example').length > 0){
      var table = $('#example').DataTable( {
          responsive: true
      } );
      new $.fn.dataTable.FixedHeader( table );
    }
} );


$('body').on('submit', '#subscribe_Form', function(e){
    e.preventDefault();
    var textEmail = $("#subscribe_Formtxt_email").val();
    if(textEmail){
        $.ajax({
            type: 'POST',
            url: SITEURL+'front/emailSubscribe',
            data: {'edata': textEmail},
            dataType: 'json',
            success: function(responce){
                if(responce.status == '200'){
                  $.alert(responce.message);
                  $("#subscribe_Form")[0].reset();
                }else{
                  $.alert(responce.message);
                }
            }
        });
    }
});


function openmembershipForm(ids){
    $("#membershipModal").modal("show");
    // alert(ids);
    if(ids){
      var htmldata= '<div class="row justify-content-md-center"><div class="col-md-10"><div class="form-group"><input type="hidden" name="txtplanId" id="txtplanId" value = "'+ids+'" class="form-control"></div><div class="form-group"><label for="txtfirstname"><b>Your First Name</b></label><input type="text" name="txtfirstname" id="txtfirstname" value = "" placeholder="Enter First Name" class="form-control"></div><div class="form-group"><label for="txtlastname"><b>Your Last Name</b></label><input type="text" name="txtlastname" id="txtlastname" value = "" placeholder="Enter Last Name" class="form-control"></div><div class="form-group"><label for="txtemail"><b>Your Email Address</b></label><input type="text" name="txtemail" id="txtemail" value = "" placeholder="Enter Email Address" class="form-control"></div><div class="form-group text-center"><button type="submit" class="btn btn-info smbtbtn" style="border-radius: 25px !important;">Save</button></div></div></div>';
      $("body #memberinfoForm").html(htmldata);
    }
}

jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid Email Address.");


$("body #memberinfoForm").validate({
  rules: {
      txtfirstname: {"required":true},
      txtlastname: {"required":true},
      txtemail: {"required":true, "validate_email":true},
  },messages: {
      txtfirstname: {"required":"First Name is required."},
      txtlastname: {"required":"Last Name is required."},
      txtemail: {"required":"Email is required.","validate_email":"Please enter valid Email."},
  }
});
$("body #memberinfoForm").submit(function(evt){
    evt.preventDefault();
    var fDATa = new FormData(this);
    if($("#memberinfoForm").valid()){
      $("body .smbtbtn").addClass('loader');
        $.ajax({
            type: 'POST',
            url: SITEURL+'membership/addsSbscriber',
            data: fDATa,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(responce){
              $("body .smbtbtn").removeClass('loader');
                if(responce.status == 200){
                    window.location.href =responce.data;
                }else{
                  $("#responce").html(responce.message);
                }
            }
        });

    }
});

$("body #payment_form").validate({
  rules: {
      txt_firstname: {"required":true},
      txt_lastname: {"required":true},
      txt_email: {"required":true, "validate_email":true},
      // txt_mobile: {"required":true},
      txt_password: {"required":true,"minlength":6},
      // txt_cnfpassword: {"required":true,"equalTo": "#txt_password"},
      select_book: {"required":true},
      txt_paymethod: {"required":true},
      txt_holdername: {"required":true},
      txt_cardnumber: {"required":true,"minlength":16,"number":true},
      txt_cvv: {"required":true,"minlength":3,"number":true},
      txt_expmonth: {"required":true},
      txt_expyear: {"required":true},
      txt_agree: {"required":true},
  },messages: {
      txt_firstname: {"required":"First Name is required."},
      txt_lastname: {"required":"Last Name is required."},
      txt_email: {"required":"Email is required.","validate_email":"Please enter valid Email."},
      // txt_mobile: {"required":"Mobile field is required."},
      txt_password: {"required":"Password is required.","minlength":"Password must be 6 characters long."},
      // txt_cnfpassword: {"required":"Enter Confirm Password Same as Password."},
      select_book: {"required":"Somali Book is required."},
      txt_paymethod: {"required":"Payment Method is required."},
      txt_holdername: {"required":"Card holder name is required."},
      txt_cardnumber: {"required":"Card number is required.","minlength":"Card number must be 16 characters.","number":"Please enter valid card number."},
      txt_cvv: {"required":"CVV number is required.","minlength":"CVV must be 3 characters.","number":"Please enter valid CVV."},
      txt_expmonth: {"required":"Expire month is required."},
      txt_expyear: {"required":"Expire year is required."},
      txt_agree: {"required": "Please agree our terms & conditions."}
  }
});
$('#printMe').click(function(){
    var divToPrint = document.getElementById('outprint');
    var htmlToPrint = '' +
        '<style type="text/css">' +
        'table{margin:0 auto;} table th, table td {' +
        'border:1px solid #000;' +
        'padding;0.5em;' +
        '}' +
        '</style>';
    htmlToPrint += divToPrint.outerHTML;
    newWin = window.open("");
    newWin.document.write("<h3 align='center'>Bill Payment Details</h3>");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
    

});

/*----------Stripe ------------------------------*/

$(function() {

    var $form         = $("#payment_form");

  $('form#payment_form').bind('submit', function(e) {

    var $form         = $("#payment_form"),

        inputSelector = ['input[type=email]', 'input[type=password]',

                         'input[type=text]', 'input[type=file]',

                         'textarea'].join(', '),

        $inputs       = $form.find('.required').find(inputSelector),

        $errorMessage = $form.find('div.error'),

        valid         = true;

        $errorMessage.addClass('hide');

 

        $('.has-error').removeClass('has-error');

    $inputs.each(function(i, el) {

      var $input = $(el);

      if ($input.val() === '') {

        $input.parent().addClass('has-error');

        $errorMessage.removeClass('hide');

        e.preventDefault();

      }

    });

     

    if (!$form.data('cc-on-file')) {

      e.preventDefault();
      // alert($form.data('stripe-publishable-key'));
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));

      Stripe.createToken({

        number: $('.card-number').val(),

        cvc: $('.card-cvc').val(),

        exp_month: $('.card-expiry-month').val(),

        exp_year: $('.card-expiry-year').val()

      }, stripeResponseHandler);

    }

    

  });

      

  function stripeResponseHandler(status, response) {
        
        if (response.error) {

            $('.error')

                .removeClass('hide')

                .find('.alert')

                .text(response.error.message);

        } else {

            var token = response['id'];

            $form.find('input[type=text]').empty();
            
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            if($("body #payment_form").valid()){
                $("body #payment_form")[0].submit(); 
            }

        }

    }

     

});
/*----------End stripe-----------------------*/


$("body #payment_formEVC").validate({
  rules: {
      txt_planidform: {"required":true},
      txt_firstname: {"required":true},
      txt_lastname: {"required":true},
      txt_email: {"required":true, "validate_email":true},
      txt_mobile: {"required":true,"number":true},
      txt_password: {"required":true,"minlength":6},
      select_book: {"required":true},
      txt_address: {"required":true},
      txt_agree2: {"required":true},
  },messages: {
      txt_planidform: {"required":"Plan is required."},
      txt_firstname: {"required":"First Name is required."},
      txt_lastname: {"required":"Last Name is required."},
      txt_email: {"required":"Email is required.","validate_email":"Please enter valid Email."},
      txt_mobile: {"required":"Mobile is required.","number":"Please enter valid Mobile."},
      txt_password: {"required":"Password is required.","minlength":"Password must be 6 characters long."},
      select_book: {"required":"Somali Book is required."},
      txt_paymethod: {"required":"Payment Method is required."},
      txt_address: {"required":"Address is required."},
      txt_agree2: {"required": "Please agree our terms & conditions."}
  }
});
 $("body #payment_formMPESA").validate({
  rules: {
      txt_planidform3: {"required":true},
      txt_firstname: {"required":true},
      txt_lastname: {"required":true},
      txt_email: {"required":true, "validate_email":true},
      txt_mobile: {"required":true,"number":true},
      txt_password: {"required":true,"minlength":6},
      select_book: {"required":true},
      txt_address: {"required":true},
      txt_agree3: {"required":true},
  },messages: {
      txt_planidform3: {"required": "Plan is required."},
      txt_firstname: {"required":"First Name is required."},
      txt_lastname: {"required":"Last Name is required."},
      txt_email: {"required":"Email is required.","validate_email":"Please enter valid Email."},
      txt_mobile: {"required":"Mobile is required.","number":"Please enter valid Mobile."},
      txt_password: {"required":"Password is required.","minlength":"Password must be 6 characters long."},
      select_book: {"required":"Somali Book is required."},
      txt_paymethod: {"required":"Payment Method is required."},
      txt_address: {"required":"Address is required."},
      txt_agree3: {"required": "Please agree our terms & conditions."}
  }
});

 $("body #subscribe_Form").validate({
  rules: {
    subscribe_Formtxt_email: {required:true,validate_email:true}
  },messages: {
    subscribe_Formtxt_email: {required:"Email field is required.",validate_email:"Enter valid email addrss."}
  }
 });

 jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid Email Address.");

function changeCheckPlan(){
    var id = document.getElementById('txt_planidform').value;
    if(id){
      var elems = document.getElementsByClassName('pdetails');
      for (var i=0;i<elems.length;i+=1){
        elems[i].style.display = 'none';
        
          }
      document.getElementsByClassName('pdetails_'+id)[0].style.display='block';
    }

}
$('body').on('click', '.imgvidoebox', function(){
    $('body #videoModal').modal('show');
    var link = $(this).attr('data');
    $('body #videoModal .modal-title').html($(this).attr('title'));
    $('body #videoModalData iframe').attr('src', '');
    $('body #videoModalData iframe').attr('src', link);
});

 /* Load more js */
$(function () {
    $(".authorDiv").slice(0, 3).css('display', 'flex').show();
    $("#moreAuthor").on('click', function (e) {
        e.preventDefault();
        $(".authorDiv:hidden").slice(0, 3).css('display', 'flex').slideDown();
        /*if ($(".authorDiv:hidden").length == 0) {
            //$("#load").fadeOut('slow');
        }*/
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});

/* Load more js */
$(function () {
    $(".videoDiv").slice(0, 4).css('display', 'flex').show();
    $("#moreVideo").on('click', function (e) {
        e.preventDefault();
        $(".videoDiv:hidden").slice(0, 4).css('display', 'flex').slideDown();
        /*if ($(".authorDiv:hidden").length == 0) {
            //$("#load").fadeOut('slow');
        }*/
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});

/* Load more js */
$(function () {
    $(".latestDideoDiv").slice(0, 2).css('display', 'flex').show();
    $("#latestVideo").on('click', function (e) {
        e.preventDefault();
        $(".latestDideoDiv:hidden").slice(0, 2).css('display', 'flex').slideDown();
        /*if ($(".authorDiv:hidden").length == 0) {
            //$("#load").fadeOut('slow');
        }*/
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});

/* Load more js */
$(function () {
    $(".popularVideoDiv").slice(0, 2).css('display', 'flex').show();
    $("#popularVideo").on('click', function (e) {
        e.preventDefault();
        $(".popularVideoDiv:hidden").slice(0, 2).css('display', 'flex').slideDown();
        /*if ($(".authorDiv:hidden").length == 0) {
            //$("#load").fadeOut('slow');
        }*/
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});



$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $('.totop a').fadeIn();
    } else {
        $('.totop a').fadeOut();
    }
});

function addLike($user_id,$video_id){
  $.ajax({
        type: 'POST',
        url: SITEURL+'videoTraining/addLike',
        data: {'user_id': $user_id, 'video_id' : $video_id},
        dataType: 'json',
        success: function(responce){
            if(responce.status == 'Success'){
              $.alert(responce.message);
             
            }else{
              $.alert(responce.message);
            }
        }
    });
}

 $(".my-rating-4").starRating({
    totalStars: 5,
    starSize: 25,
    activeColor: '#F2A80B',
    useGradient: false,
    disableAfterRate: false,
    minRating: 1,
    callback: function(currentRating, $el){
      $('#rate').val(currentRating);
    }
  });

  $(".my-rating-1").starRating({
    totalStars: 5,
    starSize: 25,
    activeColor: '#F2A80B',
    useGradient: false,
    readOnly: true,
    emptyColor: '#ffffff',
    strokeWidth: 20,
    strokeColor: '#F2A80B',
  });

  $(".my-rating-small").starRating({
    totalStars: 5,
    starSize: 20,
    activeColor: '#F2A80B',
    useGradient: false,
    readOnly: true,
    emptyColor: '#ffffff',
    strokeWidth: 20,
    strokeColor: '#F2A80B',
  });

$("#bookReview").validate({
  rules: {
      review: {"required":true},
  },messages: {
      review: {"required":"Review is required."},
  }
});

$("#bookReview").submit(function(evt){
    evt.preventDefault();
    var fDATa = new FormData(this);
    if($("#bookReview").valid()){
      //alert(fDATa);
        $.ajax({
            type: 'POST',
            url: SITEURL+'book/adduserReview',
            data: fDATa,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(responce){
              if(responce.status == 'Success'){
                $.alert(responce.message);
                setTimeout(
                function() 
                {
                  window.location.href= SITEURL+'books/'+responce.book_id;
                }, 2000);
                
                
              }else{
                $.alert(responce.message);
              }
            }
        });

    }
});

function stop_Video(){
    $('body #videoModalData iframe').attr('src', '');
}




$(window).scroll(function(){
    var t = window.location.href;
    if(t !== SITEURL){
        if($('.sticky-wrapper').hasClass('is-sticky')){
            $('.site-logo img').attr('src',SITEURL+'assets/images/white.png');
        }else{
            // $('.site-logo img').attr('src',SITEURL+'assets/images/logo.png');
        }  
    }
});

function changePaymentType($val){
  $('#txt_paymethod').val($val);
   if($val == "cc"){
    $('#submitOther').hide();
    $('#submitBTN').show();
  }else{
    $('#submitOther').show();
    $('#submitBTN').hide();
  }
  //alert($val);return false;
}

function checkoutValidateCC(form){
  form.validate({
    rules: {
        txt_firstname: {"required":true},
        txt_lastname: {"required":true},
        txt_email: {"required":true, "validate_email":true},
        txt_phone:{"required":true, "number":true},
        txt_paymethod: {"required":true},
        txt_holdername: {"required":true},
        txt_cardnumber: {"required":true,"minlength":16,"number":true},
        txt_cvv: {"required":true,"minlength":3,"number":true},
        txt_expmonth: {"required":true},
        txt_expyear: {"required":true},
        txt_agree: {"required":true},
        
    },messages: {
        txt_firstname: {"required":"First Name is required."},
        txt_lastname: {"required":"Last Name is required."},
        txt_email: {"required":"Email is required.","validate_email":"Please enter valid Email."},
        txt_paymethod: {"required":"Payment Method is required."},
        txt_phone:{"required":"Phone is required.","number":"Please enter valid phone."},
        txt_holdername: {"required":"Card holder name is required."},
        txt_cardnumber: {"required":"Card number is required.","minlength":"Carn number must be 16 characters.","number":"Please enter valid card number."},
        txt_cvv: {"required":"CVV number is required.","minlength":"CVV must be 3 characters.","number":"Please enter valid CVV."},
        txt_expmonth: {"required":"Expire month is required."},
        txt_expyear: {"required":"Expire year is required."},
        txt_agree: {"required": "Please agree our terms & conditions."}
    }
  });
}

function checkoutValidateEVC(form){
  form.validate({
    rules: {
        txt_firstname: {"required":true},
        txt_lastname: {"required":true},
        txt_email: {"required":true, "validate_email":true},
        txt_phone:{"required":true, "number":true},
        txt_paymethod: {"required":true},
        txt_address:{"required":true},
        txt_agree: {"required":true},
    },messages: {
        txt_firstname: {"required":"First Name is required."},
        txt_lastname: {"required":"Last Name is required."},
        txt_email: {"required":"Email is required.","validate_email":"Please enter valid Email."},
        txt_paymethod: {"required":"Payment Method is required."},
        txt_phone:{"required":"Phone is required.","number":"Please enter valid phone."},
        txt_address: {"required":"Address is required."},
        txt_agree: {"required": "Please agree our terms & conditions."}
    }
  });
}

function checkoutValidateMPESA(form){
  form.validate({
    rules: {
        txt_firstname: {"required":true},
        txt_lastname: {"required":true},
        txt_email: {"required":true, "validate_email":true},
        txt_phone:{"required":true, "number":true},
        txt_paymethod: {"required":true},
        txt_address:{"required":true},
        txt_agree: {"required":true},
    },messages: {
        txt_firstname: {"required":"First Name is required."},
        txt_lastname: {"required":"Last Name is required."},
        txt_email: {"required":"Email is required.","validate_email":"Please enter valid Email."},
        txt_paymethod: {"required":"Payment Method is required."},
        txt_phone:{"required":"Phone is required.","number":"Please enter valid phone."},
        txt_address: {"required":"Address is required."},
        txt_agree: {"required": "Please agree our terms & conditions."},
    }
  });
}

function checkoutFormValidation(){
  var form = $( "body #book-checkout-form");
  var payMethod = $('#txt_paymethod').val();

  if(payMethod == "cc"){
    checkoutValidateCC(form);
  }else if(payMethod == "evc"){
    checkoutValidateEVC(form);
  }else if(payMethod == "mpesa"){
    checkoutValidateMPESA(form);
  }

  if(form.valid()){
    return true;
    }else{
      return false;
    }

}
/*----------End stripe-----------------------*/

function changecatFunction(){
  var favorite = [];
  // $.each($("input[name='filterCat']:checked"), function(){            
  //     favorite.push($(this).val());
  // });
 $('#short_Form').submit();
  // $.ajax({
  //     type: 'POST',
  //     url: SITEURL+'book/',
  //     dataType: 'json',
  //     data: {'catids':favorite},
  //     success: function(responce){
  //       if(responce.status == 200){

  //       }else{
  //         alert(responce.message);
  //       }
  //     }
  // });
}

function pageLoadData(pid, totals){
    
    var pageURL = $(location). attr("href");
    $.ajax({
      type: 'POST',
      url: SITEURL+'book/catLoademoreajax',
      data: {'pageid': pid,'total': totals,'pageUrls': pageURL},
      dataType: 'json',
      success: function(responce){
        if(responce.status == 200){
            $('body .filterBooksall').html('');
            $('body .filterBooksall').html(responce.data);
            $('body .updatePN').html(responce.span);

        }else{
          alert(responce.message);
        }
      }
  });
}
$('body').on('click','.plink',function(){
  $('body .allcatpagination a').each(function(ele){
        $(this).removeClass('active');
    });
    $(this).addClass('active');
});

/*Author Signup page */
$("body #authorsignUp_Form").validate({
  rules: {
      
      txt_firstname: {"required":true},
      txt_lastname: {"required":true},
      txt_email: {"required":true, "validate_email":true},
      txt_mobile: {"required":true,"minlength":10},
      txt_address: {"required":true},
      txt_bio: {"required":true},
      txt_password: {"required":true,"minlength":6},
      txt_paypalEmail: {"required":true, "validate_email":true},
      txt_mobilenumber: {"required":true,"number":true},
  },messages: {
      txt_firstname: {"required":"First Name is required."},
      txt_lastname: {"required":"Last Name is required."},
      txt_email: {"required":"Email is required.","validate_email":"Please enter valid Email."},
      txt_mobile: {"required":"Mobile is required."},
      txt_address: {"required":"Address is required."},
      txt_bio: {"required": "Bio is required."},
      txt_password: {"required":"Password is required.","minlength":"Password must be 6 characters long."},
      txt_paypalEmail: {"required":"Paypal Email is required.","validate_email":"Please enter valid Paypal Email."}, 
      txt_mobilenumber: {"required":'Money Mobile is required.',"number":"Please enter valid Mobile."},
  }
});
function authorpaypalSubmit(){
  document.getElementById('txtmmoneyEmailDiv').innerHTML = '';
  var html2 = '<div class="form-group"><label>Email Address</label><input type="email" name="txt_paypalEmail" id="txt_paypalEmail" placeholder="Email Addresss" class="form-control" required></div>';
  document.getElementById('txtpaypalEmailDiv').innerHTML = html2; 
}

function authormmoneySubmit(){
  document.getElementById('txtpaypalEmailDiv').innerHTML = '';
  var html = '<div class="form-group"><label>Mobile Number</label><input type="text" name="txt_mobilenumber" id="txt_mobilenumber" placeholder="+91xxxxxxxxxx" class="form-control"></div>';
  document.getElementById('txtmmoneyEmailDiv').innerHTML = html;
}
$("body").on('submit', '#authorsignUp_Form', function(e){
    e.preventDefault();
    $('body input[name="authorSubmit"]').addClass('loader');
    $('#authorsignUp_Form').css('pointer-events', 'none');
    if($('#authorsignUp_Form').valid()){
        var fDATa = new FormData(this);
        $.ajax({
            type: 'POST',
            url: SITEURL+'authorSignUp/authorRegistration',
            data: fDATa,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(responce){
              $('body input[name="authorSubmit"]').removeClass('loader');
              $('#authorsignUp_Form').css('pointer-events', 'none');
              if(responce.Status == '200'){
                document. getElementById("authorsignUp_Form"). reset();
                
                //$.alert(responce.message);
                //setTimeout(function(){ window.location.href=SITEURL+'authorSignUp'; }, 2000);
              }else{
               // $.alert(responce.message);
              }
              window.location.href=SITEURL+'authorSignUp';
            }
        });

    
    }
});


$('body #addbook_Form').validate({
    rules: {
        txt_language: {required:true},
        txt_title : {required:true},
       /* txt_publisheddate: {required:true},*/
        txt_image: {required:true,extension:'png|jpg|jpeg|gif'},
        bookFile: {required:true,extension:'pdf|txt|doc|docx|ppt|pptx'},
        txt_price: {required:true,number:true},
        /*txt_category: {required:true},*/
        text_message: {required:true}
    },
    messages: {
        txt_language: {required:"Book Language is required."},
        txt_title: {required:"Book Title is required."},
        /*txt_publisheddate: {required:"published Date is required."},*/
        txt_image: {required:"Book Picture is required.",extension:"Allowed extension only png,jpg,jpeg and gif."},
        bookFile: {required:"Book file is required.",extension:"Allowed extension only pdf,txt,docx,ppt and pptx."},
        /*txt_category: {required:"Select category Field is required."},*/
        txt_price: {required:"Selling Price is required."},
        text_message: {required:'Description is required.'}
        
    }
});
 
$('body').on('submit','#addbook_Form',function(evt){
    evt.preventDefault();
    if($("#addbook_Form").valid()){
      $('#addbook_Form').css('pointer-events', 'none');
      $('body button[name="submit"]').addClass('loader');
        // var textval = $("#txtEditor").Editor("getText");
        // if(textval){
            // $("body #txt_description-error").css("display","node");
            var formData = new FormData(this);
            // formData.append("text_message", textval);
            $.ajax({
                type: "POST",
                url: SITEURL+"user/addAuthorbookbyAjax",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function(resultS){
                  $('body button[name="submit"]').removeClass('loader');
                  $('#addbook_Form').css('pointer-events', '');
                    if(resultS.status == 200){
                        //$.alert(resultS.message);
                        window.location.reload(true);
                        //setTimeout(function(){ window.location.reload(true); },5000);
                    }else{
                        $.alert(resultS.message);
                    } 
                }
            });
        // }else{
        //     // $("body #txt_description-error").css("display","block").text("Description field is required.");
        //     return false; 
        // }
    }
});


$('body #contact_form').validate({
    rules: {
        txt_firstname: {required:true},
        txt_lastname : {required:true},
        txt_email: {required:true,validate_email:true},
        txt_subject: {required:true},
        txt_messsage: {required:true},
    },
    messages: {
        txt_firstname: {required:"First Name is required."},
        txt_lastname : {required:"Last Name is required."},
        txt_email: {required:"Email is requried.",validate_email:"Please enter valid Email."},
        txt_subject: {required:"Subject is required."},
        txt_messsage: {required:"Message is required."},      
    }
});

$('body #signin_Form').validate({
    rules: {
        txt_email: {required:true,validate_email:true},
        txt_password: {required:true},
    },
    messages: {
        txt_email: {required:"Email is requried.",validate_email:"Please enter valid Email."},
        txt_password: {required:"Password is required."},
    }
});

$('body #authorprofile_Form').validate({
  rules:{
      txt_firstname: {required:true},
      txt_lastname: {required:true},
      txt_email: {required:true,validate_email:true},
      txt_mobile: {required:true,minlength:10},
      /*txt_image: {required:false,extension:'png|jpg|jpeg|gif'},*/
      txt_language: {required:true},
      /*txt_published: {required:true},*/
      txt_status: {required:true},
      txt_address: {required:true},
      /*txt_facebook: {url:true},
      txt_twitter: {url:true},
      txt_googleplus: {url:true},
      txt_instagram: {url:true},*/
  },
  messages:{
      txt_firstname: {required:"First Name is required."},
      txt_lastname: {required:"Last Name is required."},
      txt_email: {required:"Email is required."},
      txt_mobile: {required:"Mobile number is required."},
      txt_language: {required:"Please select the language."},
      /*txt_published: {required:"Published books field is required. "},*/
      /*txt_status: {required:"Status field is required."},
      txt_paypal: {required:"Paypal email field is required."},
      txt_mmoney: {required:"Mobile money field is required."},
      txt_address: {required:"Address field is required."},*/
  }
});
$('body').on('submit', '#authorprofile_Form', function(e){
  e.preventDefault();
  var pageURL = $(location). attr("href");
  if($('#authorprofile_Form').valid()){
    $('body button[name="submit"]').addClass('loader');
    $('#authorprofile_Form').css('pointer-events', 'none');
    var formData = new FormData(this);
      $.ajax({
          type: "POST",
          url: SITEURL+"profile/updateAuthorAjax",
          data: formData,
          processData: false,
          contentType: false,
          dataType: "json",
          success: function(resultS){
            $('body button[name="submit"]').removeClass('loader');
            $('#authorprofile_Form').css('pointer-events', '');
              if(resultS.Status == 200){
                  $.alert(resultS.message);
                  setTimeout(function(){ window.location.reload(resultS.url); },2000); 
              }else{
                  $.alert(resultS.message);
              } 
          }
      });
  }
});



$('body #userprofile_Form').validate({
  rules:{
      txt_firstname: {required:true},
      txt_lastname: {required:true},
      txt_email: {required:true,validate_email:true},
      txt_mobile: {required:true,number:true},
      txt_image: {required:false,extension:'png|jpg|jpeg|gif'},
     /* txt_status: {required:true},*/
      txt_address: {required:true},
  },
  messages:{
      txt_firstname: {required:"First name is required."},
      txt_lastname: {required:"Last name is required."},
      txt_email: {"required":"Email is required.","validate_email":"Please enter valid Email."},
      txt_mobile: {required:"Mobile number is required.",number:"Please enter valid Mobile number."},
      txt_image: {extension:'Allowed extension only png,jpg,jpeg and gif.'},
      /*txt_status: {required:"Status field is required."},*/
      txt_address: {required:"Address is required."},
  }
});
$('body').on('submit', '#userprofile_Form', function(e){
  e.preventDefault();
  var pageURL = $(location). attr("href");
  if($('#userprofile_Form').valid()){
    $('body button[name="submit"]').addClass('loader');
    $('#userprofile_Form').css('pointer-events', 'none');
    var formData = new FormData(this);
      $.ajax({
          type: "POST",
          url: SITEURL+"profile/updateCustiomerAjax",
          data: formData,
          processData: false,
          contentType: false,
          dataType: "json",
          success: function(resultS){
            $('body button[name="submit"]').removeClass('loader');
            $('#userprofile_Form').css('pointer-events', '');
              if(resultS.Status == 200){
                  $.alert(resultS.message);
                  setTimeout(function(){ window.location.reload(resultS.url); },2000); 
              }else{
                  $.alert(resultS.message);
              } 
          }
      });
  }
});


$('body #editAuthorbook_Form').validate({
    
rules: {
        txt_language: {required:true},
        txt_title : {required:true},
       /* txt_publisheddate: {required:true},*/
       /* txt_image: {required:true,extension:'png|jpg|jpeg|gif'},
        bookFile: {required:true,extension:'pdf|txt|doc|docx|ppt|pptx'},*/
        txt_price: {required:true,number:true},
        /*txt_category: {required:true},*/
        text_message: {required:true}
    },
    messages: {
        txt_language: {required:"Book Language is required."},
        txt_title: {required:"Book Title is required."},
        /*txt_publisheddate: {required:"published Date is required."},*/
        /*txt_image: {required:"Book Picture is required.",extension:"Allowed extension only png,jpg,jpeg and gif."},
        bookFile: {required:"Book file is required.",extension:"Allowed extension only pdf,txt,docx,ppt and pptx."},*/
        /*txt_category: {required:"Select category Field is required."},*/
        txt_price: {required:"Selling Price is required."},
        text_message: {required:'Description is required.'},
      }
       
});
 
$('body').on('submit','#editAuthorbook_Form',function(evt){
    evt.preventDefault();
    if($("#editAuthorbook_Form").valid()){
      $('#editAuthorbook_Form').css('pointer-events', 'none');
      $('body button[name="update"]').addClass('loader');
        // var textval = $("#txtEditor").Editor("getText");
        // if(textval){
            // $("body #txt_description-error").css("display","node");
            var formData = new FormData(this);
            // formData.append("text_message", textval);
            $.ajax({
                type: "POST",
                url: SITEURL+"profile/updateAuthorbookbyAjax",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function(resultS){
                  $('body button[name="update"]').removeClass('loader');
                  $('#editAuthorbook_Form').css('pointer-events', '');
                    if(resultS.status == 200){
                        $.alert(resultS.message);
                        setTimeout(function(){ window.location.reload(true); },3000);
                    }else{
                        $.alert(resultS.message);
                    } 
                }
            });
        // }else{
        //     // $("body #txt_description-error").css("display","block").text("Description field is required.");
        //     return false; 
        // }
    }
});

function viewmyauthorOrder(orderid){
    if(orderid){
        $.ajax({
            type: "POST",
            url: SITEURL+"profile/viewauthorsingleOrder",
            data: {'orderid':1},
            dataType: "json",
            success: function(resultS){
                if(resultS.status == 200){
                    $('body #myModalview').modal('show');
                    $('body #myModalview .modal-body').html(resultS.data);
                }else{
                    $.alert(resultS.message);
                } 
            }
        });
    }
}

$('#changePassword_Form').validate({
    rules:{
        txt_newpass: {required:true,minlength:6},
        txt_cnfnewpass: {required:true,equalTo: "#txt_newpass"},
    },messages:{
        txt_newpass:{required:"Password is required."},
        txt_cnfnewpass:{required:"Password must match."}
    }
});
$('body').on('submit', '#changePassword_Form', function(e){
    e.preventDefault();
    $('#changePassword_Form').css('pointer-events', 'none');
    $('body button[name="submit"]').addClass('loader');
    var formData = new FormData(this);
    $.ajax({
        type: 'POST',
        url: SITEURL+'profile/changePassword',
        data: formData,
        processData: false,
        contentType: false,
        dataType:'json',
        success: function(resREsult){
            $('#changePassword_Form').css('pointer-events', '');
            $('body button[name="submit"]').removeClass('loader');
            if(resREsult.status == 200){
              $.alert(resREsult.message);
              setTimeout(function(){ window.location.reload(true); },2000);
            }else{
              $.alert(resREsult.message);
            }
        }
    });
});

function payselectStatus(){
    var selStatus = document.getElementById('payselectStatus').value;
    var currentLocation = window.location;
    let searchParams = new URLSearchParams(window.location.search);
    if(selStatus == 'completed'){
        if(searchParams.has('fromdate') && searchParams.has('todate')){
            fromdate = searchParams.get('fromdate'); 
            todate = searchParams.get('todate'); 
            window.location.href= SITEURL+'user/viewmyOrder/completed/?fromdate='+fromdate+'&todate='+todate;  
        }else{
            window.location.href=SITEURL+'user/viewmyOrder/'+selStatus;
        }
        
    }else if(selStatus == 'pending'){
        if(searchParams.has('fromdate') && searchParams.has('todate')){
            fromdate = searchParams.get('fromdate'); 
            todate = searchParams.get('todate'); 
            window.location.href= SITEURL+'user/viewmyOrder/pending/?fromdate='+fromdate+'&todate='+todate;  
        }else{
            window.location.href=SITEURL+'user/viewmyOrder/'+selStatus;
        }
    }else{
        window.location.href=SITEURL+'user/viewmyOrder';
    }
        
}

$('body').on('change','#todate',function(){
  var fromdate = $('#fromdate').val();
  var todate = $('#todate').val();
  if(fromdate && todate){
      if(todate >= fromdate){
          var currentLocation = window.location;
          if(currentLocation == SITEURL+'user/viewmyOrder/completed'){
              window.location.href= SITEURL+'user/viewmyOrder/completed/?fromdate='+fromdate+'&todate='+todate;  
          }else if(currentLocation == SITEURL+'user/viewmyOrder/pending'){
              window.location.href= SITEURL+'user/viewmyOrder/pending/?fromdate='+fromdate+'&todate='+todate;  
          }else{
              window.location.href= SITEURL+'user/viewmyOrder/?fromdate='+fromdate+'&todate='+todate;  
          }
      }else{
          $.alert('Plase select correct from date.');
      }
  }else{
    $.alert('Please select before from date.');
  }
});

$('body #cmanagemybook_Form').validate({
  rules: {
    selectbookId: {required:true},
  },messages: {
    selectbookId: {required:'Please select Somali book.'}
  }
});
$('body').on('submit', '#cmanagemybook_Form', function(e){
  e.preventDefault();
  if($('body #cmanagemybook_Form').valid()){

    $('#cmanagemybook_Form').css('pointer-events', 'none');
    $('body button[name="submit"]').addClass('loader');
    var formData = new FormData(this);
    $.ajax({
        type: 'POST',
        url: SITEURL+'profile/managemyBook',
        data: formData,
        processData: false,
        contentType: false,
        dataType:'json',
        success: function(resREsult){
            $('#cmanagemybook_Form').css('pointer-events', '');
            $('body button[name="submit"]').removeClass('loader');
            if(resREsult.status == 200){
              $.alert(resREsult.message);
              setTimeout(function(){ window.location.reload(true); },2000);
            }else{
              $.alert(resREsult.message);
            }
        }
    });
  }
});