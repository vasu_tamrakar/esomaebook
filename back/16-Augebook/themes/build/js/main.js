function deleteAuthor(ids){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
            	$.ajax({
            		type: 'POST',
            		url: SITEURL+'dashboard/deleteAuthor',
            		data: {'delid': ids},
            		dataType: 'json',
            		success:function(responce){
            			if(responce.status == 'Success'){
            				$.alert(responce.message);
            				location.href=responce.data;
            			}else{
            				$.alert(responce.message);
            			}
            		}
            	});
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}
function deleteAuthorsDetail(ids){
	$.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
            	$.ajax({
            		type: 'POST',
            		url: SITEURL+'dashboard/deleteAuthor',
            		data: {'delid': ids},
            		dataType: 'json',
            		success:function(responce){
            			if(responce.status == 'Success'){
            				$.alert(responce.message);
            				location.href=responce.data;
            			}else{
            				$.alert(responce.message);
            			}
            		}
            	});
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}


/*Category*/

function deleteCategory(ids){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
            	$.ajax({
            		type: 'POST',
            		url: SITEURL+'dashboard/deletecategoryDetail',
            		data: {'delid': ids},
            		dataType: 'json',
            		success:function(responce){
            			if(responce.status == 'Success'){
            				$.alert(responce.message);
            				location.href=responce.data;
            			}else{
            				$.alert(responce.message);
            			}
            		}
            	});
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}
function deletecategoryDetail(ids){
	$.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
            	$.ajax({
            		type: 'POST',
            		url: SITEURL+'dashboard/deletecategoryDetail',
            		data: {'delid': ids},
            		dataType: 'json',
            		success:function(responce){
            			if(responce.status == 'Success'){
            				$.alert(responce.message);
            				location.href=responce.data;
            			}else{
            				$.alert(responce.message);
            			}
            		}
            	});
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}
function deletebookDetail(bid){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
                $.ajax({
                    type: 'POST',
                    url: SITEURL+'dashboard/deletebookDetail',
                    data: {'delid': bid},
                    dataType: 'json',
                    success:function(responce){
                        if(responce.status == 'Success'){
                            $.alert(responce.message);
                            location.href=responce.data;
                        }else{
                            $.alert(responce.message);
                        }
                    }
                });
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}
function deleteBooksDetail(id){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
                $.ajax({
                    type: 'POST',
                    url: SITEURL+'dashboard/deletebookDetail',
                    data: {'delid': id},
                    dataType: 'json',
                    success:function(responce){
                        if(responce.status == 'Success'){
                            $.alert(responce.message);
                            location.href=responce.data;
                        }else{
                            $.alert(responce.message);
                        }
                    }
                });
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}

function deleteUserDetail(uid){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
                $.ajax({
                    type: 'POST',
                    url: SITEURL+'dashboard/deleteuserDetail',
                    data: {'delid': uid},
                    dataType: 'json',
                    success:function(responce){
                        if(responce.status == 'Success'){
                            $.alert(responce.message);
                            location.href=responce.data;
                        }else{
                            $.alert(responce.message);
                        }
                    }
                });
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}
function deletePlanDetail(mpid){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
                $.ajax({
                    type: 'POST',
                    url: SITEURL+'dashboard/deleteplanDetail',
                    data: {'delid': mpid},
                    dataType: 'json',
                    success:function(responce){
                        if(responce.status == 'Success'){
                            $.alert(responce.message);
                            location.href=responce.data;
                        }else{
                            $.alert(responce.message);
                        }
                    }
                });
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}



$(function() {
  $('#txt_publisheddate').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
    // alert("You are " + years + " years old!");
  });
});

$('#addHomeslide_Form').validate({
    rules: {
        txt_heading: {required:true},
        txt_subtitle: {required:true},
        txt_tagline: {required:true},
        txt_link: {url:true},
        txt_image: {required:true,extension: "png|jpg|jpeg|gif"}
    },
    messages: {
        txt_heading: {required:'Title is required.'},
        txt_subtitle: {required:'Subtitle is required.'},
        txt_tagline: {required:'Tagline is required.'},
        txt_image: {required:"Slider Image is required.",extension: "Invalid format."}
    }
});
$('#addHomeslide_Form').submit(function(e){
    e.preventDefault();
    var Formdata = new FormData(this);
    $.ajax({
        type: 'POST',
        url: SITEURL+'dashboard/addNewSlide',
        data: Formdata,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function(responce){
            if(responce.status == 'Success'){
                location.reload(true);
            }else{
                $.alert({
                    content: responce.message,
                    title: 'Error',
                    buttons: {
                        specialKey: {
                            text: 'Cancel',
                            btnClass: 'bg-danger', 
                        }
                    }
                });
            }
        }
    });
});

function editSlidemodel(sidd){
    if(sidd){
        $.ajax({
            type: 'POST',
            url: SITEURL+'dashboard/getSlideData',
            data: {'dataid': sidd},
            dataType: 'json',
            success:function(responce){
                if(responce.status == 'Success'){
                    $('#edit_Slide').modal('show');
                    $('body #editHomeslide_Form').html(responce.data);
                }else{
                    $.alert(responce.message);
                }
            }
        });
    }
}

$('body #editHomeslide_Form').validate({
    rules: {
        edittxt_heading: {required:true},
        edittxt_subtitle: {required:true},
        edittxt_tagline: {required:true},
        edittxt_image: {extension: "png|jpg|jpeg|gif"},
        /*edittxt_link: {required:true}*/
    },
    messages: {
        edittxt_heading: {required:"Title is required."},
        edittxt_subtitle: {required:"Subtitle is required."},
        edittxt_tagline: {required:"Tagline is required."},
        edittxt_image: {extension: "Invalid Format."}
    }
});
$('body #editHomeslide_Form').submit(function(e){
    e.preventDefault();
    if($('body #editHomeslide_Form').valid()){
        var Formdata = new FormData(this);
        $.ajax({
            type: 'POST',
            url: SITEURL+'dashboard/editSlideDetail',
            data: Formdata,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(responce){
                if(responce.status == 'Success'){
                    location.reload(true);
                }else{
                    alert(responce.message);
                }
            }
        });    
    }
    
});

function deleteslideDetail(sid){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
                $.ajax({
                    type: 'POST',
                    url: SITEURL+'dashboard/deleteslideDetail',
                    data: {'delid': sid},
                    dataType: 'json',
                    success:function(responce){
                        if(responce.status == 'Success'){
                            $.alert(responce.message);
                            location.href=responce.data;
                        }else{
                            $.alert(responce.message);
                        }
                    }
                });
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}
function editSlidemodel(sidd){
    if(sidd){
        $.ajax({
            type: 'POST',
            url: SITEURL+'dashboard/getSlideData',
            data: {'dataid': sidd},
            dataType: 'json',
            success:function(responce){
                if(responce.status == 'Success'){
                    $('#edit_Slide').modal('show');
                    $('body #editHomeslide_Form').html(responce.data);
                }else{
                    $.alert(responce.message);
                }
            }
        });
    }
}

jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid Email Address.");

$('body #editHomeslide_Form').validate({
    rules: {
        edittxt_heading: {'required':true},
        edittxt_subtitle: {'required':true},
        edittxt_tagline: {'required':true},
        edittxt_image: {extension: "png|jpg|jpeg|gif"},
        edittxt_link: {url:true}
    },
    message: {
        edittxt_heading: {required:"Title is required."},
        edittxt_subtitle: {required:"Subtitle is required."},
        edittxt_tagline: {required:"Tagline is required."},
        edittxt_image: {extension: "Valid only png|jpg|jpeg|gif format."}
    }
});
$('body #editHomeslide_Form').submit(function(e){
    e.preventDefault();
    if($('body #editHomeslide_Form').valid()){
        var Formdata = new FormData(this);
        $.ajax({
            type: 'POST',
            url: SITEURL+'dashboard/editSlideDetail',
            data: Formdata,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(responce){
                if(responce.status == 'Success'){
                    location.reload(true);
                }else{
                    alert(responce.message);
                }
            }
        });    
    }
    
});

$(document).ready( function() {
    if($("#txtEditor").length > 0){
        $("#txtEditor").Editor({
//             'texteffects':true,
// 'aligneffects':true,
// 'textformats':true,
// 'fonteffects':true,
// 'actions' : true,
// 'insertoptions' : true,
// 'extraeffects' : true,
// 'advancedoptions' : true,
// 'screeneffects':true,
// 'bold': true,
// 'italics': true,
// 'underline':true,
// 'ol':true,
// 'ul':true,
// 'undo':true,
// 'redo':true,
// 'l_align':true,
// 'r_align':true,
// 'c_align':true,
// 'justify':true,
// 'insert_link':true,
// 'unlink':true,
// 'insert_img':true,
// 'hr_line':true,
// 'block_quote':true,
// 'source':true,
// 'strikeout':true,
// 'indent':true,
// 'outdent':true,
// 'fonts':"",
// 'styles':"",
// 'print':true,
// 'rm_format':true,
// 'status_bar':true,
// 'font_size':21,
// 'color':"red",
// 'splchars':"",
// 'insert_table':true,
// 'select_all':true,
// 'togglescreen':true
        });
        
    }
});

if($('#textDescription').length >0){
    var textDescription = $('#textDescription').html();
    setTimeout(function(){ $("body #txtEditor").Editor("setText",textDescription); }, 1000);

    
}
$("#author_Form").validate({
    rules:{
        txt_authorfirstname: {"required":true},
        txt_authorlastname: {"required":true},
        txt_authoremail: {"required":true,"validate_email":true},
        txt_authormobile: {"required":true,"number":true},
        /*txt_published: {"required":true,"digits": true},*/
        txt_image: {"required":false,extension: "png|jpg|jpeg|gif"},

    },
    messages:{
        txt_authorfirstname: {"required":"First name is required."},
        txt_authorlastname: {"required":"Last name is required."},
        txt_authoremail: {"required":"Email is required.","validate_email":"Please enter valid email."},
        txt_authormobile: {"required":"Mobile number is required.","number":"Please enter valid Mobile."},
      /*  txt_published: {"required":"Publish year is required."}*/

    }
});
$("body").on("submit", "#author_Form", function(e){
    e.preventDefault();
    $("#responcesResult").html('');
    var textval = $("#txtEditor").Editor("getText");
    if(textval){
        $("body #txt_message-error").css('display','none');
    
        var formData = new FormData(this);
        formData.append("text_message", textval);
        $.ajax({
            type: "POST",
            url: SITEURL+"dashboard/addAuthor",
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function(resultS){
                console.log(resultS.message);
                if(resultS.status == 200){
                    $("#responcesResult").html(resultS.message);
                    $("#author_Form")[0].reset();
                    $('.Editor-editor').html('');
                }else{
                    $("#responcesResult").html(resultS.message);
                }
            }
        });
    }else{
        $("body #txt_message-error").css('display','block').text("Description field is required.");
        return false;
    }
});



$("#editauthor_Form").validate({
    rules:{
        // txt_authorfirstname: {"required":true},
        // txt_authorlastname: {"required":true},
        // txt_authoremail: {"required":true,"validate_email":true},
        // txt_authormobile: {"required":true},
        txt_published: {"required":true,"digits": true},
        // txt_image: {extension: "png|jpg|jpeg|gif"},
        txt_facebook: {"url": true},
        txt_twitter: {"url": true},
        txt_gplush: {"url": true},
        txt_instagram: {"url": true}
    },
    messages:{
        txt_authorfirstname: {"required":"First Name field is required."},
        txt_authorlastname: {"required":"Last Name field is required."},
        txt_authoremail: {"required":"Email field is required."},
        txt_authormobile: {"required":"Mobile number field is required."},
        txt_published: {"required":"Publish books field is required."}

    }
});
$('body').on('submit','#editauthor_Form',function(evt){
    evt.preventDefault();
    if($("#editauthor_Form").valid()){


        var textval = $("#txtEditor").Editor("getText");
        var formData = new FormData(this);
        formData.append("text_message", textval);
        $.ajax({
            type: "POST",
            url: SITEURL+"dashboard/editAjaxAuthor",
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function(resultS){
                console.log(resultS.message);
                if(resultS.status == 200){
                    $.alert(resultS.message);
                    window.location.reload(true);
                }else{
                    $.alert(resultS.message);
                }
            }
        });
    }
});
$.validator.addMethod('leequelto', function(value, element, param) {
      return this.optional(element) || value <= $(param).val();
}, 'invalid value');
$('body #addnewbook_Form').validate({
    rules: {
        txt_language: {required:true},
        txt_title : {required:true},
        /*txt_authorid: {required:true},*/
        txt_publisheddate: {required:true},
        txt_image: {required:true,extension:'png|jpg|jpeg|gif'},
        bookFile: {required:true,extension:'pdf|txt|doc|docx|ppt|pptx'},
        txt_originalprice: {required:false,number:true},
        txt_sellingprice: {leequelto:'#txt_originalprice'},
        txt_category: {required:true},
    },
    messages: {
        txt_language: {required:"Select language field is required."},
        txt_title: {required:"Book name field is required."},
       /* txt_authorid: {required:"Select author field is required."},*/
        txt_publisheddate: {required:"publish date field is required."},
        txt_image: {required:"Thumbnail file field is required.",extension:"Allowed extension only png,jpg,jpeg and gif."},
        bookFile: {required:"Book file field is required.",extension:"Allowed extension only pdf,txt,docx,ppt and pptx."},
        txt_category: {required:"Select category Field is required."},
        txt_originalprice: {},
        txt_sellingprice: {leequelto:'Selling Price should less than original price.'}
        
    }
});
$('body').on('submit','#addnewbook_Form',function(evt){
    evt.preventDefault();
    if($("#addnewbook_Form").valid()){
        var textval = $("#txtEditor").Editor("getText");
        if(textval){
            $("body #txt_description-error").css("display","node");
            var formData = new FormData(this);
            formData.append("text_message", textval);
            $.ajax({
                type: "POST",
                url: SITEURL+"dashboard/books/addbookbyAjax",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function(resultS){
                    console.log(resultS.message);
                    if(resultS.status == 200){
                        $.alert(resultS.message);
                        window.location.reload(true);
                    }else{
                        $.alert(resultS.message);
                    }
                }
            });
        }else{
            $("body #txt_description-error").css("display","block").text("Description field is required.");
            return false; 
        }
    }
});

$('body #editBook_Form').validate({
    rules: {
        txt_language: {required:true},
        txt_title : {required:true},
        /*txt_authorid: {required:true},*/
        txt_publisheddate: {required:true},
         /*txt_image: {extension:'png|jpg|jpeg|gif',filesize:204800},
         bookFile: {extension:'pdf|txt|doc|docx|ppt|pptx',filesize:1024000},*/
        txt_originalprice: {required:false,number:true},
        txt_sellingprice: {leequelto:'#txt_originalprice'},
        txt_category: {required:true},
    },
    messages: {
        txt_language: {required:"Select language field is required."},
        txt_title: {required:"Book name field is required."},
        /*txt_authorid: {required:"Select author field is required."},*/
        txt_publisheddate: {required:"publish date field is required."},
        /*txt_image: {extension:"Allowed extension only png,jpg,jpeg and gif."},
        bookFile: {extension:"Allowed extension only pdf,txt,docx,ppt and pptx."},*/
        txt_category: {required:"Select category Field is required."},
        txt_originalprice: {},
        txt_sellingprice: {leequelto:'Selling Price should less than original price.'}
        
    }
});
$('body').on('submit','#editBook_Form',function(evt){
    evt.preventDefault();
    if($("#editBook_Form").valid()){
        var textval = $("#txtEditor").Editor("getText");
        if(textval){
            $("body #txt_description-error").css("display","node");
            var formData = new FormData(this);
            formData.append("text_message", textval);
            $.ajax({
                type: "POST",
                url: SITEURL+"dashboard/books/editbookbyAjax",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function(resultS){
                    console.log(resultS.message);
                    if(resultS.status == 200){
                        $.alert(resultS.message);
                        window.location.reload(true);
                    }else{
                        $.alert(resultS.message);
                    }
                }
            });
        }else{
            $("body #txt_description-error").css("display","block").text("Description field is required.");
            return false; 
        }
    }
});

/* Add Book Chapter validation start*/
$('body #addChapterForm').validate({
    rules: {
        chapter_title : {required:true},
        chapter_book: {required:true}
        
    },
    messages: {
        chapter_title: {required:"Please enter Book Chapter Title."},
        chapter_book: {required:"Please select Book."}   
           
    }
});


$( "#addChapterForm" ).submit(function( event ) {
    var textval = $("#txtEditor").Editor("getText");
        
        if(textval != ""){
            $('#txtEditor').html(textval);
        }
  return true;
  
});

function deletebookChapter(id){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
                $.ajax({
                    type: 'POST',
                    url: SITEURL+'dashboard/deletebookChapter',
                    data: {'delid': id},
                    dataType: 'json',
                    success:function(responce){
                        if(responce.status == 'Success'){
                            $.alert(responce.message);
                            location.href=responce.data;
                        }else{
                            $.alert(responce.message);
                        }
                    }
                });
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}

$('#chapter_book').change(function(event){
    var selected = $(this).val();
    if(selected != ''){
        window.location.href=SITEURL+'dashboard/viewallBooksChapter/'+selected;
    }else{
        window.location.href=SITEURL+'dashboard/viewallBooksChapter';
    }
});


/* Add book chapter validation end */

$('body #addnewuser_Form').validate({
    ignore:'',
    rules: {
        txt_firstname: {required:true},
        txt_lastname : {required:true},
        txt_email: {required:true,validate_email:true},
        txt_mobile: {required:true,number:true},
        /*txt_role: {required:true},*/
        txt_image: {extension:'png|jpg|jpeg|gif'},
        //txt_password: {required:true,minlength:6},
        txt_password: {minlength:6},

    },
    messages: {
        txt_firstname: {required:"First name is required."},
        txt_lastname : {required:"Last name is required."},
        txt_email:  {required:"Email address is required.",validate_email:"Please enter valid Email address."},
        txt_mobile: {required:"Mobile number is required.",number:"Please enter valid Mobile number."},
        //txt_mobile: {number:"Please enter valid Mobile number."},
        /*txt_role: {required:"Select role field is required."},*/
        txt_image: {extension:"Allowed extension only png,jpg,jpeg and gif."},
        //txt_password: {required:"Password field is required."},
        txt_password: {required:"Password must be 6 characters long."},
        
    }
});
$('body #edituser_Form').validate({
    rules: {
        txt_firstname: {required:true},
        txt_lastname : {required:true},
        txt_email: {required:true,validate_email:true},
        txt_mobile: {required:true},
        txt_image: {extension:'png|jpg|jpeg|gif'},

    },
    messages: {
        txt_firstname: {required:"First name is required."},
        txt_lastname : {required:"Last name is required."},
        txt_email:  {required:"Email address is required."},
        txt_mobile: {required:"Mobile number is required.",number:"Please enter valid Mobile number."},
        txt_image: {extension:"Allowed extension only png,jpg,jpeg and gif."},
    }
});


$('body #addnewcategory_Form').validate({
    rules: {
        txt_categoryname: {required:true},
        txt_catdescription : {required:true},
        txt_image: {required:true,extension:'png|jpg|jpeg|gif'},

    },
    messages: {
        txt_categoryname: {required:"Category name is required."},
        txt_catdescription : {required:"Description is required."},
        txt_image: {extension:"Allowed extension only png,jpg,jpeg and gif."},
    }
});
$('body #editcategory_Form').validate({
    rules: {
        txt_categoryname: {required:true},
        txt_catdescription : {required:true},
        txt_image: {extension:'png|jpg|jpeg|gif'},

    },
    messages: {
        txt_categoryname: {required:"Category name is required."},
        txt_catdescription : {required:"Description is required."},
        txt_image: {extension:"Allowed extension only png,jpg,jpeg and gif."},
    }
});
$('body #addnewplan_Form').validate({
    rules: {
        txt_name: {required:true},
        txt_price : {required:true,digits:true},
        txt_valididty: {required:true},
        txt_descriptions: {required:true}
    },
    messages: {
        txt_name: {required:"Plan name is required."},
        txt_price: {required:"Plan price is required."},
        txt_valididty: {required:"Validity is required."},
        txt_descriptions: {required:"Description is required."},
    }
});
$('body #editplan_Form').validate({
    rules: {
        txt_name: {required:true},
        txt_price : {required:true,digits:true},
        txt_valididty: {required:true},
        txt_descriptions: {required:true}
    },
    messages: {
        txt_name: {required:"Plan name is required."},
        txt_price: {required:"Plan price is required."},
        txt_valididty: {required:"Validity is required."},
        txt_descriptions: {required:"Description is required."},
    }
});


$('body #addnewvideo_Form').validate({
    rules: {
        txt_language2: {required:true},
        txt_category: {required:true},
        txt_title: {required:true},
        txt_videoid: {required:true},
        txt_image: {required:true,extension:"png|jpg|jpeg|gif"},
    },
    messages: {
        txt_language2: {required:"Language is required."},
        txt_category: {required:"Category is required."},
        txt_title: {required:"Title is required."},
        txt_videoid: {required:"Video id is required."},
        txt_image: {required:"Image is required.",extension:"Allowed extension only png,jpg,jpeg and gif."},
        
    }
});


$('body').on('submit','#addnewvideo_Form',function(evt){
    evt.preventDefault();
    if($("#addnewvideo_Form").valid()){
        var textval = $("#txtEditor").Editor("getText");
        if(textval){
            $("body #txt_message-error").css("display","node");
            var formData = new FormData(this);
            formData.append("text_message", textval);
            $.ajax({
                type: "POST",
                url: SITEURL+"dashboard/video/addvideoAjax",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function(resultS){
                    console.log(resultS.message);
                    if(resultS.status == 200){
                        $.alert(resultS.message);
                        window.location.reload(true);
                    }else{
                        $.alert(resultS.message);
                    }
                }
            });
        }else{
            $("body #txt_message-error").css("display","block").text("Description field is required.");
            return false; 
        }
    }
});



$('body #editvideo_Form').validate({
    rules: {
        txt_language2: {required:true},
        txt_category: {required:true},
        txt_title: {required:true},
        txt_videoid: {required:true},
       // txt_image: {required:true,extension:"png|jpg|jpeg|gif"},
    },
    messages: {
        txt_language2: {required:"Language is required."},
        txt_category: {required:"Category is required."},
        txt_title: {required:"Title is required."},
        txt_videoid: {required:"Video id is required."},
       // txt_image: {required:"Image is required.",extension:"Allowed extension only png,jpg,jpeg and gif."},
        
    }
});

$('body').on('submit','#editvideo_Form',function(evt){
    evt.preventDefault();
    if($("#editvideo_Form").valid()){
        var textval = $("#txtEditor").Editor("getText");
        if(textval){
            $("body #txt_message-error").css("display","node");
            var formData = new FormData(this);
            formData.append("text_message", textval);
            $.ajax({
                type: "POST",
                url: SITEURL+"dashboard/video/editvideoAjax",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function(resultS){
                    console.log(resultS.message);
                    if(resultS.status == 200){
                        $.alert(resultS.message);
                        window.location.reload(true);
                    }else{
                        $.alert(resultS.message);
                    }
                }
            });
        }else{
            $("body #txt_message-error").css("display","block").text("Description field is required.");
            return false; 
        }
    }
});


function deleteVideoDetail(ids){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
                $.ajax({
                    type: 'POST',
                    url: SITEURL+'dashboard/video/deleteVideo',
                    data: {'delid': ids},
                    dataType: 'json',
                    success:function(responce){
                        if(responce.status == 'Success'){
                            $.alert(responce.message);
                            location.href=responce.data;
                        }else{
                            $.alert(responce.message);
                        }
                    }
                });
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}

$('body').on('change', 'input[type=radio][name=filterbook]', function(){
    var selecteD = $(this).val();
    if(selecteD == 'somali'){
        window.location.href=SITEURL+'dashboard/viewallBooks/'+selecteD;
    }else if(selecteD == 'english'){
        window.location.href=SITEURL+'dashboard/viewallBooks/'+selecteD;
    }else{
        window.location.href=SITEURL+'dashboard/viewallBooks';
    }
});


function deletebookReview(id){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
                $.ajax({
                    type: 'POST',
                    url: SITEURL+'dashboard/deletebookReview',
                    data: {'delid': id},
                    dataType: 'json',
                    success:function(responce){
                        if(responce.status == 'Success'){
                            //$.alert(responce.message);
                            location.href=responce.data;
                        }else{
                            $.alert(responce.message);
                        }
                    }
                });
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}

function deleteOrder(id){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
                $.ajax({
                    type: 'POST',
                    url: SITEURL+'dashboard/deleteOrder',
                    data: {'delid': id},
                    dataType: 'json',
                    success:function(responce){
                        if(responce.status == 'Success'){
                            //$.alert(responce.message);
                            location.href=responce.data;
                        }else{
                            $.alert(responce.message);
                        }
                    }
                });
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}



function approvebookReview(id){
    $.ajax({
        type: 'POST',
        url: SITEURL+'dashboard/approvebookReview',
        data: {'id': id},
        dataType: 'json',
        success:function(responce){
            if(responce.status == 'Success'){
                //$.alert(responce.message);
                location.href=responce.data;
            }else{
                $.alert(responce.message);
            }
        }
    });
}

function orderStatusChange(id,status){
    $.ajax({
        type: 'POST',
        url: SITEURL+'dashboard/orderStatusChange',
        data: {'id': id,'status':status},
        dataType: 'json',
        success:function(responce){
            if(responce.status == 'Success'){
                //$.alert(responce.message);
                location.href=responce.data;
            }else{
                $.alert(responce.message);
            }
        }
    });
}


$('#book_review').change(function(event){
    var selected = $(this).val();
    if(selected != ''){
        window.location.href=SITEURL+'dashboard/manageBookReviews/'+selected;
    }else{
        window.location.href=SITEURL+'dashboard/manageBookReviews';
    }
});

$('#order_author').change(function(event){
    var statusSel = $('#order_payment_status').val();
    var selected = $(this).val();
    if(selected != ''){
        if(statusSel){
            window.location.href=SITEURL+'dashboard/manageBookOrders/'+selected+'/'+statusSel;
        }else{
            window.location.href=SITEURL+'dashboard/manageBookOrders/'+selected;
        }
        
    }else{
        if(statusSel){
            window.location.href=SITEURL+'dashboard/manageBookOrders/0/'+statusSel;
        }else{
            window.location.href=SITEURL+'dashboard/manageBookOrders';
        }
        
    }
});


$('#order_payment_status').change(function(event){
    var authorSel = $('#order_author').val();
    var selected = $(this).val();
    if(selected != ''){
        if(authorSel){
            window.location.href=SITEURL+'dashboard/manageBookOrders/'+authorSel+'/'+selected;
        }else{
            window.location.href=SITEURL+'dashboard/manageBookOrders/0/'+selected;
        }
        
    }else{
        if(authorSel){
            window.location.href=SITEURL+'dashboard/manageBookOrders/'+authorSel;
        }else{
            window.location.href=SITEURL+'dashboard/manageBookOrders';
        }
        
    }
});

function submitOrderReport(){
    alert("Hello");
    return false;
}

$(function() {
    $('#report_start_date').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
    // alert("You are " + years + " years old!");
  });
  $('#report_end_date').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
    // alert("You are " + years + " years old!");
  });
});

function authorApproved(aid,status){
    $.ajax({
        type: 'POST',
        url: SITEURL+'dashboard/authorApproved',
        data: {'id': aid,'status':status},
        dataType: 'json',
        success:function(responce){
            if(responce.status == '200'){
                $.alert(responce.message);
                setTimeout(function(){ window.location.reload(true); },3000);
            }else{
                $.alert(responce.message);
            }
        }
    });
}
function bookApproved(bid,status){
    $.ajax({
        type: 'POST',
        url: SITEURL+'dashboard/bookApproved',
        data: {'bid': bid,'status':status},
        dataType: 'json',
        success:function(responce){
            if(responce.status == '200'){
                $.alert(responce.message);
                setTimeout(function(){ window.location.reload(true); },3000);
            }else{
                $.alert(responce.message);
            }
        }
    });
}

$('body #changePassword_Form').validate({
    rules:{
        txt_newpass: {required:true, minlength:6},
        txt_cnfnewpass: {required:true,equalTo:'#txt_newpass'},
    },messages:{
        txt_newpass: {required:'Password field is required.'},
        txt_cnfnewpass: {required:'Confirm password field is required.'},
    }
});
$('body').on('submit', '#changePassword_Form', function(e){
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: 'POST',
        url: SITEURL+'dashboard/changePasswordbyajax',
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        success:function(responce){
            if(responce.status == 200){
                $.alert(responce.message);
                setTimeout(function(){ window.location.reload(true); },2000);
            }else{
                $.alert(responce.message);
            }
        }
    });
});


function paymentStatusChange(id,status){
    $.ajax({
        type: 'POST',
        url: SITEURL+'dashboard/paymentStatusChange',
        data: {'id': id,'status':status},
        dataType: 'json',
        success:function(responce){
            if(responce.status == 'success'){
                $.alert(responce.message);
                setTimeout(function(){ window.location.reload(true); },3000);
            }else{
                $.alert(responce.message);
            }
        }
    });
}
