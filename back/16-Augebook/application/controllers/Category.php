<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
	protected $selectedLanguage;
	public function __construct (){
		parent :: __construct();
		$this->load->model('user_Auth');
		$this->load->model('book_model');
		if($this->session->userdata('get_language')){
			$this->selectedLanguage = $this->session->userdata('get_language');
		}else{
			$this->selectedLanguage ='english';
		}

	}
	public function index()
	{
		$data["title"] = 'Category Page | '.SITENAME;
		$data["allCategory"] = $this->user_Auth->getData('categories', $w = array('c_status' => '1'), $s='', $sh = 'c_id ASC');
		$data['comingsoonbookbycategory'] = $this->user_Auth->getData('books', array('b_language' => $this->selectedLanguage, 'b_status' => '1'), $se='b_id', $se='b_id DESC',$g='');
		
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-archivecategory',$data);
		$this->load->view('front/common/footer',$data);
	}
	public function categoryselect($catname, $page=false)
	{
		$catname =urldecode($catname);
		$categoryData = $this->user_Auth->getData('categories',$W=array('c_slug' => $catname));
		/*echo '<pre>';
		print_r($categoryData);exit;*/
		$filtercatID =$this->input->get('filterCat');

		$catShort = (($this->input->get('txtShort'))?$this->input->get('txtShort'):false);

		$data["title"] = $categoryData[0]->c_name.' | '.SITENAME;
		$data["categoryName"] = $categoryData[0]->c_name;
		$data["selectedCategory"] = $categoryData[0]->c_id;
		$data['allCategoriesList'] = $this->user_Auth->getData('categories',$filter='', $sel='', $sh = 'c_id ASC');
		

		if($filtercatID){
			
			$mostPopularBooks = $this->user_Auth->mostpopularBooksIDbyfilteronecategory($filtercatID,$catShort);
			$data['mostPopularBooks'] = $mostPopularBooks;
			$data['allpopAuthorList'] = $this->user_Auth->mostpopauthorrefbybookid($mostPopularBooks);
			$data['allbooksidByCategory'] = $this->user_Auth->getDatabycategoryFilter('books',$w=$filtercatID, $se='b_id', $sh=$catShort,$g='', $st = 0, $end = 10);
			$data['comingsoonbookbycategory'] = $this->user_Auth->comingsoonbookbycategoryFilter('books', $w =$filtercatID, $se='b_id', $sh=$catShort,$g='');
			
		}else{
			$data['allpopAuthorList'] = $this->user_Auth->getPupularAuthor($categoryData[0]->c_id);
			$data['mostPopularBooks'] = $this->user_Auth->mostpopularBooksIDbyfilteronecategory($categoryData[0]->c_id);
			$data['allbooksidByCategory'] = $this->user_Auth->getData('books', array('b_category' => $categoryData[0]->c_id, 'b_language' => $this->selectedLanguage, 'b_status' => '1'), $se='b_id', $se='b_id ASC',$g='');
			$data['comingsoonbookbycategory'] = $this->user_Auth->getData('books', array('b_category' => $categoryData[0]->c_id, 'b_language' => $this->selectedLanguage, 'b_status' => '1'), $se='b_id', $se='b_id DESC',$g='');
		}
		
		/*echo '<pre>';
		print_r($allBooksByCategory);exit;*/

		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-category',$data);
		$this->load->view('front/common/footer',$data);
	}
	public function categories($catname, $cat){
		
		$catname =urldecode($catname);
		$cat =urldecode($cat);
		$data["title"] = $cat.' Book Page | '.SITENAME;
		$data["categoryName"] = $cat;
		$data["bookName"] = $cat;
		
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-singlebook',$data);
		$this->load->view('front/common/footer',$data);
	}
	
}
