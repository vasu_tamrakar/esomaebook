<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->library('upload');
		if(($this->is_logged_in_user_info['u_role'] < 4)){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
	}
	/*------------------------------------
		Dashboard index 
	------------------------------------*/
	public function index()
	{
		$data['title'] = 'Dashboard Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] < 4){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] = $this->is_logged_in_user_info;
		$data["customerAdded"] =$this->is_logged_in_user_info;
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-dashboard', $data);
		$this->load->view('front/common/footer', $data);
	}
	/*------------------------------------
		user profile($uid) 
	------------------------------------*/
	public function profile($uid)
	{
		$data['title'] = 'User Profile Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] < 4){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] =$this->is_logged_in_user_info;
		$data["customerAdded"] =$this->is_logged_in_user_info;
		$post =$this->input->post();
		if($post){
			$this->form_validation->set_rules('txt_firstname', 'First Name', 'required');
			$this->form_validation->set_rules('txt_lastname', 'Last Name', 'required');
			$this->form_validation->set_rules('txt_email', 'Email Address', 'required');
			$this->form_validation->set_rules('txt_mobile', 'Mobile', 'required');
			if($this->form_validation->run() === TRUE){
				$formData['uc_firstname'] = $post["txt_firstname"];
				$formData['uc_lastname'] = $post["txt_lastname"];
				$formData['uc_email'] = $post["txt_email"];
				$formData['uc_mobile'] = $post["txt_mobile"];
				if($post["txt_password"]){
					$formData['uc_mobile'] = md5($post["txt_password"]);	
				}
				$formData['uc_status'] = (($post["txt_status"] == 1)?'1':'0');
				$formData['uc_modified'] = date('Y-m-d H:i:s');

				$result = $this->user_Auth->exist_emailMobile($post["txt_email"], $post["txt_mobile"], $this->is_logged_in_user);
					if($result["status"] == 'success'){

						if($_FILES['txt_image']['name']){
							$Imgres = $this->do_upload('txt_image');
							
							if(isset($Imgres['upload_data'])){
								$formData['uc_image'] = $Imgres['upload_data']['file_name'];
								
							}else{
								$this->session->set_flashdata('alert' ,'error');
								$this->session->set_flashdata('message' ,$Imgres['error']);
								redirect('user/profile/'.$this->is_logged_in_user);
								exit();
							}
						}
						$resultUpdate = $this->user_Auth->update('user_credentials', $formData, $filter=array('uc_id' => $this->is_logged_in_user) );
						if($resultUpdate){
							$user = $this->user_Auth->getData('user_credentials', $where = array('uc_id' => $this->is_logged_in_user),$se='',$s='');

							$udata["u_id"] = $user[0]->uc_id;
							$udata["u_firstname"] = $user[0]->uc_firstname;
							$udata["u_lastname"] = $user[0]->uc_lastname;
							$udata["u_email"] = $user[0]->uc_email;
							$udata["u_mobile"] = $user[0]->uc_mobile;
							$udata["u_image"] = (($user[0]->uc_image)?$user[0]->uc_image:'user.png');
							$udata["u_role"] = $user[0]->uc_role;
							$udata["u_status"] = $user[0]->uc_status;
							$udata["u_created"] = $user[0]->uc_created;
							$udata["u_modified"] = (($user[0]->uc_modified >0)?$user[0]->uc_modified:'');
							$this->session->set_userdata('is_logged_in_user', $user[0]->uc_id);
							$this->session->set_userdata('is_logged_in_user_info', $udata);
							$this->session->set_flashdata('alert', "success");
							$this->session->set_flashdata('message', 'Profile successfully updated..');
							redirect('user/profile/'.$user[0]->uc_id);
							exit();
						}else{
							$this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,'Update Process Failed.');
							$this->load->view('front/common/header', $data);
							$this->load->view('front/page-userprofile', $data);
							$this->load->view('front/common/footer', $data);
							exit();
						}
						

					}else{
						$this->session->set_flashdata('alert' ,'error');
						$this->session->set_flashdata('message' ,$result["message"]);
						$this->load->view('front/common/header', $data);
						$this->load->view('front/page-userprofile', $data);
						$this->load->view('front/common/footer', $data);
					}

			}else{
				$this->load->view('front/common/header', $data);
				$this->load->view('front/page-userprofile', $data);
				$this->load->view('front/common/footer', $data);
			}
		}else{
			$this->load->view('front/common/header', $data);
			$this->load->view('front/page-userprofile', $data);
			$this->load->view('front/common/footer', $data);
		}
		
	}
	/*-------------------------------------------
		do_upload(name,folder);
	-------------------------------------------*/

	public function do_upload($fileName)
    {
        $config['upload_path']          = './uploads/users/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png';
        $config['max_size']             = 2048;

        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($fileName))
        {
                return array('error' => $this->upload->display_errors());

        }
        else
        {
                return array('upload_data' => $this->upload->data());

        }
    }

    /*-------------------------------------------
		addAuthor();
	-------------------------------------------*/

	public function addAuthor()
    {
        $data['title'] = 'Add New Author Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] != 4)	{
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] = $this->user_Auth->get_authorByuid($this->is_logged_in_user);
		$post = $this->input->post();
		if($post){
			$this->form_validation->set_rules('txt_authorname', 'Author Name', 'required');
			$this->form_validation->set_rules('txt_published', 'Publish Year', 'required|numeric[4]');
			$this->form_validation->set_rules('txt_description', 'Description', 'required');
			if($this->form_validation->run() === TRUE){
				$formData["a_name"] = $post["txt_authorname"];
				$formData["a_year"] = $post["txt_published"];
				$formData["a_description"] = $post["txt_description"];
				$formData["a_created"] = date('Y-m-d H:i:s');
				$formData["a_status"] = '1';
				$formData["a_fK_of_uc_id"] = $this->is_logged_in_user;
				if($post["txt_facebook"]){
					$formData["a_facebook"] = $post["txt_facebook"];
				}
				if($post["txt_twitter"]){
					$formData["a_twitter"] = $post["txt_twitter"];
				}
				if($post["txt_instagram"]){
					$formData["a_instagram"] = $post["txt_instagram"];
				}
				if($post["txt_gplush"]){
					$formData["a_gplush"] = $post["txt_gplush"];
				}
				if($_FILES["txt_image"]["name"]){

					$config['upload_path']          = './uploads/authors/';
		        	$config['allowed_types']        = 'gif|jpg|jpeg|png';
		        	$config['max_size']             = 2048;

        			$this->upload->initialize($config);
			       
					if( ! $this->upload->do_upload('txt_image') ){
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', $this->upload->display_errors());
						redirect("user/addAuthor"); exit();
					}else{
						$dataImg = $this->upload->data();
						$formData["a_image"] = $dataImg["file_name"];
						$instID = $this->user_Auth->insert('authors',$formData);
						if($instID){
							$this->session->set_flashdata('alert', 'success');
							$this->session->set_flashdata('message', 'Successfully save.');
							redirect("user/editAuthor/$instID"); exit();
						}else{
							$this->session->set_flashdata('alert', 'error');
							$this->session->set_flashdata('message', 'Insert Process failed.');
							$this->load->view('front/common/header', $data);
							$this->load->view('front/page-addnewauthor', $data);
							$this->load->view('front/common/footer', $data);
						}
					}
				}else{
					$instID = $this->user_Auth->insert('authors',$formData);
					if($instID){
						$this->session->set_flashdata('alert', 'success');
						$this->session->set_flashdata('message', 'Successfully save.');
						redirect("user"); exit();
					}else{
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', 'Insert Process failed.');
						$this->load->view('front/common/header', $data);
						$this->load->view('front/page-addnewauthor', $data);
						$this->load->view('front/common/footer', $data);
					}
				}
			}else{
				$this->load->view('front/common/header', $data);
				$this->load->view('front/page-addnewauthor', $data);
				$this->load->view('front/common/footer', $data);
			}
		}else{
			$this->load->view('front/common/header', $data);
			$this->load->view('front/page-addnewauthor', $data);
			$this->load->view('front/common/footer', $data);
		}
    }
	/*----------------------------------------------------
		editAuthor(aId)
	-----------------------------------------------------*/
	public function editAuthor($aID){
		$data['title'] = 'Edit Author Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] != 4){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] = $this->is_logged_in_user_info;
		if($data["authorAdded"]){

			$post = $this->input->post();
			if($post){
				$this->form_validation->set_rules('txt_authorname', 'Author Name', 'required');
				$this->form_validation->set_rules('txt_published', 'Publish Year', 'required|numeric[4]');
				$this->form_validation->set_rules('txt_description', 'Description', 'required');
				if($this->form_validation->run() === TRUE){
					$formData["a_name"] = $post["txt_authorname"];
					$formData["a_year"] = $post["txt_published"];
					$formData["a_description"] = $post["txt_description"];
					$formData["a_modified"] = date('Y-m-d H:i:s');
					$formData["a_status"] = (($post["txt_status"])?'1':'0');
					if($post["txt_facebook"]){
						$formData["a_facebook"] = $post["txt_facebook"];
					}
					if($post["txt_twitter"]){
						$formData["a_twitter"] = $post["txt_twitter"];
					}
					if($post["txt_instagram"]){
						$formData["a_instagram"] = $post["txt_instagram"];
					}
					if($post["txt_gplush"]){
						$formData["a_gplush"] = $post["txt_gplush"];
					}
					if($_FILES["txt_image"]["name"]){

						$config['upload_path']          = './uploads/authors/';
			        	$config['allowed_types']        = 'gif|jpg|jpeg|png';
			        	$config['max_size']             = 2048;

	        			$this->upload->initialize($config);
				       
						if( ! $this->upload->do_upload('txt_image') ){
							$this->session->set_flashdata('alert', 'error');
							$this->session->set_flashdata('message', $this->upload->display_errors());
							redirect("user/addAuthor"); exit();
						}else{
							$dataImg = $this->upload->data();
							$formData["a_image"] = $dataImg["file_name"];
							$instID = $this->user_Auth->update('authors', $formData, array("a_fK_of_uc_id" => $this->is_logged_in_user));
							if($instID){
								$this->session->set_flashdata('alert', 'success');
								$this->session->set_flashdata('message', 'Successfully save.');
								redirect("user/editAuthor/".$data["authorAdded"][0]->a_id); exit();
							}else{
								$this->session->set_flashdata('alert', 'error');
								$this->session->set_flashdata('message', 'Update Process failed.');
								$this->load->view('front/common/header', $data);
								$this->load->view('front/page-editauthor', $data);
								$this->load->view('front/common/footer', $data);
							}
						}
					}else{
						$instID = $this->user_Auth->update('authors', $formData, array("a_fK_of_uc_id" => $this->is_logged_in_user));
						
						if($instID){
							$this->session->set_flashdata('alert', 'success');
							$this->session->set_flashdata('message', 'Successfully Update.');
							redirect("user/editAuthor/".$data["authorAdded"][0]->a_id); exit();
						}else{
							$this->session->set_flashdata('alert', 'error');
							$this->session->set_flashdata('message', 'Update Process failed.');
							$this->load->view('front/common/header', $data);
							$this->load->view('front/page-editauthor', $data);
							$this->load->view('front/common/footer', $data);
						}
					}
				}else{
					$this->load->view('front/common/header', $data);
					$this->load->view('front/page-editauthor', $data);
					$this->load->view('front/common/footer', $data);
				}
			}else{
				$this->load->view('front/common/header', $data);
				$this->load->view('front/page-editauthor', $data);
				$this->load->view('front/common/footer', $data);
			}
		}else{
			redirect('user/addAuthor'); exit();
		}
	}



	/*-------------------------------------------
		addbook();
	-------------------------------------------*/

	public function addbook()
    {
        $data['title'] = 'Add New Book Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] != 4)	{
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		
		$data["authorAdded"] = $this->is_logged_in_user_info;
		$post = $this->input->post();
		if($post){
			
			$this->form_validation->set_rules('txt_bookname', 'Author Name', 'required');
			$this->form_validation->set_rules('txt_published', 'Publish Year', 'required');
			$this->form_validation->set_rules('txt_category', 'category', 'required');
			// $this->form_validation->set_rules('txt_frontimage', 'Fornt Image', 'required');
			// $this->form_validation->set_rules('txt_bookfile', 'Book File', 'required');
			$this->form_validation->set_rules('txt_rating', 'Rating', 'required');
			if($this->form_validation->run() === TRUE){
				$formData["b_title"] 		= $post["txt_bookname"];
				$formData["b_published"]	= date('Y-m-d H:i:s', strtotime($post["txt_published"]));
				$formData["b_category"] 	= $post["txt_category"];
				$formData["b_rating"] 		= $post["txt_rating"];
				$formData["b_created"] 		= date('Y-m-d H:i:s');
				$formData["b_status"] 		= '1';
				$formData["b_fk_of_aid"] 	= $authorAddedinfo[0]->a_id;
				$formData["b_fk_of_uid"] 	= $this->is_logged_in_user;
				
				if($_FILES["txt_frontimage"]["name"]){

					$config['upload_path']          = './uploads/books/';
		        	$config['allowed_types']        = 'gif|jpg|jpeg|png';
		        	$config['max_size']             = 2048;

        			$this->upload->initialize($config);
			       
					if( ! $this->upload->do_upload('txt_frontimage') ){
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', $this->upload->display_errors());
						redirect("user/addbook"); exit();
					}else{
						$dataImg = $this->upload->data();
						$formData["b_image"] = $dataImg["file_name"];
					}
				}
				if($_FILES["txt_bookfile"]["name"]){

					$configfile['upload_path']          = './uploads/books/';
		        	$configfile['allowed_types']        = 'pdf|doc|docx|ppt|pptx|pdf|txt';
		        	$configfile['max_size']             = 102400;

        			$this->upload->initialize($configfile);
			       
					if( ! $this->upload->do_upload('txt_bookfile') ){
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', $this->upload->display_errors());
						redirect("user/addbook"); exit();
					}else{

						$dataImg = $this->upload->data();
						$formData['b_filetype'] = $dataImg['file_ext'];
						$formData["b_file"] = $dataImg["file_name"];
					}
				}
				$instID = $this->user_Auth->insert('books',$formData);
				if($instID){
					$this->session->set_flashdata('alert', 'success');
					$this->session->set_flashdata('message', 'Successfully save.');
					redirect("user/addbook"); exit();
				}else{
					$this->session->set_flashdata('alert', 'error');
					$this->session->set_flashdata('message', 'Insert Process failed.');
					$this->load->view('front/common/header', $data);
					$this->load->view('front/page-addnewbook', $data);
					$this->load->view('front/common/footer', $data);
				}
					
				
			}else{
				$this->load->view('front/common/header', $data);
				$this->load->view('front/page-addnewbook', $data);
				$this->load->view('front/common/footer', $data);
			}
		}else{
			$this->load->view('front/common/header', $data);
			$this->load->view('front/page-addnewbook', $data);
			$this->load->view('front/common/footer', $data);
		}
    }
    public function viewBooks(){
    	$data['title'] = 'View Book Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] != 4)	{
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		
		$data["authorAdded"] = $this->is_logged_in_user_info;
		$post = $this->input->post();
		$data["bookeslistID"] = $this->user_Auth->getData('books', $w=array('b_fk_of_aid'=> $this->is_logged_in_user), $se='b_id', $sh='b_id ASC');
		
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-viewbook', $data);
		$this->load->view('front/common/footer', $data);
		
    }
    
    /*-------------------------------
    	addAuthorbookbyAjax()
    --------------------------------*/
    public function addAuthorbookbyAjax(){
    	$post = $this->input->post(); 
    	if($post){
			if(empty($post['txt_language'])){
				echo json_encode(array('status' => 300, 'message' => 'Book Language is required.')); die();
			}
			if(empty($post['txt_title'])){
				echo json_encode(array('status' => 300, 'message' => 'Book Title is required.')); die();
			}
			/*if(empty($post['txt_publisheddate'])){
				echo json_encode(array('status' => 300, 'message' => 'Published date is required.')); die();
			}*/
			
			if(empty($post['txt_originalprice']) && ($post['txt_sellingprice'])){
				echo json_encode(array('status' => 300, 'message' => 'Original price is required.')); die();
			}
			
			if(!empty($post['txt_originalprice']) && !empty($post['txt_sellingprice'])){
				if($post['txt_originalprice'] >=  $post['txt_sellingprice']){}else{
					echo json_encode(array('status' => 300, 'message' => 'Selling Price should less than original price.')); die();
				}
			}
			/*if(empty($post['txt_category'])){
				echo json_encode(array('status' => 300, 'message' => 'Category field is required.')); die();
			}*/
			if(empty($post['text_message'])){
				echo json_encode(array('status' => 300, 'message' => 'Description is required.')); die();
			}

			$fomdata["b_language"] = $post["txt_language"];
			$fomdata["b_title"] = $post["txt_title"];

			$slug = strtolower(trim(preg_replace('/-{2,}/','-',preg_replace('/[^a-zA-Z0-9-]/', '-', $post["txt_title"])),"-"));
			$fomdata["b_slug"] = $slug;

			if($post["txt_publisheddate"])
			$fomdata["b_published"] = date('Y-m-d',strtotime($post["txt_publisheddate"]));
			// $fomdata["b_rating"] = $post["txt_rating"];
			$fomdata["b_originalprice"] = (($post["txt_originalprice"])?$post["txt_originalprice"]:0.00);
			$fomdata["b_sellingprice"] = (($post["txt_sellingprice"])?$post["txt_sellingprice"]:$fomdata["b_originalprice"]);
			$fomdata["b_category"] = $post["txt_category"];
			$fomdata["b_status"] = '2';
			$fomdata["b_created"] = date("Y-m-d H:i:s");
			$fomdata["b_fk_of_aid"] = $this->is_logged_in_user;
			$fomdata["b_fk_of_uid"] = $this->is_logged_in_user;
			$fomdata["b_description"] = $post["text_message"];

			$fomdata["b_publisher"] = $post["txt_publisher"];
			$fomdata["b_bookpages"] = (($post["txt_bookpages"])?$post["txt_bookpages"]:'');

			if($post['txt_isbn']){
				$fomdata["b_isbn"]	= $post['txt_isbn'];
			}else{
				$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
			    $isbn = array(); 
			    $alphaLength = strlen($alphabet) - 1; 
			    for ($i = 0; $i < 8; $i++) {
			        $n = rand(0, $alphaLength);
			        $isbn[] = $alphabet[$n];
			    }
			    $fomdata["b_isbn"]	= implode($isbn);
			}
			// print_r($fomdata); die;
			if($_FILES['txt_image']['name']){

				$img_config['upload_path']          = './uploads/books/';
		        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
		        $img_config['max_size']             = 3072;
		        // $img_config['encrypt_name'] = TRUE;
		        $this->upload->initialize($img_config);
		        if ( ! $this->upload->do_upload('txt_image'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            $fomdata['b_image'] = $upload_data['file_name'];
		        }
			}
			if($_FILES['bookFile']['name']){

				$config_doc['upload_path']          = './uploads/books/';
		        $config_doc['allowed_types']        = 'movie|mov|web|mflv|avi|mpg|mpeg|wmv|txt|doc|docx|pdf|ppt|pptx|mp4';
		        $config_doc['max_size']             = 800000000;
		        // $config_doc['encrypt_name'] = TRUE;
		        $this->upload->initialize($config_doc);
		        // $this->load->library('upload', $config_doc);

		        if ( ! $this->upload->do_upload('bookFile'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		        	$upload_fdata = $this->upload->data();
		        	$fomdata['b_file'] = $upload_fdata['file_name'];
		        }
			}

			$insertRes = $this->user_Auth->insert('books', $fomdata);
			$user = $this->user_Auth->getData('user_credentials', array('uc_id'=> 1, $se= 'uc_email,uc_firstname,uc_lastname'));
			$toD = array('email' => $user[0]->uc_email,'name' =>(isset($user[0]->uc_firstname)?$user[0]->uc_firstname:''));
			$html = '<label>You have a new book for approval.The book is <a href="'.site_url("dashboard/bookDetails/".$insertRes).'">Here</a>.</label>';
			//$toD = "paresh3779@gmail.com";
			$this->sendMailNotification($toD, $s=SITENAME." New Book Approval Request", $html);
			if($insertRes){
				$this->session->set_flashdata('alert', 'success');
				$this->session->set_flashdata('message', 'Book added successfully.<br>Your book will be viewable after approval.');
				echo json_encode(array('status' => 200, 'message' => 'Book added successfully.<br>Your book will be viewable after approval.'));
			}else{
				$this->session->set_flashdata('alert', 'error');
				$this->session->set_flashdata('message', 'Insert Process failed!.');
				echo json_encode(array('status' => 100, 'message' => 'Insert Process failed!.'));
			}
    	}else{
    		echo json_encode(array('status' => 100, 'message' => 'In-valid Method!.'));
    	}
    	die();
    }

    /*-----------------------------------------------------
		sendMailNotification()
		param@ $to =array("email","","name"=>"")
		param@ $subjec=""
		param@ $message = ""
		param@ $attachment =""
    -----------------------------------------------------*/
    public function sendMailNotification($to, $subject=false, $message=false, $attachment=false){
    	
		$tomail = $to["email"];
		$toname = $to["name"];

		$tomail = "paresh3779@gmail.com";
		$toname = "Admin";

		$html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div class="col-md-12">
					                <div style="margin: 30px;display: block;">
					                    <h3>Hi '.$toname.',</h3>
					                        '.(($message)?$message:'').'
					                </div>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

        $this->email->to($tomail);
        $this->email->from('pareshnagar87@gmail.com',SITENAME);
        if($attachment){
        	$this->email->attach($attachment);	
        }
        $this->email->subject( (($subject)?$subject:SITENAME.' Notification Email') );
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
            return 1;
        }else{
            return $this->email->print_debugger();
        }
    }

    /*-------------------------------------------
    	editbook()
    ------------------------------------------*/
    public function editbook($bid){
    	$data['title'] = 'Edit Book Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] < 4){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] = $this->is_logged_in_user_info;
		$bookDetails = $this->user_Auth->getData('books',array('b_id' => $bid));
		// print_r($bookDetails[0]->b_fk_of_uid);
		if((isset($bookDetails[0]->b_fk_of_uid)) && $bookDetails[0]->b_fk_of_uid == $this->is_logged_in_user){
			$data["bookDetails"] = $bookDetails;
		}else{
			$data["bookDetails"] = FALSE;
			$this->session->set_flashdata('alert', 'warning');
			$this->session->set_flashdata('message', 'Permission Access Denide!.');
		}
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-authoreditbook', $data);
		$this->load->view('front/common/footer', $data);
    }
    /*---------------------------------------------------------
    	viewmyOrder()	// For author itself ordered book view
    ---------------------------------------------------------*/
    public function viewmyOrder($fitler=false){

    	$data['title'] = 'My Books Order Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] < 4){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] = $this->is_logged_in_user_info;
		if($fitler){
			if(($this->input->get('fromdate')) && ($this->input->get('todate'))){
				$fromdate = date('Y-m-d H:i:s',strtotime($this->input->get('fromdate')));
				$todate = date('Y-m-d H:i:s',strtotime($this->input->get('todate')));
				$data["myorderID"] = $this->user_Auth->getData('book_orders',$w = array('author_id' => $this->is_logged_in_user,'author_payment_status' => $fitler, 'created_at >=' => $fromdate, 'created_at <=' => $todate), $se='id',$sh = 'id DESC');
			}else{
				$data["myorderID"] = $this->user_Auth->getData('book_orders',$w = array('author_id' => $this->is_logged_in_user,'author_payment_status' => $fitler), $se='id',$sh = 'id DESC');
			}			
    	}else{
    		$data["myorderID"] = $this->user_Auth->getData('book_orders',$w = array('author_id' => $this->is_logged_in_user), $se='id',$sh = 'id DESC');
    	}
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-viewauthorOrder', $data);
		$this->load->view('front/common/footer', $data);
    }
    /*-----------------------------------------------
		myOrder() //For customer order display table 
    -----------------------------------------------*/
    public function myOrder(){
    	$data['title'] = 'My Order Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] < 4){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] = $this->is_logged_in_user_info;
		$data["customerAdded"] = $this->is_logged_in_user_info;
		$data["myorderID"] = $this->user_Auth->getData('book_orders',$w = array('user_id' => $this->is_logged_in_user), $se='id',$sh = 'id DESC');
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-cutomerOrderview', $data);
		$this->load->view('front/common/footer', $data);
    }
    /*--------------------------------------
    	authorchangePassord()
	--------------------------------------*/
	public function authorchangePassword(){
		$data['title'] = 'Change Password Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] < 4){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] = $this->is_logged_in_user_info;
		$data["customerAdded"] = $this->is_logged_in_user_info;
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-authorchangePassword', $data);
		$this->load->view('front/common/footer', $data);
	}
	/*--------------------------------------
    	authorchangePassord()
	--------------------------------------*/
	public function customerchangePassword(){
		$data['title'] = 'Change Password Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		$data["authorAdded"] = $this->is_logged_in_user_info;
		$data["customerAdded"] = $this->is_logged_in_user_info;
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-customrechangePassword', $data);
		$this->load->view('front/common/footer', $data);
	}
	/*--------------------------------------
		manageMybooks()
	--------------------------------------*/
	public function manageMybooks(){
		$data['title'] = 'Manage My Book Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		$data["authorAdded"] = $this->is_logged_in_user_info;
		$data["customerAdded"] = $this->is_logged_in_user_info;
		$today = date('Y-m-d H:i:s');
		$beforeamonth =date('Y-m-d H:i:s',strtotime( date( 'Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))) ."-1 month"));
		$accessbookDet = $this->user_Auth->getData('book_access',$w = array('ba_userid' => $this->is_logged_in_user), $se='');
		if($accessbookDet){
			$createAt = strtotime($accessbookDet[0]->ba_created);
			$updatedAt = strtotime($accessbookDet[0]->ba_changedate);
			if($createAt == $updatedAt) {

 				// $today
 				$dayDiff = $this->difference_bitweenDate($today, $accessbookDet[0]->ba_changedate);
 				
				if($dayDiff["days"] <= 3){
					$data['managemyBooks'] = $accessbookDet;
				}else{
					$data['managemyBooks']=FALSE;
				}
			}else{
				$oneMonthdiff = $this->difference_bitweenDate($accessbookDet[0]->ba_changedate,$today);
				//echo $oneMonthdiff["months"];exit;
				if($oneMonthdiff["months"] >= 1){
					$data['managemyBooks'] = $accessbookDet;
				}else{
					$data['managemyBooks']=FALSE;
				}
			}
		}else{
			$data['managemyBooks']=FALSE;
		}
		
/*
		if (registerddate == changebook date){
aaj ki date and change book date 3 days then allow to change
}
else{
changebook date and today's date 1month ka diff the allow to change
}*/


		
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-managemyBook', $data);
		$this->load->view('front/common/footer', $data);	
	}

	/*

	*/

	public function difference_bitweenDate($date1,$date2){
		// Declare and define two dates 
		$date1 = strtotime($date1);  
		$date2 = strtotime($date2);  
		  
		// Formulate the Difference between two dates 
		$diff = abs($date2 - $date1);  
		  
		  
		// To get the year divide the resultant date into 
		// total seconds in a year (365*60*60*24) 
		$years = floor($diff / (365*60*60*24));  
		  
		  
		// To get the month, subtract it with years and 
		// divide the resultant date into 
		// total seconds in a month (30*60*60*24) 
		$months = floor(($diff - $years * 365*60*60*24) 
		                               / (30*60*60*24));  
		  
		  
		// To get the day, subtract it with years and  
		// months and divide the resultant date into 
		// total seconds in a days (60*60*24) 
		$days = floor(($diff - $years * 365*60*60*24 -  
		             $months*30*60*60*24)/ (60*60*24)); 
		  
		  
		// To get the hour, subtract it with years,  
		// months & seconds and divide the resultant 
		// date into total seconds in a hours (60*60) 
		$hours = floor(($diff - $years * 365*60*60*24  
		       - $months*30*60*60*24 - $days*60*60*24) 
		                                   / (60*60));  
		  
		  
		// To get the minutes, subtract it with years, 
		// months, seconds and hours and divide the  
		// resultant date into total seconds i.e. 60 
		$minutes = floor(($diff - $years * 365*60*60*24  
		         - $months*30*60*60*24 - $days*60*60*24  
		                          - $hours*60*60)/ 60);  
		  
		  
		// To get the minutes, subtract it with years, 
		// months, seconds, hours and minutes  
		$seconds = floor(($diff - $years * 365*60*60*24  
		         - $months*30*60*60*24 - $days*60*60*24 
		                - $hours*60*60 - $minutes*60));  
		  
		// Print the result 
		return array("years" => $years, "months"=> $months, "days" =>$days, "hours"=>$hours,"minutes"=>$minutes, "seconds" => $seconds);	
	}
}







?>