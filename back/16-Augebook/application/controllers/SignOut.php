<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SignOut extends CI_Controller {
	// public function __construct (){
	// 	parent :: __construct();
		
	// }
	public function index()
	{
		$this->session->sess_destroy();
		$this->session->unset_userdata('is_logged_in_user');
		$this->session->unset_userdata('is_logged_in_user_info');
		$this->session->set_flashdata('alert','success');
		$this->session->set_flashdata('message','Sign out successfully.');
		redirect(base_url('signIn')); exit();
	}
}
