<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->library('upload');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}

	}
	public function index()
	{
		$data["title"] = 'Add New Books Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}else{
			$post = $this->input->post();
			if($post){
				// print_r($post); die;
				// Array ( [txt_name] => b [txt_published] => 2014 [txt_rating] => 1 [txt_category] => 7 )
				$this->form_validation->set_rules('txt_language','Book Language','required');
				$this->form_validation->set_rules('txt_title','Book Name','required');
				/*$this->form_validation->set_rules('txt_authorid','Author Id','required');*/
				$this->form_validation->set_rules('txt_publisheddate','Publish Date','required');
				/*$this->form_validation->set_rules('txt_price','Price','required');*/
				$this->form_validation->set_rules('txt_category','Category','required');
				if($this->form_validation->run() === TRUE){
					
					$fomdata["b_language"] = $post["txt_language"];
					$fomdata["b_title"] = $post["txt_title"];

					$slug = strtolower(trim(preg_replace('/-{2,}/','-',preg_replace('/[^a-zA-Z0-9-]/', '-', $post["txt_title"])),"-"));
					$fomdata["b_slug"] = $slug;

					$fomdata["b_published"] = date('Y-m-d',strtotime($post["txt_publisheddate"]));
					// $fomdata["b_rating"] = $post["txt_rating"];
					$fomdata["b_price"] = $post["txt_price"];
					$fomdata["b_category"] = $post["txt_category"];
					$fomdata["b_status"] = '1';
					$fomdata["b_created"] = date("Y-m-d H:i:s");
					$fomdata["b_fk_of_aid"] = $post["txt_authorid"];
					$fomdata["b_fk_of_uid"] = $this->is_logged_in_user;
					$fomdata["b_publisher"] = $post["txt_publisher"];
					$fomdata["b_bookpages"] = $post["txt_bookpages"];

					if($post['txt_isbn']){
						$fomdata["b_isbn"]	= implode($post['txt_isbn']);
					}else{
						$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
					    $isbn = array(); 
					    $alphaLength = strlen($alphabet) - 1; 
					    for ($i = 0; $i < 8; $i++) {
					        $n = rand(0, $alphaLength);
					        $isbn[] = $alphabet[$n];
					    }
					    $fomdata["b_isbn"]	= implode($isbn);
					}

					if($_FILES['txt_image']['name']){

						$img_config['upload_path']          = './uploads/books/';
				        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
				        $img_config['max_size']             = 3072;
				        // $img_config['encrypt_name'] = TRUE;
				        $this->upload->initialize($img_config);
				        if ( ! $this->upload->do_upload('txt_image'))
				        {
				            $this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,$this->upload->display_errors());
							
							redirect('dashboard/addnewBook'); exit();
				        }
				        else
				        {
				            $upload_data = $this->upload->data();
				            $fomdata['b_image'] = $upload_data['file_name'];
				        }
					}
					if($_FILES['bookFile']['name']){

						$config_doc['upload_path']          = './uploads/books/';
				        $config_doc['allowed_types']        = 'movie|mov|web|mflv|avi|mpg|mpeg|wmv|txt|doc|docx|pdf|ppt|pptx|mp4';
				        $config_doc['max_size']             = 800000000;
				        // $config_doc['encrypt_name'] = TRUE;
				        $this->upload->initialize($config_doc);
				        // $this->load->library('upload', $config_doc);

				        if ( ! $this->upload->do_upload('bookFile'))
				        {
				            $this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,$this->upload->display_errors());
							// print_r($fileRes['error']);echo "    ccccccccc"; die;
							redirect('dashboard/addnewBook'); exit();
				        }
				        else
				        {
				        	$upload_fdata = $this->upload->data();
				        	$fomdata['b_filetype'] = $upload_fdata['file_ext'];
							$fomdata['b_file'] = $upload_fdata['file_name'];
				        }
					}

					$insertRes = $this->user_Auth->insert('books', $fomdata);
					if($insertRes){
						$this->session->set_flashdata('alert', 'success');
						$this->session->set_flashdata('message', 'Successfully save.');
						redirect('dashboard/addnewBook'); exit(); 
					}else{
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', 'Insert Failed.');
						$this->load->view('dashboard/common/header',$data);
						$this->load->view('dashboard/page-addnewbook',$data);
						$this->load->view('dashboard/common/footer',$data);
					}
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-addnewbook',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-addnewbook',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
			 
		}
		
	}
	

    /*----------------------------------------------
		viewallBooks()
    ----------------------------------------------*/
    public function viewallBooks($filter=false){
    	$data["title"] = 'View All Books Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($filter == 'english'){
			$data['filtered'] = 'english';
			$data['allBookssids'] = $this->user_Auth->getData('books',$w=array('b_language' =>'english') ,$se='b_id', $short='b_id DESC');
		}else if($filter == 'somali'){
			$data['filtered'] = 'somali';
			$data['allBookssids'] = $this->user_Auth->getData('books',$w=array('b_language' =>'somali') ,$se='b_id', $short='b_id DESC');
		}else{
			
			$data['filtered'] = 'all';
			$data['allBookssids'] = $this->user_Auth->getData('books',$w='' ,$se='b_id', $short='b_id DESC');
		}
		
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-viewallbooks',$data);
		$this->load->view('dashboard/common/footer',$data);
    }


	/*----------------------------------------------
		bookDetails($aid)
    ----------------------------------------------*/
    public function bookDetails($bid){
    	$data["title"] = 'View Book Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($bid){
			$data['bookDetails'] = $this->user_Auth->getData('books',$w=array('b_id' => $bid) ,$se='', $short='');
	    	$this->load->view('dashboard/common/header',$data);
			$this->load->view('dashboard/page-viewbooksdetails',$data);
			$this->load->view('dashboard/common/footer',$data);
		}
		
    }

    /*----------------------------------------------
		editbookDetails($aid)
    ----------------------------------------------*/
    public function editbookDetails($bid){
    	$data["title"] = 'Edit Book Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($bid){
			$data['bookDetails'] = $this->user_Auth->getData('books',$w=array('b_id' => $bid) ,$se='', $short='');
			$post = $this->input->post();
			if($post){
				$this->form_validation->set_rules("txt_language","Language", "required");
				$this->form_validation->set_rules("txt_title","Title", "required");
				/*$this->form_validation->set_rules("txt_authorid","Author", "required");*/
				/*$this->form_validation->set_rules("txt_price","Price", "required");*/
				$this->form_validation->set_rules("txt_category","Category", "required");
				$this->form_validation->set_rules("txt_publisheddate","Publish date", "required");

				if($this->form_validation->run() === TRUE){
					$formData["b_language"] = $post["txt_language"];
					$formData["b_title"] = $post["txt_title"];

					$slug = strtolower(trim(preg_replace('/-{2,}/','-',preg_replace('/[^a-zA-Z0-9-]/', '-', $post["txt_title"])),"-"));
					$formData["b_slug"] = $slug;

					$formData["b_fk_of_aid"] = $post["txt_authorid"];
					$formData["b_published"] = date('Y-m-d', strtotime($post["txt_publisheddate"]));
					$formData["b_price"] = $post["txt_price"];
					$formData["b_category"] = $post["txt_category"];
					
					
					$formData["b_status"] = (($post["txt_status"]==1)?'1':'0');

					$formData["b_publisher"] = $post["txt_publisher"];
					$formData["b_bookpages"] = $post["txt_bookpages"];

					if($post['txt_isbn']){
						$formData["b_isbn"]	= implode($post['txt_isbn']);
					}else{
						$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
					    $isbn = array(); 
					    $alphaLength = strlen($alphabet) - 1; 
					    for ($i = 0; $i < 8; $i++) {
					        $n = rand(0, $alphaLength);
					        $isbn[] = $alphabet[$n];
					    }
					    $formData["b_isbn"]	= implode($isbn);
					}

					// if($post["txt_created"]){
					// 	$formData["b_created"] = date('Y-m-d H:i:s',strtotime($post["txt_created"]));	
					// }
					
					$formData["b_modified"] = date('Y-m-d H:i:s');
					$updated = $this->user_Auth->update('books',$formData,array('b_id'=>$bid));
					if($_FILES['txt_image']['name']){

							$img_config['upload_path']          = './uploads/books/';
					        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
					        $img_config['max_size']             = 3072;
					        // $img_config['encrypt_name'] = TRUE;
					        
					        $this->upload->initialize($img_config);
					        if ( ! $this->upload->do_upload('txt_image'))
					        {
					            $this->session->set_flashdata('alert' ,'error');
								$this->session->set_flashdata('message' ,$this->upload->display_errors());
								redirect('dashboard/editbookDetails/'.$bid); exit();
					        }
					        else
					        {
					            $upload_data = $this->upload->data();
					            $fomdata_img['b_image'] = $upload_data['file_name'];
					            $updated_2 = $this->user_Auth->update('books',$fomdata_img,array('b_id'=>$bid));
					        }
						}
					if($_FILES['bookFile']['name']){

						$config_doc['upload_path']          = './uploads/books/';
				        $config_doc['allowed_types']        = 'movie|mov|web|mflv|avi|mpg|mpeg|wmv|txt|doc|docx|pdf|ppt|pptx|mp4';
				        $config_doc['max_size']             = 800000000;
				        // $config_doc['encrypt_name'] = TRUE;
				        $this->upload->initialize($config_doc);
				        // $this->load->library('upload', $config_doc);

				        if ( ! $this->upload->do_upload('bookFile'))
				        {
				            $this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,$this->upload->display_errors());
							// print_r($fileRes['error']);echo "    ccccccccc"; die;
							redirect('dashboard/editbookDetails/'.$bid); exit();
				        }
				        else
				        {
				        	$upload_fdata = $this->upload->data();
				        	$fomdata_file['b_filetype'] = $upload_fdata['file_ext'];
							$fomdata_file['b_file'] = $upload_fdata['file_name'];
							$updated_2 = $this->user_Auth->update('books',$fomdata_file,array('b_id'=>$bid));
				        }
					}

					
					if($updated){
						$this->session->set_flashdata('alert' ,'success');
						$this->session->set_flashdata('message' ,'Successfully Updated.');
						redirect('dashboard/editbookDetails/'.$bid); exit();
					}else{
						$this->session->set_flashdata('alert' ,'error');
						$this->session->set_flashdata('message' ,'Book update failed.');
						$this->load->view('dashboard/common/header',$data);
						$this->load->view('dashboard/page-editbook',$data);
						$this->load->view('dashboard/common/footer',$data);
					}
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-editbook',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-editbook',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
	    	
		}
		
    }
    /*-----------------------------------------
		deletebookDetail()
    -----------------------------------------*/
    public function deletebookDetail(){
    	$post = $this->input->post();
    	if($post){
    		$bId = $post["delid"];
    		$delete = $this->user_Auth->delete('books', array('b_id' => $bId));
    		if($delete){
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/viewallBooks'), 'message' => 'Book Delete Successfully Done.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Process Process Failed.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid operation.'));
    	}
    }
    /*------------------------------------------------------
    	addbookbyAjax()
    ------------------------------------------------------*/
    public function addbookbyAjax(){
    	$post = $this->input->post(); 
    	if($post){
    		// print_r($post); die();

			if(empty($post['txt_language'])){
				echo json_encode(array('status' => 300, 'message' => 'Language field is required.')); die();
			}
			if(empty($post['txt_title'])){
				echo json_encode(array('status' => 300, 'message' => 'Title field is required.')); die();
			}
			if(empty($post['txt_publisheddate'])){
				echo json_encode(array('status' => 300, 'message' => 'Published date field is required.')); die();
			}
			/*if(empty($post['txt_authorid'])){
				echo json_encode(array('status' => 300, 'message' => 'Author field is required.')); die();
			}*/
			/*if(empty($post['txt_price'])){
				echo json_encode(array('status' => 300, 'message' => 'Price field is required.')); die();
			}*/
			if(empty($post['txt_originalprice']) && ($post['txt_sellingprice'])){
				echo json_encode(array('status' => 300, 'message' => 'Original price is required.')); die();
			}
			
			if(!empty($post['txt_originalprice']) && !empty($post['txt_sellingprice'])){
				if($post['txt_originalprice'] >=  $post['txt_sellingprice']){}else{
					echo json_encode(array('status' => 300, 'message' => 'Selling Price should less than original price.')); die();
				}
			}
			if(empty($post['txt_category'])){
				echo json_encode(array('status' => 300, 'message' => 'Category field is required.')); die();
			}
			if(empty($post['text_message'])){
				echo json_encode(array('status' => 300, 'message' => 'Description field is required.')); die();
			}

			$fomdata["b_language"] = $post["txt_language"];
			$fomdata["b_title"] = $post["txt_title"];

			$slug = strtolower(trim(preg_replace('/-{2,}/','-',preg_replace('/[^a-zA-Z0-9-]/', '-', $post["txt_title"])),"-"));
			$fomdata["b_slug"] = $slug;

			$fomdata["b_published"] = date('Y-m-d',strtotime($post["txt_publisheddate"]));
			// $fomdata["b_rating"] = $post["txt_rating"];
			$fomdata["b_originalprice"] = (($post["txt_originalprice"])?$post["txt_originalprice"]:0.00);
			$fomdata["b_sellingprice"] = (($post["txt_sellingprice"])?$post["txt_sellingprice"]:$fomdata["b_originalprice"]);
			$fomdata["b_category"] = $post["txt_category"];
			$fomdata["b_status"] = '1';
			$fomdata["b_created"] = date("Y-m-d H:i:s");
			$fomdata["b_fk_of_aid"] = $post["txt_authorid"];
			$fomdata["b_fk_of_uid"] = $this->is_logged_in_user;
			$fomdata["b_description"] = $post["text_message"];

			$fomdata["b_publisher"] = $post["txt_publisher"];
			$fomdata["b_bookpages"] = $post["txt_bookpages"];

			if($post['txt_isbn']){
				$fomdata["b_isbn"]	= $post['txt_isbn'];
			}else{
				$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
			    $isbn = array(); 
			    $alphaLength = strlen($alphabet) - 1; 
			    for ($i = 0; $i < 8; $i++) {
			        $n = rand(0, $alphaLength);
			        $isbn[] = $alphabet[$n];
			    }
			    $fomdata["b_isbn"]	= implode($isbn);
			}

			if($_FILES['txt_image']['name']){

				$img_config['upload_path']          = './uploads/books/';
		        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
		        $img_config['max_size']             = 3072;
		        // $img_config['encrypt_name'] = TRUE;
		        $this->upload->initialize($img_config);
		        if ( ! $this->upload->do_upload('txt_image'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            $fomdata['b_image'] = $upload_data['file_name'];
		        }
			}
			if($_FILES['bookFile']['name']){

				$config_doc['upload_path']          = './uploads/books/';
		        $config_doc['allowed_types']        = 'movie|mov|web|mflv|avi|mpg|mpeg|wmv|txt|doc|docx|pdf|ppt|pptx|mp4';
		        $config_doc['max_size']             = 800000000;
		        // $config_doc['encrypt_name'] = TRUE;
		        $this->upload->initialize($config_doc);
		        // $this->load->library('upload', $config_doc);

		        if ( ! $this->upload->do_upload('bookFile'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		        	$upload_fdata = $this->upload->data();
		        	$fomdata['b_file'] = $upload_fdata['file_name'];
		        }
			}

			$insertRes = $this->user_Auth->insert('books', $fomdata);
					
			if($insertRes){
				$this->session->set_flashdata('alert', 'success');
				$this->session->set_flashdata('message', 'Book is successfully added.');
				echo json_encode(array('status' => 200, 'message' => 'Successfully Done!.'));
			}else{
				echo json_encode(array('status' => 100, 'message' => 'Insert Process failed!.'));
			}
    	}else{
    		echo json_encode(array('status' => 100, 'message' => 'In-valid Method!.'));
    	}
    	die();
    }
    /*------------------------------------------------
    	editbookbyAjax()
	------------------------------------------------*/
    public function editbookbyAjax(){
    	$post = $this->input->post(); 
    	if($post){
    		$bookID = $post['txt_id'];
    		if(empty($post['txt_language'])){
				echo json_encode(array('status' => 300, 'message' => 'Language field is required.')); die();
			}
			if(empty($post['txt_title'])){
				echo json_encode(array('status' => 300, 'message' => 'Title field is required.')); die();
			}
			if(empty($post['txt_publisheddate'])){
				echo json_encode(array('status' => 300, 'message' => 'Published date field is required.')); die();
			}
			/*if(empty($post['txt_authorid'])){
				echo json_encode(array('status' => 300, 'message' => 'Author field is required.')); die();
			}*/
			/*if(empty($post['txt_price'])){
				echo json_encode(array('status' => 300, 'message' => 'Price field is required.')); die();
			}*/
			if(empty($post['txt_originalprice']) && ($post['txt_sellingprice'])){
				echo json_encode(array('status' => 300, 'message' => 'Original price is required.')); die();
			}
			
			if(!empty($post['txt_originalprice']) && !empty($post['txt_sellingprice'])){
				if($post['txt_originalprice'] >=  $post['txt_sellingprice']){}else{
					echo json_encode(array('status' => 300, 'message' => 'Selling Price should less than original price.')); die();
				}
			}
			if(empty($post['txt_category'])){
				echo json_encode(array('status' => 300, 'message' => 'Category field is required.')); die();
			}
			if(empty($post['text_message'])){
				echo json_encode(array('status' => 300, 'message' => 'Description field is required.')); die();
			}
			$fomdata["b_language"] = $post["txt_language"];
			$fomdata["b_title"] = $post["txt_title"];

			$slug = strtolower(trim(preg_replace('/-{2,}/','-',preg_replace('/[^a-zA-Z0-9-]/', '-', $post["txt_title"])),"-"));
			$fomdata["b_slug"] = $slug;
			
			$fomdata["b_published"] = date('Y-m-d',strtotime($post["txt_publisheddate"]));
			// $fomdata["b_rating"] = $post["txt_rating"];
			$fomdata["b_originalprice"] = (($post["txt_originalprice"])?$post["txt_originalprice"]:0.00);
			$fomdata["b_sellingprice"] = (($post["txt_sellingprice"])?$post["txt_sellingprice"]:$fomdata["b_originalprice"]);
			$fomdata["b_category"] = $post["txt_category"];
			if($post['txt_status'] == 1){ $fomdata["b_status"] = '1'; }else{	$fomdata["b_status"] = '0'; }
			
			$fomdata["b_modified"] = date("Y-m-d H:i:s");
			$fomdata["b_fk_of_aid"] = $post["txt_authorid"];
			$fomdata["b_fk_of_uid"] = $this->is_logged_in_user;
			$fomdata["b_description"] = $post["text_message"];

			$fomdata["b_publisher"] = $post["txt_publisher"];
			$fomdata["b_bookpages"] = $post["txt_bookpages"];

			if($post['txt_isbn']){
				$fomdata["b_isbn"]	= $post['txt_isbn'];
			}else{
				$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
			    $isbn = array(); 
			    $alphaLength = strlen($alphabet) - 1; 
			    for ($i = 0; $i < 8; $i++) {
			        $n = rand(0, $alphaLength);
			        $isbn[] = $alphabet[$n];
			    }
			    $fomdata["b_isbn"]	= implode($isbn);
			}

			if($_FILES['txt_image']['name']){

				$img_config['upload_path']          = './uploads/books/';
		        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
		        $img_config['max_size']             = 3072;
		        // $img_config['encrypt_name'] = TRUE;
		        $this->upload->initialize($img_config);
		        if ( ! $this->upload->do_upload('txt_image'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            $fomdata['b_image'] = $upload_data['file_name'];
		        }
			}
			if($_FILES['bookFile']['name']){

				$config_doc['upload_path']          = './uploads/books/';
		        $config_doc['allowed_types']        = 'movie|mov|web|mflv|avi|mpg|mpeg|wmv|txt|doc|docx|pdf|ppt|pptx|mp4';
		        $config_doc['max_size']             = 800000000;
		        // $config_doc['encrypt_name'] = TRUE;
		        $this->upload->initialize($config_doc);
		        // $this->load->library('upload', $config_doc);

		        if ( ! $this->upload->do_upload('bookFile'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		        	$upload_fdata = $this->upload->data();
		        	$fomdata['b_file'] = $upload_fdata['file_name'];
		        }
			}

			$updatetRes = $this->user_Auth->update('books', $fomdata, array('b_id' => $bookID));
					
			if($updatetRes){
				echo json_encode(array('status' => 200, 'message' => 'Update successfully Done!.'));
			}else{
				echo json_encode(array('status' => 100, 'message' => 'Update Process failed!.'));
			}
    	}else{
    		echo json_encode(array('status' => 100, 'message' => 'In-valid Method!.'));
    	}
    	die();
    }
    /**-----------------------------
    	bookApproval($vid)
    ------------------------------*/
    public function bookApproval($bid){
    	$data["title"] = 'New Books Approval Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($bid){
			$data['bookDetails'] = $this->user_Auth->getData('books', $w =array("b_id" => $bid));
		}
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-bookapproval',$data);
		$this->load->view('dashboard/common/footer',$data);
    }

    /*-----------------------
		bookApproved()
    -----------------------*/
    public function bookApproved(){
    	$post = $this->input->post();
    	if($post['bid']){
    		$bID = $post['bid'];
    		$status = $post['status'];
    		$updated = $this->user_Auth->update('books',array('b_status' => $status, 'b_modified' => date('Y-m-d H:i:s')),array('b_id'=>$bID));
    		// $updated =1;
    		if($updated){
    			$book = $this->user_Auth->getData('books',array('b_id' => $bID));
    			if(isset($book[0]->b_fk_of_aid)){
    				$author = $this->user_Auth->getData('user_credentials',array('uc_id' => $book[0]->b_fk_of_aid),$se='uc_email,uc_firstname,uc_lastname');	
    				$to['email'] = (($author[0]->uc_email)?$author[0]->uc_email:false);
    				$to['name'] = (($author[0]->uc_firstname)?$author[0]->uc_firstname:false);
    				
    				$msg ='<table border="2px" style="margin: 0 auto;width: 50%;">
    						<tbody>
    							<tr>
    								<td><label>Book ID: </label>
    								<td><label>'.(($book[0]->b_id)?$book[0]->b_id:"").'</label></td>
								</tr>
								<tr>
								    <td><label>Book Title: </label></td>
								    <td><label>'.(($book[0]->b_title)?$book[0]->b_title:"").'</label></td>
								</tr>
								<tr>
								    <td><label>Book Language: </label></td>
								    <td><label>'.(($book[0]->b_language)?$book[0]->b_language:"").'</label></td>
								</tr>
								<tr>
								    <td><label>Book Category: </label></td>
								    <td><label>';
						if($book[0]->b_category){
						    $categoryDetail = $this->user_Auth->getData('categories',$w=array('c_id' => $book[0]->b_category), $se='c_name');
						    $msg .=(isset($categoryDetail[0]->c_name)?$categoryDetail[0]->c_name:'');
						}
						
							$msg.='</label></td></tr>
							<tr>
						    	<td><label>Book Author: </label></td>
						    	<td><label>';
						    if($book[0]->b_fk_of_aid){
						        $authorDetails =$this->user_Auth->getData('user_credentials', $w=array('uc_id' => $book[0]->b_fk_of_aid),$se='uc_id,uc_email,uc_firstname,uc_lastname');
						        $msg.=(isset($authorDetails[0]->uc_firstname)?$authorDetails[0]->uc_firstname:'').' '.(isset($authorDetails[0]->uc_lastname)?$authorDetails[0]->uc_lastname:'');
						    }
						    $msg.='</label></td>
						</tr>
						<tr>
						    <td>
						        <label>Book Image: </label>
						    </td>
						    <td>
						        <a href="'.(($book[0]->b_image)?base_url('uploads/books/'.$book[0]->b_image):base_url('uploads/books/book.png')).'"><img src="'.(($book[0]->b_image)?base_url("uploads/books/".$book[0]->b_image):base_url("uploads/books/book.png")).'" height="50px" width="50px"></a></label>
						    </td>
						</tr>
						<tr>
						    <td>
						        <label>Book Original Price: </label>
						    </td>
						    <td>
						        <label>'.(($book[0]->b_originalprice)?$book[0]->b_originalprice:"").'</label>
						    </td>
						</tr>
						<tr>
						    <td>
						        <label>Book Selling Price: </label>
						    </td>
						    <td>
						        <label>'.(($book[0]->b_sellingprice)?$book[0]->b_sellingprice:"").'</label>
						    </td>
						</tr>
						<tr>
						    <td>
						        <label>Book published: </label>
						    </td>
						    <td>
						        <label>'.(($book[0]->b_published > 0)?date("d-M-Y",strtotime($book[0]->b_published)):"").'</label>
						    </td>
						</tr>
						<tr>
						    <td>
						        <label>Book Publisher: </label>
						    </td>
						    <td>
						        <label>'.(($book[0]->b_publisher)?$book[0]->b_publisher:"").'</label>
						    </td>
						</tr>
						<tr>
						    <td>
						        <label>Book Pages: </label>
						    </td>
						    <td>
						        <label>'.(($book[0]->b_bookpages)?$book[0]->b_bookpages:"").'</label>
						    </td>
						</tr>
						<tr>
						    <td>
						        <label>Book Created: </label>
						    </td>
						    <td>
						        <label>'.(($book[0]->b_created > 0)?date("d-M-Y H:i:s", strtotime($book[0]->b_created)):"").'</label>
						    </td>
						</tr>
						<tr>
						    <td>
						        <label>Book Modified: </label>
						    </td>
						    <td>
						        <label>'.(($book[0]->b_modified > 0)?date("d-M-Y H:i:s", strtotime($book[0]->b_modified)):"").'</label>
						    </td>
						</tr>
						<tr>
						    <td>
						        <label>Book Description: </label>
						    </td>
						    <td>
						        <label>'.(($book[0]->b_description)?$book[0]->b_description:"").'</label>
						    </td>
						</tr>
						<tr>
						    <td>
						        <label>Book Status: </label>
						    </td>
						    <td>
						        <label>';     
						            if($book[0]->b_status === '1'){
						                $msg.= "True";
						            }elseif($book[0]->b_status ==='2'){
						                $msg.= "Pending";
						            }else{
						                $msg.= "False";
						            }

						$msg.='</label></td></tr>
						<tr>
						    <td>
						        <label>Book ISBN: </label>
						    </td>
						    <td>
						        <label>'.(isset($book[0]->b_isbn)?$book[0]->b_isbn:"").'</label>
						    </td>
						</tr>
						</tbody></table>';
						
    				$attc = (($book[0]->b_file)?base_url('uplaods/books/'.$book[0]->b_file):false);
    				$this->sendMailNotification($to,$sub='Book Approved Notification',$msg,$attc);
    				echo json_encode(array('status' => 200, 'message' => 'Book Successfully approved.'));
    			}
    		}else{
    			echo json_encode(array('status' => 100, 'message' => 'Book update process failed!.'));
    		}
    	}else{
    		echo json_encode(array('Status' => 100, 'message' => 'In-valid Operaion!.'));
    	}
    	die();
    }
    /*-----------------------------------------------------
		sendMailNotification()
		param@ $to =array("email","","name"=>"")
		param@ $subjec=""
		param@ $message = ""
		param@ $attachment =""
    -----------------------------------------------------*/
    public function sendMailNotification($to, $subject=false, $message=false, $attachment=false){
    	
		$tomail = $to["email"];
		$toname = $to["name"];

		$html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div class="col-md-12">
					                <div style="margin: 30px;display: block;">
					                    <h6>Hi '.(($toname)?$toname:'').',</h6>
					                        '.(($message)?$message:'').'
					                </div>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

        $this->email->to($tomail);
        $this->email->from('pareshnagar87@gmail.com',SITENAME);
        if($attachment){
        	$this->email->attach($attachment);	
        }
        $this->email->subject( (($subject)?$subject:SITENAME.' Notification Email') );
        $this->email->message($html);
        if($tomail){
        	$sended =$this->email->send();
	        if($sended){
	            return 1;
	        }else{
	            return $this->email->print_debugger();
	        }
        }else{
        	return 'Emial not here';
        }
    }
}
