<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BooksChapter extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->library('upload');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denied!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}

	}
	public function index()
	{
		$data["title"] = 'Add Book Chapter | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		$books = $this->user_Auth->getData('books',array() ,$se='', $short='');
		$data['post'] = array();

		$booksArray = array();
		$booksArray[''] = "Select Book";
		foreach($books as $book){
			$booksArray[$book->b_id] = $book->b_title;
		}
		$data['books'] = $booksArray;
		
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}else{
			$post = $this->input->post();
			if($post){
				$data['post'] = $post;
				$this->form_validation->set_rules('chapter_title','Book Chapter Title','required',array('required' => 'Please enter %s.'));
				$this->form_validation->set_rules('chapter_book','Book','required',array('required' => 'Please select %s.'));
				/*$this->form_validation->set_rules('chapter_description','Chapter Description','required',array('required' => 'Please enter %s.'));*/
				if($this->form_validation->run() === TRUE){
					
					$fomdata["title"] = $post["chapter_title"];
					$fomdata["book_id"] = $post["chapter_book"];
					$fomdata["description"] = $post["chapter_description"];
					
					

					$insertRes = $this->user_Auth->insert('book_chapters', $fomdata);
					//$insertRes = false;
					if($insertRes){
						$this->session->set_flashdata('alert', 'success');
						$this->session->set_flashdata('message', 'Book Chapter added successfully.');
						redirect('dashboard/addnewBookChapter'); exit(); 
					}else{
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', 'Something wrong with data insertion.');
						$this->load->view('dashboard/common/header',$data);
						$this->load->view('dashboard/page-addnewbookchapter',$data);
						$this->load->view('dashboard/common/footer',$data);
					}
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-addnewbookchapter',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-addnewbookchapter',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
			 
		}
		
	}
	

    /*----------------------------------------------
		viewallBooksChapter()
    ----------------------------------------------*/
    public function viewallBooksChapter($filter=false){
    	$data["title"] = 'View All Book Chapters | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;

		$books = $this->user_Auth->getData('books',array() ,$se='', $short='');
		$booksArray = array();
		$booksArray[''] = "All Books";
		foreach($books as $book){
			$booksArray[$book->b_id] = $book->b_title;
		}
		$data['books'] = $booksArray;

		$data['filtered'] = "";
		if($filter != ''){
			$data['filtered'] = $filter;
			$data['allChapters'] = $this->user_Auth->getData('book_chapters',array('book_id'=>$filter) ,'', $short='id DESC');
		}else{
			$data['allChapters'] = $this->user_Auth->getData('book_chapters',array() ,'', $short='id DESC');
		}
		
		if(!empty($data['allChapters'])){
			foreach ($data['allChapters'] as $chapter) {
				$book = array();
				$book = $this->user_Auth->getData('books',array('b_id'=>$chapter->book_id) );
				$chapter->book = $book;
			}
		}
		
		
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-viewallbookschapter',$data);
		$this->load->view('dashboard/common/footer',$data);
    }


	/*----------------------------------------------
		chapterDetails($aid)
    ----------------------------------------------*/
    public function chapterDetails($id){
    	$data["title"] = 'Chapter Details | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($id){
			$data['chapterDetails'] = $this->user_Auth->getData('book_chapters',$w=array('id' => $id));
	    	$this->load->view('dashboard/common/header',$data);
			$this->load->view('dashboard/page-viewChapterDetails',$data);
			$this->load->view('dashboard/common/footer',$data);
		}
		
    }

    /*----------------------------------------------
		editBookChapter($id)
    ----------------------------------------------*/
    public function editBookChapter($id){
    	$data["title"] = 'Edit Book Chapter | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;

		$books = $this->user_Auth->getData('books',array() ,$se='', $short='');
		$booksArray = array();
		$booksArray[''] = "Select Book";
		foreach($books as $book){
			$booksArray[$book->b_id] = $book->b_title;
		}
		$data['books'] = $booksArray;

		if($id){
			$data['chapterDetails'] = $this->user_Auth->getData('book_chapters',$w=array('id' => $id));

			$data['post'] = array();
			$data['post']['id'] = $data['chapterDetails'][0]->id;
			$data['post']['chapter_title'] = $data['chapterDetails'][0]->title;
			$data['post']['chapter_book'] = $data['chapterDetails'][0]->book_id;
			$data['post']['description'] = $data['chapterDetails'][0]->description;
			
			$post = $this->input->post();
			if($post){
				$data['post'] = array();
				$data['post'] = $post;

				$this->form_validation->set_rules('chapter_title','Book Chapter Title','required',array('required' => 'Please enter %s.'));
				$this->form_validation->set_rules('chapter_book','Book','required',array('required' => 'Please select %s.'));

				if($this->form_validation->run() === TRUE){
					$formData["id"] = $post["id"];
					$formData["title"] = $post["chapter_title"];
					$formData["book_id"] = $post["chapter_book"];
					$formData["description"] = $post["chapter_description"];

					$updated = $this->user_Auth->update('book_chapters',$formData,array('id'=>$id));
					

					
					if($updated){
						$this->session->set_flashdata('alert' ,'success');
						$this->session->set_flashdata('message' ,'Book Chapter Updated Successfully.');
						redirect('dashboard/editBookChapter/'.$id); exit();
					}else{
						$this->session->set_flashdata('alert' ,'error');
						$this->session->set_flashdata('message' ,'Something wrong with Chapter updation.');
						$this->load->view('dashboard/common/header',$data);
						$this->load->view('dashboard/page-editchapter',$data);
						$this->load->view('dashboard/common/footer',$data);
					}
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-editchapter',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-editchapter',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
	    	
		}
		
    }
    /*-----------------------------------------
		deletebookChapter()
    -----------------------------------------*/
    public function deletebookChapter(){
    	$post = $this->input->post();
    	if($post){
    		$id = $post["delid"];
    		$delete = $this->user_Auth->delete('book_chapters', array('id' => $id));
    		if($delete){
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/viewallBooksChapter'), 'message' => 'Book Chapter Deleted Successfully.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Something Wrong with Deletion.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid request.'));
    	}
    }
    
}