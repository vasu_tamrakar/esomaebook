<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BooksOrder extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->model('BookReview_model');
		$this->load->model('Book_model');
		$this->load->library('upload');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denied!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}

	}
	public function index($filter = "",$filter1="")
	{
		$data["title"] = 'Manage Book Orders | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;

		//echo $filter.' '.$filter1;exit;
		$whereArr = array();
		if($filter != "" && $filter != 0){
			$whereArr['author_id'] = $filter;
		}
		//echo $filter1;
		if($filter1 != "" && $filter1 != 'all'){
			$whereArr['payment_status'] = $filter1;
		}
		
		$bookOrders = $this->Book_model->getAllBookOrders($whereArr);

		$data['filter'] = $filter;
		$data['filter1'] = $filter1;
		$data['allOrders'] = array();
		if($bookOrders){
			$data['allOrders'] = $bookOrders;
		}

		$authors = $this->user_Auth->getData('user_credentials',$w=array('uc_role' =>'4','uc_status' => '1'));
		$data['authors'] = array();
		$authorData = array();
		$authorData[] = 'All Authors';
		if($authors){
			//$data['authors'] = $authors;
			foreach($authors as $author){
				$authorData[$author->uc_id] = $author->uc_firstname.' '.$author->uc_lastname;
			}
		}

		$data['authors'] = $authorData;
		
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-viewallOrders',$data);
		$this->load->view('dashboard/common/footer',$data);
		
	}

	function orderReports(){
		$post = $this->input->post();
		$data["title"] = 'Manage Book Orders | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;

		$data['postData'] = array();
		$whereArr = array();
		$startDate = date('m/d/Y');
		$endDate = date('m/d/Y');
		if(isset($post['viewReport'])){
			$data['postData'] = $post;
			if($post['order_report_author'] != 0){
				$whereArr['book_orders.author_id'] = $post['order_report_author'];
			}
			if($post['report_payment_status'] != 'all'){
				$whereArr['book_orders.payment_status'] = $post['report_payment_status'];
			}
			$startDate = $post['report_start_date'];
			$endDate = $post['report_end_date'];
		}
		$bookOrders = $this->Book_model->getOrderReport($whereArr,$startDate,$endDate);
		
		$pedingTotal = 0;
		$completedTotal = 0;
		$data['pendingTotal'] = 0;
		$data['completedTotal'] = 0;
		
		$data['allOrders'] = array();
		if($bookOrders){
			$data['allOrders'] = $bookOrders;

			foreach($bookOrders as $order){
				//echo '<pre>';
				//print_r($order);
				if($order->payment_status == "completed"){
					$completedTotal += $order->total_amt;
				}else if($order->payment_status == "pending"){
					$pedingTotal += $order->total_amt;
				}
			}
			$data['pendingTotal'] = $pedingTotal;
			$data['completedTotal'] = $completedTotal;
		}

		//print_r($this->db->last_query());   

		$authors = $this->user_Auth->getData('user_credentials',$w=array('uc_role' =>'4','uc_status' => '1'));
		$data['authors'] = array();
		$authorData = array();
		$authorData[] = 'All Authors';
		if($authors){
			foreach($authors as $author){
				$authorData[$author->uc_id] = $author->uc_firstname.' '.$author->uc_lastname;
			}
		}

		$data['authors'] = $authorData;
		
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-orderReport',$data);
		$this->load->view('dashboard/common/footer',$data);
	}

	/*----------------------------------------------
		reviewDetails($aid)
    ----------------------------------------------*/
    public function viewOrder($id){
    	$data["title"] = 'Order Details | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($id){
			$data['bookOrder'] = array(); 
			$bookOrder = $this->Book_model->getBookOrder($id);
			if($bookOrder){
				$data['bookOrder'] = $bookOrder[0];
			}
			/*echo '<pre>';
			print_r($bookOrder);exit;*/

	    	$this->load->view('dashboard/common/header',$data);
			$this->load->view('dashboard/page-viewOrderDetails',$data);
			$this->load->view('dashboard/common/footer',$data);
		}else{
			redirect('dashboard/manageBookReviews'); exit();
		}
		
    }
	

    /*-----------------------------------------
		deletebookChapter()
    -----------------------------------------*/
    public function deleteOrder(){
    	$post = $this->input->post();
    	if($post){
    		$id = $post["delid"];
    		$delete = $this->user_Auth->delete('book_orders', array('id' => $id));
    		if($delete){
    			$this->session->set_flashdata('alert' ,'success');
				$this->session->set_flashdata('message' ,'Order Deleted Successfully.');
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/manageBookOrders'), 'message' => 'Order Deleted Successfully.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Something Wrong with Deletion.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid request.'));
    	}
    }

    /*-----------------------------------------
		approvebookReview()
    -----------------------------------------*/
    public function orderStatusChange(){
    	$post = $this->input->post();
    	if($post){
    		$id = $post["id"];

			$formData["payment_status"] = $post['status'];

			$updated = $this->user_Auth->update('book_orders',$formData,array('id'=>$id));

    		if($updated){
    			$this->session->set_flashdata('alert' ,'success');
				$this->session->set_flashdata('message' ,'Order Updated Successfully.');
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/manageBookOrders'), 'message' => 'Book Review Approved Successfully.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Something Wrong with Order Updation.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid request.'));
    	}
    }
    
    
}