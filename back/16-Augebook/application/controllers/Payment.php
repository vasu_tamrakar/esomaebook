<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	protected $payerData;
	public function __construct (){
	 	parent :: __construct();
		if($this->session->userdata('is_logged_in_user')){
			redirect(base_url());  exit();
		}
		$this->load->model('user_Auth');
	}
	public function index()
	{
		$this->payerData = $this->session->userdata("payerData");
		if($this->payerData){
			$data["title"] = 'Payment Page | '.SITENAME;
			// print_r($this->payerData);
			if($this->payerData["payMethod"] == 'cc'){
				$this->load->view("front/page-stripe", $data);
			}
			if($this->payerData["payMethod"] == 'evc'){
				$this->load->view("front/page-evc", $data);
			}
			if($this->payerData["payMethod"] == 'mpesa'){
				$this->load->view('front/page-mpesa', $data);
			}
		}else{
			$this->session->set_flashdata('alert', 'info');
			$this->session->set_flashdata('message', 'Invalid operation!.');
			redirect('membership'); exit();
		}
	}
}