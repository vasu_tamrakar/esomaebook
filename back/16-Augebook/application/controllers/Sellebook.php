<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sellebook extends CI_Controller {

	public function __construct (){
		parent :: __construct();
		$this->load->model('user_Auth');
		//$this->load->model('book_model');
	}
	/*------------------------------------
		Sellebook index 
	------------------------------------*/
	public function index()
	{
		$data['title'] = 'Sell Your eBooks page |'.SITENAME;
		
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-sellebook', $data);
		$this->load->view('front/common/footer', $data);
	}
}
?>