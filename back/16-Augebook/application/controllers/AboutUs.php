<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AboutUs extends CI_Controller {
	public function __construct (){
		parent :: __construct();
		$this->load->model('user_Auth');
		$this->load->model('book_model');
	}
	public function index()
	{
		$data["title"] = 'About Us Page | '.SITENAME;

		$recentlyreleaseBooks = $this->user_Auth->getData('books', $w = array('b_status' => '1'), $s='b_id', $sh = 'b_published DESC');
		$data["recentlyreleaseBooksID"] = array();
		if($recentlyreleaseBooks)
		$data["recentlyreleaseBooksID"] = $recentlyreleaseBooks;

		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-aboutus',$data);
		$this->load->view('front/common/footer',$data);
	}
}
