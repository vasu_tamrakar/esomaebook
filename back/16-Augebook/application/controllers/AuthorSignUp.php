<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthorSignUp extends CI_Controller {
	protected $planDetails;
	public function __construct (){
	 	parent :: __construct();
		if($this->session->userdata('is_logged_in_user')){
			redirect(base_url());  exit();
		}
		$this->load->model('user_Auth');
	}
	public function txngenreate(){
			
			$mt = explode(' ',microtime());
			$round = date('dmY').$mt[1];
			return $round;
		}
	public function index()
	{
		$data["title"] = 'Author Sign Up Page | '.SITENAME;

		$post = $this->input->post();
		/* Check Form from post*/
		if($post){
			
			// print_r($post); die;
			
			// $post["txt_firstname"];
			// $post["txt_lastname"];
			// $post["txt_email"];
			// $post["txt_mobile"];
			// $post["txt_password"];
			// $post["txt_cnfpassword"];
			// $post["select_book"];
			// $post["txt_holdername"];
			// $post["txt_cardnumber"];
			// $post["txt_cvv"];
			// $post["txt_expmonth"];
			// $post["txt_expyear"];
			// $post["txt_agree"];
			// $post["txt_userID"];
			// $post["txt_planID"];
			// $post["stripeToken"];

			/*

			$this->form_validation->set_rules('txt_firstname','First Name','required');
			$this->form_validation->set_rules('txt_lastname','Last Name','required');
			$this->form_validation->set_rules('txt_email','Email Address','required|valid_email|is_unique[user_credentials.uc_email]');
			if(isset($post["txt_mobile"]) && ($post["txt_mobile"])){
				$this->form_validation->set_rules('txt_mobile','Mobile','required|is_unique[user_credentials.uc_mobile]');
			}
			$this->form_validation->set_rules('txt_password','Password','required');
			// $this->form_validation->set_rules('txt_cnfpassword','Password','required|matches[txt_password]');
			if($post["txt_paymethod"] != 'cc'){
				$this->form_validation->set_rules('txt_address','Address','required');	
			}
			/* Check validation *//*
			if($this->form_validation->run() === TRUE){
				
				$resExistemail =  $this->user_Auth->existEmail($post["txt_email"]);
				if(isset($post['txt_mobile']) && ($post['txt_mobile'])){
					$resExistmobile = $this->user_Auth->existMobile($post["txt_mobile"]);
				}else{
					$resExistmobile = FALSE;
				}
				
				/* Check email exist ornot *//*
				if($resExistemail){
					$this->session->set_flashdata('alert','info');
					$this->session->set_flashdata('message',$post["txt_email"].' Email Address Already Registered.');
					redirect('signUp'); exit();
				}elseif($resExistmobile){
					$this->session->set_flashdata('alert','info');
					$this->session->set_flashdata('message',$post["txt_mobile"].' Mobile Number Already Registered.');
					redirect('signUp'); exit();
				}else{

					$formData["uc_firstname"] = $post["txt_firstname"];
					$formData["uc_lastname"] = $post["txt_lastname"];
					$formData["uc_email"] = $post["txt_email"];
					if(isset($post["txt_mobile"]) && ($post["txt_mobile"])){
						$formData["uc_mobile"] = $post["txt_mobile"];
					}
					$formData["uc_password"] = md5($post["txt_password"]);
					$formData["uc_role"] = 5;
					$formData["uc_created"] = date("Y-m-d H:i:s");
					$formData["uc_status"] = '0';
					$formData["uc_active"] = '0';
					if($post["txt_paymethod"] != 'cc'){
						$formData["uc_address"] = $post["txt_address"];
					}
					$instID =  $this->user_Auth->insert('user_credentials',$formData);
					
					/* Session set for goto payment page info. *//*
					
					if($instID){
						$user = $this->user_Auth->getData('user_credentials',$where=array('uc_id' =>$instID),$sel='',$sort ='');						
						if( $post["txt_paymethod"] === 'cc' ){
							// $post["txt_planid"];
							// $post["txt_firstname"];
							// $post["txt_lastname"];
							// $post["txt_email"];
							// $post["txt_mobile"];
							// $post["txt_password"];
							// $post["txt_cnfpassword"];
							// $post["select_book"];
							// $post["txt_holdername"];
							// $post["txt_cardnumber"];
							// $post["txt_cvv"];
							// $post["txt_expmonth"];
							// $post["txt_expyear"];
							// $post["txt_agree"];
							// $post["txt_userID"];
							// $post["txt_planID"];
							// $post["stripeToken"];
							$bookID = $post["select_book"];
							$this->stripePayment($post["txt_planID"],$instID,$bookID);		

						}else if( $post["txt_paymethod"] === 'evc' ){

							/*evc process todo *//*

							$planID = $post["txt_planidform"];
							
							$post["txt_paymethod"];
							$post["txt_firstname"];
							$post["txt_lastname"];
							$post["txt_email"];
							$post["txt_mobile"];
							$post["txt_password"];
							$post["select_book"];
							$post["txt_address"];
							$post["txt_agree2"];
							$planDetail = $this->user_Auth->getData('membershipplan',array('mp_id' =>$planID));
							$txtID = $this->txngenreate();
							$paymentDetails["pd_txnid"] = $txtID;
				            $paymentDetails["pd_planid"] = $planID;
				            $paymentDetails["pd_planprice"] = $planDetail[0]->mp_price;
				            $paymentDetails["pd_userid"] = $instID;
				            $paymentDetails["pd_currency"] = 'usd';
				            $paymentDetails["pd_status"] = 'waiting';
				            $paymentDetails["pd_payby"] = $post["txt_paymethod"];
				            
				            $paymentDetails["pd_created"] = date('Y-m-d H;i:s');
				            $bookID = $post["select_book"];
				            $paymentDetails["pd_bookid"] = $bookID;
				            $paydetailRES = $this->user_Auth->insert("paymentDetails", $paymentDetails);
				            $this->session->unset_userdata('signUpdataid');
				            if($paydetailRES){
				            	$access = array("ba_bookid" => $bookID,"ba_userid" => $instID,"ba_created"=> date('Y-m-d H:i:s'));
					            $user = $this->user_Auth->getData('user_credentials',array('uc_id'=> $instID));
					            $this->user_Auth->insert("book_access", $access);
					            $this->signup_verification($user[0]->uc_email,$sub='Sign Up Process Notification of '.SITENAME, $user, $paydetailRES);
					            

				            	$this->session->set_flashdata('alert','success');
				            	$this->session->set_flashdata('message','SignUp Process Successfully Done!.');
				            	
				            	redirect('thankyou/'.base64_encode($txtID)); exit();
				            }else{
				            	$this->session->set_flashdata('alert','error');
				            	$this->session->set_flashdata('message','pay process failed.');
				            	redirect('failure'); exit();
				            }
						}else{

							/* mpesage process block *//*
							$planID = $post["txt_planidform3"];
							$post["txt_paymethod"];
							$post["txt_firstname"];
							$post["txt_lastname"];
							$post["txt_email"];
							$post["txt_mobile"];
							$post["txt_password"];
							$post["txt_cnfpassword"];
							$post["select_book"];
							$post["txt_address"];
							$post["txt_agree3"];
							$bookID = $post["select_book"];
							$planDetail = $this->user_Auth->getData('membershipplan',array('mp_id' =>$planID));
							$txtID = $this->txngenreate();
							$paymentDetails["pd_txnid"] = $txtID;
				            $paymentDetails["pd_planid"] = $planID;
				            $paymentDetails["pd_planprice"] = $planDetail[0]->mp_price;
				            $paymentDetails["pd_userid"] = $instID;
				            $paymentDetails["pd_currency"] = 'usd';
				            $paymentDetails["pd_status"] = 'waiting';
				            $paymentDetails["pd_payby"] = $post["txt_paymethod"];
				            $paymentDetails["pd_bookid"] = $bookID;
				            $paymentDetails["pd_created"] = date('Y-m-d H;i:s');
				            
				            
				            $paydetailRES = $this->user_Auth->insert("paymentDetails", $paymentDetails);
				            $this->session->unset_userdata('signUpdataid');
				            if($paydetailRES){
				            	$access = array("ba_bookid" => $bookID,"ba_userid" => $instID,"ba_created"=> date('Y-m-d H:i:s'));
					            $user = $this->user_Auth->getData('user_credentials',array('uc_id'=> $instID));
					            $this->user_Auth->insert("book_access", $access);
					            $this->signup_verification($user[0]->uc_email,$sub='SignUp Process Notification of '.SITENAME, $user, $paydetailRES);
					            

				            	$this->session->set_flashdata('alert','success');
				            	$this->session->set_flashdata('message','SignUp Process Successfully Done!.');
				            	redirect('thankyou/'.base64_encode($txtID)); exit();
				            }else{
				            	$this->session->set_flashdata('alert','error');
				            	$this->session->set_flashdata('message','pay process failed.');
				            	redirect('failure'); exit();
				            }
						}
						
					}else{
						$this->session->set_flashdata('alert','error');
						$this->session->set_flashdata('message','Registration process failed Please try to after some time.');
						redirect('signUp'); exit();
					}
				}
			}else{	
				$this->load->view('front/common/header',$data);
				$this->load->view('front/page-signup',$data);
				$this->load->view('front/common/footer',$data);
			}*/
		}else{
			$this->load->view('front/common/header',$data);
			$this->load->view('front/page-authorsignup',$data);
			$this->load->view('front/common/footer',$data);	
		}

		
	}
	/*--------------------------------------------------
		authorRegistration()
	---------------------------------------------------*/
	public function authorRegistration(){
		$post = $this->input->post();
		if($post){

			$fromData['uc_firstname'] = $post["txt_firstname"];
			$fromData['uc_lastname'] = $post["txt_lastname"];
			$fromData['uc_email'] = $post["txt_email"];
			$fromData['uc_mobile'] = $post["txt_mobile"];
			$fromData['uc_address'] = $post["txt_address"];
			$fromData['uc_password'] = md5($post["txt_password"]);
			if(isset($post["txt_paypalEmail"])){
				$txt_paypalEmail = $post["txt_paypalEmail"];
			}
			
			if(isset($post["txt_mobilenumber"])){
				$txt_mobilenumber = $post["txt_mobilenumber"];
			}

			$resExistemail =  $this->user_Auth->existEmail($post["txt_email"]);


			/* Check email exist ornot */
			if($resExistemail){
				$this->session->set_flashdata('alert','error');
				    $this->session->set_flashdata('message','Email Address Already Registered.');
				    
				echo json_encode(array('Status' => '400', 'message' => $post["txt_email"].' Email Address Already Registered.'));
				die();
			}else{
				$fromData['uc_created'] = date('Y-m-d H:i:s');
				$fromData['uc_role'] = 4;
				$fromData['uc_status'] = '0';
				$fromData['uc_active'] = '0';
				$insertID = $this->user_Auth->insert('user_credentials',$fromData);
				if($insertID){
					$aData['a_fK_of_uc_id'] = $insertID;
					$aData['a_lang'] = 'somali';
					$aData['a_description'] = $post["txt_bio"];
					$aData['a_paypal'] = (isset($txt_paypalEmail)?$txt_paypalEmail:'');
					$aData['a_mmoney'] = (isset($txt_mobilenumber)?$txt_mobilenumber:'');
					$this->user_Auth->insert('authors',$aData);
					$regBy = (isset($txt_paypalEmail)?'paypal':'mmoney');
					$this->registerAuthornotification($insertID, $regBy);
					$this->authorApproval_mail($insertID);

					$this->session->set_flashdata('alert','success');
				    $this->session->set_flashdata('message','Your Profile is Successfully completed!<br>You can login after admin approval.');
					echo json_encode(array('Status' => '200','message' => 'Your Profile is Successfully completed!<br>You can login after admin approval.'));;
				}else{
					$this->session->set_flashdata('alert','error');
						$this->session->set_flashdata('message','Sign-Up process failed!. Please try to after some time.');

					echo json_encode(array('Status' => '100','message' => 'Sign-Up process failed!.'));;
				}
				 die();
			}
		}else{
			echo json_encode(array('Status' => '100','message' => 'In-valid input data.'));
		}
		die();
	}


	/*------------------------------------------------------------------
        registerAuthornotification
    ------------------------------------------------------------------*/
    public function registerAuthornotification($uid,$byregister=false){
        
    	$userDetails = $this->user_Auth->getData("user_credentials", array('uc_id' => $uid));

	    $addedhtml = "<p>Your profile is successfully completed.
	    			<br>You can login to our site and sell your e-books after your account is approved.<p>";
        
        
    	$html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div class="col-md-12">
					                <div style="margin: 30px;display: block; width:100%">
					                    <h3>Hi '.$userDetails[0]->uc_firstname.',</h3>
					                        '.$addedhtml.'
					                </div>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

        $this->email->to($userDetails[0]->uc_email);
        $this->email->from('pareshnagar87@gmail.com',SITENAME);

        $this->email->subject( 'SignUp Verification Email' );
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
            return 1;
        }else{
            return $this->email->print_debugger();
        }
    }
    public function authorApproval_mail($uid,$byregister=false){
    	$userDetails = $this->user_Auth->getData("user_credentials", array('uc_id' => $uid));
    	$admin = $this->user_Auth->getData("user_credentials", array('uc_id' => 1),$se='uc_email');
    	$adminemail =(isset($admin[0]->uc_email)?$admin[0]->uc_email:'');
    	$url = site_url('dashboard/authorDetails/'.$userDetails[0]->uc_id);
        $html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div class="col-md-12">
					                <div style="margin: 30px;display: block;">
					                    <h3>Hi admin,</h3>
					                    <p>We have new author registration request. Please review author details.</p>
                                    	<p>To review accout details of author, please click <a href="'.$url.'">View Author Details</a></p>
					                </div>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

        
       // $this->email->to($adminemail);
		$this->email->to("paresh3779@gmail.com");
        $this->email->from('pareshnagar87@gmail.com',SITENAME);

        $this->email->subject( 'New Author Registartion Request' );
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
            return 1;
        }else{
            $this->email->print_debugger();
        }
    }
	
}