<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership extends CI_Controller {
	public function __construct (){
		parent :: __construct();
		if($this->session->userdata('is_logged_in_user')){
			redirect(base_url());  exit();
		}
		$this->load->model('user_Auth');
	}
	public function index()
	{
		// $this->load->model('user_Auth');
		$data["title"] = 'Membership page | '.SITENAME;
		$data['membershipDetailIDs'] = $this->user_Auth->getData('membershipplan',array('mp_status' => '1'),$se= 'mp_id',$sh='');
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-membership',$data);
		$this->load->view('front/common/footer',$data);
	}

	/*-----------------------------------------------------
	addsSbscriber()
	-----------------------------------------------------*/
	public function addsSbscriber(){
		// $this->load->model('user_Auth');
		$post = $this->input->post();
		
		if($post){
			if(empty($post["txtfirstname"])){
				echo json_encode(array("status" => "error","message" => "First Name field is required!."));	
				die();
			}
			if(empty($post["txtlastname"])){
				echo json_encode(array("status" => "error","message" => "Last name field is required!."));	
				die();
			}
			if(empty($post["txtemail"])){
				echo json_encode(array("status" => "error","message" => "Email field is required!."));	
				die();
			}

			$formData["es_firstname"] = $post["txtfirstname"];
			$formData["es_lastname"] = $post["txtlastname"];
			$formData["es_email"] = $post["txtemail"];
			$formData["es_planid"] = $post["txtplanId"];
			$formData["es_created"] = date('Y-m-d H:i:s');
			$formData["es_status"] = "1";

			$existEmail = $this->user_Auth->getData("emailSubscriber", array("es_email" => $formData["es_email"]), $s="",$sh="") ;
			if($existEmail){
				$updateDAta = array(
					"es_firstname" => $formData["es_firstname"],
					"es_lastname" => $formData["es_lastname"],
					"es_planid"	   => $formData["es_planid"],
					"es_status"	   => '1'
					);
				$this->user_Auth->update('emailSubscriber',$updateDAta,array("es_id" => $existEmail[0]->es_id));
				$this->session->set_userdata('signUpdataid', $existEmail[0]->es_id);
				echo json_encode(array("status" => "200", "data" => site_url('signUp'), "message" => "Succesfully Done."));

			}else{
				$insertID = $this->user_Auth->insert('emailSubscriber',$formData);
				if($insertID){
					$this->sendSubscriberMail($formData);
					$this->session->set_userdata('signUpdataid', $insertID);
					echo json_encode(array("status" => "200", "data" => site_url('signUp'), "message" => "Succesfully Done."));	
				}else{
					echo json_encode(array("status" => "fail","message" => "Insert Process failed!."));
				}
			}
		}else{
			echo json_encode(array("status" => "fail","message" => "Invalid operation!."));
		}
		die();
	}

	public function sendSubscriberMail($user = array()){
		$html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block; width:100%">
					                    <h3>Hi '.$user['es_email'].',</h3>
					                    <p>Thanks for subscribing.</p>
                                    	';
                                    	
                                    	
                                    	$html .= '
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

        
        $this->email->to($user['es_email']);
        $this->email->from('pareshnagar87@gmail.com',SITENAME);

        $this->email->subject('Subscription Notification');
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
            return 1;
        }else{
            $this->email->print_debugger();
        }
	}
}
