<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thankyou extends CI_Controller {
	
	public function __construct (){
	 	parent :: __construct();
		$this->load->model('user_Auth');
	}
	public function index($txnid)
	{
		$data["title"] = 'Thank You Page | '.SITENAME;
		$txnid = base64_decode($txnid);

		if($txnid){
			$data["transectionDetails"] = $this->user_Auth->getData("paymentdetails",array("pd_txnid" => $txnid));
		}
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-thankyou', $data);
		$this->load->view('front/common/footer', $data);	
		
	}
}