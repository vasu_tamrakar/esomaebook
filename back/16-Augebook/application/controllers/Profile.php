<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->library('upload');
	}
	/*-------------------------------
		updateAuthorAjax()
	-------------------------------*/
	public function updateAuthorAjax(){
		$update_at = date('Y-m-d H:i:s');
		$post= $this->input->post();
		if($post){

			if(empty($post["txt_firstname"])){
				echo json_encode(array('Status' => 300, 'message' => 'Please Fill First Name.')); die();
			}
			if(empty($post["txt_lastname"])){
				echo json_encode(array('Status' => 300, 'message' => 'Please Fill Last Name.')); die();
			}
			if(empty($post["txt_mobile"])){
				echo json_encode(array('Status' => 300, 'message' => 'Please Fill Mobile Number.')); die();
			}
			if(empty($post["txt_address"])){
				echo json_encode(array('Status' => 300, 'message' => 'Please Fill The Address.')); die();
			}
			if(empty($post["txt_language"])){
				echo json_encode(array('Status' => 300, 'message' => 'Please Select The Language.')); die();
			}
			
			/*if(empty($post["txt_published"])){
				echo json_encode(array('Status' => 300, 'message' => 'Please Fill Published Books.')); die();
			}*/

			if($post["txt_paymmobile"] == 1){
				if(empty($post["txt_paypal"])){
					echo json_encode(array('Status' => 300, 'message' => 'Please Fill Paypal Email Address.')); die();
				}
			}else{
				if(empty($post["txt_mmoney"])){
					echo json_encode(array('Status' => 300, 'message' => 'Please Fill Money Mobile Number.')); die();
				}
			}

			$formData['uc_firstname'] = $post["txt_firstname"];
			$formData['uc_lastname'] = $post["txt_lastname"];
			$formData['uc_mobile'] = $post["txt_mobile"];			
			$formData['uc_status'] = $post["txt_status"];			
			$formData['uc_address'] = $post["txt_address"];
			$formData['uc_modified'] 			= $update_at;
			$exitMobile = $this->user_Auth->existUpdateunique('user_credentials', array('uc_mobile' => $formData['uc_mobile']),array('uc_id' =>$this->is_logged_in_user));
			if($exitMobile){
				echo json_encode(array('Status' => 400, 'message' => 'Mobile already registered Please Chnage mobile number.')); die();
			}
			if($_FILES['txt_image']['name']){
		        $config['upload_path']          = './uploads/users/';
		        $config['allowed_types']        = 'gif|jpg|jpeg|png';
		        $config['max_size']             = 2048;
        		$this->upload->initialize($config);
		        if ( ! $this->upload->do_upload('txt_image'))
		        {
		            echo json_encode(array('Status' => 100, 'message' => $this->upload->display_errors())); die();
		        }else{
		            $upImage = $this->upload->data();
					$formData['uc_image'] = $upImage["file_name"];
		        }

			}
			$updated = $this->user_Auth->update('user_credentials', $formData, array('uc_id' =>$this->is_logged_in_user));
			$formData2['a_lang'] 			= $post["txt_language"];
			/*$formData2['a_publishbooks'] 	= $post["txt_published"];*/
			if($post["txt_paymmobile"] == 1){
				$formData2['a_paypal'] 			= $post["txt_paypal"];
			}else{
				$formData2['a_mmoney'] 			= $post["txt_mmoney"];
			}
			$formData2['a_facebook'] 			= $post["txt_facebook"];
			$formData2['a_twitter'] 			= $post["txt_twitter"];
			$formData2['a_gplush'] 			= $post["txt_googleplus"];
			$formData2['a_instagram'] 			= $post["txt_instagram"];
			$formData2['a_description'] 			= $post["txt_description"];
			$formData2['a_modified'] 			= $update_at;
			$updated1 = $this->user_Auth->update('authors', $formData2, array('a_fK_of_uc_id' => $this->is_logged_in_user));
			if(($updated)&&($updated1)){
				$user = $this->user_Auth->getData('user_credentials', $where = array('uc_id' => $this->is_logged_in_user),$se='',$s='');

				$udata["u_id"] = $user[0]->uc_id;
				$udata["u_firstname"] = $user[0]->uc_firstname;
				$udata["u_lastname"] = $user[0]->uc_lastname;
				$udata["u_email"] = $user[0]->uc_email;
				$udata["u_mobile"] = $user[0]->uc_mobile;
				$udata["u_image"] = (($user[0]->uc_image)?$user[0]->uc_image:'user.png');
				$udata["u_role"] = $user[0]->uc_role;
				$udata["u_status"] = $user[0]->uc_status;
				$udata["u_created"] = $user[0]->uc_created;
				$udata["u_modified"] = (($user[0]->uc_modified >0)?$user[0]->uc_modified:'');
				$this->session->set_userdata('is_logged_in_user', $user[0]->uc_id);
				$this->session->set_userdata('is_logged_in_user_info', $udata);
				// $this->session->set_flashdata('alert', "success");
				// $this->session->set_flashdata('message', 'Profile successfully updated..');
				echo json_encode(array('Status' => 200, 'url' => site_url('user/profile/'.$this->is_logged_in_user), 'message' => 'Profile Successfully Updated!.')); die();
			}else{
				echo json_encode(array('Status' => 100, 'message' => 'Update Process failed!.')); die();
			}

		}else{
			echo json_encode(array('Status' => 100, 'message' => 'In-valid Method data!.'));
		}
		die();
	}
	/*-----------------------------
		deleteBookDetails()
	------------------------------*/
	public function deleteBookDetails(){
		$post = $this->input->post();
		if($post){
			$delID = $post["del_ID"];
			$status = $this->user_Auth->delete('books', $w= array('b_id' => $delID));
			// $status =1;
			if($status){
				echo json_encode(array('status' => 200, 'message' => 'Deleted Successfully.'));
			}else{
				echo json_encode(array('status' => 300, 'message' => 'Delete process aborted.'));
			}
		}else{
			echo json_encode(array('status' => 100, 'message' => 'In-valid Method Data!.'));
		}
		die();
	}
	/*--------------------------------------
		viewBookDetails()
    --------------------------------------*/
    public function viewBookDetails(){
    	$post = $this->input->post();
    	if($post){
    		$viewID = $post['view_ID'];
    		$book = $this->user_Auth->getData('books', array('b_id' => $viewID),$se ='', $sh='');
    		if($book){
    			$bookAauthor = $this->user_Auth->getData('user_credentials', array('uc_id' => $book[0]->b_fk_of_aid),$se ='uc_id,uc_email,uc_firstname,uc_lastname', $sh='');
    			$bookCategory = $this->user_Auth->getData('categories', array('c_id' => $book[0]->b_category),$se ='c_id,c_name', $sh='');
    			

    			$html = '<div calss="container"><table id="booksdetails" class="table table-striped">
    			<thead>
    			<tr>
    				<th>Name</th>
    				<th>Details</th>
    				<th>Name</th>
    				<th>Details</th>
    			</tr>
    			</thead>
    			<tbody>
    				<tr>
    					<td> Book ID</td><td> '.$book[0]->b_id.'</td>
    					<td> Book Name</td><td> '.$book[0]->b_title.'</td>
    				</tr>
    				<tr>
    					<td> Book Author Name</td><td> '.(isset($bookAauthor[0]->uc_firstname)?$bookAauthor[0]->uc_firstname:'').' '.(isset($bookAauthor[0]->uc_lastname)?$bookAauthor[0]->uc_lastname:'').'</td>
    					<td> Book Language</td><td style="text-transform: capitalize;"> '.$book[0]->b_language.'</td>
    				</tr>
    				<tr>
    					<td> Original Price</td><td> '.(($book[0]->b_originalprice)?$book[0]->b_originalprice:'').'</td>
    					<td> Selling Price</td><td> '.(($book[0]->b_sellingprice)?$book[0]->b_sellingprice:'').'</td>
    				</tr>
    				<tr>
    					<td> Discount</td><td> ';
    					if($book[0]->b_originalprice >=  $book[0]->b_sellingprice){
    						$per = ((($book[0]->b_originalprice -  $book[0]->b_sellingprice)*100)/$book[0]->b_originalprice);
    						$html.= (($per > 0)?number_format($per, 2).'%':'0%');
    					}else{
    						$html.= '0%';
    					}
    					$html.='</td>
    					<td> Publisher</td><td> '.(($book[0]->b_publisher)?$book[0]->b_publisher:'').'</td>
    				</tr>
    				<tr>
    					<td> Book Rating</td><td> '.$book[0]->b_rating.'</td>
    					<td> Book Category </td><td> '.(isset($bookCategory[0]->c_name)?$bookCategory[0]->c_name:'').'</td>
    				</tr>
    				<tr>
    					<td> Book Status</td><td> ';
    					
    					if($book[0]->b_status == 1){
    						$html.= 'True';
    					}else if($book[0]->b_status == 2){
    						$html.= 'Pending';
    					}else{
    						$html.= 'False';
    					}
    					$html.= '</td>
    					<td> Published Books</td><td> '.(($book[0]->b_published)?$book[0]->b_published:'').'</td>
    				</tr>
    				<tr>
    					<td> Pages</td><td> '.(($book[0]->b_bookpages)?$book[0]->b_bookpages:'').'</td>
    					<td> ISBN</td><td> '.(($book[0]->b_isbn)?$book[0]->b_isbn:'').'</td>
    				</tr>
    				<tr>
    					<td> Descriptions</td><td> '.(($book[0]->b_description)?$book[0]->b_description:'').'</td>
    					<td> Downloads</td><td> '.(($book[0]->b_downloads)?$book[0]->b_downloads:'').'</td>
    				</tr>
    				
    				<tr>
    					<td> Book Created</td><td> '.(($book[0]->b_created >0)?date('d-M-Y H:i:s',strtotime($book[0]->b_created)):'').'</td>
    					<td> Book Modified </td><td> '.(($book[0]->b_modified >0)?date('d-M-Y H:i:s',strtotime($book[0]->b_modified)):'').'</td>
    				</tr>
    				
    				<tr>
    					<td> Book Picture</td><td colspan="3"> <div style="width:150px;"><a target="_blank" href="'.base_url("uploads/books/".$book[0]->b_file).'"><img src="'.base_url("uploads/books/".$book[0]->b_image).'" class="img-fluid"></a></div></td>
    				</tr>
    			</tbody>
    			</table></div>';
    			echo json_encode(array('status' => 'Success', 'data' => $html,'message' => 'Successfully Done!.'));
    		}else{
    			$html = 'No more data';
    			echo json_encode(array('status' => 'failed', 'data' => $html,'message' => 'No More data!.'));
    		}
    		
    	}else{
    		echo json_encode(array('status' => 'Fail', 'message' => 'In-valid Mehtod!.'));
    	}
    	die();
    }

    /*-------------------------------
		updateAuthorbookbyAjax()
	-------------------------------*/
	public function updateAuthorbookbyAjax(){
		$updated = date('Y-m-d H:i:s');
		$post= $this->input->post();
		if($post){

    		$bID = $post['txt_bid'];
			if(empty($post['txt_language'])){
				echo json_encode(array('status' => 300, 'message' => 'Language field is required.')); die();
			}
			if(empty($post['txt_title'])){
				echo json_encode(array('status' => 300, 'message' => 'Title field is required.')); die();
			}
			/*if(empty($post['txt_publisheddate'])){
				echo json_encode(array('status' => 300, 'message' => 'Published date field is required.')); die();
			}*/
			
			if(empty($post['txt_originalprice']) && ($post['txt_sellingprice'])){
				echo json_encode(array('status' => 300, 'message' => 'Original price is required.')); die();
			}
			
			if(!empty($post['txt_originalprice']) && !empty($post['txt_sellingprice'])){
				if($post['txt_originalprice'] >=  $post['txt_sellingprice']){}else{
					echo json_encode(array('status' => 300, 'message' => 'Selling Price should less than original price.')); die();
				}
			}
		/*	if(empty($post['txt_publisher'])){
				echo json_encode(array('status' => 300, 'message' => 'Publisher name field is required.')); die();
			}*/
			if(empty($post['txt_category'])){
				echo json_encode(array('status' => 300, 'message' => 'Category field is required.')); die();
			}
			if(empty($post['text_message'])){
				echo json_encode(array('status' => 300, 'message' => 'Description field is required.')); die();
			}
			$fomdata["b_language"] = $post["txt_language"];
			$fomdata["b_title"] = $post["txt_title"];

			$slug = strtolower(trim(preg_replace('/-{2,}/','-',preg_replace('/[^a-zA-Z0-9-]/', '-', $post["txt_title"])),"-"));
			$fomdata["b_slug"] = $slug;

			$fomdata["b_published"] = date('Y-m-d',strtotime($post["txt_publisheddate"]));
			// $fomdata["b_rating"] = $post["txt_rating"];
			$fomdata["b_originalprice"] = (($post["txt_originalprice"])?$post["txt_originalprice"]:0.00);
			$fomdata["b_sellingprice"] = (($post["txt_sellingprice"])?$post["txt_sellingprice"]:$fomdata["b_originalprice"]);
			$fomdata["b_category"] = $post["txt_category"];
			// $fomdata["b_status"] = '2';
			$fomdata["b_modified"] = $updated;

			$fomdata["b_description"] = $post["text_message"];

			$fomdata["b_publisher"] = $post["txt_publisher"];
			$fomdata["b_bookpages"] = (($post["txt_bookpages"])?$post["txt_bookpages"]:'');

			if($post['txt_isbn']){
				$fomdata["b_isbn"]	= $post['txt_isbn'];
			}else{
				$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
			    $isbn = array(); 
			    $alphaLength = strlen($alphabet) - 1; 
			    for ($i = 0; $i < 8; $i++) {
			        $n = rand(0, $alphaLength);
			        $isbn[] = $alphabet[$n];
			    }
			    $fomdata["b_isbn"]	= implode($isbn);
			}
			// print_r($fomdata); die;
			if($_FILES['txt_image']['name']){

				$img_config['upload_path']          = './uploads/books/';
		        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
		        $img_config['max_size']             = 3072;
		        // $img_config['encrypt_name'] = TRUE;
		        $this->upload->initialize($img_config);
		        if ( ! $this->upload->do_upload('txt_image'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            $fomdata['b_image'] = $upload_data['file_name'];
		        }
			}
			if($_FILES['bookFile']['name']){

				$config_doc['upload_path']          = './uploads/books/';
		        $config_doc['allowed_types']        = 'movie|mov|web|mflv|avi|mpg|mpeg|wmv|txt|doc|docx|pdf|ppt|pptx|mp4';
		        $config_doc['max_size']             = 800000000;
		        // $config_doc['encrypt_name'] = TRUE;
		        $this->upload->initialize($config_doc);
		        // $this->load->library('upload', $config_doc);

		        if ( ! $this->upload->do_upload('bookFile'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		        	$upload_fdata = $this->upload->data();
		        	$fomdata['b_file'] = $upload_fdata['file_name'];
		        }
			}

			$updateRes = $this->user_Auth->update('books', $fomdata, array('b_id' =>$bID));
			
			if($updateRes){
				echo json_encode(array('status' => 200, 'message' => 'Your book Successfully updated.!'));
			}else{
				echo json_encode(array('status' => 100, 'message' => 'Update Process failed!.'));
			}
    	}else{
    		echo json_encode(array('status' => 100, 'message' => 'In-valid Method!.'));
    	}
    	die();
	}
	/*-----------------------------------
		viewauthorsingleOrder()
	-----------------------------------*/
	public function viewauthorsingleOrder(){
		$post = $this->input->post();
		if($post['orderid']){
			$orderId = $post['orderid'];
			$orderDetail = $this->user_Auth->getData('book_orders', array('id' => $orderId), $se='', $sh='');
            $authorDetail = $this->user_Auth->getData('user_credentials', array('uc_id' => $orderDetail[0]->author_id), $se='uc_id,uc_firstname,uc_lastname,uc_email', $sh='');
            $bookDetail = $this->user_Auth->getData('books', array('b_id' => $orderDetail[0]->book_id), $se='b_id,b_title,b_language', $sh='');
            // $html = $orderDetail;
            // $html .= $authorDetail;
            // $html .= $bookDetail;
            $html = '<div class="authorvIeworder"><div class="col-md-12">
					    <div class="row" style="background: #cbf3c0;">
					        <div class="col-md-3">
					            <label><strong>Order ID</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($orderDetail[0]->id)?$orderDetail[0]->id:"").'</label>
					        </div>
					        <div class="col-md-3">
					            <label><strong>Transaction ID</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($orderDetail[0]->transaction_id)?$orderDetail[0]->transaction_id:"").'</label>
					        </div>
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        <div class="col-md-3">
					            <label><strong>Book Name</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($bookDetail[0]->b_title)?$bookDetail[0]->b_title:"").'</label>
					        </div>
					        <div class="col-md-3">
					            <label><strong>Author Name</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($authorDetail[0]->uc_firstname)?$authorDetail[0]->uc_firstname:'').' '.(($authorDetail[0]->uc_lastname)?$authorDetail[0]->uc_lastname:'').'</label>
					        </div>
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row" style="background: #cbf3c0;">
					        <div class="col-md-3">
					            <label><strong>Qty</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($orderDetail[0]->qty)?$orderDetail[0]->qty:'').'</label>
					        </div>
					        <div class="col-md-3">
					            <label><strong>Subtotal</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($orderDetail[0]->subtotal)?$orderDetail[0]->subtotal:'').'</label>
					        </div>
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        <div class="col-md-3">
					            <label><strong>Tax</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($orderDetail[0]->tax)?$orderDetail[0]->tax:'').'</label>
					        </div>
					        <div class="col-md-3">
					            <label><strong>Total Amount</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($orderDetail[0]->total_amt)?$orderDetail[0]->total_amt:'').'</label>
					        </div>
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row" style="background: #cbf3c0;">
					        <div class="col-md-3">
					            <label><strong>Payment Status</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($orderDetail[0]->payment_status)?$orderDetail[0]->payment_status:'').'</label>
					        </div>
					        <div class="col-md-3">
					            <label><strong>Author Payment Status</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($orderDetail[0]->author_payment_status)?$orderDetail[0]->author_payment_status:'').'</label>
					        </div>
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        <div class="col-md-3">
					            <label><strong>Created</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($orderDetail[0]->created_at >0)?date("d-M-Y H:i:s",strtotime($orderDetail[0]->created_at)):'').'</label>
					        </div>  
					        <div class="col-md-3">
					            <label><strong>Modified</strong></label>
					        </div>
					        <div class="col-md-3">
					            <label>'.(($orderDetail[0]->updated_at >0)?date("d-M-Y H:i:s",strtotime($orderDetail[0]->updated_at)):'').'</label>
					        </div>
					    </div>
					</div></div>';
			echo json_encode(array('status' => 200, 'data' => $html, 'message' => 'Successfully Done.'));
		}else{
			echo json_encode(array('status' => 100, 'message' => 'In-valid Method!.'));
		}
		die();
	}

	/*----------------------------------
		updateCustiomerAjax()
	----------------------------------*/
	public function updateCustiomerAjax(){
		$post = $this->input->post();
		if($post){

			if(empty($post["txt_firstname"])){
				echo json_encode(array('Status' => 300, 'message' => 'Please Fill First Name.')); die();
			}
			if(empty($post["txt_lastname"])){
				echo json_encode(array('Status' => 300, 'message' => 'Please Fill Last Name.')); die();
			}
			if(empty($post["txt_mobile"])){
				echo json_encode(array('Status' => 300, 'message' => 'Please Fill Mobile Number.')); die();
			}
			if(empty($post["txt_address"])){
				echo json_encode(array('Status' => 300, 'message' => 'Please Fill The Address.')); die();
			}

			$formData['uc_firstname'] = $post["txt_firstname"];
			$formData['uc_lastname'] = $post["txt_lastname"];
			$formData['uc_mobile'] = $post["txt_mobile"];			
			/*$formData['uc_status'] = $post["txt_status"];	*/		
			$formData['uc_address'] = $post["txt_address"];
			$formData['uc_modified'] 			= date('Y-m-d H:i:s');
			/*$exitMobile = $this->user_Auth->existUpdateunique('user_credentials', array('uc_mobile' => $formData['uc_mobile']),array('uc_id' =>$this->is_logged_in_user));
			if($exitMobile){
				echo json_encode(array('Status' => 400, 'message' => 'Mobile already registered Please Chnage mobile number.')); die();
			}*/
			if($_FILES['txt_image']['name']){
		        $config['upload_path']          = './uploads/users/';
		        $config['allowed_types']        = 'gif|jpg|jpeg|png';
		        $config['max_size']             = 2048;
        		$this->upload->initialize($config);
		        if ( ! $this->upload->do_upload('txt_image'))
		        {
		            echo json_encode(array('Status' => 100, 'message' => $this->upload->display_errors())); die();
		        }else{
		            $upImage = $this->upload->data();
					$formData['uc_image'] = $upImage["file_name"];
		        }

			}
			$updated = $this->user_Auth->update('user_credentials', $formData, array('uc_id' =>$this->is_logged_in_user));
			if($updated){
				$user = $this->user_Auth->getData('user_credentials', $where = array('uc_id' => $this->is_logged_in_user),$se='',$s='');

				$udata["u_id"] = $user[0]->uc_id;
				$udata["u_firstname"] = $user[0]->uc_firstname;
				$udata["u_lastname"] = $user[0]->uc_lastname;
				$udata["u_email"] = $user[0]->uc_email;
				$udata["u_mobile"] = $user[0]->uc_mobile;
				$udata["u_image"] = (($user[0]->uc_image)?$user[0]->uc_image:'user.png');
				$udata["u_role"] = $user[0]->uc_role;
				$udata["u_status"] = $user[0]->uc_status;
				$udata["u_created"] = $user[0]->uc_created;
				$udata["u_modified"] = (($user[0]->uc_modified >0)?$user[0]->uc_modified:'');

				$this->session->set_userdata('is_logged_in_user', $user[0]->uc_id);
				$this->session->set_userdata('is_logged_in_user_info', $udata);
				echo json_encode(array('Status' => 200, 'url' => site_url('user/profile/'.$this->is_logged_in_user), 'message' => 'Profile Successfully Updated!.')); die();
			}else{
				echo json_encode(array('Status' => 100, 'message' => 'Update Process failed!.')); die();
			}
			echo json_encode(array('status' => 100, 'message' => 'In-valid Mehtod!.'));
		}else{
			echo json_encode(array('status' => 100, 'message' => 'In-valid Mehtod!.'));
		}
		die();
	}
	/*-----------------------------------
		changePassword()
	-----------------------------------*/
	public function changePassword(){
		$post = $this->input->post();
		if($post['txt_newpass']){
			$pass = $post['txt_newpass'];
			$dataF=array('uc_password' => md5($pass), 'uc_modified' => date('Y-m-d H:i:s'));
			 $res = $this->user_Auth->update('user_credentials',$dataF, $F=array('uc_id' => $this->is_logged_in_user));
			//$res = 1;
			if($res){
				echo json_encode(array('status' => 200, 'message' => 'Password changed successfully!.'));	
			}else{
				echo json_encode(array('status' => 300, 'message' => 'Update Process failed!.'));
			}
		}else{
			echo json_encode(array('status' => 100, 'message' => 'In-valid Method!.'));
		}
		die;
	}
	/*---------------------------
		managemyBook()
	---------------------------*/
	public function managemyBook(){
		$post = $this->input->post();
		if($post){
			if($post['accessid']){
				$filter['ba_id'] = $post['accessid'];
			}

			$filter['ba_userid'] = $this->is_logged_in_user;

			$dataUP['ba_bookid'] = $post['selectbookId'];
			$dataUP['ba_changedate'] = date('Y-m-d H:i:s');
			
			$res = $this->user_Auth->update('book_access',$dataUP,$filter);
			if($res){
				echo json_encode(array('status' => 200, 'message' => 'Successfully Updated!.'));
			}else{
				echo json_encode(array('status' => 100, 'message' => 'Update Process Failed!.'));
			}
		}else{
			echo json_encode(array('status' => 100, 'message' => 'In-valid Method!.'));
		}
		die();
	}
}