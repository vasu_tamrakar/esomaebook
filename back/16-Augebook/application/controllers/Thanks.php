<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thanks extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if($this->session->userdata('is_logged_in_user')){
			$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');	
		}
		if($this->session->userdata('is_logged_in_user_info')){
			$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');	
		}
		$this->load->model('user_Auth');
		
		
	}
	public function index()
	{
		$data["title"] = 'Thans page | '.SITENAME;
		
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-thanks',$data);
		$this->load->view('front/common/footer',$data);
	}
}