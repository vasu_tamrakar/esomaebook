<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">     
    <div class="">
        <div class="page-title">
            <div class="title_left">
              <h3>New Author Approval Details</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>New Author Approval<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                          <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div id="responcesResult"></div>
                    <div class="x_content">
                        <br />
                        <?php 
                        if(isset($authorDetails) && ($authorDetails)){
                           // $authorDetails[0]->uc_id;
                        ?>
                        <div class="col-md-6 col-md-offset-3">
                        <div class="row">
                            <div class="col-md-6"><label>First Name: </label></div><div class="col-md-6"><label><?php echo (($authorDetails[0]->uc_firstname)?$authorDetails[0]->uc_firstname:''); ?></label></div>
                        </div>
                    
                        
                        <div class="row">
                            <div class="col-md-6"><label>Last Name: </label></div><div class="col-md-6"><label><?php echo (($authorDetails[0]->uc_lastname)?$authorDetails[0]->uc_lastname:''); ?></label></div>
                        </div>
                        


                        <div class="row">
                            <div class="col-md-6"><label>Email: </label></div><div class="col-md-6"><label><?php echo (($authorDetails[0]->uc_email)?$authorDetails[0]->uc_email:''); ?></label></div>
                        </div>


                        <div class="row">
                            <div class="col-md-6"><label>Mobile: </label></div><div class="col-md-6"><label><?php echo (($authorDetails[0]->uc_mobile)?$authorDetails[0]->uc_mobile:''); ?></label></div>
                        </div>


                        <div class="row">
                            <div class="col-md-6"><label>Image: </label></div><div class="col-md-6"><img src="<?php echo (($authorDetails[0]->uc_image)?base_url('uploads/users/'.$authorDetails[0]->uc_image):base_url('uploads/users/user.png')); ?>" height="50px" width="50px"></label></div>
                        </div>

                        <?php 
                        
                        $moredetails = $this->user_Auth->getData('authors',$w=array('a_fk_of_uc_id' => $authorDetails[0]->uc_id)); 
                        if($moredetails[0]){ ?>
                        <div class="row">
                            <div class="col-md-6"><label>Description: </label></div><div class="col-md-6"><label><?php echo (($moredetails[0]->a_description)?$moredetails[0]->a_description:''); ?></label></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"><label><?php echo (($moredetails[0]->a_paypal)?'Paypal Email':'Mmoney'); ?>: </label></div><div class="col-md-6"><label><?php echo (($moredetails[0]->a_paypal)?$moredetails[0]->a_paypal:$moredetails[0]->a_mmoney); ?></label></div>
                        </div>
                        <?php } ?>


                        <div class="row">
                            <div class="col-md-6"><label>Address: </label></div><div class="col-md-6"><label><?php echo (($authorDetails[0]->uc_address)?$authorDetails[0]->uc_address:''); ?></label></div>
                        </div>


                        <div class="row">
                            <div class="col-md-6"><label>Created: </label></div><div class="col-md-6"><label><?php echo (($authorDetails[0]->uc_created > 0)?date('d-M-Y H:i:s', strtotime($authorDetails[0]->uc_created)):''); ?></label></div>
                        </div>


                        <div class="row">
                            <div class="col-md-6"><label>Modified: </label></div><div class="col-md-6"><label><?php echo (($authorDetails[0]->uc_modified > 0)?date('d-M-Y H:i:s', strtotime($authorDetails[0]->uc_modified)):''); ?></label></div>
                        </div>


                        <div class="row">
                            <div class="col-md-6"><label>Status: </label></div><div class="col-md-6"><label><?php echo (($authorDetails[0]->uc_status)?'True':'FALSE'); ?></label></div>
                        </div>


                        <div class="row">
                            <div class="col-md-6"><label>Active: </label></div><div class="col-md-6"><label><?php echo (($authorDetails[0]->uc_active =='1')?'Active':'Deactive'); ?></label></div>
                        </div>
                        <?php if ($authorDetails[0]->uc_active == '0'){ ?>
                        <button type="button" class="btn btn-success" onclick="authorApproved(<?php echo $authorDetails[0]->uc_id; ?>,'1')">Approve</button>
                        <?php }else{ ?>
                        <button type="button" class="btn btn-success" onclick="authorApproved(<?php echo $authorDetails[0]->uc_id; ?>,'0')">Decline</button>
                        <?php } ?>
                        <button type="button" class="btn btn-danger" onclick="deleteAuthorsDetail(<?php echo (($authorDetails[0]->uc_id)?$authorDetails[0]->uc_id:''); ?>)">Delete</button>
                        </div>
                        <?php
                        }else{
                            echo '<label> In-valid Author details</label>';
                        } 
                         
                        ?>
                    </div>
                </div>
            </div>

        </div> 
    </div>
</div>
        <!-- /page content -->