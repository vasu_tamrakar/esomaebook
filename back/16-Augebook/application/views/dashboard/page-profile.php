<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>User Profile</h3>
              </div>

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2><?php echo $is_logged_in_user_info['u_firstname'].' '.$is_logged_in_user_info['u_lastname']; ?><small> Profile</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                  <div class="x_content">
                    <br />
                    <div style="width:150px;height:150px;border: 1px solid #ccc; margin: 0 auto 10px auto;">
                          <img src="<?php echo base_url('uploads/users/').$is_logged_in_user_info['u_image']?>" class="img-responsive">
                        </div>
                    <form name="profile_Form" id="profile_Form" action="<?php echo site_url('dashboard/profile/'.$login_userid)?>" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_firstname">First Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_firstname" id="txt_firstname" value="<?php echo $is_logged_in_user_info['u_firstname']; ?>" placeholder="First Name" class="form-control col-md-7 col-xs-12">
                          <?php echo form_error('txt_firstname','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_lastname">Last Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_lastname" id="txt_lastname" value="<?php echo $is_logged_in_user_info['u_lastname']; ?>" placeholder="Last Name" class="form-control col-md-7 col-xs-12">
                          <?php echo form_error('txt_firstname','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_email" id="txt_email" value="<?php echo $is_logged_in_user_info['u_email']; ?>" placeholder="Email Address" class="form-control col-md-7 col-xs-12">
                          <?php echo form_error('txt_email','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_mobile">Mobile <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_mobile" id="txt_mobile" value="<?php echo $is_logged_in_user_info['u_mobile']; ?>" placeholder="Mobile Number" class="form-control col-md-7 col-xs-12">
                          <?php echo form_error('txt_mobile','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      
                      <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_address">Address <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input name="txt_address" id="txt_address" placeholder="Address" class="form-control col-md-7 col-xs-12" type="text">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_role">Role <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php
                          $rid = $is_logged_in_user_info['u_role'];
                          if($rid == 1){
                              $where = '';
                          }else{
                              $where = array('ur_id >='=> $rid);
                          }
                          $role = $this->user_Auth->getData('user_roles',$where,$sel='',$short='ur_id ASC'); 
                          if($role){
                          ?>
                          <select name="txt_role" id="txt_role" class="form-control col-md-7 col-xs-12">
                            <option value="">Select the role</option>
                            <?php
                              foreach ($role as $value) {
                                  ?>
                                  <option value="<?php echo $value->ur_id; ?>" <?php if($rid == $value->ur_id){ echo 'selected';} ?>><?php echo $value->ur_name; ?></option>
                                  <?php
                              }
                              ?>
                          </select><?php
                          }?>
                          <?php echo form_error('txt_role','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_img">Image <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="txt_img" id="txt_img" class="form-control col-md-7 col-xs-12" type="file" accept="image/*">
                        </div>
                        
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="<?php echo base_url(); ?>" class="btn btn-primary" type="button">Cancel</a>
              
                          <button type="submit" class="btn btn-success">Update</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

            
          </div>
        </div>
        <!-- /page content -->