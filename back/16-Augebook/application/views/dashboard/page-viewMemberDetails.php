<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Member Details</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>View Member<small></small></h2>
                         <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                          <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="x_content">
                        <br />
                        <div class="col-md-8 col-md-offset-2">
                        <?php if($member){ ?>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <label class="control-label">ID</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $member->pd_id; ?> </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">User</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $member->uc_firstname.' '.$member->uc_lastname; ?></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">User Email</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $member->uc_email; ?></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Plan Details</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $member->mp_name.' ( $'.$member->mp_price.'-'.$member->mp_validity.' )'; ?></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Payment Type</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">
                                    <?php 
                                        if($member->pd_payby == 'cc'){
                                        echo 'Credit Card';
                                      }else if($member->pd_payby == 'evc'){
                                        echo 'EVC';
                                      }else if($member->pd_payby == 'mpesa'){
                                        echo 'Mpesa';
                                      }
                                    ?>
                                        
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Payment Status</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">
                                    <?php 
                                        echo $member->pd_status;
                                    ?>
                                        
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Order Date</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">
                                    <?php 
                                        echo date('m-d-Y',strtotime($member->pd_created));
                                    ?>
                                        
                                    </label>
                                </div>
                            </div>
                            
                            
                            <br><br>    
                            <div class="row">
                                  <div class="col-md-12">
                                      <center>
                                        
                                          <a href="<?php echo site_url('dashboard/viewAllMembers'); ?>" class="btn btn-warning">Back</a>
                                          <?php if($member->pd_status == 'pending'){?>
                                          <button type="button" onclick="paymentStatusChange(<?php echo $member->pd_id; ?>,'completed')" class="btn btn-primary">Approve</button>
                                          <?php }else if($member->pd_status == 'completed'){ ?>
                                          <button type="button" onclick="paymentStatusChange(<?php echo $member->pd_id; ?>,'pending')" class="btn btn-danger">Decline</button>
                                        <?php }?>
                                         <!--  <button type="button" onclick="deleteMember(<?php echo $member->pd_id; ?>)" class="btn btn-info">Delete</button> -->
                                      </center>
                                  </div>
                            </div>
                    
                        <?php
                        }else{
                        ?><label class="control-label"> Invalid Member Details Request.</label><?php
                        } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->