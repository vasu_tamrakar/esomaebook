 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Datatables -->
<link href="<?php echo base_url('themes/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
              <h3>Book Reviews</h3>
            </div>
        </div>
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Book Reviews</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                        <?php 
                          $alert = $this->session->flashdata('alert');
                          if($alert){
                              ?>
                              <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                                <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                        <?php
                        }
                        ?>

                         <div class="row">
                            <div class="form-group">
                                <?php 
                                    $attributes = array(
                                            'class' => 'control-label col-md-3 col-sm-3 col-xs-12',
                                    );

                                    echo form_label('Search Book Review By Book<span class="required"> *</span>', 'book_review', $attributes);
                                ?>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php 
                                   $options = $books;

                                   $extra = 'id="book_review" class="form-control col-md-7 col-xs-12"';
                                   echo form_dropdown('book_review', $options,$filtered,$extra);
                                ?>
                                </div>
                            </div>
                        </div> 

                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Book</th>
                                    <th>User</th>
                                    <th>Review</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php
                            if($bookReviews) { ?>
                      
                            <tbody>
                              <?php 
                                foreach ($bookReviews as $review) {
                                  echo '<tr>';
                                  echo '<td>'.$review->id.'</td>';
                                  echo '<td>'.$review->b_title.'</td>';
                                  echo '<td>'.$review->uc_firstname.' '.$review->uc_lastname.'</td>';
                                  echo '<td>';
                                    $len = strlen($review->review);
                                    if($len > 15){
                                        echo substr($review->review,0,15).'...';
                                    }else{
                                        echo $review->review;
                                    } 
                                  echo '</td>';
                                  echo '<td>';
                                  echo ($review->approved=='1')?'Approved':'Pending';
                                  echo '</td>';
                                  echo '<td>';
                                  ?>

                                    <div class="action-menu">
                                           <a title="View Details" class="btn btn-info" href="<?php echo site_url('dashboard/reviewDetails/'.$review->id); ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                          
                                          <a title="Delete" data-title="Goto twitter?" class="btn btn-info" href="javascript:void(0)" onclick="deletebookReview(<?php echo $review->id; ?>)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
                                      </div>
                                      <div class="clearfix"></div>
                                  <?php
                                  echo '</td>';
                                  echo '</tr>';
                                }
                              ?>
                            </tbody>
                            <?php 
                            }else{ ?>
                            
                            No Reviews..
                            <?php
                            } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->