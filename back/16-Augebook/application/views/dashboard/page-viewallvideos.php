 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Datatables -->
<link href="<?php echo base_url('themes/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
              <h3>Veiw All Videos</h3>
            </div>

            <!-- <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
              </div>
            </div> -->
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>View All Videos</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <?php 
                        $alert = $this->session->flashdata('alert');
                        if($alert){
                            ?>
                            <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                                <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php
                        }
                        ?>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Language</th>
                                    <th>Category</th>
                                    <th>Is Premium</th>
                                  <!--   <th>V like</th> -->
                                    <th>Thumbnail</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php
                            if($allVideosids) { ?>
                            <tbody>
                            <?php
                            foreach ($allVideosids as $vID) {
                                $video = $this->user_Auth->getData('videos', $w=array('v_id' => $vID->v_id),$se='',$sh='');
                            ?>
                                <tr>
                                    <td><?php echo $video[0]->v_id; ?></td>
                                    <td><?php echo (($video[0]->v_title)?$video[0]->v_title:""); ?></td>
                                    <td><?php echo (($video[0]->v_language == 'somali')?"Somali":"English"); ?></td>
                                    <td><?php echo $video[0]->v_category; ?></td>
                                    <td><?php echo (($video[0]->v_premium == 'true')?'No':'Yes'); ?></td>
                                    <!-- <td><?php echo $video[0]->v_like; ?></td> -->
                                    <td><div style="display:block;width:32px;height:32px;"><a href="<?php echo (($video[0]->v_image)?base_url('uploads/videos/'.$video[0]->v_image):base_url('uploads/videos/Video-Icon-PNG-Image.png')); ?>"> <img src="<?php echo (($video[0]->v_image)?base_url('uploads/videos/'.$video[0]->v_image):base_url('uploads/videos/Video-Icon-PNG-Image.png')); ?>" class="img-responsive"></a></div></td>

                                    <td><div style="text-align: center; font-size: 20px;"><?php echo (($video[0]->v_status == '1')?"<i class='fa fa-check'></i>":"<i class='fa fa-times-circle'></i>"); ?> </div></td>
                                    <td>
                                      <div class="action-menu">
                                          <a title="View" class="btn btn-info" href="<?php echo site_url('dashboard/videoDetails/'.$vID->v_id); ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                          <a title="Edit" class="btn btn-info" href="<?php echo site_url('dashboard/editvideoDetails/'.$vID->v_id); ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                          <a title="Delete" data-title="Goto twitter?" class="btn btn-info" href="javascript:void(0)" onclick="deleteVideoDetail(<?php echo $vID->v_id; ?>)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                      </div>
                                      <div class="clearfix"></div>
                                  
                                      <?php //echo $author[0]->a_id; ?>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                            <?php 
                            }else{ ?>
                            
                            No More Data..
                            <?php
                            } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->