<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Add User</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
     
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>Add User Form<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                  <div class="x_content">
                    <br />

                    <form name="addnewuser_Form" id="addnewuser_Form" action="<?php echo site_url('dashboard/addnewUser'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left needs-validation" novalidate>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_firstname">First Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_firstname" id="txt_firstname" value="<?php echo set_value('txt_firstname'); ?>" placeholder="First Name" class="form-control col-md-7 col-xs-12" max-length="150">
                          <?php echo form_error('txt_firstname','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_lastname">Last Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_lastname" id="txt_lastname" value="<?php echo set_value('txt_lastname'); ?>" placeholder="Last Name" class="form-control col-md-7 col-xs-12" max-length="150">
                          <?php echo form_error('txt_lastname','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_email">Email Address <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_email" id="txt_email" value="<?php echo set_value('txt_email'); ?>" placeholder="Email Address" class="form-control col-md-7 col-xs-12" max-length="250">
                          <?php echo form_error('txt_email','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_mobile">Mobile Number <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_mobile" id="txt_mobile" value="<?php echo set_value('txt_mobile'); ?>" placeholder="Mobile Number" class="form-control col-md-7 col-xs-12">
                          <?php echo form_error('txt_mobile','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      
                      <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_role">User Role </label>
                        <?php $selrole =  set_value('txt_role') ?>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php $rolesids = $this->user_Auth->getData('user_roles', $w = '', $se='ur_id,ur_name', $sh='ur_id ASC');

                          if($rolesids){ ?>
                          <select name="txt_role" id="txt_role" class="form-control col-md-7 col-xs-12">
                              <option value="" selected>Select The Roles</option>
                              <?php foreach ($rolesids as $userrole) { ?>
                                <option value="<?php echo $userrole->ur_id; ?>" <?php if($userrole->ur_id === $rolesids){echo 'selected';}?>> <?php echo $userrole->ur_name; ?> </option>
                              <?php } ?>
                          </select>
                          <?php echo form_error('txt_role','<span class="text-danger">','</span>'); ?>
                          <?php } ?>
                        </div>
                      </div> -->
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_image">User Image</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="file" name="txt_image" id="txt_image"  class="form-control col-md-7 col-xs-12" accept="image/*">
                          <span class="text-danger">Accept file only .png,jpeg,gif.</span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_password">Password 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="password" name="txt_password" id="txt_password" value="<?php echo set_value('txt_password'); ?>" placeholder="******" class="form-control col-md-7 col-xs-12" max-length="50">
                          Note:- If you do not enter any value, password will be auto generated.
                          <?php echo form_error('txt_password','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Save</button>
                            <a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary" type="button">Cancel</a>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

            
          </div>
        </div>
        <!-- /page content -->