<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">     
    <div class="">
        <div class="page-title">
            <div class="title_left">
              <h3>New Book Approval Details</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>New Book Approval<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                          <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div id="responcesResult"></div>
                    <div class="x_content">
                        <br />
                        <?php 
                        // print_r($bookDetails);
                        if(isset($bookDetails) && ($bookDetails)){
                            $bookDetails[0]->b_id;
                        ?>
                        <div class="col-md-6 col-md-offset-3">
                        <div class="row">
                            <div class="col-md-6"><label>Book ID: </label></div><div class="col-md-6"><label><?php echo (($bookDetails[0]->b_id)?$bookDetails[0]->b_id:''); ?></label></div>
                        </div>
                    
                        
                        <div class="row">
                            <div class="col-md-6"><label>Book Title: </label></div><div class="col-md-6"><label><?php echo (($bookDetails[0]->b_title)?$bookDetails[0]->b_title:''); ?></label></div>
                        </div>
                        


                        <div class="row">
                            <div class="col-md-6"><label>Book Language: </label></div><div class="col-md-6"><label><?php echo (($bookDetails[0]->b_language)?$bookDetails[0]->b_language:''); ?></label></div>
                        </div>


                        <div class="row">
                            <div class="col-md-6"><label>Book Category: </label></div><div class="col-md-6"><label>
                            <?php
                            if($bookDetails[0]->b_category){
                                $categoryDetail = $this->user_Auth->getData('categories',$w=array('c_id' => $bookDetails[0]->b_category), $se='c_name');
                                echo (isset($categoryDetail[0]->c_name)?$categoryDetail[0]->c_name:'');
                            }
                            ?></label></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"><label>Book Author: </label></div><div class="col-md-6"><label>
                            <?php
                            if($bookDetails[0]->b_fk_of_aid){
                                $authorDetails =$this->user_Auth->getData('user_credentials', $w=array('uc_id' => $bookDetails[0]->b_fk_of_aid),$se='uc_id,uc_email,uc_firstname,uc_lastname');
                                echo (isset($authorDetails[0]->uc_firstname)?$authorDetails[0]->uc_firstname:'').' '.(isset($authorDetails[0]->uc_lastname)?$authorDetails[0]->uc_lastname:'');
                            }
                            ?></label></div>
                        </div>

                        <div class="row">
                            <div class="col-md-6"><label>Book Image: </label></div><div class="col-md-6"><a href="<?php echo (($bookDetails[0]->b_image)?base_url('uploads/books/'.$bookDetails[0]->b_image):base_url('uploads/books/book.png')); ?>"><img src="<?php echo (($bookDetails[0]->b_image)?base_url('uploads/books/'.$bookDetails[0]->b_image):base_url('uploads/books/book.png')); ?>" height="50px" width="50px"></a></label></div>
                        </div>

                        <div class="row">
                            <div class="col-md-6"><label>Book Price: </label></div><div class="col-md-6"><label><?php echo (($bookDetails[0]->b_price)?$bookDetails[0]->b_price:''); ?></label></div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6"><label>Book published: </label></div><div class="col-md-6"><label><?php echo (($bookDetails[0]->b_published > 0)?date('d-M-Y',strtotime($bookDetails[0]->b_published)):''); ?></label></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"><label>Book Publisher: </label></div><div class="col-md-6"><label><?php echo (($bookDetails[0]->b_publisher)?$bookDetails[0]->b_publisher:''); ?></label></div>
                        </div>
                        

                        <div class="row">
                            <div class="col-md-6"><label>Book Pages: </label></div><div class="col-md-6"><label><?php echo (($bookDetails[0]->b_bookpages)?$bookDetails[0]->b_bookpages:''); ?></label></div>
                        </div>


                        <div class="row">
                            <div class="col-md-6"><label>Book Created: </label></div><div class="col-md-6"><label><?php echo (($bookDetails[0]->b_created > 0)?date('d-M-Y H:i:s', strtotime($bookDetails[0]->b_created)):''); ?></label></div>
                        </div>


                        <div class="row">
                            <div class="col-md-6"><label>Book Modified: </label></div><div class="col-md-6"><label><?php echo (($bookDetails[0]->b_modified > 0)?date('d-M-Y H:i:s', strtotime($bookDetails[0]->b_modified)):''); ?></label></div>
                        </div>


                        <div class="row">
                            <div class="col-md-6"><label>Book Description: </label></div><div class="col-md-6"><label><?php echo (($bookDetails[0]->b_description)?$bookDetails[0]->b_description:''); ?></label></div>
                        </div>


                        <div class="row">
                            <div class="col-md-6"><label>Book Status: </label></div><div class="col-md-6"><label>
                            <?php 
                            if($bookDetails[0]->b_status === '1'){
                                echo "True";
                            }elseif($bookDetails[0]->b_status ==='2'){
                                echo "Pending";
                            }else{
                                echo "False";
                            }

                            ?></label></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"><label>Book ISBN: </label></div><div class="col-md-6"><label><?php echo (isset($bookDetails[0]->b_isbn)?$bookDetails[0]->b_isbn:''); ?></label></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"><label>Book File: </label></div><div class="col-md-6"><label>
                            <?php
                            echo (isset($bookDetails[0]->b_file)?'<a target="_blank" href="'.base_url("uploads/books/".$bookDetails[0]->b_file).'">'.$bookDetails[0]->b_file.'</a>':''); ?></label></div>
                        </div>
                        <?php if($bookDetails[0]->b_status == '2'){?>
                        <button type="button" class="btn btn-success" onclick="bookApproved(<?php echo (($bookDetails[0]->b_id)?$bookDetails[0]->b_id:''); ?>)">Approved</button>
                        
                        <button type="button" class="btn btn-danger" onclick="deletebookDetail(<?php echo (($bookDetails[0]->b_id)?$bookDetails[0]->b_id:''); ?>)">Delete</button>
                        </div>
                        <?php } ?>
                        <?php
                        }else{
                            echo '<label> In-valid Author details</label>';
                        } 
                         
                        ?>
                    </div>
                </div>
            </div>

        </div> 
    </div>
</div>
        <!-- /page content -->