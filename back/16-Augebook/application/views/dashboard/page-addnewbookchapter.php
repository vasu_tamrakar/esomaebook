<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">     
    <div class="">
<!--         <div class="page-title">
            <div class="title_left">
              <h3>Add Book Chapter</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div> -->
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">

                        <h2>Add Book Chapter<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                          <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="x_content">
                        <br />
                        <?php 
                            echo form_open('dashboard/addnewBookChapter', 'class="form-horizontal form-label-left needs-validation" id="addChapterForm"');
                        ?>
                            <div class="row">
                                <div class="form-group">
                                    <?php 
                                        $attributes = array(
                                                'class' => 'control-label col-md-3 col-sm-3 col-xs-12',
                                        );

                                        echo form_label('Chapter Title<span class="required"> *</span>', 'chapter_title', $attributes);
                                    ?>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?php 
                                        $data = array(
                                                'name'  => 'chapter_title',
                                                'id'    => 'chapter_title',
                                                'placeholder' => 'Chapter Title',
                                                'class' => 'form-control col-md-7 col-xs-12',
                                                'value' => @$post['chapter_title']
                                        );
                                        echo form_input($data);

                                        echo form_error('chapter_title','<span class="text-danger">','</span>'); 
                                    ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <?php 
                                        $attributes = array(
                                                'class' => 'control-label col-md-3 col-sm-3 col-xs-12',
                                        );

                                        echo form_label('Select Book<span class="required"> *</span>', 'chapter_book', $attributes);
                                    ?>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?php 
                                       $options = $books;

                                       $extra = 'id="chapter_book" class="form-control col-md-7 col-xs-12"';
                                       echo form_dropdown('chapter_book', $options,@$post['chapter_book'],$extra);

                                        echo form_error('chapter_book','<span class="text-danger">','</span>'); 
                                    ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <?php 
                                        $attributes = array(
                                            'class' => 'control-label col-md-3 col-sm-3 col-xs-12',
                                    );

                                    echo form_label('Chapter Description', 'chapter_desc', $attributes);
                                    echo '<br>';
                                    $extra = 'id="txtEditor" class="form-control col-md-6 col-sm-6 col-xs-12"';
                                    $textValue = array(
                                        'name' => 'chapter_description',
                                        
                                    );
                                        echo form_textarea($textValue,'',$extra);

                                    echo form_error('chapter_description','<span class="text-danger">','</span>'); 
                                    ?>
                                    
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="<?php echo base_url('dashboard/viewallBooksChapter'); ?>" class="btn btn-primary" type="button">Cancel</a>
                                </div>
                            </div>
                        <?php 
                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
        <!-- /page content -->