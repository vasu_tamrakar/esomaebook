<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Book Review Details</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>View Review<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                          <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="x_content">
                        <br />
                        <div class="col-md-8 col-md-offset-2">
                        <?php if($bookReviews[0]){ ?>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <label class="control-label">Review ID</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookReviews[0]->id; ?> </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Book Title</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookReviews[0]->b_title; ?></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">User</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookReviews[0]->uc_firstname,' '.$bookReviews[0]->uc_lastname; ?></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Review</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookReviews[0]->review; ?></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Review Status</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">
                                    <?php 
                                        echo ($bookReviews[0]->approved=='1')?'Approved':'Pending';
                                    ?>
                                        
                                    </label>
                                </div>
                            </div>
                            
                            
                            <br><br>    
                            <div class="row">
                                  <div class="col-md-12">
                                      <center>
                                        
                                          <a href="<?php echo site_url('dashboard/manageBookReviews'); ?>" class="btn btn-warning">Back</a>
                                          <?php if($bookReviews[0]->approved == '0'){?>
                                          <button type="button" onclick="approvebookReview(<?php echo $bookReviews[0]->id; ?>)" class="btn btn-primary">Approve</button>
                                          <?php } ?>
                                          <button type="button" onclick="deletebookReview(<?php echo $bookReviews[0]->id; ?>)" class="btn btn-info">Delete</button>
                                      </center>
                                  </div>
                            </div>
                    
                        <?php
                        }else{
                        ?><label class="control-label"> Invalid Review Details Request.</label><?php
                        } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->