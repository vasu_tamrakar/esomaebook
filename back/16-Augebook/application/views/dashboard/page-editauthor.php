<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Edit Author Page</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                      <h2>Edit Author Form<small></small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                          <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="x_content">
                        <?php if(isset($authorDetails[0])){ ?>
                        <div style="width:150px;margin:11px auto;">
                            <img src="<?php echo (($authorDetails[0]->uc_image)?base_url('uploads/users/'.$authorDetails[0]->uc_image):base_url('uploads/users/author.png')); ?>" class="img-responsive">
                        </div>
                        <form name="editauthor_Form" id="editauthor_Form" action="<?php echo site_url('dashboard/editauthorDetails/'.$authorDetails[0]->uc_id); ?>" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="row">
                                <input type="hidden" name="txt_authorid" id="txt_authorid" value="<?php echo $authorDetails[0]->uc_id; ?>">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_authorfirstname">First Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_authorfirstname" id="txt_authorfirstname" value="<?php echo $authorDetails[0]->uc_firstname; ?>" placeholder="Author First Name" class="form-control col-md-7 col-xs-12" maxlength="180" disabled>
                                            <?php echo form_error('txt_authorfirstname','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_authorlastname">Last Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_authorlastname" id="txt_authorlastname" value="<?php echo $authorDetails[0]->uc_lastname; ?>" placeholder="Author Last Name" class="form-control col-md-7 col-xs-12" maxlength="180" disabled>
                                            <?php echo form_error('txt_authorlastname','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_authoremail">Email <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_authoremail" id="txt_authoremail" value="<?php echo $authorDetails[0]->uc_email; ?>" placeholder="Author Emial" class="form-control col-md-7 col-xs-12" disabled>
                                            <?php echo form_error('txt_authoremail','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_published">Published Books <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <?php $author = $this->user_Auth->getData('authors', array('a_fK_of_uc_id' => $authorDetails[0]->uc_id)); ?>
                                            <input type="text" name="txt_published" id="txt_published" value="<?php echo (isset($author[0]->a_publishbooks)?$author[0]->a_publishbooks:""); ?>" placeholder="2018" class="form-control col-md-7 col-xs-12" maxlength="4">
                                            <?php echo form_error('txt_published','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_lang">Native Language</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="form-group">
                                                <select name="txt_lang" id="txt_lang" class="form-control col-md-7 col-xs-12">
                                                    <option value="english" <?php echo ($author[0]->a_lang == "english")?"selected":"";?> >English</option>
                                                    <option value="somali" <?php echo ($author[0]->a_lang == "somali")?"selected":"";?>>Somali</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_facebook">Facebook <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_facebook" id="txt_facebook" value="<?php echo (isset($author[0]->a_facebook)?$author[0]->a_facebook:''); ?>" placeholder="Facebook Link" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_twitter">Twitter <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_twitter" id="txt_twitter" value="<?php echo (isset($author[0]->a_twitter)?$author[0]->a_twitter:''); ?>" placeholder="Twitter link" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_gplush">Google+ <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_gplush" id="txt_gplush" value="<?php echo (isset($author[0]->a_gplush)?$author[0]->a_gplush:''); ?>" placeholder="Google plus link" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group-row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_instagram">Instagram+ <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_instagram" id="txt_instagram" value="<?php echo (isset($author[0]->a_instagram)?$author[0]->a_instagram:''); ?>" placeholder="Instagram Link" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <textarea name="txtEditor" id="txtEditor"></textarea>
                                    <div id="textDescription" style="display:none;"><?php echo (isset($author[0]->a_description)?$author[0]->a_description:''); ?> </div>
                                </div>
                            </div>
                            
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Update</button>
                                    <a href="<?php echo base_url('dashboard/viewallAuthors'); ?>" class="btn btn-primary" type="button">Cancel</a>
                                    <a href="<?php echo base_url('dashboard/addnewAuthor'); ?>" class="btn btn-primary" type="button">Add New Author</a>
                                </div>
                            </div>

                        </form>
                        <?php }else{ ?>
                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                <strong style="text-transform: capitalize;">Note !</strong> In- Valid Data!.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <!-- /page content -->