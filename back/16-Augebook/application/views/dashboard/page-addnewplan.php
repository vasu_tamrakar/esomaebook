<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Add New Membership Plan</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
     
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>Add New Plan Form<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                  <div class="x_content">
                    <br />

                    <form name="addnewplan_Form" id="addnewplan_Form" action="<?php echo site_url('dashboard/addnewPlan'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left needs-validation" novalidate>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_name">Plan Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_name" id="txt_name" value="<?php echo set_value('txt_name'); ?>" placeholder="Plan Name" class="form-control col-md-7 col-xs-12" max-length="100">
                          <?php echo form_error('txt_name','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_price">Plan Price <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_price" id="txt_price" value="<?php echo set_value('txt_price'); ?>" placeholder="plan price" class="form-control col-md-7 col-xs-12" max-length="6">
                          <?php echo form_error('txt_price','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_valididty">Plan Validity <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_valididty" id="txt_valididty" value="<?php echo set_value('txt_valididty'); ?>" placeholder="Enter Validity" class="form-control col-md-7 col-xs-12" max-length="150">
                          <?php echo form_error('txt_valididty','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_descriptions">Description <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea name="txt_descriptions" id="txt_descriptions" placeholder="Descriptions" class="form-control col-md-7 col-xs-12"><?php echo set_value('txt_descriptions'); ?></textarea>
                          <?php echo form_error('txt_descriptions','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">Save</button>
                          <a href="<?php echo base_url('dashboard/viewallPlans'); ?>" class="btn btn-primary" type="button">Cancel</a>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

            
          </div>
        </div>
        <!-- /page content -->