<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2">
            <?php include 'frontleftmenu.php'; ?>
        </div>
        <div class="col-md-10 col-lg-10">
            <div class="row">
                <div class="col-md-12">
                <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                  <div class="mb-5">
                      <h2 class="heading text-center"><strong>My orders</strong></h2>
                  </div>

                  <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Transaction ID</th>
                              <th>Book Name</th>
                              <th>Qty</th>
                              <th>Total Amount</th>
                              <th>Payment Status</th>
                              <th>Order Date</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php
                      if($myorderID){
                          foreach ($myorderID as $value) {
                            $orderDetail = $this->user_Auth->getData('book_orders', array('id' => $value->id), $se='', $sh='');
                            $authorDetail = $this->user_Auth->getData('user_credentials', array('uc_id' => $orderDetail[0]->author_id), $se='uc_id,uc_firstname,uc_lastname,uc_email', $sh='');
                            $bookDetail = $this->user_Auth->getData('books', array('b_id' => $orderDetail[0]->book_id), $se='b_id,b_title,b_language', $sh='');
                      ?>
                          <tr>
                              <td><?php echo (($orderDetail[0]->id)?$orderDetail[0]->id:''); ?></td>
                              <td><?php echo (($orderDetail[0]->transaction_id)?$orderDetail[0]->transaction_id:''); ?></td>
                              <td><?php echo (($bookDetail[0]->b_title)?$bookDetail[0]->b_title:''); ?></td>
                              <td><?php echo (($orderDetail[0]->qty)?$orderDetail[0]->qty:''); ?></td>
                              <td><?php echo (($orderDetail[0]->total_amt)?$orderDetail[0]->total_amt:''); ?></td>
                              <td><?php echo (($orderDetail[0]->payment_status)?$orderDetail[0]->payment_status:''); ?></td>
                              <td><?php echo (($orderDetail[0]->created_at > 0)?date('d-M-Y H:i:s', strtotime($orderDetail[0]->created_at)):''); ?></td>
                              <td><?php echo (($orderDetail[0]->id)?'<span title="View Details" class="btn btn-info" onclick="viewmyauthorOrder('.$orderDetail[0]->id.')"><i class="fa fa-eye" aria-hidden="true"></i></span>':''); ?></td>
                          </tr>
                        <?php    
                          }
                      }
                      ?> 
                      </tbody>
                      <!-- <tfoot>
                          <tr>
                              <th>O ID</th>
                              <th>Otxn Id</th>
                              <th>Book Name</th>
                              <th>O qty</th>
                              <th>Total Amt</th>
                              <th>Payment Status</th>
                              <th>O created</th>
                              <th>Action</th>
                          </tr>
                      </tfoot> -->
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Button to Open the Modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Open modal
</button> -->

<!-- The Modal -->
<div class="modal fade" id="myModalview">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Order Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<style type="text/css">
body {
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 102px;
height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.site-section {
    padding: 0;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

a.site_title:hover{
    color: #fff;
}
.navbar-nav .nav-link {
    padding: 0.3rem 1rem;
}
.fa::after {
  margin-right: 2px;
  content: ;
  content: "";
}
#subscribe_Formtxt_email {
    height: 43px;
}
@media only screen and (max-width: 992px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 167px !important;
        height: 21vh;
    }
} 
</style>