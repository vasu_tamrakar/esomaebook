<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $title; ?></title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php
    if($this->uri->segment(2) == 'bookDetails' && (isset($book->b_id))){
    ?>
    <meta property="og:url"           content="<?php echo site_url('book/bookDetails/'.$book->b_id); ?>" />
    <meta property="og:type"          content="<?php echo 'book'; ?>" />
    <meta property="og:title"         content="<?php echo $book->b_title; ?>" />
    <meta property="og:description"   content="<?php echo $book->c_description; ?>" />
    <meta property="og:image"         content="<?php echo (($book->b_image)?base_url('uploads/books/'.$book->b_image):base_url('uploads/books/book.png'))?>" />
    <?php
    }
    ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/icomoon/style.css'); ?>">
    <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery-ui.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.theme.default.min.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.fancybox.min.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker.css'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/flaticon/font/flaticon.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/aos.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/star-rating-svg.css'); ?>">
    <style>
    .alert.alert-error.alert-dismissible.fade.show {
        color: #ea0d0d;
        background-color: #dd8b8b;
        border-color: #c25b5b;
    }
    .alert strong {
        text-transform: capitalize;
    }
    .text-danger {
        color: red !important;
    }
    </style>
  <script>const SITEURL = '<?php echo site_url();?>';</script>
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  
  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
    
    <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">

          <div class="container">
            <div class="row align-items-center">
              <div class="col-4 col-xl-2">
                  <h1 class="mb-0 site-logo">
                    <a href="<?php echo base_url(); ?>" class="h2 mb-0">
                      <?php if($this->uri->segment(1)){
                        ?><img src="<?php echo base_url('assets/images/white.png'); ?>" class="img-fluid"><?php
                      }else{
                        ?><img src="<?php echo base_url('assets/images/logo.png'); ?>" class="img-fluid"><?php
                      } ?>
                      
                    </a>
                  </h1>
              </div>

            <?php 
              $categories = $this->user_Auth->getData('categories',$w=array('c_status' =>1) ,$se='', $short='c_name asc');
            ?>
          <div class="col-8 col-md-3 d-xl-block">
            <nav class="site-navigation position-relative text-left" role="navigation">
              <ul id="headercatmenu" class="nav navbar-nav ml-auto site-menu dropdown-menu-right">
                
                  <li class="nav-item dropdown">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">CATEGORIES </a>
                      
                      <?php if(!empty($categories)){?>                      
                      <div class="dropdown-menu rightmenu">
                        <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                          <?php 
                            foreach ($categories as $category) { ?>
                              <a href="<?php echo site_url('category/'.$category->c_slug); ?>" class="dropdown-item"><?php echo $category->c_name?> </a>
                          <?php  
                            }
                          ?>
                      </div>
                    <?php } ?>
                  </li>
              </ul>
            </nav>
          </div>
          <div class="col-12 col-md-5  d-xl-block">
            <form name="frontsearch_Form" id="frontsearch_Form" action="<?php echo site_url('search'); ?>" method="get">
              <input type="text" name="search" id="search" placeholder="Search by title/Author/Keyword">
              <button type="submit" class="searchfronttextbtn"><img src="<?php echo base_url('assets/images/search.png'); ?>" class="img-fluid"></button>
            </form>
          </div>
          <div class="col-12 col-md-2  d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul id="headermenu" class="nav navbar-nav ml-auto site-menu">
                  <li class="nav-item dropdown">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">MENU</a>
                      <div class="dropdown-menu dropdown-menu-right rightmenu">
                          <a href="<?php echo base_url(); ?>" class="dropdown-item">Home </a>
                          <a href="<?php echo base_url('aboutUs'); ?>" class="dropdown-item">About </a>
                          <a href="<?php echo base_url('videoTraining'); ?>" class="dropdown-item">Video trainning </a>
                          <a href="<?php echo base_url('membership'); ?>" class="dropdown-item">Membership </a>
                          <a href="<?php echo base_url('somaliAuthors'); ?>" class="dropdown-item">Somali Authors </a>
                          <a href="<?php echo base_url(); ?>" class="dropdown-item">Language </a>
                          
                          <a href="<?php echo base_url('contact'); ?>" class="dropdown-item">Contact </a>
                          <?php 
                          $user_ID = $this->session->userdata('is_logged_in_user');
                          $loginuserData = $this->session->userdata('is_logged_in_user_info');
                          if($this->session->userdata('is_logged_in_user')){
                              if($loginuserData["u_role"] <= 3){
                                  ?>
                                  <a href="<?php echo site_url('dashboard'); ?>" class="dropdown-item">My Account </a>
                                  <a href="<?php echo site_url('dashboard/profile/'.$user_ID); ?>" class="dropdown-item">Profile </a>
                                  <?php
                              }else{ ?>
                                  <a href="<?php echo site_url('user'); ?>" class="dropdown-item">My Account </a>
                                  <a href="<?php echo site_url('user/profile/'.$user_ID); ?>" class="dropdown-item">Profile </a>
                              <?php
                              } ?>
                          
                          <a href="<?php echo site_url('signOut'); ?>" class="dropdown-item">Sign Out </a>
                          <?php }else{
                            ?>
                          <a href="<?php echo base_url('authorSignUp'); ?>" class="dropdown-item">Author Sign Up </a>
                          <a href="<?php echo base_url('membership'); ?>" class="dropdown-item">Sign Up </a>
                          <a href="<?php echo base_url('signIn'); ?>" class="dropdown-item">Sign In </a><?php
                            
                          } ?>
                        

                          <!-- <div class="dropdown-divider"></div>
                          <a href="#"class="dropdown-item">Logout</a> -->
                      </div>
                  </li>
              </ul>
            </nav>
          </div>


          <!-- <div class="col-5 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black float-right"><span class="icon-menu h3"></span></a></div> -->

        </div>
      </div>
    </header>