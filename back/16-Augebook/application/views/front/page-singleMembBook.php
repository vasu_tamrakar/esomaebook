<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">
</div>
    <section class="site-section bg-white" id="contact-section">
      <div class="container">
        <div class="row mb-4">
          <div class="col-md-12 text-center">
            <br/>
            <br/>
            <h2 class="section-title mb-3">Published Book</h2>
            <div class="text-center">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0);">Category</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url('category/'.$book->c_name); ?>"><?php echo $book->c_name; ?></a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0);"><?php echo $book->b_title?></a></li>
              </ol>
            </nav>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-9">
            <div class="card">
                <div class="card-body book-section">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="book-img">
                                <img src="<?php echo base_url('uploads/books/'.$book->b_image); ?>" alt="<?php echo $book->b_title?>" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <h3 style="margin-bottom: 0.3rem;"><strong><?php echo $book->b_title?></strong></h3>
                            
                            <div class="rating">
                                <div class="my-rating-1" data-rating="<?php echo $avgRating;?>" ></div>

                                <span><?php echo $totalRating;?> Ratings</span>
                                <span><?php echo count($bookReviews);?> reviews</span>
                            </div>
                            <div class="description">
                                <?php echo $book->b_description;?>    
                            </div>
                            <br>
                            <div class="details">
                                <h5 style="color: #57C6E6;margin-bottom:0.2rem;"><strong>Book Details</strong></h5>
                                <p>Published: <?php echo date('F ,Y',strtotime($book->b_published))?></p>

                                <?php if($book->b_bookpages){?>
                                <p>pages: <?php echo $book->b_bookpages;?> </p>
                                <?php }?>

                                <?php if($book->b_isbn){?>
                                <p>ISBN: <?php echo $book->b_isbn;?></p>
                                <?php }?>

                                <?php if($book->b_publisher){?>
                                <p>Publisher: <?php echo $book->b_publisher;?></p>
                                <?php }?>


                                <form action="<?php echo site_url('membership'); ?>" method="post">
                                    <!-- <input type="hidden" name="book_id" value="<?php echo $book->b_id;?>">
                                    <input type="hidden" name="user_id" value="<?php echo $login_userid;?>"> -->
                                    <?php if($login_userid == ""){?>
                                    <button type="submit" class="btn btn-primary loadmore">Buy Membership</button>
                                    <?php } ?>
                                </form>
                                <!-- Load Facebook SDK for JavaScript -->
                                <div id="fb-root"></div>
                                  <script>(function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s); js.id = id;
                                    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
                                    fjs.parentNode.insertBefore(js, fjs);
                                  }(document, 'script', 'facebook-jssdk'));</script>
                                  <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0&appId=470017186888654&autoLogAppEvents=1"></script>

                                <!-- Your share button code -->
                                <div style="margin-top:8px;" class="fb-share-button" data-href="<?php echo site_url('book/bookDetails/'.$book->b_id); ?>" data-layout="button_count" data-size="large">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-5"></div>

            <?php 
                if(!empty($bookChapters)){
            ?>
                <div id="accordion" class="accordion">
                    <div class="card mb-0">
                       <?php 
                            $i = 0;
                            foreach ($bookChapters as $chapter) {
                            ?>

                                <div class="card-header <?php echo ($i==0)?"":"collapsed" ?> " data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i;?>">
                                    <a class="card-title">
                                      <?php echo $chapter->title; ?>
                                    </a>
                                </div>
                                <div id="collapse<?php echo $i;?>" class="card-body collapse <?php echo ($i==0)?"show":"" ?>" data-parent="#accordion" >
                                    <?php echo $chapter->description; ?>
                                </div>
                        <?php 
                                $i++;
                            }
                       ?> 
                    </div>
                </div>
            <?php
                }
            ?>
            
            <?php if($login_userid != ""){
                    if(empty($bookReview)){
                ?>
                <br>
                <div class="col-md-12">
                    <h3>Book Review & Rating</h3>
                    <form id="bookReview">
                    <div class="my-rating-4" ></div>
                    <input type="hidden" id="rate" name="rate" >
                    Review: <textarea class="form-control" name="review"></textarea>
                    <input type="hidden" id="" name="book_id" value="<?php echo $book->b_id;?>" >
                    <input type="hidden" id="" name="user_id" value="<?php echo $login_userid;?>" >
                    
                    <input type="submit" class="btn btn-primary loadmore" value="Submit Review">
                    </form>
                </div>
            <?php 
                }else{
                    if($bookReview[0]->approved == '1'){
                    ?>
                        <div class="col-sm-12">
                            <div class="heading">
                                <div class="title">Your Review</div>
                            </div>
                            
                            <div class="body">
                                 <div class="my-rating-1" data-rating="<?php echo $bookReview[0]->rating;?>" ></div>
                                 <br>
                                 <p><?php echo $bookReview[0]->review;?></p>                          
                            </div>
                        </div>
                    <?php 
                    }else{
                        echo '<div class="col-sm-12">
                                <div class="heading">
                                    <div class="title">Your Review</div>
                                </div>
                                
                                <div class="body">
                                     <p>Your Review is not approved yet.<br>
                                    Your review will be displayed once Admin aprroved your review.
                                    </p>                        
                                </div>
                            </div>';
                    }
                 ?>
                    
            <?php
                }

            } ?>

            <?php 
                if(!empty($bookReviews)){
            ?>
                <div id="accordion1" class="accordion">
                    <div class="card mb-0">
                        <div class="card-header collapsed " data-toggle="collapse" data-parent="#accordion1" href="#collapse-reviews">
                            <a class="card-title">
                              All Reviews
                            </a>
                        </div>
                        <div id="collapse-reviews" class="card-body collapse" data-parent="#accordion1" >
                            <div class="container">
                           <?php 
                                foreach ($bookReviews as $review) {
                                ?>
                                    <div class="card reviewCard">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <?php 
                                                        if($review->uc_image != ""){ ?>
                                                            <img src="<?php echo base_url('uploads/users/'.$review->uc_image); ?>" class="img img-rounded img-fluid"/>
                                                                
                                                        <?php }else{ ?>
                                                            <img src="<?php echo base_url('uploads/users/user.png'); ?>" class="img img-rounded img-fluid"/>
                                                        <?php }
                                                    ?>
                                                    
                                                    <p class="text-secondary text-center"><?php echo date('d F, Y', strtotime($review->created_at));?></p>
                                                </div>
                                                <div class="col-md-10">
                                                    
                                                        <a class="float-left" href="javascript:void(0)"><strong><?php echo $review->uc_email;?></strong></a>
                                                        <div class="my-rating-1 float-right" data-rating="<?php echo $review->rating;?>" ></div>

                                                   
                                                   <div class="clearfix"></div>
                                                    <p><?php echo $review->review;?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                           ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                }
            ?>

          </div>
          <div class="col-md-3">
            
            <div class="sidebar">
                <div class="sidebarboxheading">You may also like</div>
                    <?php 
                        if(!empty($booksOfSameCategory)){
                            foreach ($booksOfSameCategory as $catBook) {
                            ?>
                                <a class="sidebarBookLink" href="<?php echo base_url('books/'.$catBook->b_id); ?>">
                                <div class="sidebarbox">
                                    <div class="imgbox">
                                        <img src="<?php echo base_url('uploads/books/'.$catBook->b_image); ?>" class="img-fluid">

                                    </div>
                                    
                                    <div class="desc">
                                        <?php 
                                            $bookReviews = array();
                                            $allAvgRating = array();
                                            $avgRating = 0;
                                            $totalReview = 0;
                                            $bookReviews = $this->book_model->getBookReviews($catBook->b_id);
                                            if($bookReviews){
                                                $totalReview = count($bookReviews);
                                            }
                                            $allAvgRating = $this->book_model->getAvgRating($catBook->b_id);
                                            $avgRating = $allAvgRating[0]->rating;
                                        ?>
                                        <h6><strong><?php echo $catBook->b_title;?></strong></h6>
                                        <div class="rating">

                                            <div class="my-rating-small" data-rating="<?php echo $avgRating;?>" ></div>
                                        </div>
                                        <p><?php echo $totalReview;?> Reviews</p>
                                        <p><?php echo date('F ,Y',strtotime($catBook->b_published));?></p>
                                    </div>
                                </div>
                                </a>
                            <?php
                            }

                        }else{
                            echo "<h3>No Books of this Category.</h3>";
                        }
                    ?>
                <div class="likebookMore">
                    <a href="<?php echo base_url('category/'.$book->c_slug); ?>"><button type="button">see More</button></a>
                </div>
            </div>
          </div>

        </div>

      </div>
    </section>
<style type="text/css">
body{
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 83px;
    height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
div.plans button:hover div.plans{
    box-shadow: 1px 2px 8px #54bfe3 !important;
}
.plans {
    background: #fff;
    padding: 0px 0px 32px 0;
    border-top-left-radius: 60px;
    width: 86%;
    margin: 15px auto;
    box-shadow: 1px 2px 8px black;
    background-image: url(./assets/images/membership_plans_forma_tag.png);
    background-repeat: no-repeat;
    background-position-y: bottom;
}

.plans h1 {
    font-weight: bold;
    border-bottom: 1px solid gray;
    text-align: center;
    margin: 25px;
}
.plans h6 {
    font-size: 20px;
    text-align: center;
    font-family: monospace;
    font-weight: 600;
}
.plans p {
    text-align: center;
    text-decoration: underline;
    letter-spacing: 1px;
    padding: 13px;
}
.plans button {/*panel-collapse in collapse show*/
    background-color: #656161;
    margin: 0 auto;
    border: 1px solid gray;
    color: #fff;
    font-weight: bold;
    text-transform: capitalize;
    padding: 7px 34px;
    cursor: pointer;
}
.plans button:hover{
    background-color: #54bfe3;
    color: #fff;
}

.site-section {
    padding: 0em 0 4em 0;
}

.rating span {
    display: inline-block;
    margin: 0px 9px;
    font-size: 15px;
    cursor: pointer;
}
.description {
    margin: 4px 0;
    font-size: 15px;
    font-family: Lato,sans-serif;
}
.description p {
    margin-bottom: 0px;
}
.details p {
    margin-bottom: 2px;
    font-size: 14px;
}
label {
    display: inline-block;
    margin-bottom: 0rem;
}

.sidebar {
    background: #a9dcdb2e;
    padding: 0 0 2px 0px;
}
.sidebarboxheading {
    background: #fff;
    box-shadow: 0px 0px 6px #00bcd459;
    margin-bottom: 1.3rem;
    padding: 6px;
}
.AuthorbookMore, .likebookMore {
    margin: 0 0 12px 0px;
    text-align: center;
}

.AuthorbookMore button, .likebookMore button{
    background: transparent;
    border-bottom: 2px solid #00BCD4;
    color: #00BCD4;
    border-top: 0;
    border-left: 0;
    border-right: 0;
    text-transform: capitalize;
    font-size: 14px;
    cursor: pointer;
    padding: 0px 0px;
}
.sidebarbox {
    margin: 5px;
    padding: 8px;
    clear: both;
}
.imgbox {
    display: block;
    float: left;
    width: 30%;
}
.desc {
    display: inline-block;
    /* margin: 0px 11px; */
    width: 63%;
    margin-left: 11px;
}
.desc p {
    margin: 3px 0px;
    font-size: 15px;
}
.desc h6 {
   /* margin-bottom: 2px;
    margin-top: 2px;*/
}
.sidebarbox:hover {
    box-shadow: 0 0 6px #7ff1f1c9;    
    /*border-bottom: 1px solid #80808059;
    margin: 28px 0px;*/
    background: #fff;
    cursor: pointer;
}
    
.breadcrumb {
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-inline-box;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    padding: 0.4rem 1rem;
    margin: 0 auto;
    list-style: none;
    background-color: #e9ecef0a;
    border-radius: 0.25rem;
}
.breadcrumb-item+.breadcrumb-item {
    padding-left: 0.3rem;
}
.breadcrumb-item a {
    color: #54bfe3;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #6c757d;
    content: ".";
}
.accordion .card-header.collapsed:after {
    content: "\f0d7";
}
.accordion .card-header{
    cursor: pointer;
}
.accordion .card-header:after {
    font-family: 'FontAwesome';
    content: "\f0d8";
    margin: 0px 0px 0px 7px;
    font-size: 24px;
    
}
.card {
    border: 0;
}
.card-header {
    background-color: #fff;
    font-size: 19px;
    font-weight: bold;
    color: #00BCD4;
}
.card-header.collapsed {
    color: gray;
}
.title {
    color: #55C0E1;
    font-weight: bold;
    font-size: 26px;
    background: 2px;
    font-family: Lato,sans-serif;
}
.subtitle {
    font-size: 15px;
    font-family: Lato,sans-serif;
}
.body p{
    font-size: 14px;
    font-family: Lato,sans-serif;
}
.socialicons{
    color: gray;
    font-size: 15px;
    font-family: Lato,sans-serif;
}
.socialicons a {
    color: gray;
    font-size: 17px;
    margin-left: 12px;
}
.socialicons a:hover {
    color: #00BCD4;
}
.sidebarBookLink:link,.sidebarBookLink:hover,.sidebarBookLink:visited,.sidebarBookLink:active{
    color: gray;
}
.loadmore {
    background: #54bfe3 !important;
    border-radius: 1px !important;
    color: #fff !important;
    font-weight: bold;
    font-size: 15px;
    margin-top: 20px;
    border: 1px solid #54bfe3 !important;
    padding: 12px 20px;
    display: block;
}

.reviewCard{
    box-shadow:0 0 6px #7ff1f1c9;
}
.img-rounded{
    border-radius: 50%;
}
.book-section .book-price{
    margin-top:10px;
    /*text-align: center;*/
    font-size: 24px !important;
}


@media only screen and (max-width: 992px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 168px !important;
        height: 22vh;
    }
    .breadcrumb {
        display: flex;
    }
}
</style>
  