<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/contact_bg.png'); ?>);">
    <div class="container">
        <div class="row">
        <div class="bannerheading"><h1>Sign In</h1></div>
        </div>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active">Sign In</li>
          </ol>
        </nav>

    </div>
    
</div>


<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="col-md-12">
            <?php 
            $alert = $this->session->flashdata('alert');
            if($alert){
                ?>
                <div class="alert alert-<?php echo $alert; ?> alert-dismissible fade show" role="alert">
                  <strong><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <?php
            }
            ?>
            <div class="heading"><h1>Sing In</h1></div>
            <div class="row">
                <div class="col-md-8">
                    <form name="signin_Form" id="signin_Form" action="<?php echo site_url('signIn'); ?>" method="post">
                        <div class="form-group">
                            <label for="email">Email & Mobile</label>
                            <input type="text" name="txt_email" id="txt_email" value="<?php echo set_value('txt_email'); ?>" class="form-control" placeholder="Enter Email Or Mobile" tabindex="-1" autofocus>
                            <?php echo form_error('txt_email','<small class="form-text text-danger">','</small>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="pass">Password</label>
                            <input type="password" name="txt_password" id="txt_password" class="form-control" placeholder="******">
                            <?php echo form_error('txt_password','<small class="form-text text-danger">','</small>'); ?>
                        </div>
                      <button type="submit" class="btn btn-primary loadmore">Sign In</button>
                      <a href="<?php echo site_url('forgotPassword'); ?>" class="btn btn-primary loadmore">Forgot Password</a>
                      <a href="<?php echo site_url('membership'); ?>" class="btn btn-primary loadmore">Sign Up</a>
                      
                    </form>
                </div>

                <div class="col-md-4">
                    <div class="address">
                        <p><b>Phone</b></p><p><label> +249 90 255 2006</label></p>
                        <div class="border-top pt-4"></div>
                        <p><b>Email</b></p><p><label> Info@esomabooks.com</label></p>
                        <div class="border-top pt-4"></div>
                        <p><b>Info</b></p><p><label> Info@esomabooks.com</label></p>
                        <div class="border-top pt-4"></div>
                        <p><b>Follow us</b></p>
                        
                        <div class="socialicon">
                            <a href="" target="_blank" class="facebook"></a>
                            <a href="" target="_blank" class="twitter"></a>
                            <a href="" target="_blank" class="instagram"></a>
                            <a href="" target="_blank" class="linkedin"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 328px;
    height: calc(40vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar{
    background-color: #54bfe3;
}
.sticky-wrapper .is-sticky.nav-link.dropdown-toggle {
    color: #fff !important;
}
/*.sticky-wrapper.is-sticky .site-navbar ul li a {
    color: #505048c4 !important;
}*/

header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}

/*#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}*/


.bannerheading {
    margin: auto;
    display: block;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    font-size: 55px;
    font-family: unset;
}
.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}
form#contact_form input {
    font-size: 14px;
}
form#contact_form {
    font-size: 14px;
    margin: 22px 0px;
}
.loadmore {
    background: #54bfe3 !important;
    border-radius: 1px !important;
    color: #fff !important;
    font-weight: bold;
    font-size: 15px;
    margin-top: 20px;
    border: 1px solid #54bfe3 !important;
    padding: 12px 80px;
}
.address {
    background: #00BCD4;
    padding: 50px;
}
.address p {
    text-align: center;
    color: #fff;
}
.address label {
    text-align: center !important;
    margin: 0 auto !important;
    color: #fff;
    font-size: large;
}
.socialicon{
  text-align: center;
}
.socialicon a {
    padding: 10px;
    background-repeat: no-repeat;
    background-size: cover;
    display: inline-block;
    width: 30px;
    height: 30px;
    margin: 4px;
}
.socialicon a:hover {
    opacity: 0.6;
}
.form-control:active, .form-control:focus {

    border-color: #00BCD4;

}
a.facebook {
    background: url(./assets/images/follow_us_fb.png);
}
a.twitter {
    background: url(./assets/images/follow_us_tw.png);
}
a.google {
    background: url(./assets/images/follow_us_g.png);
}
a.instagram {
    background: url(./assets/images/follow_us_insta.png);
}
a.linkedin {
    background: url(./assets/images/follow_us_link.png);
}

/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {

    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 130px;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
}
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {

} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

}
</style>
  