<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2">
            <?php include 'frontleftmenu.php'; ?>
        </div>
        <div class="col-md-10 col-lg-10">
            <div class="row">
                <div class="col-md-12">
                <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }/*
                  ?>
                  <div class="card">
                      <div class="card-header bg-info"><span style="color: #fff;">Edit author form</span></div>
                      <div class="card-body">
                        <form name="editauthor_Form" id="editauthor_Form" action="" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_authorname">Author Name:</label>
                                        <input type="text" name="txt_authorname" id="txt_authorname" value="<?php echo (($authorAdded["u_id"])?$authorAdded["u_id"]:""); ?>" class="form-control" placeholder="Enter Author Name" max-length="150">
                                        <?php echo form_error('txt_authorname', '<small class="error">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_published">Published Year:</label>
                                        <input type="text" name="txt_published" id="txt_published" value="<?php echo (($authorAdded[0]->a_year)?$authorAdded[0]->a_year:""); ?>" class="form-control" placeholder="Enter year" max-length="4">
                                        <?php echo form_error('txt_published', '<small class="error">', '</small>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_description">Description :</label>
                                        <textarea name="txt_description" id="txt_description" class="form-control" placeholder="Enter Descriptions..." rows="5"><?php echo (($authorAdded[0]->a_description)?$authorAdded[0]->a_description:""); ?></textarea>
                                        <?php echo form_error('txt_description', '<small class="error">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_image">Profile Image :</label>
                                        <input type="file" name="txt_image" id="txt_image" class="form-control-file" accept="image/*">
                                        <small class="text-info">Image format accept only gif,jpg and png.</small>
                                        <div style="display: block; width: 74px; border: 1px solid darkturquoise; box-shadow: 3px 0px 9px #17a2b8;">
                                            <img src="<?php echo (($authorAdded["u_id"])?base_url('uploads/authors/'.$authorAdded["u_id"]):base_url('uploads/authors/author.png'))?>" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_facebook">Facebook Link :</label>
                                        <input type="text" name="txt_facebook" id="txt_facebook" class="form-control" placeholder="https://www.faceboook.com/zzxsdsd..." value="<?php echo (($authorAdded[0]->a_facebook)?$authorAdded[0]->a_facebook:""); ?>" max-lenght="200">
                                        <?php echo form_error('txt_facebook', '<small class="error">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_twitter">Twitter Link :</label>
                                        <input type="text" name="txt_twitter" id="txt_twitter" class="form-control" placeholder="https://www.twitter.in/zzxsdsd..." value="<?php echo (($authorAdded[0]->a_twitter)?$authorAdded[0]->a_twitter:""); ?>" max-lenght="200">
                                        <?php echo form_error('txt_twitter', '<small class="error">', '</small>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_instagram">Instagram Link :</label>
                                        <input type="text" name="txt_instagram" id="txt_instagram" class="form-control" placeholder="https://www.instagram.com/zzxsdsd..." value="<?php echo (($authorAdded[0]->a_instagram)?$authorAdded[0]->a_instagram:""); ?>" max-lenght="200">
                                        <?php echo form_error('txt_instagram', '<small class="error">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_gplush">Google Plus Link :</label>
                                        <input type="text" name="txt_gplush" id="txt_gplush" class="form-control" placeholder="https://www.gplush.in/zzxsdsd..." value="<?php echo (($authorAdded[0]->a_gplush)?$authorAdded[0]->a_gplush:""); ?>" max-lenght="200">
                                        <?php echo form_error('txt_gplush', '<small class="error">', '</small>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_status">Status :</label>
                                        <label for="txt_status1">
                                          <input type="radio" name="txt_status" id="txt_status1" value="1" <?php echo (($authorAdded["u_status"] =='1')?'checked':''); ?>>TRUE
                                        </label>
                                        <label for="txt_status0">
                                          <input type="radio" name="txt_status" id="txt_status0" value="0" <?php echo (($authorAdded["u_status"] == '0')?'checked':''); ?>>FALSE
                                        </label>
                                        <?php echo form_error('txt_status', '<small class="error">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control"> Created Date : 
                                            <small class="text-info"><?php echo (($authorAdded["u_created"] > 0)?date('d-M-Y H:i:s',strtotime($authorAdded["u_created"])):''); ?>
                                            </small>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control"> Last Modified Date : 
                                            <small class="text-info"><?php echo (($authorAdded["u_modified"] > 0)?date('d-M-Y H:i:s',strtotime($authorAdded["u_modified"])):''); ?>
                                            </small>
                                        </label>
                                    </div>
                                </div>
                            </div>
                              <button type="submit" class="btn btn-info">Update</button>
                        </form>
                      </div>
                  </div>
                  <?php */ ?>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
body {
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 102px;
height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.site-section {
    padding: 0;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
a.site_title:hover{
    color: #fff;
}
.navbar-nav .nav-link {
    padding: 0.3rem 1rem;
}
.fa::after {
  margin-right: 2px;
  content: ;
  content: "";
}
@media only screen and (max-width: 992px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 167px !important;
        height: 21vh;
    }
} 


</style>
  