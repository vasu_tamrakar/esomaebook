<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section"></div>
<section class="site-section bg-white" id="contact-section">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2">
            <?php include 'frontleftmenu.php'; ?>
        </div>
        <div class="col-md-10 col-lg-10">
            <div class="row">
                <div class="col-md-12">
                  <div class="row mt-5">
                      <div class="col-12 text-center">
                          <h2 class="section-title mb-3" style="color:#54BFE3;">Change Password</h2>
                      </div>
                  </div>
                <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                    <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                      <?php
                  }
                  ?>

                    <div class="row justify-content-md-center">
                        <div class="col-md-9">
                            <div class="card" style="box-shadow: 0 0 3px 0px black;">
                                <div class="card-body">

                                    <form name="changePassword_Form" id="changePassword_Form" method="post">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_newpass">New Password</label>
                                                    <input type="password" name="txt_newpass" id="txt_newpass" class="form-control" placeholder="New password" max-length="25">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_cnfnewpass">Confirm New Password</label>
                                                    <input type="password" name="txt_cnfnewpass" id="txt_cnfnewpass" class="form-control" placeholder="Confirm New password" max-length="25">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-md-center">
                                          <button type="submit" name="submit" class="btn btn-info">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<style type="text/css">
body {
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 102px;
height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.site-section {
    padding: 0;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

a.site_title:hover{
    color: #fff;
}
.navbar-nav .nav-link {
    padding: 0.3rem 1rem;
}
.fa::after {
  margin-right: 2px;
  content: ;
  content: "";
}
@media only screen and (max-width: 992px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 167px !important;
        height: 21vh;
    }
} 
</style>