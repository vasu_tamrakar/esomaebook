<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style type="text/css">
.card-title {
display: inline;
font-weight: bold;
}
.display-table {
    display: table;
}
.display-tr {
    display: table-row;
}
.display-td {
    display: table-cell;
    vertical-align: middle;
    width: 61%;
}
</style>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" /> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<link rel="stylesheet" href="<?php echo base_url('assets/build/css/intlTelInput.css'); ?>">
<!-- <link rel="stylesheet" href="<?php echo base_url('assets/build/css/demo.css'); ?>"> -->
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/contact_bg.png'); ?>);">
    <div class="container">
        <div class="row">
            <div class="bannerheading"><h1>Sign Up</h1></div>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active">Sign Up</li>
            </ol>
        </nav>
    </div>
</div>


<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="col-md-12">
            <?php 
            $alert = $this->session->flashdata('alert');
            if($alert){
            ?>
            <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                <strong><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php
            }
            ?>
            <div class="heading"><h1>Customer Information</h1></div>
            <div class="row pt-5">
                <div class="col-md-8">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="card">
                                    
                                <div class="card-body">
                                    <div class="heading"><h4>Customer Information</h4></div>
                                    <?php $planDetails = $this->user_Auth->getData('membershipplan',array("mp_id" => $subscriberData[0]->es_planid));
                                    ?>
                                    <div class="row justify-content-md-center pt-4">
                                        <label class="label-control col-sm-10 col-md-10 col-xl-10">Email Address</label>
                                          <input type="text" id="email" name="email" value="<?php echo $subscriberData[0]->es_email;?>" class="form-control col-sm-10 col-md-10 col-xl-10" readonly>
                                    </div>

                                    <div class="heading pt-4"><h4>Billing Information</h4></div>
                                    <?php /*
                                    <label class="label-control">Choose Plan: <strong><?php echo (isset($planDetails[0]->mp_name)?$planDetails[0]->mp_name:''); ?></strong></label></br>
                                    <!-- <label>Plan price: <strong><span>&#36;</span><?php echo (isset($planDetails[0]->mp_price)?$planDetails[0]->mp_price:''); ?></strong></label></br> -->
                                    <label>Sub Total: <strong> <?php echo (isset($planDetails[0]->mp_price)?$planDetails[0]->mp_price:''); ?></strong></label></br>

                                    <label> Total: <strong><?php echo (isset($planDetails[0]->mp_price)?$planDetails[0]->mp_price:''); ?></strong></label></br>
                                    <label>Plan Descriptions: <?php echo (isset($planDetails[0]->mp_descriptions)?$planDetails[0]->mp_descriptions:''); ?></label></br> */ ?>
                                    <label class="text-info pt-5">Payment Method</label>
                                    
                                    <!-- Nav tabs -->
                                    <ul id="paytab" class="nav nav-tabs nav-pills nav-justified">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#cc"><i class="fa fa-credit-card" aria-hidden="true"></i>  Credit Card</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#evc"><i class="fa fa-etsy" aria-hidden="true"></i>  EVC</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#mpesa"><i class="fa fa-money" aria-hidden="true"></i>  Mpesa</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content pt-4">
                                        <div class="tab-pane container active" id="cc">
                                            <!-- Start tab panel stripe  -->
                                            <form role="form" name="payment_form" action="<?php echo site_url('signUp'); ?>" method="post" class="require-validation"

                                                     data-cc-on-file="false"

                                                    data-stripe-publishable-key="<?php echo $this->config->item('stripe_key') ?>"

                                                    id="payment_form">
                                                    
                                                <input type="hidden" name="txt_firstname" id="txt_firstname" value="<?php echo $subscriberData[0]->es_firstname; ?>" class="form-control" placeholder="Enter First Name" maxl-enght="150" readonly="true">
                                                
                                                <input type="hidden" name="txt_lastname" id="txt_lastname" value="<?php echo $subscriberData[0]->es_lastname; ?>" class="form-control" placeholder="Enter Last Name" maxl-enght="150" readonly="true">
                                                          
                                                    
                                                <?php /*/*<div class="form-row">
                                                    <div class="form-group col-md-6">
                                                          <label for="txt_password">Password <span>*</span></label>
                                                          <input type="password" name="txt_password" id="txt_password" value="<?php echo set_value('txt_password'); ?>" class="form-control" placeholder="******" maxl-enght="50">
                                                          <?php echo form_error('txt_password','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                          <label for="txt_cnfpassword">Confirm Password <span>*</span></label>
                                                          <input type="password" name="txt_cnfpassword" id="txt_cnfpassword" value="<?php echo set_value('txt_cnfpassword'); ?>" class="form-control" placeholder="******" maxl-enght="50">
                                                          <?php echo form_error('txt_cnfpassword','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                </div>*/ ?>
                                                    
                                                <input type="hidden" id="txt_paymethod" name="txt_paymethod" value="cc">
                                                <div class="carda card-default credit-card-box">
                                                    <?php /*<div class="card-header display-table" >
                                                        <div class="row display-tr" >
                                                            <h3 class="card-title display-td" >Payment Details</h3>
                                                            <div class="display-td" >                            
                                                                <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                                                            </div>
                                                        </div>                    
                                                    </div>*/ ?>
                                                    <div class="card-bodya">
                                                        <div class='form-row row'>
                                                            <div class='col-md-12 form-group required'>

                                                                <label class='control-label'>Cardholder Name <span>*</span></label> <input

                                                                    class='form-control' size='4' type='text' name='txt_holdername' maxlength="180" value="<?php echo (($subscriberData[0]->es_firstname)?$subscriberData[0]->es_firstname:'').' '.(($subscriberData[0]->es_lastname)?$subscriberData[0]->es_lastname:''); ?>">
                                                            </div>
                                                        </div>
                                                        <div class='form-row row'>
                                                            <div class='col-md-12 form-group card required'>
                                                                <label class='control-label'>Card Number <span>*</span>
                                                                  <div style="display: block;float: right;height: 33px;width: 50%;"><img src="<?php echo base_url('assets/images/allcard.png'); ?>" class="img-fluid"></div>
                                                                    </label> <input

                                                                        autocomplete='off' class='form-control card-number' size='20'

                                                                        type='text' name='txt_cardnumber' maxlength="20">
                                                            </div>
                                                        </div>
                                                        <div class='form-row row'>
                                                            <div class='col-sm-12 col-md-4 form-group cvc required'>
                                                                <label class='control-label'>CVV <span>*</span></label> <input autocomplete='off'

                                                                    class='form-control card-cvc' placeholder='ex. 311' size='4'

                                                                    type='text' name='txt_cvv' maxlength="4">
                                                            </div>
                                                            <div class='col-sm-12 col-md-4 form-group expiration required'>
                                                                <label class='control-label'>Expiration Month <span>*</span></label> <select

                                                                    class='form-control card-expiry-month' placeholder='Month'

                                                                    name='txt_expmonth'>
                                                                    <option value="">Month</option>
                                                                    <option value="01">01</option>
                                                                    <option value="02">02</option>
                                                                    <option value="03">03</option>
                                                                    <option value="04">04</option>
                                                                    <option value="05">05</option>
                                                                    <option value="06">06</option>
                                                                    <option value="07">07</option>
                                                                    <option value="08">08</option>
                                                                    <option value="09">09</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                </select>
                                                            </div>
                                                            <div class='col-sm-12 col-md-4 form-group expiration required'>
                                                                <label class='control-label'>Expiration Year <span>*</span></label> <select

                                                                    class='form-control card-expiry-year'

                                                                    name='txt_expyear'>
                                                                    <option value="">Year</option>
                                                                    <script>
                                                                    
                                                                    var d= new Date(); 
                                                                    var x= d.getFullYear();
                                                                    var y= x+50
                                                                        for(;x < y ; x++){
                                                                        document.write('<option value="'+x+'">'+x+'</option>');
                                                                        }
                                                                    </script>
                                                                    </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-6">
                                                              <label for="txt_email">Email <span>*</span></label><br>
                                                              <input type="text" name="txt_email" id="txt_email" value="<?php echo $subscriberData[0]->es_email; ?>" class="form-control" placeholder="Enter Email Address" maxl-enght="200" >
                                                              <?php echo form_error('txt_email','<small class="form-text text-danger">','</small>'); ?>
                                                            </div>
                                                             <div class="form-group col-md-6">
                                                                  <label for="txt_password">Password <span>*</span></label>
                                                                  <input type="password" name="txt_password" id="txt_password" value="<?php echo set_value('txt_password'); ?>" class="form-control" placeholder="******" maxl-enght="50">
                                                                  <?php echo form_error('txt_password','<small class="form-text text-danger">','</small>'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-12">
                                                                  <label for="select_book">Select Somali book <span>*</span></label>
                                                                  <?php 
                                                                  $somalibookS = $this->user_Auth->getData('books', array('b_fk_of_aid' => 0, 'b_category' => '5', 'b_status' => '1'), $se='b_id,b_title', $sh='b_id DESC');
                                                                  if($somalibookS) {?>
                                                                  <select name="select_book" id="select_book" class="form-control">
                                                                      <option value=''>Select the Book</option>
                                                                      <?php 
                                                                      foreach($somalibookS as $somalibook){
                                                                      ?>
                                                                      <option value='<?php echo $somalibook->b_id;  ?>'><?php echo $somalibook->b_title;  ?></option>
                                                                      <?php } ?>
                                                                  <?php echo form_error('select_book','<small class="form-text text-danger">','</small>'); ?>
                                                                </select>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        if(isset($subscriberData["es_planid"])){
                                                            $planDetails = $this->user_Auth->getData('membershipplan', array("mp_id" => $subscriberData["es_planid"]));
                                                            // $planDetails[0]->mp_name;
                                                            // $planDetails[0]->mp_validity;
                                                        }
                                                        ?>
                                                        <div class="row justify-content-md-center">
                                                            <div class="form-group col-md-12">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="txt_agree" name="txt_agree" value="true">
                                                                    <label class="custom-control-label" for="txt_agree">I agree to the payment terms as stated above.</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class='form-row row pt-4'>
                                                            <div class='col-md-12 error form-group hide'>
                                                                <div class='alert-danger alert'>Please correct the errors and try again.<a href="#" class="close" data-dismiss="alert">&times;</a></div>
                                                            </div>
                                                        </div> -->
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button class="btn btn-info btn-lg btn-block" type="submit" name="titlesubmit" value="stripe">Pay Now ($ <?php echo $planDetails[0]->mp_price; ?>)</button>
                                                            </div>
                                                        </div>                                                                
                                                            <input type="hidden" name="txt_userID" id="txt_userID" value="<?php echo $subscriberData[0]->es_id; ?>">
                                                            <input type="hidden" name="txt_planID" id="txt_planID" value="<?php echo $subscriberData[0]->es_planid; ?>">
                                                    </div>
                                                </div>
                                            <!-- End tab panel stripe -->
                                            </form>
                                        </div>


                                        <div class="tab-pane container fade" id="evc">

                                            <!-- Start tab panel EVC -->
                                            <form role="form" name="payment_formEVC" action="<?php echo site_url('signUp'); ?>" method="post" class="require-validation" id="payment_formEVC">
                                                <input type="hidden" name="txt_planid" id="txt_planid2" value="<?php echo $subscriberData[0]->es_planid; ?>">
                                                <input type="hidden" id="txt_paymethod2" name="txt_paymethod" value="evc">
                                                <div class="form-row">
                                                    <?php $planLists = $this->user_Auth->getData('membershipplan',array('mp_status' => '1')); if($planDetails){ ?>
                                                    <div class="form-group col-md-6">
                                                        <label for="txt_planidform">Plan<span> *</span></label>

                                                        <select name="txt_planidform" id="txt_planidform" class="form-control" onchange="changeCheckPlan()">
                                                            <option value=""> -Select Option- </option>
                                                            <?php 
                                                            foreach ($planLists as $planL) {
                                                            ?>
                                                            <option value="<?php echo $planL->mp_id; ?>" <?php if($planL->mp_id == $subscriberData[0]->es_planid){ echo 'selected'; } ?>><?php echo $planL->mp_id.' '.$planL->mp_validity.' '.$planL->mp_price; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                        <?php echo form_error('txt_planidform','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                    <?php } ?>
                                                    <div class="form-group col-md-6">
                                                        <label for="txt_mobile2">Mobile</label><br>
                                                        <input type="text" name="txt_mobile" id="txt_mobile2" value="<?php echo set_value('txt_mobile'); ?>" class="form-control" placeholder="+91xxxxxxxxxx" max-length="15">
                                                        <?php echo form_error('txt_mobile','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="txt_firstname">First Name<span> *</span></label><br>
                                                        <input type="text" name="txt_firstname" id="txt_firstname1" value="<?php echo $subscriberData[0]->es_firstname; ?>" class="form-control" placeholder="Enter First Name" maxl-enght="150">
                                                        <?php echo form_error('txt_firstname','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="txt_lastname">Last Name<span> *</span></label><br>
                                                        <input type="text" name="txt_lastname" id="txt_lastname2" value="<?php echo $subscriberData[0]->es_lastname; ?>" class="form-control" placeholder="Enter Last Name" maxl-enght="150">
                                                        <?php echo form_error('txt_lastname','<small class="form-text text-danger">','</small>'); ?>
                                                      </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                
                                                        <label for="txt_email2">Email<span> *</span></label><br>
                                                        <input type="text" name="txt_email" id="txt_email2" value="<?php echo $subscriberData[0]->es_email; ?>" class="form-control" placeholder="Enter Email Address" maxl-enght="200">
                                                        <?php echo form_error('txt_email','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="txt_password2">Create Password<span> *</span></label>
                                                        <input type="password" name="txt_password" id="txt_password2" value="<?php echo set_value('txt_password'); ?>" class="form-control" placeholder="******" maxl-enght="50">
                                                        <?php echo form_error('txt_password','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                          <label for="select_book2">Select Somali book <span>*</span></label>
                                                          <?php 
                                                          $somalibookS = $this->user_Auth->getData('books', array('b_fk_of_aid' => 0, 'b_category' => '5', 'b_status' => '1'), $se='b_id,b_title', $sh='b_id DESC');
                                                          if($somalibookS) {?>
                                                          <select name="select_book" id="select_book2" class="form-control">
                                                              <option value=''>Select the Book</option>
                                                              <?php 
                                                              foreach($somalibookS as $somalibook){
                                                              ?>
                                                              <option value='<?php echo $somalibook->b_id;  ?>'><?php echo $somalibook->b_title;  ?></option>
                                                              <?php } ?>
                                                          <?php echo form_error('select_book','<small class="form-text text-danger">','</small>'); ?>
                                                        </select>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="txt_address2">Address</label>
                                                        <textarea name="txt_address" id="txt_address2" class="form-control" placeholder="Address..." max-lenght="250"></textarea>
                                                        <?php echo form_error('txt_address','<small class="form-text text-danger">','</small>'); ?>

                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-12 text-center">
                                                        OUR CUSTOMER SERVICES IN SOMALIA HAFSA HASSAN +252-61-3148376
                                                    </div>
                                                </div>
                                                <div class="row justify-content-md-center">
                                                    <div class="form-group col-md-12">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="txt_agree2" name="txt_agree2" value="true">
                                                            <label class="custom-control-label" for="txt_agree2">I agree to the payment terms as stated above.</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button class="btn btn-info btn-lg btn-block" type="submit">Submit Now</button>
                                            </form>
                                            <!-- End tab panel EVC -->
                                        </div>


                                        <div class="tab-pane container fade" id="mpesa">

                                            <!-- Start tab panel Mpesa -->
                                            <form role="form" name="payment_formMPESA" action="<?php echo site_url('signUp'); ?>" method="post" class="require-validation" id="payment_formMPESA">
                                                <input type="hidden" name="txt_planid" id="txt_planid3" value="<?php echo $subscriberData[0]->es_planid; ?>">
                                                <input type="hidden" id="txt_paymethod3" name="txt_paymethod" value="mpesa">
                                                <div class="form-row">
                                                
                                                    <?php $planLists = $this->user_Auth->getData('membershipplan',array('mp_status' => '1')); if($planDetails){ ?>
                                                    <div class="form-group col-md-6">
                                                        <label for="txt_planidform3">Plan<span> *</span></label>

                                                        <select name="txt_planidform3" id="txt_planidform3" class="form-control" onchange="changeCheckPlan()">
                                                            <option value=""> -Select Option- </option>
                                                            <?php 
                                                            foreach ($planLists as $planL) {
                                                            ?>
                                                            <option value="<?php echo $planL->mp_id; ?>" <?php if($planL->mp_id == $subscriberData[0]->es_planid){ echo 'selected'; } ?>><?php echo $planL->mp_id.' '.$planL->mp_validity.' '.$planL->mp_price; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                        <?php echo form_error('txt_planidform3','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                    <?php } ?>
                                                
                                                    <div class="form-group col-md-6">
                                                        <label for="txt_mobile3">Mobile</label><br>
                                                        <input type="text" name="txt_mobile" id="txt_mobile3" value="<?php echo set_value('txt_mobile'); ?>" class="form-control" placeholder="+91xxxxxxxxxx" max-length="15">
                                                        <?php echo form_error('txt_mobile','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                      <label for="txt_firstname3">First Name</label>
                                                      <input type="text" name="txt_firstname" id="txt_firstname3" value="<?php echo $subscriberData[0]->es_firstname; ?>" class="form-control" placeholder="Enter First Name" maxl-enght="150">
                                                      <?php echo form_error('txt_firstname','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                      <label for="txt_lastname3">Last Name</label>
                                                      <input type="text" name="txt_lastname" id="txt_lastname3" value="<?php echo $subscriberData[0]->es_lastname; ?>" class="form-control" placeholder="Enter Last Name" maxl-enght="150">
                                                      <?php echo form_error('txt_lastname','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                
                                                        <label for="txt_email3">Email</label><br>
                                                        <input type="text" name="txt_email" id="txt_email3" value="<?php echo $subscriberData[0]->es_email; ?>" class="form-control" placeholder="Enter Email Address" maxl-enght="200">
                                                        <?php echo form_error('txt_email','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="txt_password3">Password</label>
                                                        <input type="password" name="txt_password" id="txt_password3" value="<?php echo set_value('txt_password'); ?>" class="form-control" placeholder="******" maxl-enght="50">
                                                        <?php echo form_error('txt_password','<small class="form-text text-danger">','</small>'); ?>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                          <label for="select_book3">Select Somali book <span>*</span></label>
                                                          <?php 
                                                          $somalibookS = $this->user_Auth->getData('books', array('b_fk_of_aid'  => 0, 'b_category' => '5', 'b_status' => '1'), $se='b_id,b_title', $sh='b_id DESC');
                                                          if($somalibookS) {?>
                                                          <select name="select_book" id="select_book3" class="form-control">
                                                              <option value=''>Select the Book</option>
                                                              <?php 
                                                              foreach($somalibookS as $somalibook){
                                                              ?>
                                                              <option value='<?php echo $somalibook->b_id;  ?>'><?php echo $somalibook->b_title;  ?></option>
                                                              <?php } ?>
                                                          <?php echo form_error('select_book','<small class="form-text text-danger">','</small>'); ?>
                                                        </select>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="txt_address3">Address</label>
                                                        <textarea name="txt_address" id="txt_address3" class="form-control" placeholder="Address..." max-lenght="250"></textarea>
                                                        <?php echo form_error('txt_address','<small class="form-text text-danger">','</small>'); ?>

                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12 text-center">
                                                        Dear Valued Customer please send the money through Mpesa and call us to confirm. (Mpesa Number) 0725-893-222 We are still working on to the our system automatic.
                                                    </div>
                                                </div>
                                                <div class="row justify-content-md-center">
                                                    <div class="form-group col-md-12">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="txt_agree3" name="txt_agree3" value="true">
                                                            <label class="custom-control-label" for="txt_agree3">I agree to the payment terms as stated above.</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button class="btn btn-info btn-lg btn-block" type="submit">Submit Now</button>
                                            </form>
                                            <!-- End tab panel Mpesa -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            

                <div class="col-md-4">
                    <div class="plandDtailsDiv">
                        <?php
                        if($planLists){ 
                            foreach ($planLists as $value) {
                            ?>
                            <div class="pdetails pdetails_<?php echo $value->mp_id; ?>" style="display:<?php if($value->mp_id == $subscriberData[0]->es_planid){ echo 'block'; }else{ echo 'none'; } ?>;">
                                <p><?php echo $value->mp_descriptions; ?></p>
                                <div>
                                    <span style="float:left;">Subtotal</span><span style="float:right;">$<?php echo $value->mp_price; ?></span>
                                </div>
                                <br/>
                                <div class="border-bottom"></div>
                                <div>
                                    <span style="float:left;">Total</span><span style="float:right;">$<?php echo $value->mp_price; ?></span>
                                </div>
                                <div class="border-bottom"></div>
                            </div> 
                            <?php
                            }
                        ?>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 328px;
    height: calc(40vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar {
    background: #54bfe3;
}
/*.sticky-wrapper.is-sticky .site-navbar ul li a {
    color: #505048c4 !important;
}*/
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}


.bannerheading {
    margin: auto;
    display: block;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    font-size: 55px;
    font-family: unset;
}
.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}
form#contact_form input {
    font-size: 14px;
}
form#contact_form {
    font-size: 14px;
    margin: 22px 0px;
}
.loadmore {
    background: #54bfe3 !important;
    border-radius: 1px !important;
    color: #fff !important;
    font-weight: bold;
    font-size: 15px;
    margin-top: 20px;
    border: 1px solid #54bfe3 !important;
    padding: 12px 80px;
}
.address {
    background: #00BCD4;
    padding: 50px;
}
.address p {
    text-align: center;
    color: #fff;
}
.address label {
    text-align: center !important;
    margin: 0 auto !important;
    color: #fff;
    font-size: large;
}
.socialicon{
  text-align: center;
}
.socialicon a {
    padding: 10px;
    background-repeat: no-repeat;
    background-size: cover;
    display: inline-block;
    width: 30px;
    height: 30px;
    margin: 4px;
}
.socialicon a:hover {
    opacity: 0.6;
}
.form-control:active, .form-control:focus {

    border-color: #00BCD4;

}
a.facebook {
    background: url(./assets/images/follow_us_fb.png);
}
a.twitter {
    background: url(./assets/images/follow_us_tw.png);
}
a.google {
    background: url(./assets/images/follow_us_g.png);
}
a.instagram {
    background: url(./assets/images/follow_us_insta.png);
}
a.linkedin {
    background: url(./assets/images/follow_us_link.png);
}

label#txt_paymethod-error ,label#txt_agree2-error{
    position: absolute;
    bottom: -80px;
    left: 0;
}
label#txt_agree2-error,label#txt_agree3-error{
    position: absolute;
    bottom: -25px;
    left: 0;
}
.custom-radio .custom-control-input:checked~.custom-control-label:before {
    background-color: #08c;
}
.custom-checkbox .custom-control-input:checked~.custom-control-label:before {
    background-color: #08c;
}
#txt_agree-error {
    display: block;
    position: absolute;
    bottom: -27px;
}
#paytab a.nav-link.active {
    color: #17a2b8;
}
#paytab a.nav-link {
    color: #827979;
}
.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #eaeaea;
}

label {
    display: inline-block;
    margin-bottom: 0.5rem;
    color: #332727;
}
/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {

    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 130px;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
}
.iti {
    width: 100%;
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
}
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {

} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

}
</style>
<script src="<?php echo base_url('assets/build/js/intlTelInput.js'); ?>"></script>
  <script>
/*    var input = document.querySelector(".mobileFieldClass");
    window.intlTelInput(input, {
      allowExtensions: true,
          autoFormat: false,
          autoHideDialCode: false,
          autoPlaceholder: false,
          defaultCountry: "auto",
          ipinfoToken: "yolo",
          nationalMode: false,
          numberType: "MOBILE",
          //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
          preferredCountries: ['us', 'ca', 'in'],
          // preventInvalidNumbers: true,
          // utilsScript: "lib/libphonenumber/build/utils.js"
          preventInvalidNumbers: true,
      utilsScript: "assets/build/js/utils.js",

    });*/
  </script>

