<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section"></div>
<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 text-center">
                <br/>
                <br/>
                <h2 class="section-title mb-3">Search keyword of "<?php echo (($searchKeyword)?$searchKeyword:''); ?>"</h2>
                <h3 class="section-sub-title">
                    Search result related of 
                    <?php
                    if($searchData){
                        foreach ($searchData as $key => $value) {
                            echo $key.', ';
                        }
                    }else{
                        echo 'Authors,books';
                    } ?>
                </h3>
            </div>
        </div>
        <?php
        if($searchData){ 
        ?>
        <nav>
            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
            <?php
            $c=1;
            foreach ($searchData as $key => $value) {
            ?>
                <a class="nav-item nav-link <?php if($c == 1){ echo 'active'; } $c++; ?>" id="nav-<?php echo $key; ?>-tab" data-toggle="tab" href="#nav-<?php echo $key; ?>" role="tab" aria-controls="nav-<?php echo $key; ?>" aria-selected="true"><?php echo $key; ?></a>
                <?php   
            }
            
            ?>
                <!-- <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>
                <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">About</a> -->
            </div>
        </nav>

        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
            <?php
            $a=1;
            foreach ($searchData as $key => $value) {
            ?>
                <div class="tab-pane fade <?php if($a == 1){ echo 'show active'; } $a++; ?>" id="nav-<?php echo $key; ?>" role="tabpanel" aria-labelledby="nav-<?php echo $key; ?>-tab">

                    <?php 
                    $newkey= $searchData[$key];
                    foreach ($newkey as $sdata) {
                        if($key == 'authors'){
                            $authorData = $this->user_Auth->getData('user_credentials',array('uc_id' => $sdata->uc_id));
                            $authorMore = $this->user_Auth->getData('authors',array('a_fK_of_uc_id' => $sdata->uc_id));
                            ?>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="authorDivsearch">
                                        <div class="authorImage">
                                            <img src ="<?php echo ($authorData[0]->uc_image)?base_url("uploads/users/".$authorData[0]->uc_image):base_url("uploads/users/author.png"); ?>" class="img-fluid">
                                        </div>
                                        <div class="authorName">
                                            <h3><?php echo (($authorData[0]->uc_firstname)?$authorData[0]->uc_firstname:"").' '.(($authorData[0]->uc_lastname)?$authorData[0]->uc_lastname:""); ?>
                                            </h3>
                                            <label>Published Books: <?php echo (isset($authorMore[0]->a_publishbooks)?$authorMore[0]->a_publishbooks:'No'); ?></label>
                                        </div>
                                        <div class="athordesc">
                                            <?php  echo (isset($authorMore[0]->a_description)?$authorMore[0]->a_description:''); ?>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <?php
                        }
                        if($key == 'books'){
                            $booksData = $this->user_Auth->getData('books',array('b_id' => $sdata->b_id));
                            if(isset($booksData[0]->b_id)){
                            ?>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="authorDivsearch">
                                        <div class="authorImage">
                                            <a href="<?php echo site_url('book/bookdetails/'.$booksData[0]->b_id); ?>"><img src="<?php echo (($booksData[0]->b_image)?base_url("uploads/books/".$booksData[0]->b_image):base_url("uploads/books/book.png")); ?>" class="img-fluid"></a>
                                        </div>
                                        <div class="authorName">
                                            <h3><a href="<?php echo site_url('book/bookdetails/'.$booksData[0]->b_id); ?>"><?php echo (($booksData[0]->b_title)?$booksData[0]->b_title:''); ?></a></h3>
                                            <label>Published : <?php echo (($booksData[0]->b_published > 0)?date('M, d Y',strtotime($booksData[0]->b_published)):''); ?>
                                            </label>
                                        </div><br/>

                                    
                                        <label>Price: <?php echo '$ '.(($booksData[0]->b_sellingprice)?$booksData[0]->b_sellingprice:''); ?></label><br/>
                                        <label style="text-transform: capitalize;">Language: <?php echo (($booksData[0]->b_language)?$booksData[0]->b_language:''); ?></label><br/>
                                        <label>
                                            Author Name : 
                                            <?php
                                            $userDetails = $this->user_Auth->getData('user_credentials', array('uc_id' => $booksData[0]->b_fk_of_aid));
                                            echo (isset($userDetails[0]->uc_firstname)?$userDetails[0]->uc_firstname:'').' '.(isset($userDetails[0]->uc_lastname)?$userDetails[0]->uc_lastname:''); ?>
                                        </label><br/>
                                        <label>Category :
                                            <?php $categories = $this->user_Auth->getData('categories', array('c_id' => $booksData[0]->b_category)); ?>
                                            <a href="<?php echo site_url('category/'.$categories[0]->c_slug); ?>"><?php echo (isset($categories[0]->c_name)?$categories[0]->c_name:'').' '.(isset($categories[0]->c_name)?$categories[0]->c_name:''); ?><a>
                                        </label><br/>

                                        <?php print_r($booksData[0]->b_description);
                                        // $booksData[0]->b_id
                                        // $booksData[0]->b_fk_of_aid;
                                        // $booksData[0]->b_fk_of_uid;
                                        // $booksData[0]->b_language;
                                        // 
                                        // $booksData[0]->b_published;
                                        // $booksData[0]->b_sellingprice;
                                        // $booksData[0]->b_rating;
                                        // $booksData[0]->b_category;
                                        
                                        // $booksData[0]->b_description;
                                        // $booksData[0]->b_file;
                                        // $booksData[0]->b_downloads;
                                        // $booksData[0]->b_status;
                                        // $booksData[0]->b_created;
                                        // $booksData[0]->b_modified;
                                         ?>
                                    </div>
                                </div>
                            </div>
                            
                            <?php
                            }
                        }
                    } ?>
                    
                </div>
            <?php   
            }
            ?>
            
        </div>
        <?php
        }else{
            echo '"'.$searchKeyword.'"';
            ?>
            instead of search result not found!. 
            <?php 
        }
        ?>
        <?php 
        //print_r($searchData)?>
    </div>
</section>
<?php /* Button to Open the Modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Open modal
</button> */?>

<!-- The Modal -->
<div class="modal fade" id="membershipModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center d-block">
                <h4 class="modal-title d-inline-block text-info"><strong>Membership Information</strong></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form name="memberinfoForm" id="memberinfoForm" method="post" class="was-validated">
                
                </form>
            </div>

            <?php /* Modal footer -->
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> --*/ ?>
        </div>
    </div>
</div>
<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 83px;
    height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
@media only screen and (max-width: 600px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 0px !important;
        height: 22vh;
    }
}
@media only screen and (max-width: 767px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 0px !important;
        height: 22vh;
    }
} 

</style>
 