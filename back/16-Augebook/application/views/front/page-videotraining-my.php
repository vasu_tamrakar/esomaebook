<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/video_training_bg.png'); ?>);">
    <div class="container">
        <div class="row">
        <div class="bannerheading"><h1>Video Trainning</h1></div>
        </div>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Video Training</li>
          </ol>
        </nav>

    </div>
    
</div>


    <section class="site-section bg-white" id="contact-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <br/>
            <br/>
            <h2 class="section-title mb-3">Enrich your knowladge with our tutorials</h2>
            <!-- <h3 class="section-sub-title">over 3.5 millions ebooks Read with no limit...</h3> -->
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
              
            
            <ul class="nav nav-tab justify-content-center">
                <li><a data-toggle="tab" href="#home" class="active show">All video</a></li>
                <li><a data-toggle="tab" href="#menu2">Latest Video</a></li>
                <li><a data-toggle="tab" href="#menu3">most Popular</a></li>
            </ul>
          </div>
          <br/>
          <br/>

          <div class="col-md-1"></div>
          <div class="col-md-10">
            <div class="tab-content">
              <div id="home" class="tab-pane fade in active show">

                <!-- Video list start -->
                <div class="row">
                  <?php 
                    //$category = "";
                    foreach($videos as $video){
                        
                    ?>
                      <div class="col-md-6">
                        <div class="box-common">
                          <iframe width="100%" height="240" src="https://player.vimeo.com/video/<?php echo $video->v_videoid; ?>?autoplay=0&loop=1&autopause=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                      
                          <div class="likesharediv">
                              <div class="date"><?php echo date('F d,Y',strtotime($video->v_created));?></div>
                              <p><?php echo $video->v_title; ?></p>
                              <div class="sharelike">

                              <div class="like">like : <a href=""><i class="fa fa-thumbs-up" aria-hidden="true"></i></a></div>
                              <div class="share">Share:
                                <a href="" class=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="" class=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                <a href="" class=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                              </div>
                              </div>
                          </div>
                          </div>
                      </div>
                  <?php } ?>
                  <!-- Video list start -->

                </div>
                <center>
                      <button id="allvideo" class="btn btn-info loadmore">Load More</button>
                  </center>

              </div>
              <!-- End Menu id 1 -->
              <!-- Menu id 1 -->

              <div id="menu2" class="tab-pane fade">
                <!-- Video list start -->
                <div class="row">
                  <?php 
                    //$category = "";
                    foreach($videos as $video){
                        
                    ?>
                      <div class="col-md-6">
                        <div class="box-common">
                          <iframe width="100%" height="240" src="https://player.vimeo.com/video/<?php echo $video->v_videoid; ?>?autoplay=0&loop=1&autopause=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                      
                          <div class="likesharediv">
                              <div class="date"><?php echo date('F d,Y',strtotime($video->v_created));?></div>
                              <p><?php echo $video->v_title; ?></p>
                              <div class="sharelike">

                              <div class="like">like : <a href=""><i class="fa fa-thumbs-up" aria-hidden="true"></i></a></div>
                              <div class="share">Share:
                                <a href="" class=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="" class=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                <a href="" class=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                              </div>
                              </div>
                          </div>
                          </div>
                      </div>
                  <?php } ?>
                  <!-- Video list start -->

                </div>
                <center>
                      <button id="allvideo" class="btn btn-info loadmore">Load More</button>
                  </center>
              </div>
              <!-- End Menu id 2 -->
              <!-- Menu id 3 -->
              <div id="menu3" class="tab-pane fade">
                <!-- Video list start -->
                <div class="row">
                  <?php 
                    //$category = "";
                    foreach($videos as $video){
                        
                    ?>
                      <div class="col-md-6">
                        <div class="box-common">
                          <iframe width="100%" height="240" src="https://player.vimeo.com/video/<?php echo $video->v_videoid; ?>?autoplay=0&loop=1&autopause=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                      
                          <div class="likesharediv">
                              <div class="date"><?php echo date('F d,Y',strtotime($video->v_created));?></div>
                              <p><?php echo $video->v_title; ?></p>
                              <div class="sharelike">

                              <div class="like">like : <a href=""><i class="fa fa-thumbs-up" aria-hidden="true"></i></a></div>
                              <div class="share">Share:
                                <a href="" class=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="" class=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                <a href="" class=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                              </div>
                              </div>
                          </div>
                          </div>
                      </div>
                  <?php } ?>
                  <!-- Video list start -->

                </div>
                <center>
                      <button id="allvideo" class="btn btn-info loadmore">Load More</button>
                  </center>
              </div>
              
            </div>
          </div>
          <div class="col-md-1"></div>
              <!-- End Menu id 3 -->
          </div>
        </div>
            
        
      </div>
      </div>
    </section>
<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 328px;
    height: calc(40vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar ul li a {
    color: #505048c4 !important;
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}


.bannerheading {
    margin: 0 auto;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    top: 130px;
    font-size: 55px;
}
.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
.authorImg {
    box-shadow: 0px 1px 5px 0px black;
}
h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}
.nav-tab .active {

    background: #54BFE3;
    color: #fff;

}

.sharelike a {

    color: #817979e6;

}
.nav-tab li a {
    background: #EDF3F2;
    margin: 6px;
    padding: 7px 14px;
    color: #54BFE3;
    font-family: unset;
    text-transform: capitalize;
    border: 1px solid #54BFE3;
    border-radius: 4px;
    font-weight: bold;
    display: inline-block;
}
.nav.nav-tab.justify-content-center {
    margin-bottom: 60px;
}

.sharelike {
    display: block;
    padding: 6px;
}
.sharelike a {
    color: #817979e6;
}
.likesharediv .date {
    background: #54BFE3;
    padding: 6px;
    color: #fff;
    font-size: 13px;

}
.likesharediv p {
    padding: 6px;
    color: #382c2c;
    font-size: 15px;
    text-align: center;

}
.like {
    display: inline-block;
}
.share {
    display: inline-block;
    float: right;
}
.share a {
    margin: 4px;
}
.like a:hover {
    color: #54BFE3;
}
.share a:hover {
    color: #54BFE3;
}

center {

    text-align: center;
    margin: 0 auto;

}
.loadmore {

    background: #f2ededb3 !important;
    border-radius: 8px !important;
    color: #54bfe3 !important;
    font-weight: bold;
    font-size: 15px;
    margin-top: 20px;

}
.loadmore:hover {

    color: #fff !important;
    background: #54BFE3 !important;

}

/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {
    .authors .owl-next {
        height: 24px;
        width: 24px;
        background-image: url(./assets/images/button_next_2.png) !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
        margin: 0px 295px !important;
        float: right;
    }
    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 130px;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {
  .authors .owl-next {
        height: 24px;
        width: 24px;
        background-image: url(./assets/images/button_next_2.png) !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
        margin: 0px 260px !important;
        float: right;
    }
    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 130px;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
}
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {

} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

}
</style>
  