-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 19, 2019 at 04:30 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebook_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `b_id` bigint(20) NOT NULL,
  `b_fk_of_aid` bigint(20) NOT NULL,
  `b_fk_of_uid` bigint(20) NOT NULL,
  `b_language` enum('somali','english') NOT NULL,
  `b_title` varchar(255) NOT NULL,
  `b_published` date NOT NULL,
  `b_price` double(8,2) NOT NULL,
  `b_rating` int(5) NOT NULL,
  `b_category` int(11) NOT NULL,
  `b_image` text NOT NULL,
  `b_description` text NOT NULL,
  `b_file` text NOT NULL,
  `b_downloads` bigint(20) NOT NULL,
  `b_status` enum('1','0') NOT NULL,
  `b_created` datetime NOT NULL,
  `b_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`b_id`, `b_fk_of_aid`, `b_fk_of_uid`, `b_language`, `b_title`, `b_published`, `b_price`, `b_rating`, `b_category`, `b_image`, `b_description`, `b_file`, `b_downloads`, `b_status`, `b_created`, `b_modified`) VALUES
(6, 7, 1, 'english', 'dsdas dasd asd asd asdsdafasdf', '2019-06-25', 0.00, 4, 8, '', '.pdf', 'Mail_A_Ticket.pdf', 0, '1', '2019-06-25 12:24:15', '0000-00-00 00:00:00'),
(7, 7, 1, 'english', 'vcxvxcvxcv', '2019-06-25', 0.00, 3, 5, '', '.mp4', 'What_are_you_waiting_for_-_30_seconds.mp4', 0, '1', '2019-06-25 12:25:10', '0000-00-00 00:00:00'),
(9, 7, 1, 'english', '2x323xc23v2xc32xc', '2019-06-25', 0.00, 3, 6, '', '.pdf', 'b93f896e80a23a2fa1187ab3d0b65e9d.pdf', 0, '1', '2019-06-25 13:07:12', '0000-00-00 00:00:00'),
(10, 7, 1, 'english', 'TEST ONE', '2010-05-01', 0.00, 4, 2, 'book_1.png', '.pdf', 'book.pdf', 0, '1', '2019-06-25 13:18:04', '2019-06-26 08:41:50'),
(11, 8, 7, 'english', 'vvvvvvvvvvvvws', '2003-06-02', 0.00, 1, 8, 'book_4.png', '.pdf', 'pdf-sample3.pdf', 0, '1', '2019-06-25 13:18:53', '2019-06-26 08:23:53'),
(12, 7, 1, 'english', 'ghgffgfghfhfgh', '2001-05-30', 0.00, 4, 7, '9cf582c45558e41f4c3f0ea859899f58.png', '.pdf', '8dd95ecb22e109e1c50c77159cdfd36d.pdf', 0, '1', '2019-06-25 13:22:22', '0000-00-00 00:00:00'),
(13, 11, 12, 'english', 'dsadasd asdsa', '0000-00-00', 0.00, 3, 3, 'wormhole-abstract-hd-wallpaper-iltwmt2.png', '', 'Mail_A_Ticket1.pdf', 1, '1', '2019-07-05 11:24:20', '0000-00-00 00:00:00'),
(14, 11, 12, 'english', 'B?HDSODSKdfsh', '0000-00-00', 0.00, 3, 2, 'Summer-Wallpaper-Pictures1.jpg', '.pdf', 'demoform11.pdf', 0, '1', '2019-07-05 11:27:33', '0000-00-00 00:00:00'),
(15, 25, 1, 'somali', 'TEST SOMALI1', '2019-07-10', 0.00, 3, 10, 'book_6.png', '.pdf', 'Mail_A_Ticket2.pdf', 0, '1', '2019-07-08 15:54:18', '2019-07-15 08:47:57'),
(16, 12, 1, 'somali', 'vvvvvvvvv', '2004-07-14', 0.00, 3, 10, 'book_51.png', '.pdf', 'demoform12.pdf', 0, '1', '2019-07-12 13:52:48', '0000-00-00 00:00:00'),
(17, 27, 1, 'somali', 'TESTS DOMALIDFDF', '2019-07-12', 0.00, 3, 7, '', '.pdf', '5096011b280c5ebfac82e87f6f37ab74.pdf', 0, '1', '2019-07-12 14:36:01', '0000-00-00 00:00:00'),
(18, 30, 1, 'english', 'SDFGpp', '2019-07-09', 0.00, 4, 10, 'book_41.png', '.pdf', 'pdf-sample(2).pdf', 0, '0', '2019-07-12 15:45:17', '2019-07-12 15:49:27'),
(19, 2, 1, 'english', 'Tset book', '2002-05-02', 0.00, 3, 6, 'book_52.png', '.pdf', 'demoform13.pdf', 0, '1', '2019-07-15 07:10:30', '2019-07-17 15:04:22'),
(20, 4, 1, 'english', 'This is test book', '1991-02-02', 50.00, 0, 6, 'book_53.png', '.pdf', 'demoform14.pdf', 0, '1', '2019-07-19 08:31:52', '0000-00-00 00:00:00'),
(21, 2, 1, 'english', 'rtTtest nooksd', '2010-06-30', 34.00, 0, 4, 'high-definition-download-hd-wallpaper3.jpg', 'WWWsdfs dfs dfs fsdf sdfs dfds fsdf sdf sdf<br>', 'pdf-sample7.pdf', 0, '1', '2019-07-19 15:03:04', '0000-00-00 00:00:00'),
(22, 4, 1, 'english', 'Second Testnn', '2004-07-15', 30.00, 0, 8, 'kPl0TGN1.jpg', 'This is second book description. <span style=\"font-weight: bold;\">KLK</span><br><blockquote><blockquote>szdafg asdfasd fasdf asdf asdfasd fsadf sadf asas dfaf asd<br>ksjklasdjlflsadlkf;lssdf<br></blockquote></blockquote>\r\n<hr>sdfdsaf asdf asdf sdaf asdf', 'demoform15.pdf', 0, '1', '2019-07-19 15:04:50', '2019-07-19 15:25:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`b_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `b_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
