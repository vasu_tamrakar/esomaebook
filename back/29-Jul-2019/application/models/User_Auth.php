<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Auth extends CI_Model {
	/*----------------------------------
		insert($tablename,$data);

	----------------------------------*/
	public function insert($tname,$data){
		$this->db->insert($tname,$data);
		$id = $this->db->insert_id();
		if($id > 0){
			return $id; 
		}else{
			return FALSE;
		}
	}
	/*----------------------------------
		update($tablename,$data,$filter);

	----------------------------------*/
	public function update($tname,$data,$filter){
		$this->db->where($filter);
		$query = $this->db->update($tname,$data);
		if($this->db->affected_rows() > 0){
			return TRUE; 
		}else{
			return FALSE;
		}
	}
	/*----------------------------------
		delete($tablename,$filter);

	----------------------------------*/
	public function delete($tname,$filter){
		$this->db->where($filter);
		$query = $this->db->delete($tname);
		if($this->db->affected_rows() > 0){
			return TRUE; 
		}else{
			return FALSE;
		}
	}

	/*----------------------------------
		getData($tablename,$filter,$selected,$order);

	----------------------------------*/
	public function getData($tname,$filter=false,$selected=false,$order=false,$groupby=false){
		if($selected){
			$this->db->select($selected);	
		}
		if($filter){
			$this->db->where($filter);	
		}
		if($order){
			$this->db->order_by($order);	
		}
		if($groupby){
			$this->db->group_by($groupby);
		}
		$query = $this->db->get($tname);
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return FALSE;
		}
	}
	/*----------------------------------
		existEmail($email);

	----------------------------------*/
	public function existEmail($email){
		$query = $this->db->get_where('user_credentials',array('uc_email' =>$email));
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return FALSE;
		}
	}
	/*----------------------------------
		existMobile($mobile);

	----------------------------------*/
	public function existMobile($mobile){
		$query = $this->db->get_where('user_credentials',array('uc_mobile' =>$mobile));
		if($query->num_rows() > 0){
			return $query->result(); 
		}else{
			return FALSE;
		}
	}

	/*----------------------------------
		checkAuthSignIn($userlogin,$password);

	----------------------------------*/
	public function checkAuthSignIn($userlogin,$password){
		$queryEmail = $this->db->get_where('user_credentials',array('uc_email' =>$userlogin));
		$queryMobile = $this->db->get_where('user_credentials',array('uc_mobile' =>$userlogin));
		if($queryEmail->num_rows() > 0){
			$queryRes = $this->db->get_where('user_credentials',array('uc_email' => $userlogin, 'uc_password' => md5($password)));
			if($queryRes->num_rows() > 0){
				if(($queryRes->result()[0]->uc_status == '1') && ($queryRes->result()[0]->uc_active == '1')){
					return array('status'=>'Success', 'data' => $queryRes->result()[0], 'message' => $userlogin.' scccessfully signIn.');
				}
				if(($queryRes->result()[0]->uc_status == '0') && ($queryRes->result()[0]->uc_active == '0')){
					return array('status'=>'Invalid', 'message' => 'Account is dectivated.');
				}
				if(($queryRes->result()[0]->uc_status == '1') && ($queryRes->result()[0]->uc_active == '0')){
					return array('status'=>'Invalid', 'message' => 'Account is dectivated.');
				}
				if($queryRes->result()[0]->uc_status == '0'){
					return array('status'=>'Invalid', 'message' => 'Please contact to administrator.');
				}
				
				
			}else{
				return array('status'=>'Invalid', 'message' => 'In-Valid credentials.');
			}
			
		}elseif($queryMobile->num_rows() > 0){
			$queryResmob = $this->db->get_where('user_credentials',array('uc_mobile' =>$userlogin, 'uc_password' =>md5($password)));
			if($queryResmob->num_rows() > 0){

				if(($queryResmob->result()[0]->is_status == '1') && ($queryResmob->result()[0]->is_active == '1')){
					return array('status'=>'Success', 'data' => $queryResmob->result()[0],'message' => $userlogin.' scccessfully signIn.');
				}
				if(($queryResmob->result()[0]->is_status == '0') && ($queryResmob->result()[0]->is_active == '0')){
					return array('status'=>'Invalid', 'message' => 'Account is dectivated.');
				}
				if(($queryResmob->result()[0]->is_status == '1') && ($queryResmob->result()[0]->is_active == '0')){
					return array('status'=>'Invalid', 'message' => 'Account is dectivated.');
				}
				if($queryResmob->result()[0]->is_status == '0'){
					
					return array('status'=>'Invalid', 'message' => 'Please contact to administrator.');
				}

			}else{
				return array('status'=>'Invalid','message' => 'In-Valid credentials.');
			}
		}else{
			return array("status"=> "Signup","message" => $userlogin. " Not registered. click <a href='".site_url('signUp')."'>here </a>for register.");
		}
	}
	/*----------------------------------
		ForgotPassword($email);

	----------------------------------*/
	public function ForgotPassword($email){

		$num = rand(0,9999);
		$this->db->where('uc_email',$email);
		$result = $this->db->update('user_credentials', array('uc_secrete' => $num, 'uc_password' => md5('.lzxkc;;'),'uc_status' => '0'));
		
		if($this->db->affected_rows() > 0){
			return $num;
		}else{
			return FALSE;
		}
	}

	/*----------------------------------
		exist_emailMobile($email,$mobile, $id);

	----------------------------------*/
	public function exist_emailMobile($email,$mobile=false,$uid){
		$this->db->where(array('uc_email' => $email));
		$this->db->where_not_in('uc_id',$uid);
		$emailEx = $this->db->get('user_credentials');
		if($emailEx->num_rows > 0){
			return array('status'=>'error', 'message'=>'Email Already registered.Please change the email address.');
		}
		if($mobile){
			$this->db->where(array('uc_mobile' => $mobile));
			$this->db->where_not_in('uc_id',$uid);
			$mobileEx = $this->db->get('user_credentials');
			if($mobileEx->num_rows > 0){
				return array('status'=>'error', 'message'=>'Mobile Number Already registered.Please change the mobile number.');
			}
		}
		return array('status'=>'success', 'message'=>'not');
		
	}
	
	/*-------------------------------------
		activeAccoount(id,email)
	-------------------------------------*/
	public function activeAccoount($uid,$email){
		$this->db->where(array('uc_id' => $uid,'uc_email' => $email));
		$this->db->update('user_credentials', array('uc_status' => '1', 'uc_active' => '1'));
		if($this->db->affected_rows() > 0 ){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	/*-------------------------------------------
		countAll(tname,$where)
	-------------------------------------------*/
	public function countAll($name, $where=false){
		if($where){
			$this->db->where($where);	
		}
		return $this->db->count_all($name);
	
	}
	/*-------------------------------------------
		countAllwhere(tname,$where)
	-------------------------------------------*/
	public function countAllwhere($name, $where=false){
		if($where){
			$this->db->where($where);	
		}
		return  $this->db->count_all_results($name);
	
	}
	/*----------------------------------
		get_authorByuid(userId)
	----------------------------------*/
	public function get_authorByuid($userId){
		$query = $this->db->get_where('authors',array("a_fK_of_uc_id" => $userId));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	/*----------------------------------
		get_Totaldownloadcount($tname, $select)
	----------------------------------*/
	public function get_Totaldownloadcount($tname, $select){
		$this->db->select_sum($select);
		$query = $this->db->get($tname);
		return $query->result()[0];
	}
	/*------------------------------------------
		emailSubscribe($em)
	------------------------------------------*/
	public function emailSubscribe($em){
		
		$existE = $this->db->get_where('emailSubscriber', array('es_email' => $em));

		if($existE->num_rows() > 0){
			$dataemail= $existE->result();
			if($dataemail[0]->es_status === '0'){
				$this->db->where(array('es_email' => $em));
				$this->db->update('emailSubscriber',array('es_status' => '1', 'es_modified'=> date('Y-m-d H:i:s')));
				// $qry = $this->db->insert_id();
				if($this->db->affected_rows()){
					return array('status' => 200, 'message' => $em." address successfully subscribe!.");
				}
			}else{
				return array('status' => 100, 'message' => $em." address already subscribed!.");
			}
		}else{
			$this->db->insert('emailSubscriber',array('es_email' =>$em, 'es_status' => '1', 'es_created'=> date('Y-m-d H:i:s')));
			$qry = $this->db->insert_id();
			if($qry > 0){
				return array('status' => 200, 'message' => $em." address successfully subscribe!.");
			}else{
				return array('status' => 100, 'message' => 'Process failed.');
			}
		}
		die();
	}
	/*-----------------------------------------
		mostpopularBooksID()
	-----------------------------------------*/
	public function mostpopularBooksID(){
		$mostpopularBooksID = $this->getData('paymentDetails', $w = '', $se= 'pd_bookid', $sh = '', $grp ='pd_bookid');

		if($mostpopularBooksID){
			$this->db->select('b_id');
			$this->db->where('b_status', '1');
			$this->db->where_in('b_id');
			$this->db->order_by('b_id DESC');
			$qurery = $this->db->get('books');
			if($qurery->num_rows() > 0 ){
				return $qurery->result();
			}else{
				return FALSE;
			}	
		}else{
			return FALSE;
		}
	}


	/*----------------------------------------
		get_textfreesearch(text)
	-----------------------------------------*/
	public function get_textfreesearch($sText){

		$this->db->select('uc_id');
		
		$this->db->like('uc_firstname', $sText);
		$this->db->or_like('uc_lastname', $sText);
		$this->db->where(array('uc_role', 4, 'uc_status', '1', 'uc_active', '1'));
		$query = $this->db->get('user_credentials');
		if($query->num_rows() > 0){
			$searchdata["authors"] = $query->result();
		}
		$this->db->select('b_id');
		$this->db->where('b_status', '1');
		$this->db->like('b_title', $sText);
		$query2 = $this->db->get('books');

		if($query2->num_rows() > 0){
			$searchdata["books"] = $query2->result();
		}
		if(empty($searchdata)){
			return FALSE;
		}else{
			return $searchdata;
		}
	}
}
?>