<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Failure extends CI_Controller {
	protected $payerData;
	public function __construct (){
	 	parent :: __construct();
		if($this->session->userdata('is_logged_in_user')){
			redirect(base_url());  exit();
		}
		$this->load->model('user_Auth');
	}
	public function index()
	{
		$data["title"] = 'Failure Payment Page | '.SITENAME;
		$this->payerData = $this->session->userdata("payerData");
		if($this->payerData){
			$this->session->unset_userdata('payerData');
			$this->session->unset_userdata('signUpdataid');
			$this->load->view('front/common/header', $data);
			$this->load->view('front/page-failure', $data);
			$this->load->view('front/common/page-footer', $data);
		}else{
			$this->session->set_flashdata('alert', 'error');
			$this->session->set_flashdata('message', 'Invalid operation!.');
			redirect('membership'); exit();
		}
	}
}