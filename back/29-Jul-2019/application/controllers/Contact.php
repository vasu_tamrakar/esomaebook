<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if($this->session->userdata('is_logged_in_user')){
			$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');	
		}
		if($this->session->userdata('is_logged_in_user_info')){
			$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');	
		}
		$this->load->model('user_Auth');
		
		
	}
	public function index()
	{
		$data["title"] = 'Contact Page | '.SITENAME;
		$post = $this->input->post();
		$this->form_validation->set_rules('txt_firstname','First Name','required');
		// $this->form_validation->set_rules('txt_lastname','Last Name','required');
		$this->form_validation->set_rules('txt_subject','Subject','required');
		$this->form_validation->set_rules('txt_messsage','Message','required');

		if($post){
			$formData["c_firstname"] = $post["txt_firstname"];
			$formData["c_lastname"] = $post["txt_lastname"];
			$formData["c_subject"] = $post["txt_subject"];
			$formData["c_message"] = $post["txt_messsage"];
			$formData["c_created"] = date('Y-m-d H:i:s');
			if($this->form_validation->run() === TRUE){


				$config['protocol'] 	= 'smtp';
		        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
		        $config['smtp_port'] 	= '465';
		        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
		        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
		        $config['mailtype'] 	= 'html';
		        $config['charset'] 		= 'utf-8';
		        $config['newline'] 		= "\r\n";
		        $config['wordwrap'] 	= TRUE;
				$this->email->initialize($config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");

				$this->email->to('pareshnagar87@gmail.com', SITENAME);
				$this->email->subject((($formData["c_subject"])?$formData["c_subject"]:SITENAME ." Contact form"));
				$this->email->message($formData["c_message"]);
				$sendMail = $this->email->send();
				if($sendMail){
					$this->session->set_flashdata('alert', 'success');
					$this->session->set_flashdata('message', 'Thanks for contacting and email successfully send.');
					redirect('contact'); exit();
				}else{
					$this->session->set_flashdata('alert', 'error');
					$this->session->set_flashdata('message', 'Email sending failed!..');
					redirect('contact'); exit();
				}
			}else{
				$this->load->view('front/common/header',$data);
				$this->load->view('front/page-contact',$data);
				$this->load->view('front/common/footer',$data);	
			}
		}else{
			$this->load->view('front/common/header',$data);
			$this->load->view('front/page-contact',$data);
			$this->load->view('front/common/footer',$data);
		}
	}
}
