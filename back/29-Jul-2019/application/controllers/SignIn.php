<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SignIn extends CI_Controller {
	public function __construct (){
	 	parent :: __construct();
		if($this->session->userdata('is_logged_in_user')){
			redirect(base_url());  exit();
		}
		$this->load->model('user_Auth');
	}
	public function index()
	{
		$data["title"] = 'Sign In Page | '.SITENAME;

		$post = $this->input->post();
		/* Check Form from post*/
		if($post){
			$this->form_validation->set_rules('txt_email','Email & Mobile','required');
			$this->form_validation->set_rules('txt_password','Password','required');
			/* Check validation */
			if($this->form_validation->run() === TRUE){
				
				$formData["uc_email"] = $post["txt_email"];
				$formData["uc_password"] = $post["txt_password"];
				$ckecksignIn = $this->user_Auth->checkAuthSignIn($formData["uc_email"],$formData["uc_password"]);
				/* Check login */
				if($ckecksignIn["status"] == 'Success'){
					$details = $ckecksignIn["data"];
					$udata["u_id"] = $details->uc_id;
					$udata["u_firstname"] = $details->uc_firstname;
					$udata["u_lastname"] = $details->uc_lastname;
					$udata["u_email"] = $details->uc_email;
					$udata["u_mobile"] = $details->uc_mobile;
					$udata["u_image"] = (($details->uc_image)?$details->uc_image:'user.png');
					$udata["u_role"] = $details->uc_role;
					$udata["u_status"] = $details->uc_status;
					$udata["u_created"] = $details->uc_created;
					$udata["u_modified"] = (($details->uc_modified >0)?$details->uc_modified:'');
					$this->session->set_userdata('is_logged_in_user', $details->uc_id);
					$this->session->set_userdata('is_logged_in_user_info', $udata);
					$this->session->set_flashdata('alert', "success");
					$this->session->set_flashdata('message', $ckecksignIn["message"]);
					
					if($details->uc_role <= 3){
						redirect(base_url('dashboard')); exit();	
					}else if($details->uc_role > 3){
						redirect(base_url('user')); exit();	
					}else{
						redirect(base_url()); exit();	
					}

					


				}elseif($ckecksignIn["status"] == 'Signup'){
					$this->session->set_flashdata('alert', 'info');
					$this->session->set_flashdata('message', $ckecksignIn["message"]);
					redirect(site_url('signIn')); exit();
				}else{
					$this->session->set_flashdata('alert', 'error');
					$this->session->set_flashdata('message', $ckecksignIn["message"]);
					$this->load->view('front/common/header',$data);
					$this->load->view('front/page-signin',$data);
					$this->load->view('front/common/footer',$data);	
				}
			}else{	
				$this->load->view('front/common/header',$data);
				$this->load->view('front/page-signin',$data);
				$this->load->view('front/common/footer',$data);
			}
		}else{
			$this->load->view('front/common/header',$data);
			$this->load->view('front/page-signin',$data);
			$this->load->view('front/common/footer',$data);	
		}
		
	}
	public function activated(){
		$get = $this->input->get();
		$uid = (isset($get["time"])?base64_decode($get["time"]):'');
		$email = (isset($get["slot"])?base64_decode($get["slot"]):'');
		$data['title'] = 'User Account Activated Page | '.SITENAME;
		$res = $this->user_Auth->activeAccoount($uid,$email);
		if($res){
			$data['email'] = $email;
			$userdata = $this->user_Auth->getData('user_credentials', $wh = array('uc_id' =>$uid), $se = '', $sh='');
			$this->signupNotification($email,$subject=false,$userdata);
			$this->session->set_flashdata('alert','success');
			$this->session->set_flashdata('message','Your account successfully Activated.');
			$this->load->view('front/common/header', $data);
			$this->load->view('front/page-active', $data);
			$this->load->view('front/common/footer', $data);
		}else{
			$this->session->set_flashdata('alert','error');
			$this->session->set_flashdata('message','Activation failed please contact to ebook administrator.');
			$this->load->view('front/common/header', $data);
			$this->load->view('front/page-active', $data);
			$this->load->view('front/common/footer', $data);
		}
	}
	public function signupNotification($to,$subject=false,$user){
		$html = '<!DOCTYPE html>
					<html lang="en">
					  <head>
					    <title>'.SITENAME.'</title>
					    <meta charset="utf-8">
					    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					    <link rel="stylesheet" href="'.base_url().'/assets/fonts/icomoon/style.css">
					    <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					    <link rel="stylesheet" href="'.base_url().'/assets/css/bootstrap.min.css">
					    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					    <link rel="stylesheet" href="'.base_url().'/assets/css/style.css">
					    <style>
					    </style>
					  </head>
					  <body>
					    <div class="container">
					        <div class="row">
					            <div style="display: block;height: 80px;width: 100%;text-align: center;padding: 12px;background: #54BFE3;">
					                <h2 style="color: #fff;font-size: 40px;font-weight: bold;"><strong>'.SITENAME.'</strong></h2>
					            </div>
					            <div style="margin: 30px;display: block; width:100%">
					                <h6>Hi '.$user[0]->uc_firstname.',</h6>
					                <p>Thank for Activation.</p>
					            </div>
					            <div style="display: block;width: 100%;background: #54bfe3;">
					                <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					            </div>
					        </div>
					    </div>
					  </body>
					</html>
					';

		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['newline'] = "\r\n";
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);
		$subject = (($subject)?$subject:'Account Activation');
		$this->email->to($to);
        $this->email->from('no_reply@esomabooks.com',SITENAME);
        $this->email->subject($subject);
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
        	return 1;
        }else{
    		$this->email->print_debugger();
    	}
	}
}
