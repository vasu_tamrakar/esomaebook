<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class StripeController extends CI_Controller {

    protected $payerData,$planDetails;

    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->payerData = $this->session->userdata("payerData");
       $this->load->model('user_Auth');
       $this->planDetails = $this->user_Auth->getData('membershipplan', array("mp_id" => $this->payerData["planID"]));
    }


    /**
     * Get All Data from this method.
     *
     * @return Response
    */

    public function index()

    {
        $this->load->view('front/page-stripe');
    }

       

    /**
     * Get All Data from this method.
     *
     * @return Response
    */

    public function stripePost()

    {
        $post = $this->input->post();
        if($post){
            $cardHolder     = $post["txt_holdername"];
            $cardNumber     = $post["txt_cardnumber"];
            $cvvNumber      = $post["txt_cvv"];
            $cardexpMonth   = $post["txt_expmonth"];
            $cardexpyear    = $post["txt_expyear"];
        

            require_once('application/libraries/stripe-php/init.php');

        

            \Stripe\Stripe::setApiKey($this->config->item('stripe_secret'));

         

            $charge =  \Stripe\Charge::create ([

                    "amount"        => 100 * ($this->planDetails[0]->mp_price),
                    "currency"      => "usd",
                    "source"        => $this->input->post('stripeToken'),
                    "description"   => $this->planDetails[0]->mp_name.','.$this->planDetails[0]->mp_validity.' '.SITENAME,
                    "metadata"      =>  array('planID' => $this->planDetails[0]->mp_id, "customerID" => $this->payerData["userID"])

            ]);
            $chargeJson = $charge->jsonSerialize(); 
            if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
                $transactionID      = $chargeJson['balance_transaction']; 
                $paidAmount         = $chargeJson['amount']; 
                $paidCurrency       = $chargeJson['currency']; 
                $payment_status     = $chargeJson['status'];
                
                $description        = $chargeJson["description"];
                $metadata           = $chargeJson["metadata"];
                $application        = $chargeJson["application"];
                $chargeId           = $chargeJson["id"];

                $paymentDetails["pd_txnid"] = $transactionID;
                $paymentDetails["pd_planid"] = $metadata["planID"];
                $paymentDetails["pd_planprice"] = ($paidAmount/100);
                $paymentDetails["pd_userid"] = $metadata["customerID"];
                $paymentDetails["pd_currency"] = $paidCurrency;
                $paymentDetails["pd_status"] = $payment_status;
                
                $paymentDetails["pd_cardholder"] = $cardHolder;
                $paymentDetails["pd_cardnumber"] = $cardNumber;
                $paymentDetails["pd_cvvnumber"] = $cvvNumber;
                $paymentDetails["pd_cartexpmonth"] = $cardexpMonth;
                $paymentDetails["pd_cardexpyear"] = $cardexpyear;
                $paymentDetails["pd_chargeid"] = $chargeId;
                $paymentDetails["pd_created"] = date('Y-m-d H;i:s');
                
                
                $this->user_Auth->insert("paymentDetails", $paymentDetails);

                //  Insert detail in
                if($payment_status == 'succeeded'){
                    $this->session->unset_userdata("payerData");
                    $this->session->unset_userdata("signUpdataid");

                    $access = array("ba_bookid" => $this->payerData["bookID"],"ba_userid" => $this->payerData["userID"]);
                    $user = $this->user_Auth->getData('user_credentials',array('uc_id'=> $this->payerData["userID"]));
                    $this->signup_verification($user[0]->uc_email,$sub='', $user);
                    $this->user_Auth->insert("book_access", $access);
                    $this->session->set_flashdata('alert','success');
                    $this->session->set_flashdata('message','Transaction Successfully completed.');
                    redirect("thankyou/".base64_encode($transactionID));
                    exit();
                }else{
                    $this->session->set_flashdata('alert','error');
                    $this->session->set_flashdata('message',$charege);
                    redirect("failure");
                    exit();
                }
            }else{

            }
        }else{
            redirect('payment', 'refresh');  
        }  
        

    }
    /*------------------------------------------------------------------
        signup_verification
    ------------------------------------------------------------------*/
    public function signup_verification($to, $subject=false, $data){
        $url = site_url('signIn/activated/?time='.base64_encode($data[0]->uc_id).'&slot='.base64_encode($data[0]->uc_email));
        $html = '<!DOCTYPE html>
                    <html lang="en">
                      <head>
                        <title>'.SITENAME.'</title>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
                        <link rel="stylesheet" href="'.base_url().'/assets/fonts/icomoon/style.css">
                        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
                        <link rel="stylesheet" href="'.base_url().'/assets/css/bootstrap.min.css">
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                        <link rel="stylesheet" href="'.base_url().'/assets/css/style.css">
                        <style>
                        </style>
                      </head>
                      <body>
                        <div class="container">
                            <div class="row">
                                <div style="display: block;height: 80px;width: 100%;text-align: center;padding: 12px;background: #54BFE3;">
                                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;"><strong>'.SITENAME.'</strong></h2>
                                </div>
                                <div style="margin: 30px;display: block; width:100%">
                                    <h6>Hi '.$data[0]->uc_firstname.',</h6>
                                    <p>For activation your account click  <a href="'.$url.'"> me</a></p>
                                </div>
                                <div style="display: block;width: 100%;background: #54bfe3;">
                                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
                                </div>
                            </div>
                        </div>
                      </body>
                    </html>
                    ';
    
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);
        $this->email->to($to);
        $this->email->from('no_reply@esomabooks.com',SITENAME);

        $this->email->subject( (($subject)?$subject:'SignUp Verification') );
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
            return 1;
        }else{
            $this->email->print_debugger();
        }
    }

}