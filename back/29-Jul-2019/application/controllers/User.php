<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->library('upload');
		if(($this->is_logged_in_user_info['u_role'] < 4)){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
	}
	/*------------------------------------
		Dashboard index 
	------------------------------------*/
	public function index()
	{
		$data['title'] = 'Dashboard Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] < 4){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] = $this->user_Auth->get_authorByuid($this->is_logged_in_user);
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-dashboard', $data);
		$this->load->view('front/common/footer', $data);
	}
	/*------------------------------------
		user profile($uid) 
	------------------------------------*/
	public function profile($uid)
	{
		$data['title'] = 'User Profile Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] < 4){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] = $this->user_Auth->get_authorByuid($this->is_logged_in_user);
		$post =$this->input->post();
		if($post){
			$this->form_validation->set_rules('txt_firstname', 'First Name', 'required');
			$this->form_validation->set_rules('txt_lastname', 'Last Name', 'required');
			$this->form_validation->set_rules('txt_email', 'Email Address', 'required');
			$this->form_validation->set_rules('txt_mobile', 'Mobile', 'required');
			if($this->form_validation->run() === TRUE){
				$formData['uc_firstname'] = $post["txt_firstname"];
				$formData['uc_lastname'] = $post["txt_lastname"];
				$formData['uc_email'] = $post["txt_email"];
				$formData['uc_mobile'] = $post["txt_mobile"];
				if($post["txt_password"]){
					$formData['uc_mobile'] = md5($post["txt_password"]);	
				}
				$formData['uc_status'] = (($post["txt_status"] == 1)?'1':'0');
				$formData['uc_modified'] = date('Y-m-d H:i:s');

				$result = $this->user_Auth->exist_emailMobile($post["txt_email"], $post["txt_mobile"], $this->is_logged_in_user);
					if($result["status"] == 'success'){

						if($_FILES['txt_image']['name']){
							$Imgres = $this->do_upload('txt_image');
							
							if(isset($Imgres['upload_data'])){
								$formData['uc_image'] = $Imgres['upload_data']['file_name'];
								
							}else{
								$this->session->set_flashdata('alert' ,'error');
								$this->session->set_flashdata('message' ,$Imgres['error']);
								redirect('user/profile/'.$this->is_logged_in_user);
								exit();
							}
						}
						$resultUpdate = $this->user_Auth->update('user_credentials', $formData, $filter=array('uc_id' => $this->is_logged_in_user) );
						if($resultUpdate){
							$user = $this->user_Auth->getData('user_credentials', $where = array('uc_id' => $this->is_logged_in_user),$se='',$s='');

							$udata["u_id"] = $user[0]->uc_id;
							$udata["u_firstname"] = $user[0]->uc_firstname;
							$udata["u_lastname"] = $user[0]->uc_lastname;
							$udata["u_email"] = $user[0]->uc_email;
							$udata["u_mobile"] = $user[0]->uc_mobile;
							$udata["u_image"] = (($user[0]->uc_image)?$user[0]->uc_image:'user.png');
							$udata["u_role"] = $user[0]->uc_role;
							$udata["u_status"] = $user[0]->uc_status;
							$udata["u_created"] = $user[0]->uc_created;
							$udata["u_modified"] = (($user[0]->uc_modified >0)?$user[0]->uc_modified:'');
							$this->session->set_userdata('is_logged_in_user', $user[0]->uc_id);
							$this->session->set_userdata('is_logged_in_user_info', $udata);
							$this->session->set_flashdata('alert', "success");
							$this->session->set_flashdata('message', 'Profile successfully updated..');
							redirect('user/profile/'.$user[0]->uc_id);
							exit();
						}else{
							$this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,'Update Process Failed.');
							$this->load->view('front/common/header', $data);
							$this->load->view('front/page-userprofile', $data);
							$this->load->view('front/common/footer', $data);
							exit();
						}
						

					}else{
						$this->session->set_flashdata('alert' ,'error');
						$this->session->set_flashdata('message' ,$result["message"]);
						$this->load->view('front/common/header', $data);
						$this->load->view('front/page-userprofile', $data);
						$this->load->view('front/common/footer', $data);
					}

			}else{
				$this->load->view('front/common/header', $data);
				$this->load->view('front/page-userprofile', $data);
				$this->load->view('front/common/footer', $data);
			}
		}else{
			$this->load->view('front/common/header', $data);
			$this->load->view('front/page-userprofile', $data);
			$this->load->view('front/common/footer', $data);
		}
		
	}
	/*-------------------------------------------
		do_upload(name,folder);
	-------------------------------------------*/

	public function do_upload($fileName)
    {
        $config['upload_path']          = './uploads/users/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png';
        $config['max_size']             = 2048;

        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($fileName))
        {
                return array('error' => $this->upload->display_errors());

        }
        else
        {
                return array('upload_data' => $this->upload->data());

        }
    }

    /*-------------------------------------------
		addAuthor();
	-------------------------------------------*/

	public function addAuthor()
    {
        $data['title'] = 'Add New Author Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] != 4)	{
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] = $this->user_Auth->get_authorByuid($this->is_logged_in_user);
		$post = $this->input->post();
		if($post){
			$this->form_validation->set_rules('txt_authorname', 'Author Name', 'required');
			$this->form_validation->set_rules('txt_published', 'Publish Year', 'required|numeric[4]');
			$this->form_validation->set_rules('txt_description', 'Description', 'required');
			if($this->form_validation->run() === TRUE){
				$formData["a_name"] = $post["txt_authorname"];
				$formData["a_year"] = $post["txt_published"];
				$formData["a_description"] = $post["txt_description"];
				$formData["a_created"] = date('Y-m-d H:i:s');
				$formData["a_status"] = '1';
				$formData["a_fK_of_uc_id"] = $this->is_logged_in_user;
				if($post["txt_facebook"]){
					$formData["a_facebook"] = $post["txt_facebook"];
				}
				if($post["txt_twitter"]){
					$formData["a_twitter"] = $post["txt_twitter"];
				}
				if($post["txt_instagram"]){
					$formData["a_instagram"] = $post["txt_instagram"];
				}
				if($post["txt_gplush"]){
					$formData["a_gplush"] = $post["txt_gplush"];
				}
				if($_FILES["txt_image"]["name"]){

					$config['upload_path']          = './uploads/authors/';
		        	$config['allowed_types']        = 'gif|jpg|jpeg|png';
		        	$config['max_size']             = 2048;

        			$this->upload->initialize($config);
			       
					if( ! $this->upload->do_upload('txt_image') ){
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', $this->upload->display_errors());
						redirect("user/addAuthor"); exit();
					}else{
						$dataImg = $this->upload->data();
						$formData["a_image"] = $dataImg["file_name"];
						$instID = $this->user_Auth->insert('authors',$formData);
						if($instID){
							$this->session->set_flashdata('alert', 'success');
							$this->session->set_flashdata('message', 'Successfully save.');
							redirect("user/editAuthor/$instID"); exit();
						}else{
							$this->session->set_flashdata('alert', 'error');
							$this->session->set_flashdata('message', 'Insert Process failed.');
							$this->load->view('front/common/header', $data);
							$this->load->view('front/page-addnewauthor', $data);
							$this->load->view('front/common/footer', $data);
						}
					}
				}else{
					$instID = $this->user_Auth->insert('authors',$formData);
					if($instID){
						$this->session->set_flashdata('alert', 'success');
						$this->session->set_flashdata('message', 'Successfully save.');
						redirect("user"); exit();
					}else{
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', 'Insert Process failed.');
						$this->load->view('front/common/header', $data);
						$this->load->view('front/page-addnewauthor', $data);
						$this->load->view('front/common/footer', $data);
					}
				}
			}else{
				$this->load->view('front/common/header', $data);
				$this->load->view('front/page-addnewauthor', $data);
				$this->load->view('front/common/footer', $data);
			}
		}else{
			$this->load->view('front/common/header', $data);
			$this->load->view('front/page-addnewauthor', $data);
			$this->load->view('front/common/footer', $data);
		}
    }
	/*----------------------------------------------------
		editAuthor(aId)
	-----------------------------------------------------*/
	public function editAuthor($aID){
		$data['title'] = 'Edit Author Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] != 4){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$data["authorAdded"] = $this->user_Auth->get_authorByuid($this->is_logged_in_user);
		if($data["authorAdded"]){

			$post = $this->input->post();
			if($post){
				$this->form_validation->set_rules('txt_authorname', 'Author Name', 'required');
				$this->form_validation->set_rules('txt_published', 'Publish Year', 'required|numeric[4]');
				$this->form_validation->set_rules('txt_description', 'Description', 'required');
				if($this->form_validation->run() === TRUE){
					$formData["a_name"] = $post["txt_authorname"];
					$formData["a_year"] = $post["txt_published"];
					$formData["a_description"] = $post["txt_description"];
					$formData["a_modified"] = date('Y-m-d H:i:s');
					$formData["a_status"] = (($post["txt_status"])?'1':'0');
					if($post["txt_facebook"]){
						$formData["a_facebook"] = $post["txt_facebook"];
					}
					if($post["txt_twitter"]){
						$formData["a_twitter"] = $post["txt_twitter"];
					}
					if($post["txt_instagram"]){
						$formData["a_instagram"] = $post["txt_instagram"];
					}
					if($post["txt_gplush"]){
						$formData["a_gplush"] = $post["txt_gplush"];
					}
					if($_FILES["txt_image"]["name"]){

						$config['upload_path']          = './uploads/authors/';
			        	$config['allowed_types']        = 'gif|jpg|jpeg|png';
			        	$config['max_size']             = 2048;

	        			$this->upload->initialize($config);
				       
						if( ! $this->upload->do_upload('txt_image') ){
							$this->session->set_flashdata('alert', 'error');
							$this->session->set_flashdata('message', $this->upload->display_errors());
							redirect("user/addAuthor"); exit();
						}else{
							$dataImg = $this->upload->data();
							$formData["a_image"] = $dataImg["file_name"];
							$instID = $this->user_Auth->update('authors', $formData, array("a_fK_of_uc_id" => $this->is_logged_in_user));
							if($instID){
								$this->session->set_flashdata('alert', 'success');
								$this->session->set_flashdata('message', 'Successfully save.');
								redirect("user/editAuthor/".$data["authorAdded"][0]->a_id); exit();
							}else{
								$this->session->set_flashdata('alert', 'error');
								$this->session->set_flashdata('message', 'Update Process failed.');
								$this->load->view('front/common/header', $data);
								$this->load->view('front/page-editauthor', $data);
								$this->load->view('front/common/footer', $data);
							}
						}
					}else{
						$instID = $this->user_Auth->update('authors', $formData, array("a_fK_of_uc_id" => $this->is_logged_in_user));
						
						if($instID){
							$this->session->set_flashdata('alert', 'success');
							$this->session->set_flashdata('message', 'Successfully Update.');
							redirect("user/editAuthor/".$data["authorAdded"][0]->a_id); exit();
						}else{
							$this->session->set_flashdata('alert', 'error');
							$this->session->set_flashdata('message', 'Update Process failed.');
							$this->load->view('front/common/header', $data);
							$this->load->view('front/page-editauthor', $data);
							$this->load->view('front/common/footer', $data);
						}
					}
				}else{
					$this->load->view('front/common/header', $data);
					$this->load->view('front/page-editauthor', $data);
					$this->load->view('front/common/footer', $data);
				}
			}else{
				$this->load->view('front/common/header', $data);
				$this->load->view('front/page-editauthor', $data);
				$this->load->view('front/common/footer', $data);
			}
		}else{
			redirect('user/addAuthor'); exit();
		}
	}



	/*-------------------------------------------
		addbook();
	-------------------------------------------*/

	public function addbook()
    {
        $data['title'] = 'Add New Book Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] != 4)	{
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$authorAddedinfo = $this->user_Auth->get_authorByuid($this->is_logged_in_user);
		$data["authorAdded"] = $authorAddedinfo;
		$post = $this->input->post();
		if($post){
			
			$this->form_validation->set_rules('txt_bookname', 'Author Name', 'required');
			$this->form_validation->set_rules('txt_published', 'Publish Year', 'required');
			$this->form_validation->set_rules('txt_category', 'category', 'required');
			// $this->form_validation->set_rules('txt_frontimage', 'Fornt Image', 'required');
			// $this->form_validation->set_rules('txt_bookfile', 'Book File', 'required');
			$this->form_validation->set_rules('txt_rating', 'Rating', 'required');
			if($this->form_validation->run() === TRUE){
				$formData["b_title"] 		= $post["txt_bookname"];
				$formData["b_published"]	= date('Y-m-d H:i:s', strtotime($post["txt_published"]));
				$formData["b_category"] 	= $post["txt_category"];
				$formData["b_rating"] 		= $post["txt_rating"];
				$formData["b_created"] 		= date('Y-m-d H:i:s');
				$formData["b_status"] 		= '1';
				$formData["b_fk_of_aid"] 	= $authorAddedinfo[0]->a_id;
				$formData["b_fk_of_uid"] 	= $this->is_logged_in_user;
				
				if($_FILES["txt_frontimage"]["name"]){

					$config['upload_path']          = './uploads/books/';
		        	$config['allowed_types']        = 'gif|jpg|jpeg|png';
		        	$config['max_size']             = 2048;

        			$this->upload->initialize($config);
			       
					if( ! $this->upload->do_upload('txt_frontimage') ){
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', $this->upload->display_errors());
						redirect("user/addbook"); exit();
					}else{
						$dataImg = $this->upload->data();
						$formData["b_image"] = $dataImg["file_name"];
					}
				}
				if($_FILES["txt_bookfile"]["name"]){

					$configfile['upload_path']          = './uploads/books/';
		        	$configfile['allowed_types']        = 'pdf|doc|docx|ppt|pptx|pdf|txt';
		        	$configfile['max_size']             = 102400;

        			$this->upload->initialize($configfile);
			       
					if( ! $this->upload->do_upload('txt_bookfile') ){
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', $this->upload->display_errors());
						redirect("user/addbook"); exit();
					}else{

						$dataImg = $this->upload->data();
						$formData['b_filetype'] = $dataImg['file_ext'];
						$formData["b_file"] = $dataImg["file_name"];
					}
				}
				$instID = $this->user_Auth->insert('books',$formData);
				if($instID){
					$this->session->set_flashdata('alert', 'success');
					$this->session->set_flashdata('message', 'Successfully save.');
					redirect("user/addbook"); exit();
				}else{
					$this->session->set_flashdata('alert', 'error');
					$this->session->set_flashdata('message', 'Insert Process failed.');
					$this->load->view('front/common/header', $data);
					$this->load->view('front/page-addnewbook', $data);
					$this->load->view('front/common/footer', $data);
				}
					
				
			}else{
				$this->load->view('front/common/header', $data);
				$this->load->view('front/page-addnewbook', $data);
				$this->load->view('front/common/footer', $data);
			}
		}else{
			$this->load->view('front/common/header', $data);
			$this->load->view('front/page-addnewbook', $data);
			$this->load->view('front/common/footer', $data);
		}
    }
    public function viewBooks(){
    	$data['title'] = 'View Book Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] != 4)	{
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$authorAddedinfo = $this->user_Auth->get_authorByuid($this->is_logged_in_user);
		$data["authorAdded"] = $authorAddedinfo;
		$post = $this->input->post();
		$data["bookeslistID"] = $this->user_Auth->getData('books', $w=array('b_fk_of_aid'=> $authorAddedinfo[0]->a_id,'b_status'=>'1'), $se='b_id', $sh='b_id ASC');
		
		$this->load->view('front/common/header', $data);
		$this->load->view('front/page-viewbook', $data);
		$this->load->view('front/common/footer', $data);
		
    }
    /*--------------------------------------
		viewBookDetails()
    --------------------------------------*/
    public function viewBookDetails(){
    	$post = $this->input->post();
    	if($post){
    		$viewID = $post['view_ID'];
    		$book = $this->user_Auth->getData('books', array('b_id' => $viewID),$se ='', $sh='');
    		if($book){
    			$book[0]->b_id;
    			$book[0]->b_fk_of_aid;
    			$book[0]->b_fk_of_uid;
    			$book[0]->b_language;
    			$book[0]->b_title;
    			$book[0]->b_published;
    			$book[0]->b_rating;
    			$book[0]->b_category;
	            $book[0]->b_image;
	            $book[0]->b_filetype;
	            $book[0]->b_file;
	            $book[0]->b_downloads;
	            $book[0]->b_status;
	           	$book[0]->b_created;
	            $book[0]->b_modified;
    			$html = '<div calss="container"><table id="booksdetails" class="table-responsive table-striped">
    			<thead>
    			<tr>
    				<th>Name</th>
    				<th>Details</th>
    			</tr>
    			</thead>
    			<tbody>
    				<tr>
    					<td style="width:200px;"> Book ID</td><td> '.$book[0]->b_id.'</td>
    				</tr>
    				<tr>
    					<td> Book Name</td><td> '.$book[0]->b_title.'</td>
    				</tr>
    				<tr>
    					<td> Book Author Name</td><td> '.$book[0]->b_fk_of_aid.'</td>
    				</tr>
    				<tr>
    					<td> Book Language Type</td><td> '.$book[0]->b_language.'</td>
    				</tr>
    				<tr>
    					<td> Book Rating</td><td> '.$book[0]->b_rating.'</td>
    				</tr>
    				<tr>
    					<td> Book Category </td><td> '.$book[0]->b_category.'</td>
    				</tr>
    				<tr>
    					<td> Book Status</td><td> '.$book[0]->b_status.'</td>
    				</tr>
    				<tr>
    					<td> Book Added DateTime</td><td> '.$book[0]->b_created.'</td>
    				</tr>
    				<tr>
    					<td> Book Status</td><td> '.$book[0]->b_status.'</td>
    				</tr>
    				<tr>
    					<td> Book </td><td> <div style="width:150px;"><a href="'.base_url("uploads/books/".$book[0]->b_file).'"><img src="'.base_url("uploads/books/".$book[0]->b_image).'" class="img-fluid"></a></div></td>
    				</tr>
    			</tbody>
    			</table></div>';
    			echo json_encode(array('status' => 'Success', 'data' => $html,'message' => 'Successfully Done!.'));
    		}else{
    			$html = 'No more data';
    			echo json_encode(array('status' => 'failed', 'data' => $html,'message' => 'No More data!.'));
    		}
    		
    	}else{
    		echo json_encode(array('status' => 'Fail', 'message' => 'In-valid Mehtod!.'));
    	}
    	die();
    }
}

?>