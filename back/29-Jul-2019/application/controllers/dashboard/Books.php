<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->library('upload');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}

	}
	public function index()
	{
		$data["title"] = 'Add New Books Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}else{
			$post = $this->input->post();
			if($post){
				// print_r($post); die;
				// Array ( [txt_name] => b [txt_published] => 2014 [txt_rating] => 1 [txt_category] => 7 )
				$this->form_validation->set_rules('txt_language','Book Language','required');
				$this->form_validation->set_rules('txt_title','Book Name','required');
				$this->form_validation->set_rules('txt_authorid','Author Id','required');
				$this->form_validation->set_rules('txt_publisheddate','Publish Date','required');
				$this->form_validation->set_rules('txt_price','Price','required');
				$this->form_validation->set_rules('txt_category','Category','required');
				if($this->form_validation->run() === TRUE){
					
					$fomdata["b_language"] = $post["txt_language"];
					$fomdata["b_title"] = $post["txt_title"];
					$fomdata["b_published"] = date('Y-m-d',strtotime($post["txt_publisheddate"]));
					// $fomdata["b_rating"] = $post["txt_rating"];
					$fomdata["b_price"] = $post["txt_price"];
					$fomdata["b_category"] = $post["txt_category"];
					$fomdata["b_status"] = '1';
					$fomdata["b_created"] = date("Y-m-d H:i:s");
					$fomdata["b_fk_of_aid"] = $post["txt_authorid"];
					$fomdata["b_fk_of_uid"] = $this->is_logged_in_user;
					// b_name
					// b_published
					// b_rating
					// b_image
					// b_filetype
					// b_file
					// b_status
					// b_created
					// b_modified 
					if($_FILES['txt_image']['name']){

						$img_config['upload_path']          = './uploads/books/';
				        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
				        $img_config['max_size']             = 3072;
				        // $img_config['encrypt_name'] = TRUE;
				        $this->upload->initialize($img_config);
				        if ( ! $this->upload->do_upload('txt_image'))
				        {
				            $this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,$this->upload->display_errors());
							
							redirect('dashboard/addnewBook'); exit();
				        }
				        else
				        {
				            $upload_data = $this->upload->data();
				            $fomdata['b_image'] = $upload_data['file_name'];
				        }
					}
					if($_FILES['bookFile']['name']){

						$config_doc['upload_path']          = './uploads/books/';
				        $config_doc['allowed_types']        = 'movie|mov|web|mflv|avi|mpg|mpeg|wmv|txt|doc|docx|pdf|ppt|pptx|mp4';
				        $config_doc['max_size']             = 800000000;
				        // $config_doc['encrypt_name'] = TRUE;
				        $this->upload->initialize($config_doc);
				        // $this->load->library('upload', $config_doc);

				        if ( ! $this->upload->do_upload('bookFile'))
				        {
				            $this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,$this->upload->display_errors());
							// print_r($fileRes['error']);echo "    ccccccccc"; die;
							redirect('dashboard/addnewBook'); exit();
				        }
				        else
				        {
				        	$upload_fdata = $this->upload->data();
				        	$fomdata['b_filetype'] = $upload_fdata['file_ext'];
							$fomdata['b_file'] = $upload_fdata['file_name'];
				        }
					}

					$insertRes = $this->user_Auth->insert('books', $fomdata);
					if($insertRes){
						$this->session->set_flashdata('alert', 'success');
						$this->session->set_flashdata('message', 'Successfully save.');
						redirect('dashboard/addnewBook'); exit(); 
					}else{
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', 'Insert Failed.');
						$this->load->view('dashboard/common/header',$data);
						$this->load->view('dashboard/page-addnewbook',$data);
						$this->load->view('dashboard/common/footer',$data);
					}
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-addnewbook',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-addnewbook',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
			 
		}
		
	}
	

    /*----------------------------------------------
		viewallBooks()
    ----------------------------------------------*/
    public function viewallBooks($filter=false){
    	$data["title"] = 'View All Books Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($filter == 'english'){
			$data['filtered'] = 'english';
			$data['allBookssids'] = $this->user_Auth->getData('books',$w=array('b_language' =>'english') ,$se='b_id', $short='b_id DESC');
		}else if($filter == 'somali'){
			$data['filtered'] = 'somali';
			$data['allBookssids'] = $this->user_Auth->getData('books',$w=array('b_language' =>'somali') ,$se='b_id', $short='b_id DESC');
		}else{
			
			$data['filtered'] = 'all';
			$data['allBookssids'] = $this->user_Auth->getData('books',$w='' ,$se='b_id', $short='b_id DESC');
		}
		
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-viewallbooks',$data);
		$this->load->view('dashboard/common/footer',$data);
    }


	/*----------------------------------------------
		bookDetails($aid)
    ----------------------------------------------*/
    public function bookDetails($bid){
    	$data["title"] = 'View Book Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($bid){
			$data['bookDetails'] = $this->user_Auth->getData('books',$w=array('b_id' => $bid) ,$se='', $short='');
	    	$this->load->view('dashboard/common/header',$data);
			$this->load->view('dashboard/page-viewbooksdetails',$data);
			$this->load->view('dashboard/common/footer',$data);
		}
		
    }

    /*----------------------------------------------
		editbookDetails($aid)
    ----------------------------------------------*/
    public function editbookDetails($bid){
    	$data["title"] = 'Edit Book Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($bid){
			$data['bookDetails'] = $this->user_Auth->getData('books',$w=array('b_id' => $bid) ,$se='', $short='');
			$post = $this->input->post();
			if($post){
				$this->form_validation->set_rules("txt_language","Language", "required");
				$this->form_validation->set_rules("txt_title","Title", "required");
				$this->form_validation->set_rules("txt_authorid","Author", "required");
				$this->form_validation->set_rules("txt_price","Price", "required");
				$this->form_validation->set_rules("txt_category","Category", "required");
				$this->form_validation->set_rules("txt_publisheddate","Publish date", "required");

				if($this->form_validation->run() === TRUE){
					$formData["b_language"] = $post["txt_language"];
					$formData["b_title"] = $post["txt_title"];
					$formData["b_fk_of_aid"] = $post["txt_authorid"];
					$formData["b_published"] = date('Y-m-d', strtotime($post["txt_publisheddate"]));
					$formData["b_price"] = $post["txt_price"];
					$formData["b_category"] = $post["txt_category"];
					
					
					$formData["b_status"] = (($post["txt_status"]==1)?'1':'0');

					// if($post["txt_created"]){
					// 	$formData["b_created"] = date('Y-m-d H:i:s',strtotime($post["txt_created"]));	
					// }
					
					$formData["b_modified"] = date('Y-m-d H:i:s');
					$updated = $this->user_Auth->update('books',$formData,array('b_id'=>$bid));
					if($_FILES['txt_image']['name']){

							$img_config['upload_path']          = './uploads/books/';
					        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
					        $img_config['max_size']             = 3072;
					        // $img_config['encrypt_name'] = TRUE;
					        
					        $this->upload->initialize($img_config);
					        if ( ! $this->upload->do_upload('txt_image'))
					        {
					            $this->session->set_flashdata('alert' ,'error');
								$this->session->set_flashdata('message' ,$this->upload->display_errors());
								redirect('dashboard/editbookDetails/'.$bid); exit();
					        }
					        else
					        {
					            $upload_data = $this->upload->data();
					            $fomdata_img['b_image'] = $upload_data['file_name'];
					            $updated_2 = $this->user_Auth->update('books',$fomdata_img,array('b_id'=>$bid));
					        }
						}
					if($_FILES['bookFile']['name']){

						$config_doc['upload_path']          = './uploads/books/';
				        $config_doc['allowed_types']        = 'movie|mov|web|mflv|avi|mpg|mpeg|wmv|txt|doc|docx|pdf|ppt|pptx|mp4';
				        $config_doc['max_size']             = 800000000;
				        // $config_doc['encrypt_name'] = TRUE;
				        $this->upload->initialize($config_doc);
				        // $this->load->library('upload', $config_doc);

				        if ( ! $this->upload->do_upload('bookFile'))
				        {
				            $this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,$this->upload->display_errors());
							// print_r($fileRes['error']);echo "    ccccccccc"; die;
							redirect('dashboard/editbookDetails/'.$bid); exit();
				        }
				        else
				        {
				        	$upload_fdata = $this->upload->data();
				        	$fomdata_file['b_filetype'] = $upload_fdata['file_ext'];
							$fomdata_file['b_file'] = $upload_fdata['file_name'];
							$updated_2 = $this->user_Auth->update('books',$fomdata_file,array('b_id'=>$bid));
				        }
					}

					
					if($updated){
						$this->session->set_flashdata('alert' ,'success');
						$this->session->set_flashdata('message' ,'Successfully Updated.');
						redirect('dashboard/editbookDetails/'.$bid); exit();
					}else{
						$this->session->set_flashdata('alert' ,'error');
						$this->session->set_flashdata('message' ,'Book update failed.');
						$this->load->view('dashboard/common/header',$data);
						$this->load->view('dashboard/page-editbook',$data);
						$this->load->view('dashboard/common/footer',$data);
					}
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-editbook',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-editbook',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
	    	
		}
		
    }
    /*-----------------------------------------
		deletebookDetail()
    -----------------------------------------*/
    public function deletebookDetail(){
    	$post = $this->input->post();
    	if($post){
    		$bId = $post["delid"];
    		$delete = $this->user_Auth->delete('books', array('b_id' => $bId));
    		if($delete){
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/viewallBooks'), 'message' => 'Book Delete Successfully Done.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Process Process Failed.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid operation.'));
    	}
    }
    /*------------------------------------------------------
    	addbookbyAjax()
    ------------------------------------------------------*/
    public function addbookbyAjax(){
    	$post = $this->input->post(); 
    	if($post){
    		// print_r($post); die();

			if(empty($post['txt_language'])){
				echo json_encode(array('status' => 300, 'message' => 'Language field is required.')); die();
			}
			if(empty($post['txt_title'])){
				echo json_encode(array('status' => 300, 'message' => 'Title field is required.')); die();
			}
			if(empty($post['txt_publisheddate'])){
				echo json_encode(array('status' => 300, 'message' => 'Published date field is required.')); die();
			}
			if(empty($post['txt_authorid'])){
				echo json_encode(array('status' => 300, 'message' => 'Author field is required.')); die();
			}
			if(empty($post['txt_price'])){
				echo json_encode(array('status' => 300, 'message' => 'Price field is required.')); die();
			}
			if(empty($post['txt_category'])){
				echo json_encode(array('status' => 300, 'message' => 'Category field is required.')); die();
			}
			if(empty($post['text_message'])){
				echo json_encode(array('status' => 300, 'message' => 'Description field is required.')); die();
			}

			$fomdata["b_language"] = $post["txt_language"];
			$fomdata["b_title"] = $post["txt_title"];
			$fomdata["b_published"] = date('Y-m-d',strtotime($post["txt_publisheddate"]));
			// $fomdata["b_rating"] = $post["txt_rating"];
			$fomdata["b_price"] = $post["txt_price"];
			$fomdata["b_category"] = $post["txt_category"];
			$fomdata["b_status"] = '1';
			$fomdata["b_created"] = date("Y-m-d H:i:s");
			$fomdata["b_fk_of_aid"] = $post["txt_authorid"];
			$fomdata["b_fk_of_uid"] = $this->is_logged_in_user;
			$fomdata["b_description"] = $post["text_message"];

			if($_FILES['txt_image']['name']){

				$img_config['upload_path']          = './uploads/books/';
		        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
		        $img_config['max_size']             = 3072;
		        // $img_config['encrypt_name'] = TRUE;
		        $this->upload->initialize($img_config);
		        if ( ! $this->upload->do_upload('txt_image'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            $fomdata['b_image'] = $upload_data['file_name'];
		        }
			}
			if($_FILES['bookFile']['name']){

				$config_doc['upload_path']          = './uploads/books/';
		        $config_doc['allowed_types']        = 'movie|mov|web|mflv|avi|mpg|mpeg|wmv|txt|doc|docx|pdf|ppt|pptx|mp4';
		        $config_doc['max_size']             = 800000000;
		        // $config_doc['encrypt_name'] = TRUE;
		        $this->upload->initialize($config_doc);
		        // $this->load->library('upload', $config_doc);

		        if ( ! $this->upload->do_upload('bookFile'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		        	$upload_fdata = $this->upload->data();
		        	$fomdata['b_file'] = $upload_fdata['file_name'];
		        }
			}

			$insertRes = $this->user_Auth->insert('books', $fomdata);
					
			if($insertRes){
				echo json_encode(array('status' => 200, 'message' => 'Successfully Done!.'));
			}else{
				echo json_encode(array('status' => 100, 'message' => 'Insert Process failed!.'));
			}
    	}else{
    		echo json_encode(array('status' => 100, 'message' => 'In-valid Method!.'));
    	}
    	die();
    }
    /*------------------------------------------------
    	editbookbyAjax()
	------------------------------------------------*/
    public function editbookbyAjax(){
    	$post = $this->input->post(); 
    	if($post){
    		$bookID = $post['txt_id'];
    		if(empty($post['txt_language'])){
				echo json_encode(array('status' => 300, 'message' => 'Language field is required.')); die();
			}
			if(empty($post['txt_title'])){
				echo json_encode(array('status' => 300, 'message' => 'Title field is required.')); die();
			}
			if(empty($post['txt_publisheddate'])){
				echo json_encode(array('status' => 300, 'message' => 'Published date field is required.')); die();
			}
			if(empty($post['txt_authorid'])){
				echo json_encode(array('status' => 300, 'message' => 'Author field is required.')); die();
			}
			if(empty($post['txt_price'])){
				echo json_encode(array('status' => 300, 'message' => 'Price field is required.')); die();
			}
			if(empty($post['txt_category'])){
				echo json_encode(array('status' => 300, 'message' => 'Category field is required.')); die();
			}
			if(empty($post['text_message'])){
				echo json_encode(array('status' => 300, 'message' => 'Description field is required.')); die();
			}
			$fomdata["b_language"] = $post["txt_language"];
			$fomdata["b_title"] = $post["txt_title"];
			$fomdata["b_published"] = date('Y-m-d',strtotime($post["txt_publisheddate"]));
			// $fomdata["b_rating"] = $post["txt_rating"];
			$fomdata["b_price"] = $post["txt_price"];
			$fomdata["b_category"] = $post["txt_category"];
			if($post['txt_status'] == 1){ $fomdata["b_status"] = '1'; }else{	$fomdata["b_status"] = '0'; }
			
			$fomdata["b_modified"] = date("Y-m-d H:i:s");
			$fomdata["b_fk_of_aid"] = $post["txt_authorid"];
			$fomdata["b_fk_of_uid"] = $this->is_logged_in_user;
			$fomdata["b_description"] = $post["text_message"];

			if($_FILES['txt_image']['name']){

				$img_config['upload_path']          = './uploads/books/';
		        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
		        $img_config['max_size']             = 3072;
		        // $img_config['encrypt_name'] = TRUE;
		        $this->upload->initialize($img_config);
		        if ( ! $this->upload->do_upload('txt_image'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            $fomdata['b_image'] = $upload_data['file_name'];
		        }
			}
			if($_FILES['bookFile']['name']){

				$config_doc['upload_path']          = './uploads/books/';
		        $config_doc['allowed_types']        = 'movie|mov|web|mflv|avi|mpg|mpeg|wmv|txt|doc|docx|pdf|ppt|pptx|mp4';
		        $config_doc['max_size']             = 800000000;
		        // $config_doc['encrypt_name'] = TRUE;
		        $this->upload->initialize($config_doc);
		        // $this->load->library('upload', $config_doc);

		        if ( ! $this->upload->do_upload('bookFile'))
		        {
		            echo json_encode(array('status' => 100, 'message' => $this->upload->display_errors())); die();
		        }
		        else
		        {
		        	$upload_fdata = $this->upload->data();
		        	$fomdata['b_file'] = $upload_fdata['file_name'];
		        }
			}

			$updatetRes = $this->user_Auth->update('books', $fomdata, array('b_id' => $bookID));
					
			if($updatetRes){
				echo json_encode(array('status' => 200, 'message' => 'Update successfully Done!.'));
			}else{
				echo json_encode(array('status' => 100, 'message' => 'Update Process failed!.'));
			}
    	}else{
    		echo json_encode(array('status' => 100, 'message' => 'In-valid Method!.'));
    	}
    	die();
    }
}
