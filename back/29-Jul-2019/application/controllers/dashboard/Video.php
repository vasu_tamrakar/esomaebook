<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<strong>Warning: </strong>Access denide!..';
			exit();
		}
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->library('upload');
	}
	public function index()
	{
		$data["title"] = 'Add New Vodeo Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<strong>Warning: </strong>Access denide!..';
		}else{
			$post = $this->input->post();
			if($post){
				

				$this->form_validation->set_rules('txt_title','Video Title','required');
				$this->form_validation->set_rules('txt_description','Descriptions','required');
				
				// if($this->form_validation->run() === TRUE){
					

				// 	$formData['v_title'] = $post["txt_title"];
				// 	$formData['v_description'] = $post["txt_description"];
				// 	$formData['v_created'] = date('Y-m-d H:i:s');
				// 	$formData['v_status'] = '1';

				// 	if($_FILES['txt_video']['name']){
							
				// 			$img_config['upload_path']          = './uploads/videos/';
				// 	        $img_config['allowed_types']        = 'avi|mp4|avi|mkv';
				// 	        $img_config['max_size']             = 800000;
				// 	        // $img_config['encrypt_name'] = TRUE;
					      
				// 	        $this->upload->initialize($img_config);
				// 	        if ( ! $this->upload->do_upload('txt_video'))
				// 	        {
				// 	        	$this->session->set_flashdata('alert' ,'error');
				// 				$this->session->set_flashdata('message' ,$this->upload->display_errors());
				// 				redirect('dashboard/addnewVideo/'); exit();
				// 	        }
				// 	        else
				// 	        {
				// 	            $upload_data = $this->upload->data();
				// 	            $formData['v_file'] = $upload_data['file_name'];
				// 	            $formData['v_extension'] = $upload_data['file_ext'];
				// 	            $instID =  $this->user_Auth->insert('videos',$formData);
				// 				if($instID){
				// 					$this->session->set_flashdata('alert','success');
				// 					$this->session->set_flashdata('message','Video Successfully Added.');
				// 					redirect('dashboard/addnewVideo'); exit();	
				// 				}else{
				// 					$this->session->set_flashdata('alert','success');
				// 					$this->session->set_flashdata('message','Video added failed.');
				// 					redirect('dashboard/addnewVideo'); exit();
				// 				}
				// 	        }
					        
				// 		}
						
				// }else{
				// 	$this->load->view('dashboard/common/header',$data);
				// 	$this->load->view('dashboard/page-addnewvideo',$data);
				// 	$this->load->view('dashboard/common/footer',$data);
				// }
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-addnewvideo',$data);
				$this->load->view('dashboard/common/footer',$data);
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-addnewvideo',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
			 
		}
		
	}
	

    /*----------------------------------------------
		viewallVideos()
    ----------------------------------------------*/
    public function viewallVideos(){
    	$data["title"] = 'View All Videos Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		$data['allVideosids'] = $this->user_Auth->getData('videos',$w='' ,$se='v_id', $short='v_id DESC');
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-viewallvideos',$data);
		$this->load->view('dashboard/common/footer',$data);
    }


	/*----------------------------------------------
		videoDetails($cid)
    ----------------------------------------------*/
    public function videoDetails($vid){
    	$data["title"] = 'View Video Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($vid){
			$data['videoDetails'] = $this->user_Auth->getData('videos',$w=array('v_id' => $vid) ,$se='', $short='');
	    	$this->load->view('dashboard/common/header',$data);
			$this->load->view('dashboard/page-viewvideodetails',$data);
			$this->load->view('dashboard/common/footer',$data);
		}
		
    }

    /*----------------------------------------------
		editvideoDetails($aid)
    ----------------------------------------------*/
    public function editvideoDetails($vid){
    	$data["title"] = 'Edit Video Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($vid){
			$data['videoDetails'] = $this->user_Auth->getData('videos',$w=array('v_id' => $vid) ,$se='', $short='');
			$post = $this->input->post();
			if($post){
				$this->form_validation->set_rules('txt_title','Video Title','required');
				$this->form_validation->set_rules('txt_description','Video Descriptions','required');
				if($this->form_validation->run() === TRUE){
					
					$formData["v_title"] = $post["txt_title"];
					$formData["v_description"] = $post["txt_description"];
					if($post["txt_like"]){
						$formData["v_like"] =$post["txt_like"];	
					}
					if($post["txt_created"]){
						$formData["v_created"] = date('Y-m-d H:i:s',strtotime($post["txt_created"]));	
					}
					$formData["v_status"] = (($post["txt_status"] == 1)?'1':'0');
					$formData['v_modified'] = date('Y-m-d H:i:s');
					$resultUpdate = $this->user_Auth->update('videos', $formData, $filter=array('v_id' => $vid) );
					if($_FILES['txt_video']['name']){
						$img_config['upload_path']          = './uploads/videos/';
				        $img_config['allowed_types']        = 'avi|mp4|avi|mkv';
				        $img_config['max_size']             = 800000;
				        // $img_config['encrypt_name'] = TRUE;
				      
				        $this->upload->initialize($img_config);
				        if ( ! $this->upload->do_upload('txt_video'))
				        {
				        	$this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,$this->upload->display_errors());
							redirect('dashboard/editvideoDetails/'.$vid); exit();
				        }
				        else
				        {
				            $upload_data = $this->upload->data();
				            $formData2['v_file'] = $upload_data['file_name'];
				            $formData2['v_extension'] = $upload_data['file_ext'];
				            $resultUpdate = $this->user_Auth->update('videos', $formData2, $filter=array('v_id' => $vid) );
				        }
					}
					
					if($resultUpdate){
						$this->session->set_flashdata('alert', "success");
						$this->session->set_flashdata('message', 'Video successfully updated..');
						redirect('dashboard/editvideoDetails/'.$vid);
						exit();
					}else{
						$this->session->set_flashdata('alert' ,'error');
						$this->session->set_flashdata('message' ,'Update Process Failed.');
						$this->load->view('dashboard/common/header', $data);
						$this->load->view('dashboard/page-editvideo', $data);
						$this->load->view('dashboard/common/footer', $data);
						exit();
					}
			
				}else{
					$this->load->view('dashboard/common/header', $data);
					$this->load->view('dashboard/page-editvideo', $data);
					$this->load->view('dashboard/common/footer', $data);
				}
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-editvideo',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
	    	
		}
		
    }
    
    /*----------------------------------------------
		addvideoAjax()
    ----------------------------------------------*/
    public function addvideoAjax(){
    	$post = $this->input->post();
    	if($post){

    		if(empty($post["txt_language2"])){
    			echo json_encode(array("status" => 400, "message" => "Lanaguage is field empty.")); die();
    		}
    		if(empty($post["txt_category"])){
    			echo json_encode(array("status" => 400, "message" => "Category is field empty.")); die();
    		}
    		if(empty($post["txt_title"])){
    			echo json_encode(array("status" => 400, "message" => "Title is field empty.")); die();
    		}
    		if(empty($post["txt_videoid"])){
    			echo json_encode(array("status" => 400, "message" => "Video is field empty.")); die();
    		}
    		if(empty($post["text_message"])){
    			echo json_encode(array("status" => 400, "message" => "Lanaguage is field empty.")); die();
    		}

    		$FormData["v_language"] = $post["txt_language2"];
    		$FormData["v_category"] = $post["txt_category"];
    		$FormData["v_title"] = $post["txt_title"];
    		$FormData["v_videoid"] = $post["txt_videoid"];
    		if((isset($post["txt_premium"])) && ($post["txt_premium"])){
    			$FormData["v_premium"] = 'true';
    		}else{
    			$FormData["v_premium"] = 'false';
    		}
    		$FormData["v_description"] = $post["text_message"];

    		if($_FILES["txt_image"]["name"]){
    			$img_config['upload_path']          = './uploads/videos/';
		        $img_config['allowed_types']        = 'png|jpg|jpeg|gif';
		        $img_config['max_size']             = 102400;
		        $img_config['max_width']             = 500;
		        $img_config['max_height']             = 500;
				        // $img_config['encrypt_name'] = TRUE;
				      
		        $this->upload->initialize($img_config);
		        if ( ! $this->upload->do_upload('txt_image'))
		        {
		        	
		        	echo json_encode(array("status" => 400, "message" => $this->upload->display_errors()));
		        	die();
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            $FormData['v_image'] = $upload_data['file_name'];
		        }
    		}
    		
    		$FormData["v_status"] = "1";
    		$FormData["v_created"] = date("Y-m-d H:i:s");
    		$insertID = $this->user_Auth->insert('videos',$FormData);
    		if($insertID){
    			echo json_encode(array("status" => 200, "message" => "Inserted Successfully Done!."));
			}else{
				echo json_encode(array("status" => 100, "message" => "Insert process failed!."));
			}
    	}else{
    		echo json_encode(array("status" => 100, "message" => "In-valid Method!."));
    	}
    	die();
    }

    

     /*----------------------------------------------
		editvideoAjax()
    ----------------------------------------------*/
    public function editvideoAjax(){
    	$post = $this->input->post();
    	if($post){
    		$vid = $post["txt_id"];
    		if(empty($post["txt_language2"])){
    			echo json_encode(array("status" => 400, "message" => "Lanaguage is field empty.")); die();
    		}
    		if(empty($post["txt_category"])){
    			echo json_encode(array("status" => 400, "message" => "Category is field empty.")); die();
    		}
    		if(empty($post["txt_title"])){
    			echo json_encode(array("status" => 400, "message" => "Title is field empty.")); die();
    		}
    		if(empty($post["txt_videoid"])){
    			echo json_encode(array("status" => 400, "message" => "Video is field empty.")); die();
    		}
    		if(empty($post["text_message"])){
    			echo json_encode(array("status" => 400, "message" => "Lanaguage is field empty.")); die();
    		}

    		$FormData["v_language"] = $post["txt_language2"];
    		$FormData["v_category"] = $post["txt_category"];
    		$FormData["v_title"] = $post["txt_title"];
    		$FormData["v_videoid"] = $post["txt_videoid"];
    		if((isset($post["txt_premium"])) && ($post["txt_premium"])){
    			$FormData["v_premium"] = 'true';
    		}else{
    			$FormData["v_premium"] = 'false';
    		}
    		$FormData["v_description"] = $post["text_message"];

    		
    		if($_FILES["txt_image"]["name"]){
    			$img_config['upload_path']          = './uploads/videos/';
		        $img_config['allowed_types']        = 'png|jpg|jpeg|gif';
		        $img_config['max_size']             = 102400;
		        $img_config['max_width']             = 500;
		        $img_config['max_height']             = 500;
				        // $img_config['encrypt_name'] = TRUE;
				      
		        $this->upload->initialize($img_config);
		        if ( ! $this->upload->do_upload('txt_image'))
		        {
		        	
		        	echo json_encode(array("status" => 400, "message" => $this->upload->display_errors()));
		        	die();
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            $FormData['v_image'] = $upload_data['file_name'];
		        }
    		}
    		
    		$FormData["v_status"] = (isset($post["txt_status"])?'1':'0');
    		$FormData["v_modified"] = date("Y-m-d H:i:s");

    		$insertID = $this->user_Auth->update('videos',$FormData, array("v_id" => $vid));
    		if($insertID){
    			echo json_encode(array("status" => 200, "message" => "Inserted Successfully Done!."));
			}else{
				echo json_encode(array("status" => 100, "message" => "Insert process failed!."));
			}
    	}else{
    		echo json_encode(array("status" => 100, "message" => "In-valid Method!."));
    	}
    	die();
    }

    /*----------------------------------------------
		deleteVideo();
    ----------------------------------------------*/
    public function deleteVideo(){
    	$post = $this->input->post();
    	if($post){
    		$vId = $post["delid"];
    		$delete = $this->user_Auth->delete('videos', array('v_id' => $vId));
    		if($delete){
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/viewallVideos'), 'message' => 'Video Delete Successfully Done.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Process Process Failed.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid operation.'));
    	}
    }
    /*------------------------------------------
		viewvideoDetailsAjax()
    ------------------------------------------*/
    public function viewvideoDetailsAjax(){
    	$post = $this->input->post();
    	if($post){
    		$vid = $post['vid'];
    		$videoDet = $this->user_Auth->getDeta('videos', array('v_id'=>$vid));
    		if($videoDet){
    			$videoDet[0]->v_id;
    			$videoDet[0]->v_language;
    			$videoDet[0]->v_category;
    			$videoDet[0]->v_title;
    			$videoDet[0]->v_premium;
    			$videoDet[0]->v_description;
    			$videoDet[0]->v_image;
    			$videoDet[0]->v_videoid;
    			$videoDet[0]->v_like;
    			$videoDet[0]->v_status;
    			$videoDet[0]->v_created;
    			$videoDet[0]->v_modified;

    			$html = 'ssd';
    			echo json_encode(array('status'=> 200, 'data' => $html, 'message' => 'In-valid Data!.'));
    		}else{
    			echo json_encode(array('status'=> 100,'message' => 'No more Data!.'));
    		}
    	}else{
    		echo json_encode(array('status'=> 100,'message' => 'In-valid Data!.'));
    	}
    	die();
    }
}


