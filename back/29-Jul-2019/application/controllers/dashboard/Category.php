<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		
	}
	public function index()
	{
		$data["title"] = 'Add New Category Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}else{
			$post = $this->input->post();

			if($post){
				// Array ( [txt_categoryname] => va [txt_catdescription] => sd ) 
				$this->form_validation->set_rules('txt_categoryname','Category Name','required');
				if($this->form_validation->run() == TRUE){
					$fomdata["c_name"] = $post["txt_categoryname"];
					$fomdata["c_description"] = $post["txt_catdescription"];
					$fomdata["c_status"] = '1';
					$fomdata["c_created"] = date("Y-m-d H:i:s");

					if($_FILES['txt_image']['name']){
						$Imgres = $this->do_upload('txt_image');
							
						if(isset($Imgres['upload_data'])){
							$fomdata['c_image'] = $Imgres['upload_data']['file_name'];
							
						}else{
							$this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,$Imgres['error']);
							redirect('dashboard/addnewCategory'); exit();
							exit();
						}
					}
					$insertRes = $this->user_Auth->insert('categories', $fomdata);
					if($insertRes){
						$this->session->set_flashdata('alert', 'success');
						$this->session->set_flashdata('message', 'Successfully save.');
						redirect('dashboard/addnewCategory'); exit(); 
					}else{
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', 'Insert Failed.');
						$this->load->view('dashboard/common/header',$data);
						$this->load->view('dashboard/page-addnewcategory',$data);
						$this->load->view('dashboard/common/footer',$data);
					}
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-addnewcategory',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-addnewcategory',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
			 
		}
		
	}
	/*-------------------------------------------
		do_upload(name,folder);
	-------------------------------------------*/

	public function do_upload($fileName)
    {
        $config['upload_path']          = './uploads/category/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2048;
        $config['max_width']             = 261;
        $config['mix_height']             = 398;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($fileName))
        {
                return array('error' => $this->upload->display_errors());

        }
        else
        {
                return array('upload_data' => $this->upload->data());

        }
    }

    /*----------------------------------------------
		viewallCategory()
    ----------------------------------------------*/
    public function viewallCategory(){
    	$data["title"] = 'View All Category Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		$data['allcategoryids'] = $this->user_Auth->getData('categories ',$w='' ,$se='c_id', $short='c_id DESC');
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-viewallcategories',$data);
		$this->load->view('dashboard/common/footer',$data);
    }


	/*----------------------------------------------
		categoryDetails($aid)
    ----------------------------------------------*/
    public function categoryDetails($cid){

    	$data["title"] = 'View Category Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($cid){
			$data['categoryDetails'] = $this->user_Auth->getData('categories',$w=array('c_id' => $cid) ,$se='', $short='');
	    	$this->load->view('dashboard/common/header',$data);
			$this->load->view('dashboard/page-viewcategorydetails',$data);
			$this->load->view('dashboard/common/footer',$data);
		}
		
    }

    /*----------------------------------------------
		editCategoryDetails($cid)
    ----------------------------------------------*/
    public function editCategoryDetails($cid){
    	$data["title"] = 'Edit Category Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($cid){
			$data['categoryDetails'] = $this->user_Auth->getData('categories',$w=array('c_id' => $cid) ,$se='', $short='');
			$post = $this->input->post();
			if($post){
				$this->form_validation->set_rules('txt_categoryname','Category Name ','required');
				if($this->form_validation->run() === TRUE){

				
					$formData["c_name"] = $post["txt_categoryname"];
					$formData["c_description"] = $post["txt_catdescription"];

					$formData["c_status"] = (($post["txt_categorystatus"]==1)?'1':'0');

					if($post["txt_createddate"]){
						$formData["c_created"] = date('Y-m-d H:i:s',strtotime($post["txt_createddate"]));	
					}
					
					$formData["c_modified"] = date('Y-m-d H:i:s');
					if($_FILES['txt_image']['name']){
						if($_FILES['txt_image']['name']){
							$Imgres = $this->do_upload('txt_image');
								
							if(isset($Imgres['upload_data'])){
								$formData['c_image'] = $Imgres['upload_data']['file_name'];
								
							}else{
								$this->session->set_flashdata('alert' ,'error');
								$this->session->set_flashdata('message' ,$Imgres['error']);
								redirect('dashboard/editcategoryDetails/'.$cid);
								exit();
							}
						}
					}
					$updated = $this->user_Auth->update('categories',$formData,array('c_id'=>$cid));
					if($updated){
						$this->session->set_flashdata('alert' ,'success');
						$this->session->set_flashdata('message' ,'Successfully Updated.');
						redirect('dashboard/editcategoryDetails/'.$cid); exit();
					}else{
						$this->session->set_flashdata('alert' ,'error');
						$this->session->set_flashdata('message' ,'Author update failed.');
						$this->load->view('dashboard/common/header',$data);
						$this->load->view('dashboard/page-editcategory',$data);
						$this->load->view('dashboard/common/footer',$data);
					}
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-editcategory',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-editcategory',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
	    	
		}
		
    }
    /*-----------------------------------------
    	deletecategoryDetail() 
    -----------------------------------------*/
    public function deletecategoryDetail(){
    	$post = $this->input->post();
    	if($post){
    		$cId = $post["delid"];
    		$delete = $this->user_Auth->delete('categories', array('c_id' => $cId));
    		if($delete){
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/viewallCategory'), 'message' => 'Category Delete Successfully Done.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Process Process Failed.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid operation.'));
    	}
    }
    
}
