<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ResetPassword extends CI_Controller {
	public function __construct (){
	 	parent :: __construct();

	 	//$this->session->session_destroy();
		if($this->session->userdata('is_logged_in_user')){
			redirect(base_url());  exit();
		}
		$this->load->model('user_Auth');
	}
	public function index()
	{
		$data["title"] = 'Reset Password Page | '.SITENAME;
		$get = $this->input->get();
		if($get){

			$num  = base64_decode($get["data"]);
			$email = base64_decode($get["s"]);
			$retData = $this->user_Auth->getData('user_credentials',$where=array('uc_email'=>$email,'uc_secrete'=>$num,'uc_status'=>'0'),$sel='',$short='');
			

			if($retData){
				$data["email"] = $email;
				$data["num"] = $num;
				$this->load->view('front/common/header',$data);
				$this->load->view('front/page-resetpassword',$data);
				$this->load->view('front/common/footer',$data);
			}else{
				$this->load->view('front/common/header',$data);
				$this->load->view('front/page-resetpassword',$data);
				$this->load->view('front/common/footer',$data);
			}
		}

	}
	public function done()
	{
		$post = $this->input->post();

		if($post['txt_email'] && $post['txt_id']){
			$this->form_validation->set_rules('txt_password','Password','required');
			if($this->form_validation->run() === TRUE){
				$email = $post['txt_email'];
				$num = $post['txt_id'];
				$pass = md5($post['txt_password']);
				$retData = $this->user_Auth->getData('user_credentials',$where=array('uc_email'=>$email,'uc_secrete'=>$num,'uc_status'=>'0'),$sel='',$short='');
				if($retData){
					$updateData = $this->user_Auth->update('user_credentials',$data=array('uc_password' => $pass,'uc_secrete'=>'','uc_status'=>'1'),$filter=array('uc_email'=>$email, 'uc_secrete' => $num));
					$this->resetpassNotification($email,$subject='',$retData);
					$this->session->set_flashdata('alert','success');
					$this->session->set_flashdata('message','Password Reset Successfully.');
					redirect('signIn'); exit();
				}
			}else{
				$this->session->set_flashdata('alert','error');
				$this->session->set_flashdata('message','Please enter new password.');
				$url = site_url('resetPassword/?data=').base64_encode($post['txt_id']).'&s='.base64_encode($post['txt_email']);
				redirect($url); exit();
			}
			
			
		}else{
			$this->session->set_flashdata('alert','error');
			$this->session->set_flashdata('message','Please contact to administrator.');
			$url = site_url('resetPassword/?data=').base64_encode($post['txt_id']).'&s='.base64_encode($post['txt_email']);
			redirect($url); exit();
		}
	}
	public function resetpassNotification($to,$subject=false,$user){
		$html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>Reset Password Of '.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block; width:100%">
					                    <h6>Hi '.$user[0]->uc_firstname.',</h6>
					                	<p>Your Password Successfully Reset.</p>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		$subject = (($subject)?$subject:'Reset Password Of '.SITENAME);
		$this->email->to($to);
        $this->email->from('pareshnagar87@gmail.com', SITENAME);
        $this->email->subject($subject);
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
        	return 1;
        }else{
    		$this->email->print_debugger();
    	}
    	die();
	}
}
