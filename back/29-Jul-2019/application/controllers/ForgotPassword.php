<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ForgotPassword extends CI_Controller {
	public function __construct (){
	 	parent :: __construct();

	 	//$this->session->session_destroy();
		if($this->session->userdata('is_logged_in_user')){
			redirect(base_url());  exit();
		}
		$this->load->model('user_Auth');
	}
	public function index()
	{
		$data["title"] = 'Forgot Password Page | '.SITENAME;

		$post = $this->input->post();
		/* Check Form from post*/
		if($post){
			$this->form_validation->set_rules('txt_email','Email','required|valid_email');
			/* Check validation */
			if($this->form_validation->run() === TRUE){
				
				$formData["uc_email"] = $post["txt_email"];
				
				$stringenrarted = $this->user_Auth->ForgotPassword($formData["uc_email"]);
				/* Check login */
				if($stringenrarted){
					$user = $this->user_Auth->getData('user_credentials',array('uc_email' =>$formData["uc_email"]),$sel ='',$sh='');
					$name = (($user[0]->uc_firstname)?$user[0]->uc_firstname:'');
					
			        // $url = site_url('resetPassword/?data='.base64_encode($stringenrarted).'&s='.base64_encode($user[0]->uc_email));
			        
			        // $html = '<p>Hi '.((isset($user[0]->uc_firstname))?$user[0]->uc_firstname:'').', </p> <p>';
			        
					$html = '<!DOCTYPE html>
						<html lang="en">
						    <head>
						        <title>Forgot Password Of '.SITENAME.'</title>
						        <meta charset="utf-8">
						        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
						        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
						        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
						        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
						        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
						        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
						        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
						        <style>
						        </style>
						    </head>
						    <body>
						        <div class="container">
						            <div class="row">
						                <div style="background: #54bfe3;width: 100%;">
						                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
						                </div>
						                <div style="margin: 30px;display: block; width:100%">
						                    <h6>Hi '.((isset($user[0]->uc_firstname))?$user[0]->uc_firstname:'').',</h6>
					                		<p>Click <a href="'.site_url('resetPassword/?data='.base64_encode($stringenrarted).'&s='.base64_encode($user[0]->uc_email)).'">me</a> for reset password of ebook.
						                </div>
						                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
						                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
						                </div>
						            </div>
						        </div>
						    </body>
						</html>';
			        $config['protocol'] 	= 'smtp';
			        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
			        $config['smtp_port'] 	= '465';
			        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
			        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
			        $config['mailtype'] 	= 'html';
			        $config['charset'] 		= 'utf-8';
			        $config['newline'] 		= "\r\n";
			        $config['wordwrap'] 	= TRUE;
					$this->email->initialize($config);
					$this->email->set_mailtype("html");
					$this->email->set_newline("\r\n");

					$this->email->to($formData["uc_email"]);
			        $this->email->from('pareshnagar87@gmail.com', SITENAME);
			        $this->email->subject('Forgot Password Link of '.SITENAME);
			        $this->email->message($html);
			        $ret = $this->email->send();

			        if($ret){
			        	$this->session->set_flashdata('alert', 'success');
						$this->session->set_flashdata('message', 'Check the registered mail.');
			        	redirect('signIn'); exit();
			        }else{
			        	$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', 'Mail sending failed.');
						$this->load->view('front/common/header',$data);
						$this->load->view('front/page-forgotpassword',$data);
						$this->load->view('front/common/footer',$data);
			        }
					
					
				}
			}else{	
				$this->load->view('front/common/header',$data);
				$this->load->view('front/page-forgotpassword',$data);
				$this->load->view('front/common/footer',$data);
			}
		}else{
			$this->load->view('front/common/header',$data);
			$this->load->view('front/page-forgotpassword',$data);
			$this->load->view('front/common/footer',$data);	
		}
		
	}
}
