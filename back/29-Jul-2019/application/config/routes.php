<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'front';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['dashboard'] = 'dashboard/dashboard';
$route['dashboard/homeSlider'] = 'dashboard/dashboard/homeSlider';
$route['dashboard/addNewSlide'] = 'dashboard/dashboard/addNewSlide';
$route['dashboard/deleteslideDetail'] = 'dashboard/dashboard/deleteslideDetail';
$route['dashboard/getSlideData'] = 'dashboard/dashboard/getSlideData';
$route['dashboard/editSlideDetail'] = 'dashboard/dashboard/editSlideDetail';

$route['dashboard/profile/(:num)'] = 'dashboard/profile/index/$1';


$route['dashboard/addnewAuthor'] = 'dashboard/author/index';
$route['dashboard/addAuthor'] = 'dashboard/author/addAuthor';
$route['dashboard/editAjaxAuthor'] = 'dashboard/author/editAjaxAuthor';



$route['dashboard/viewallAuthors'] = 'dashboard/author/viewallAuthors';
$route['dashboard/authorDetails/(:num)'] = 'dashboard/author/authorDetails/$1';
$route['dashboard/editauthorDetails/(:num)'] = 'dashboard/author/editauthorDetails/$1';
$route['dashboard/deleteAuthor'] = 'dashboard/author/deleteAuthor';

/* Book */
$route['dashboard/addnewBook'] = 'dashboard/books/index';
$route['dashboard/viewallBooks'] = 'dashboard/books/viewallBooks';
$route['dashboard/viewallBooks/(:any)'] = 'dashboard/books/viewallBooks/$1';
$route['dashboard/deletebookDetail'] = 'dashboard/books/deletebookDetail/';
$route['dashboard/bookDetails/(:num)'] = 'dashboard/books/bookDetails/$1';
$route['dashboard/editbookDetails/(:num)'] = 'dashboard/books/editbookDetails/$1';

$route['dashboard/video/addbookbyAjax'] = 'dashboard/books/addbookbyAjax';
$route['dashboard/video/editbookbyAjax'] = 'dashboard/books/editbookbyAjax';



/* Category */
$route['dashboard/addnewCategory'] = 'dashboard/category/index';
$route['dashboard/viewallCategory'] = 'dashboard/category/viewallCategory';
$route['dashboard/categoryDetails/(:num)'] = 'dashboard/category/categoryDetails/$1';
$route['dashboard/editcategoryDetails/(:num)'] = 'dashboard/category/editcategoryDetails/$1';
$route['dashboard/deletecategoryDetail'] = 'dashboard/category/deletecategoryDetail';

/* USERS */
$route['dashboard/addnewUser'] = 'dashboard/user/index';
$route['dashboard/viewallUsers'] = 'dashboard/user/viewallUsers';
$route['dashboard/userDetails/(:num)'] = 'dashboard/user/userDetails/$1';
$route['dashboard/edituserDetails/(:num)'] = 'dashboard/user/edituserDetails/$1';
$route['dashboard/deleteuserDetail'] = 'dashboard/user/deleteuserDetail';

/*  Subscription  */

$route['dashboard/addnewPlan'] = 'dashboard/plan/index';
$route['dashboard/viewallPlans'] = 'dashboard/plan/viewallPlans';
$route['dashboard/editplanDetails/(:num)'] = 'dashboard/plan/editplanDetails/$1';
$route['dashboard/deleteplanDetail'] = 'dashboard/plan/deleteplanDetail/';

/* Video */

$route['dashboard/addnewVideo'] = 'dashboard/Video/index';
$route['dashboard/viewallVideos'] = 'dashboard/Video/viewallVideos';
$route['dashboard/editvideoDetails/(:num)'] = 'dashboard/Video/editvideoDetails/$1';
$route['dashboard/videoDetails/(:num)'] = 'dashboard/ideo/videoDetails/$1';

/*---------------
|	Fornt
---------------*/
$route['category/(:any)'] = 'category/category/$1';
$route['category/(:any)/(:num)'] = 'category/category/$1/$1';
$route['categories/(:any)/(:any)'] = 'category/categories/$1/$1';

$route['subscribe']  = 'front/unsubscribeMail';

$route['thankyou/(:any)'] = 'thankyou/index/$1';