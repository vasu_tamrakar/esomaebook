<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/somali_authors_bg.png'); ?>);">
    <div class="container">
        <div class="row">
        <div class="bannerheading"><h1>Somali Authors</h1></div>
        </div>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Authors</li>
          </ol>
        </nav>

    </div>
    
</div>


    <section class="site-section bg-white" id="contact-section">
      <div class="container">
        <!-- <div class="row mb-5">
          <div class="col-12 text-center">
            <br/>
            <br/>
            <h2 class="section-title mb-3">Membership plans</h2>
            <h3 class="section-sub-title">over 3.5 millions ebooks Read with no limit...</h3>
          </div>
        </div> -->
        <div class="row">
  
            <div class="col-md-3">
                <div class="authorImg">
                    <img src="./assets/images/Somali_authors_3.png" class="img-fluid">
                </div>    
            </div>
            <div class="col-md-9">
                <div class="authordetails">
                    <h2 class="heading">Jamila Gordon </h2>
                    <h6>published books 1970 </h6>
                    <p>W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.
                    </p>
                    <p>W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.
                    </p>
                </div>


                <div class="col-lg-12">
                    <div>
                        <h4 class="heading">Books of Jamila Gardon</h4>
                        <div class="socialicondiv">
                            <a href="https://facebook.com" target="_blank" class="facebook"></a>
                            <a href="https://twitter.com" target="_blank" class="twitter"></a>
                            <a href="https://googleplush.com" target="_blank" class="googleplush"></a>
                            <a href="https://instagram.com" target="_blank" class="instagram"></a>
                        </div>
                    </div>
                    <div class="border-top pt-4"></div>
                        <div id ="author" class="authors owl-carousel owl-theme">
                            <div class="item">
                                <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                                  </div>
                                  <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Abandoned </label>
                                    <span> 24 April 2018 </span>
                                  </div>
                                  <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('2'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Gear </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('2'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_5.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('3'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>People of the raven </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('3'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_4.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('4'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>war & space </label>
                                    <span>  24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('4'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                                <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                                  </div>
                                  <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Abandoned </label>
                                    <span> 24 April 2018 </span>
                                  </div>
                                  <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Gear </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_5.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>People of the raven </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_4.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>war & space </label>
                                    <span>  24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            
                        </div>

                  </div>
     
            </div>
            <div class="border-top pt-4"></div>
        </div>
        <div class="row">
  
            <div class="col-md-3">
                <div class="authorImg">
                    <img src="./assets/images/Somali_authors_2.png" class="img-fluid">
                </div>    
            </div>
            <div class="col-md-9">
                <div class="authordetails">
                    <h2 class="heading">Almal Aden </h2>
                    <h6>published books 1570 </h6>
                    <p>W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.
                    </p>
                    <p>W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.
                    </p>
                </div>


                <div class="col-lg-12">
                    <div>
                        <h4 class="heading">Books of Almal Aden</h4>
                        <div class="socialicondiv">
                            <a href="https://facebook.com" target="_blank" class="facebook"></a>
                            <a href="https://twitter.com" target="_blank" class="twitter"></a>
                            <a href="https://googleplush.com" target="_blank" class="googleplush"></a>
                            <a href="https://instagram.com" target="_blank" class="instagram"></a>
                        </div>
                    </div>
                    <div class="border-top pt-4"></div>
                        <div id ="author" class="authors owl-carousel owl-theme">
                            <div class="item">
                                <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                                  </div>
                                  <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Abandoned </label>
                                    <span> 24 April 2018 </span>
                                  </div>
                                  <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_3.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('2'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Gear </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('2'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_5.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('3'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>People of the raven </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('3'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_6.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('4'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>war & space </label>
                                    <span>  24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('4'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                                <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                                  </div>
                                  <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Abandoned </label>
                                    <span> 24 April 2018 </span>
                                  </div>
                                  <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_3.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('2'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Gear </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('2'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_5.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('3'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>People of the raven </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('3'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_6.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('4'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>war & space </label>
                                    <span>  24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('4'); ?>">READ NOW</a></div>
                            </div>
                            
                        </div>

                  </div>
     
            </div>
            <div class="border-top pt-4"></div>
        </div>
        <div class="row">
  
            <div class="col-md-3">
                <div class="authorImg">
                    <img src="./assets/images/Somali_authors_1.png" class="img-fluid">
                </div>    
            </div>
            <div class="col-md-9">
                <div class="authordetails">
                    <h2 class="heading">Edmonton </h2>
                    <h6>published books 1970 </h6>
                    <p>W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.
                    </p>
                    <p>W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2019 by Refsnes Data. All Rights Reserved.
                    </p>
                </div>


                <div class="col-lg-12">
                    <div>
                        <h4 class="heading">Books of Edmonton</h4>
                        <div class="socialicondiv">
                            <a href="https://facebook.com" target="_blank" class="facebook"></a>
                            <a href="https://twitter.com" target="_blank" class="twitter"></a>
                            <a href="https://googleplush.com" target="_blank" class="googleplush"></a>
                            <a href="https://instagram.com" target="_blank" class="instagram"></a>
                        </div>
                    </div>
                    <div class="border-top pt-4"></div>
                        <div id ="author" class="authors owl-carousel owl-theme">
                            <div class="item">
                                <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                                  </div>
                                  <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Abandoned </label>
                                    <span> 24 April 2018 </span>
                                  </div>
                                  <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('2'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Gear </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('2'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_5.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('3'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>People of the raven </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('3'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_4.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url('4'); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>war & space </label>
                                    <span>  24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url('4'); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                                <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                                  </div>
                                  <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Abandoned </label>
                                    <span> 24 April 2018 </span>
                                  </div>
                                  <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>Gear </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_5.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>People of the raven </label>
                                    <span> 24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="item">
                              <div class="featurebooks">
                                    <a href="<?php echo site_url(); ?>">
                                        <img src="<?php echo base_url('assets/images/book_4.png'); ?>" alt="Image" class="img-fluid">
                                    </a>
                                    <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                                </div>
                                <div class="describe">
                                    <div class="rates">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                      <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <label>war & space </label>
                                    <span>  24 April 2018 </span>
                                </div>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            
                        </div>

                  </div>
            </div>
            <div class="border-top pt-4"></div>
        </div>
        <div class="moreAuthor"><center><button type="button" id="moreAuthor" data="0" num="0"> More Author</button></center></div>
      </div>
    </section>
<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 328px;
    height: calc(40vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar{
    background: #54bfe3;
}
.sticky-wrapper.is-sticky .site-navbar ul li a {
    color: #505048c4 !important;
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}


.bannerheading {
    margin: 0 auto;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    top: 130px;
    font-size: 55px;
}
.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
.authorImg {
    box-shadow: 0px 1px 5px 0px black;
}
h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}
.authordetails .h6, h6 {
    font-size: 14px;
}
.authordetails p {
font-size: 14px;
margin: 1rem 0;
}
h4.heading {
    color: #757575bd;
    display: block;
    font-weight: 600;
    font-size: 23px;
    font-family: inherit;
}
.socialicondiv {
    width: 157px;
    position: absolute;
    right: 15px;
    top: -6px;
    display: block;
}
.socialicondiv a {
    background-repeat: no-repeat;
    background-size: cover;
    width: 36px;
    height: 36px;
    background-position: center;
    display: inline-block;
}
.socialicondiv a.facebook{
    background-image: url(./assets/images/Somali_authors_social_fb.png);
}
.socialicondiv a.twitter{
    background-image: url(./assets/images/Somali_authors_social_tw.png);
}
.socialicondiv a.googleplush{
    background-image: url(./assets/images/Somali_authors_social_g+.png);
}
.socialicondiv a.instagram{
    background-image: url(./assets/images/Somali_authors_social_insta.png);
}

.authors .item {
    margin: 8px;
}

/*.authors .owl-nav {
    position: absolute;
    top: 107px;
    width: auto;
}
.authors .owl-prev {
    height: 24px;
    width: 24px;
    background-image: url(./assets/images/button_previous_2.png) !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
    display: inline-block !important;
    float: left;
    margin: 0px 0px 0px -25px !important;
}
.authors .owl-next {
    height: 24px;
    width: 24px;
    background-image: url(./assets/images/button_next_2.png) !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
    margin: 0px 796px !important;
    float: right;
}
.owl-dots{
    display: none;
}*/


.authors .owl-nav {
    position: absolute;
    top: 50%;
    width: 100%;

}

.authors .owl-prev {
    height: 24px;
    width: 24px;
    background-image: url(./assets/images/button_previous_2.png) !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
    display: inline-block !important;
    left: -2em;
    margin: 0px !important;
}
.authors .owl-next {
    height: 24px;
    width: 24px;
    background-image: url(./assets/images/button_next_2.png) !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
    right: -2em;
    margin: 0px;
}
.owl-dots{
    display: none;
}
/*.featurebooks:hover .readdiv {
    display: block;
    position: absolute;
    background: #151414 !important;
    color: #ffff !important;
    opacity: 1 !important;
    margin: -134px 0px 0px 49px;
    font-size: small;
    font-weight: bold;
    padding: 2px 14px;
    border-radius: 4px;
    cursor: pointer;
}*/
.featurebooks img {
    height: unset;
    width: 100%;
}
.readdiv a{
    color: #fff !important;
}
button#moreAuthor {
    color: #54bfe3;
    background: #f3f3f39c;
    border: 1px solid #54bfe3;
    border-radius: 7px;
    padding: 4px 44px;
    margin-top: 50px;
    cursor: pointer;
}
button#moreAuthor:hover{
    background-color: #54bfe3;
    color: #fff;
}


/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {
 
    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 130px;
        font-size: 55px;
        text-align: center;
    }
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
    .socialicondiv {
    width: 157px;
    position: relative !important;
    right: 15px;
    top: -6px;
    display: block;
}
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {
  
    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 130px;
        font-size: 55px;
        text-align: center;
    }
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
    
    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 130px;
        font-size: 55px;
        text-align: center;
    }
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {
   
} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {
    
}
</style>
  