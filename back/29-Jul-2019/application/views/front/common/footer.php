<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<footer class="site-footer">
  <div class="link">
  <a id="back-to-top" href="#" class="sadfs" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left">
    <img src="<?php echo base_url('assets/images/carrisol.png'); ?>" class="img-fluid">
  </a>
</div>
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="row">
              <!-- <div class="col-md-5">
                <h2 class="footer-heading mb-4">About Us</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque facere laudantium magnam voluptatum autem. Amet aliquid nesciunt veritatis aliquam.</p>
              </div> -->
              <div class="col-md-4 ml-auto">
                <h2 class="footer-heading mb-4">CATEGORIES</h2>
                <ul class="list-unstyled footerlink">
                  <li><a href="<?php echo site_url('category/Arts & Photography'); ?>">Arts & Photography</a></li>
                  <li><a href="<?php echo site_url('category/Biographies & Memories'); ?>">Biographies & Memories</a></li>
                  <li><a href="<?php echo site_url('category/Business & Invasting'); ?>">Business & Invasting</a></li>
                  <li><a href="<?php echo site_url('category/Childrens Books'); ?>">Children's Books</a></li>
                  <li><a href="<?php echo site_url('category/Comics & Graphic Novels'); ?>">Comics & Graphic Novels</a></li>
                </ul>
              </div>
              <div class="col-md-4 ml-auto">
                <!-- <h2 class="footer-heading mb-4">Quick Links</h2> -->
                <ul class="list-unstyled footerlink" style="margin-top: 43px;">
                  <li><a href="<?php echo site_url('category/Computers & Internet'); ?>">Computers & Internet</a></li>
                  <li><a href="<?php echo site_url('category/Cooking, Food & Wine'); ?>">Cooking, Food & Wine</a></li>
                  <li><a href="<?php echo site_url('category/Entertainment'); ?>">Entertainment</a></li>
                  <li><a href="<?php echo site_url('category/Health, Mind & Body'); ?>">Health, Mind & Body</a></li>
                </ul>
              </div><div class="col-md-4 ml-auto">
                <h2 class="footer-heading mb-4">COMPANY</h2>
                <ul class="list-unstyled footerlink">
                  <li><a href="<?php echo site_url('aboutUs'); ?>">About Us</a></li>
                  <li><a href="<?php echo site_url('membership'); ?>">Membership</a></li>
                  <li><a href="<?php echo site_url('help'); ?>">Get Help</a></li>
                  <li><a href="<?php echo site_url('videoTraining'); ?>">Video & training</a></li>
                  <li><a href="<?php echo site_url('contact'); ?>">Contact Us</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-4">
              <h2 class="footer-heading mb-4 ">Apps coming soon <label style="float: right;">JOIN TODAY</label></h2>

              <form name="subscribe_Form" id="subscribe_Form" method="post" class="footer-subscribe">
                <div class="input-group mb-3">
                  <input id="subscribe_Formtxt_email" name="subscribe_Formtxt_email" type="email" class="form-control border-secondary" placeholder="Enter your email ID " aria-label="Enter Email" aria-describedby="button-addon2" required>
                  <div class="input-group-append">
                    <button class="btn btn-info text-black" type="submit" id="button-addon2">Subscribe</button>
                  </div>
                </div>
            </form>  
            </div>
            
            <div class="">
              <h2 class="footer-heading mb-4" style="display: inline-block;">FOLLOW US</h2>
                <div style="display: inline;margin: 15px;">
                    <a href="https://www.facebook.com/esomabooks" target="_blank" class="pl-0 pr-3 socialfacebook"></a>
                    <a href="https://www.instagram.com/esomabooks/" target="_blank" class="pl-3 pr-3 socialinstagram"></a>
                    <a href="#" target="_blank" class="pl-3 pr-3 socialyoutube"></a>
                </div>
            </div>


          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-12">
            <div class="border-top pt-4">
            
              <div style="float: left;">Designed By: <a href="https://www.impetrosys.com" target="_blank">Impetrosys Software Solution Pvt. Ltd.</a></div>  <div style="float: right;">Copyright &copy; 2019 All rights reserved.</div>
            
            </div>
          </div>
          
        </div>
      </div>
    </footer>

  </div> <!-- .site-wrap -->

  <script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-migrate-3.0.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-ui.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/popper.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/owl.carousel.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.stellar.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.countdown.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap-datepicker.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.easing.1.3.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/aos.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.fancybox.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.sticky.js'); ?>"></script>


  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
  
  <script src="<?php echo base_url('assets/js/main.js'); ?>"></script>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    
  </body>
</html>