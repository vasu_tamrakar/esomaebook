<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" href="<?php echo base_url('assets/build/css/intlTelInput.css'); ?>">
<!-- <link rel="stylesheet" href="<?php echo base_url('assets/build/css/demo.css'); ?>"> -->
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/contact_bg.png'); ?>);">
    <div class="container">
        <div class="row">
        <div class="bannerheading"><h1>Sign Up</h1></div>
        </div>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active">Sign Up</li>
          </ol>
        </nav>

    </div>
    
</div>


<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="col-md-12">
            <?php 
            $alert = $this->session->flashdata('alert');
            if($alert){
                ?>
            <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
              <strong><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
            </div>
                <?php
            }
            ?>
            <div class="heading"><h1>Customer Information</h1></div>
            <div class="row">
                <div class="col-md-8">
                    <form name="signup_form" id="signup_form" action="<?php echo site_url('signUp'); ?>" method="post">
                        <input type="hidden" name="txt_planid" id="txt_planid" value="<?php echo $subscriberData[0]->es_planid; ?>">
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="fname">First Name</label>
                            <input type="text" name="txt_firstname" id="txt_firstname" value="<?php echo $subscriberData[0]->es_firstname; ?>" class="form-control" placeholder="Enter First Name" maxl-enght="150" readonly="true">
                            <?php echo form_error('txt_firstname','<small class="form-text text-danger">','</small>'); ?>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="lname">Last Name</label>
                            <input type="text" name="txt_lastname" id="txt_lastname" value="<?php echo $subscriberData[0]->es_lastname; ?>" class="form-control" placeholder="Enter Last Name" maxl-enght="150" readonly="true">
                            <?php echo form_error('txt_lastname','<small class="form-text text-danger">','</small>'); ?>
                          </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                      
                              <label for="email">Email</label><br>
                              <input type="text" name="txt_email" id="txt_email" value="<?php echo $subscriberData[0]->es_email; ?>" class="form-control" placeholder="Enter Email Address" maxl-enght="200" readonly="true">
                              <?php echo form_error('txt_email','<small class="form-text text-danger">','</small>'); ?>
                            </div>
                             <div class="form-group col-md-6">
                                  <label for="mobile">Mobile</label><br>
                                  <input type="text" name="txt_mobile" id="txt_mobile" value="<?php echo set_value('txt_mobile'); ?>" class="form-control" placeholder="Enter Mobile no." max-length="15">
                                  <?php echo form_error('txt_mobile','<small class="form-text text-danger">','</small>'); ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                  <label for="pass">Password</label>
                                  <input type="password" name="txt_password" id="txt_password" value="<?php echo set_value('txt_password'); ?>" class="form-control" placeholder="******" maxl-enght="50">
                                  <?php echo form_error('txt_password','<small class="form-text text-danger">','</small>'); ?>
                            </div>
                            <div class="form-group col-md-6">
                                  <label for="txt_cnfpassword">Confirm Password</label>
                                  <input type="password" name="txt_cnfpassword" id="txt_cnfpassword" value="<?php echo set_value('txt_cnfpassword'); ?>" class="form-control" placeholder="******" maxl-enght="50">
                                  <?php echo form_error('txt_cnfpassword','<small class="form-text text-danger">','</small>'); ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                  <label for="select_book">Select Somali book</label>
                                  <?php 
                                  $somalibookS = $this->user_Auth->getData('books', array('b_language' => 'somali', 'b_status' => '1'), $se='b_id,b_name', $sh='b_id DESC');
                                  if($somalibookS) {?>
                                  <select name="select_book" id="select_book" class="form-control">
                                      <option value=''>Select the Book</option>
                                      <?php 
                                      foreach($somalibookS as $somalibook){
                                      ?>
                                      <option value='<?php echo $somalibook->b_id;  ?>'><?php echo $somalibook->b_name;  ?></option>
                                      <?php } ?>
                                  <?php echo form_error('select_book','<small class="form-text text-danger">','</small>'); ?>
                                </select>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <div class="card">
                                    <div class="card-header text-info">
                                        Billing Information
                                    </div>
                                    <div class="card-body">
                                      <?php $planDetails = $this->user_Auth->getData('membershipplan',array("mp_id" => $subscriberData[0]->es_planid));
                                          
                                      ?>
                                        <label class="label-control">Choose Plan: <strong><?php echo (isset($planDetails[0]->mp_name)?$planDetails[0]->mp_name:''); ?></strong></label></br>
                                        <!-- <label>Plan price: <strong><span>&#36;</span><?php echo (isset($planDetails[0]->mp_price)?$planDetails[0]->mp_price:''); ?></strong></label></br> -->
                                        <label>Sub Total: <strong> <?php echo (isset($planDetails[0]->mp_price)?$planDetails[0]->mp_price:''); ?></strong></label></br>

                                        <label> Total: <strong><?php echo (isset($planDetails[0]->mp_price)?$planDetails[0]->mp_price:''); ?></strong></label></br>
                                        <label>Plan Descriptions: <?php echo (isset($planDetails[0]->mp_descriptions)?$planDetails[0]->mp_descriptions:''); ?></label></br>
                                        <label class="text-info">Payment Options</label>
                                        
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="txt_paymethod1" name="txt_paymethod" value="cc">
                                            <label class="custom-control-label" for="txt_paymethod1">Credit Card</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="txt_paymethod2" name="txt_paymethod" value="evc">
                                            <label class="custom-control-label" for="txt_paymethod2">EVC</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="txt_paymethod3" name="txt_paymethod" value="mpesa">
                                            <label class="custom-control-label" for="txt_paymethod3">Mpesa</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      <div class="form-row">
                          <div class="form-group col-md-6">
                              <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="txt_agree" name="txt_agree" value="true">
                                  <label class="custom-control-label" for="txt_agree">I agree to the payment terms as stated above.</label>
                              </div>
                          </div>
                      </div>
                      <button type="submit" class="btn btn-primary loadmore">Pay</button>
                    
                      </form>
                       
                        
                    </div>
                </div>

            <div class="col-md-4">

            </div>
        </div>
    </div>
</section>
<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 328px;
    height: calc(40vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar ul li a {
    color: #505048c4 !important;
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}


.bannerheading {
    margin: 0 auto;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    top: 130px;
    font-size: 55px;
}
.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}
form#contact_form input {
    font-size: 14px;
}
form#contact_form {
    font-size: 14px;
    margin: 22px 0px;
}
.loadmore {
    background: #54bfe3 !important;
    border-radius: 1px !important;
    color: #fff !important;
    font-weight: bold;
    font-size: 15px;
    margin-top: 20px;
    border: 1px solid #54bfe3 !important;
    padding: 12px 80px;
}
.address {
    background: #00BCD4;
    padding: 50px;
}
.address p {
    text-align: center;
    color: #fff;
}
.address label {
    text-align: center !important;
    margin: 0 auto !important;
    color: #fff;
    font-size: large;
}
.socialicon{
  text-align: center;
}
.socialicon a {
    padding: 10px;
    background-repeat: no-repeat;
    background-size: cover;
    display: inline-block;
    width: 30px;
    height: 30px;
    margin: 4px;
}
.socialicon a:hover {
    opacity: 0.6;
}
.form-control:active, .form-control:focus {

    border-color: #00BCD4;

}
a.facebook {
    background: url(./assets/images/follow_us_fb.png);
}
a.twitter {
    background: url(./assets/images/follow_us_tw.png);
}
a.google {
    background: url(./assets/images/follow_us_g.png);
}
a.instagram {
    background: url(./assets/images/follow_us_insta.png);
}
a.linkedin {
    background: url(./assets/images/follow_us_link.png);
}

label#txt_paymethod-error {
    position: absolute;
    bottom: -80px;
    left: 0;
}
.custom-radio .custom-control-input:checked~.custom-control-label:before {
    background-color: #08c;
}
.custom-checkbox .custom-control-input:checked~.custom-control-label:before {
    background-color: #08c;
}
#txt_agree-error {
    display: block;
    position: absolute;
    bottom: -27px;
}
/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {

    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 130px;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
}
.iti {
    width: 100%;
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
}
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {

} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

}
</style>
<script src="<?php echo base_url('assets/build/js/intlTelInput.js'); ?>"></script>
  <script>
    var input = document.querySelector("#txt_mobile");
    window.intlTelInput(input, {
      allowExtensions: true,
          autoFormat: false,
          autoHideDialCode: false,
          autoPlaceholder: false,
          defaultCountry: "auto",
          ipinfoToken: "yolo",
          nationalMode: false,
          numberType: "MOBILE",
          //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
          preferredCountries: ['us', 'ca', 'in'],
          // preventInvalidNumbers: true,
          // utilsScript: "lib/libphonenumber/build/utils.js"
          preventInvalidNumbers: true,
      utilsScript: "assets/build/js/utils.js",

    });
  </script>
