<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Add New Category</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
     
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>Add New Category Form<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                  <div class="x_content">
                    <br />

                    <form name="addnewcategory_Form" id="addnewcategory_Form" action="<?php echo site_url('dashboard/addnewCategory'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_categoryname">Category Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_categoryname" id="txt_categoryname" value="<?php echo set_value('txt_categoryname'); ?>" placeholder="Category Name" class="form-control col-md-7 col-xs-12">
                          <?php echo form_error('txt_categoryname','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_catdescription">Category Descriptions <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea name="txt_catdescription" id="txt_catdescription" rows="5" placeholder="Descriptions..." class="form-control col-md-7 col-xs-12"><?php echo set_value('txt_catdescription'); ?></textarea>
                          <?php echo form_error('txt_catdescription','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_image">Image <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="file" name="txt_image" id="txt_image"  class="form-control col-md-7 col-xs-12" accept="image/*">
                          <small class="text-info">Image format accept only png,jpeg,jpg,gif 261 x 184-398 px. </small>
                        </div>
                      </div>
                      

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">Save</button>
                          <a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary" type="button">Cancel</a>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

            
          </div>
        </div>
        <!-- /page content -->