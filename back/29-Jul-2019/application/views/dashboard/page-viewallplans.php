 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Datatables -->
    <link href="<?php echo base_url('themes/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Veiw All Plans List</h3>
            </div>

            <!-- <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
              </div>
            </div> -->
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>View All<small>Plans</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <?php 
                        $alert = $this->session->flashdata('alert');
                        if($alert){
                            ?>
                            <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                              <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <?php
                        }
                        ?>
      
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>P id</th>
                                    <th>P Name</th>
                                    <th>P Price</th>
                                    <th>P Validity</th>
                                    <th>P Status</th>
                                    <th>P Desc.</th>
                                    <th>P created</th>
                                    <th>U Action</th>
                                </tr>
                            </thead>
                            <?php
                            if($allplanids) { ?>
                  
                            <tbody>
                            <?php
                            foreach ($allplanids as $pID) {
                                $memberplan = $this->user_Auth->getData('membershipplan', $w=array('mp_id' => $pID->mp_id),$se='',$sh='');
                            ?>
                                <tr>
                                    <td><?php echo $memberplan[0]->mp_id; ?></td>
                                    <td><?php echo (($memberplan[0]->mp_name)?$memberplan[0]->mp_name:""); ?></td>
                                    <td><?php echo (isset($memberplan[0]->mp_price)?$memberplan[0]->mp_price:''); ?> </td>
                                    <td><?php echo (isset($memberplan[0]->mp_validity)?$memberplan[0]->mp_validity:''); ?> </td>
                                    <td>
                                        <div style="margin: 0 auto;width: 24px;">
                                        <?php echo (($memberplan[0]->mp_status == '1')?"<i class='fa fa-check'></i>":"<i class='fa fa-times-circle'></i>"); ?> 
                                    </td>
                                    <td><?php 
                                        $len = strlen($memberplan[0]->mp_descriptions);
                                        if($len > 15){
                                            echo substr($memberplan[0]->mp_descriptions,0,15).'...';
                                        }else{
                                            echo $memberplan[0]->mp_descriptions;
                                        } 
                                        ?>
                                    </td>
                                    <td><?php echo (($memberplan[0]->mp_created > 0)?date('d-M-Y H:i:s',strtotime($memberplan[0]->mp_created)):""); ?> </td>
                                    <td>
                                        <div class="action-menu">
                                            <a title="Edit" class="btn btn-info" href="<?php echo site_url('dashboard/editplanDetails/'.$pID->mp_id); ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                            <a title="Delete" data-title="Goto twitter?" class="btn btn-info" href="javascript:void(0)" onclick="deletePlanDetail(<?php echo $pID->mp_id; ?>)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                        </div>
                                        <div class="clearfix"></div>
                                      <?php //echo $author[0]->a_id; ?>
                                    </td>
                                </tr>
                            <?php
                          
                            }
                            ?>
                            </tbody>
                            <?php 
                            }else{ ?>
                  
                            No More Data..
                            <?php
                            } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->