 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Datatables -->
<link href="<?php echo base_url('themes/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('themes/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
              <h3>Veiw All Books List</h3>
            </div>

            <?php /*!-- <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
              </div>
            </div> --*/?>
        </div>
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>View All<small>Books</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                        <?php 
                          $alert = $this->session->flashdata('alert');
                          if($alert){
                              ?>
                              <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                                <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                        <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-md-12 col-md-offset-2">
                                <div class="form-group-row">
                                    <label class="control-label" for="txt_price">Book Filter</label>
                                    
                                    <label for="filterbookall" class="control-label">
                                        <input type="radio" id="filterbookall" name="filterbook" value="all" <?php if($filtered == 'all'){ echo 'checked'; } ?>>All
                                    </label>
                                    <label for="filterbookso" class="control-label">
                                        <input type="radio" id="filterbookso" name="filterbook" value="somali" <?php if($filtered == 'somali'){ echo 'checked'; } ?>>Somali
                                    </label>
                                    <label for="filterbooken" class="control-label">
                                        <input type="radio" id="filterbooken" name="filterbook" value="english" <?php if($filtered == 'english'){ echo 'checked'; } ?>>English
                                    </label>
                                    
                                </div>
                            </div>
                        </div>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>B id</th>
                                    <th>B Title</th>
                                    <th>B Category</th>
                                    <th>B Author</th>
                                    <th>B Publish</th>
                                    <th>B Price</th>
                                    <th>B Image</th>
                                    <th>B Status</th>
                                    <th>B Lang</th>
                                    <th>B Action</th>
                                </tr>
                            </thead>
                            <?php
                            if($allBookssids) { ?>
                      
                            <tbody>
                            <?php
                            foreach ($allBookssids as $bID) {
                            $book = $this->user_Auth->getData('books', $w=array('b_id' => $bID->b_id),$se='',$sh='');
                            ?>
                                <tr>
                                    <td><?php echo $book[0]->b_id; ?></td>
                                    <td><?php echo $book[0]->b_title; ?></td>
                                    <td>
                                       <?php
                                      $category = $this->user_Auth->getData('categories', $w=array('c_id' => $book[0]->b_category), $se='c_id, c_name',$su='');
                                      echo (isset($category[0]->c_name)?$category[0]->c_name:'');
                                      ?>
                                    </td>
                                    <td>
                                      <?php
                                      $author = $this->user_Auth->getData('user_credentials', $w=array('uc_id' => $book[0]->b_fk_of_aid), $se='uc_id, uc_firstname, uc_lastname, uc_email',$su='');
                                      if(isset($author[0]->uc_id)){
                                      ?>
                                      <a href="<?php echo site_url('dashboard/authorDetails/'.$author[0]->uc_id); ?>">
                                      <?php echo (isset($author[0]->uc_firstname)?$author[0]->uc_firstname:'').' '.(isset($author[0]->uc_lastname)?$author[0]->uc_lastname:''); ?></a>
                                      <?php
                                      }
                                      ?>
                                    </td>
                                    <td><?php echo (($book[0]->b_published > 0)?date('M d Y', strtotime($book[0]->b_published)):""); ?></td>
                                    <td><?php echo (($book[0]->b_price > 0)?$book[0]->b_price:""); ?></td>
                                    <td>
                                        <div style="width:50px;">
                                            <img src="<?php echo (($book[0]->b_image)?base_url('uploads/books/'.$book[0]->b_image):base_url('uploads/books/book.png')); ?>" class="img-responsive">
                                        </div>
                                    </td>
                                    <td><div style="text-align: center; font-size: 20px;">
                                      <?php
                                      echo (($book[0]->b_status == '1')?"<i class='fa fa-check'></i>":"<i class='fa fa-times-circle'></i>");
                                      
                                       ?>
                                       </div>
                                    </td>
                                    <td>
                                      <?php
                                      echo (($book[0]->b_language)?$book[0]->b_language:"");                    
                                       ?>
                                    </td>
                                    <td>
                                        <div class="action-menu">
                                            <a title="View Details" class="btn btn-info" href="<?php echo site_url('dashboard/bookDetails/'.$bID->b_id); ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                            <a title="Edit" class="btn btn-info" href="<?php echo site_url('dashboard/editbookDetails/'.$bID->b_id); ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                            <a title="Delete" data-title="Goto twitter?" class="btn btn-info" href="javascript:void(0)" onclick="deleteBooksDetail(<?php echo $bID->b_id; ?>)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                        </div>
                                        <div class="clearfix"></div>
                                      
                                      <?php //echo $author[0]->a_id; ?>
                                    </td>
                                </tr>
                              <?php
                              }
                              ?>
                            </tbody>
                            <?php 
                            }else{ ?>
                            
                            No More Data..
                            <?php
                            } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->