<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Author Details</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo (($authorDetails)?$authorDetails[0]->uc_firstname.' '.$authorDetails[0]->uc_lastname:''); ?> <small> Author Details.</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                      ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                            <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                      </div>
                      <?php
                    }
                    ?>
                    <?php
                    if($authorDetails[0]->uc_role == 4){
                    ?>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_content">
                            <br />
                            <?php
                            if($authorDetails){
                                $authorDetails[0]->uc_id; ?>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                      <th>Name</th>
                                      <th>Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><label> Author ID: </label></td>
                                        <td><label> <?php echo $authorDetails[0]->uc_id; ?> </label></td>
                                    </tr>
                                    <tr>
                                        <td><label> Author Name: </label></td>
                                        <td><label> <?php echo (($authorDetails[0]->uc_firstname)?$authorDetails[0]->uc_firstname:"").' '.(($authorDetails[0]->uc_lastname)?$authorDetails[0]->uc_lastname:""); ?> </label></td>
                                    </tr>
                                    <tr>
                                        <td><label> Author Email: </label></td>
                                        <td><label> <?php echo $authorDetails[0]->uc_email; ?> </label></td>
                                    </tr>
                                    <tr>
                                        <td><label> Author Mobile: </label></td>
                                        <td><label> <?php echo $authorDetails[0]->uc_mobile; ?> </label></td>
                                    </tr>
                                    
                                    <tr>
                                        <?php $author = $this->user_Auth->getData('authors', array('a_fK_of_uc_id' => $authorDetails[0]->uc_id)); ?>
                                        <td><label> Published Books: </label></td>
                                        <td><label> <?php echo (isset($author[0]->a_publishbooks)?$author[0]->a_publishbooks:""); ?> </label></td>
                                    </tr>
                                    <tr>
                                        <td><label> Facebook Link: </label></td>
                                        <td><label>
                                            <?php if(isset($author[0]->a_facebook)){ ?>
                                                <a href="<?php echo (isset($author[0]->a_facebook)?$author[0]->a_facebook:""); ?>" target="_blank" title="Facebook"><?php echo $author[0]->a_facebook; ?> </a>
                                                <?php } ?> 
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label> Twitter Link:</label></td>
                                        <td><label>
                                              <?php if(isset($author[0]->a_twitter)){ ?>
                                              <a href="<?php echo (isset($author[0]->a_twitter)?$author[0]->a_twitter:""); ?>" target="_blank" title="Twitter"><?php echo $author[0]->a_twitter; ?> </a>
                                              <?php } ?>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label> Google Plus:</label></td>
                                        <td>
                                          <label> 
                                            <?php if(isset($author[0]->a_gplush)){ ?>
                                                <a href="<?php echo (isset($author[0]->a_gplush)?$author[0]->a_gplush:""); ?>" target="_blank" title="Google+"><?php echo $author[0]->a_gplush; ?> </a>
                                            <?php } ?>
                                          </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label> Instagram:</label></td>
                                        <td>
                                          <label>
                                            <?php if(isset($author[0]->a_instagram)){ ?>
                                                <a href="<?php echo (isset($author[0]->a_gplush)?$author[0]->a_instagram:""); ?>" target="_blank" title="Instagram"><?php echo $author[0]->a_instagram; ?> </a>
                                            <?php } ?>
                                          </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label> Status:</label></td>
                                        <td><label> <?php echo (($authorDetails[0]->uc_status == '1')?"TRUE":"FALSE"); ?></label></td>
                                    </tr>
                                    <tr>
                                        <td><label> Created Date:</label></td>
                                        <td><label> <?php echo (($authorDetails[0]->uc_created > 0)?date('d-M-Y H:i:s',strtotime($authorDetails[0]->uc_created)):''); ?></label></td>
                                    </tr>
                                    <tr>
                                        <td><label> Modified Date:</label></td>
                                        <td><label> <?php echo (($authorDetails[0]->uc_modified > 0)?date('d-M-Y H:i:s',strtotime($authorDetails[0]->uc_modified)):''); ?></label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <label> Author Description:</label>
                                        </td>
                                        <td><p><?php echo (isset($author[0]->a_description)?$author[0]->a_description:""); ?></p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <label> Author Image :</label>
                                        </td>
                                        <td>
                                          <div style="width:150px;">
                                              <a href="<?php echo (($authorDetails[0]->uc_image)?base_url('uploads/users/'.$authorDetails[0]->uc_image):base_url('uploads/users/author.png')); ?>"><img src="<?php echo (($authorDetails[0]->uc_image)?base_url('uploads/users/'.$authorDetails[0]->uc_image):base_url('uploads/users/author.png')); ?>" class="img-responsive"></a>
                                          </div>
                                        </td>
                                    </tr>
                                <tbody>
                            </table>
                            <center>
                               <a href="<?php echo site_url('dashboard/editauthorDetails/'.$authorDetails[0]->uc_id); ?>" class="btn btn-info">Edit Info</a>
                               <a href="<?php echo site_url('dashboard/addnewAuthor/'); ?>" class="btn btn-info">Add New Author</a>
                               <button name="deleteAuthor" id="deleteAuthor" class="btn btn-info" onclick="deleteAuthor(<?php echo $authorDetails[0]->uc_id; ?>)">Delete</button>
                            </center>
                    <?php }else{
                      echo "No details.";
                    }
                    
                     ?>
                     
                        </div>
                    </div>
                    <?php
                    }else{
                        echo 'In-valid information.';
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
        if($authorDetails[0]->uc_role == 4){
        ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">

                        <h2>Author Books sliders</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                  
                    <div class="x_content">
                        <br />
                        <div class="row">
                            <?php
                            $sliders = $this->user_Auth->getData('books', array('b_fk_of_aid' => $authorDetails[0]->uc_id,'b_status' => '1'), $s='',$sh='');
                            if($sliders){
                                foreach ($sliders as $value) {
                                    // $value->b_id;
                                    // echo $value->b_fk_of_aid;
                                    // echo $value->b_fk_of_uid;
                                    // echo $value->b_language;
                                    // echo $value->b_name;
                                    // echo $value->b_published;
                                    // echo $value->b_rating;
                                    // echo $value->b_category;
                                    // echo $value->b_image;
                                    // echo $value->b_filetype;
                                    // echo $value->b_file;
                                    // echo $value->b_downloads;
                                    // echo $value->b_status;
                                    // echo $value->b_created;
                                    // echo $value->b_modified;
                                    // echo '<br/>';
                                    ?>
                                    <div class="col-md-3">
                                        <div class="gallerybox">
                                            <div class="imgbox">
                                                <img src="<?php echo base_url('uploads/books/'.(($value->b_image)?$value->b_image:"slide.png")); ?>" alt="<?php echo $value->b_title; ?>" class="img-fluid">
                                                <div class="textbox"><a href="<?php echo site_url('dashboard/bookDetails/'.$value->b_id); ?>"><?php echo $value->b_title; ?></a></div>
                                            </div>
                                        </div>
                                    </div><?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
    </div>
</div>
        <!-- /page content -->

<style>
.gallerybox {
    text-align: center;
    display: block;
    min-height: 283px;
}
.imgbox {
    width: 180px;
    text-align: center !important;
    display: block;
    margin: 0 auto;
    cursor: pointer;
}
.imgbox img {
    width: 100%;
    height: 100%;
}
.gallerybox .textbox {
    bottom: 0%;
    position: absolute;
    transition: all 0.6s;
    text-align: center !important;
    display: block !important;
    margin: 0 35px;
    padding: 4px;
    border-radius: 5px;
    font-size: 14px;
    color: #3498db;
}
.imgbox:hover .textbox {
    position: absolute;
    bottom: 50%;
    transition: all 0.6s;
    text-align: center !important;
    display: block !important;
    margin: 0 35px;
    padding: 4px;
    border-radius: 5px;
    font-size: 14px;
    color: #fff;
    background: #3b4e5133;
    /*cursor: pointer;*/
}
</style>