-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 26, 2019 at 04:40 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebook_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `a_id` bigint(20) NOT NULL,
  `a_name` varchar(255) NOT NULL,
  `a_fK_of_uc_id` bigint(20) NOT NULL,
  `a_publishbooks` bigint(20) NOT NULL,
  `a_image` text NOT NULL,
  `a_description` text NOT NULL,
  `a_facebook` varchar(255) NOT NULL,
  `a_twitter` varchar(255) NOT NULL,
  `a_gplush` varchar(255) NOT NULL,
  `a_instagram` varchar(255) NOT NULL,
  `a_status` enum('1','0') NOT NULL,
  `a_created` datetime NOT NULL,
  `a_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`a_id`, `a_name`, `a_fK_of_uc_id`, `a_publishbooks`, `a_image`, `a_description`, `a_facebook`, `a_twitter`, `a_gplush`, `a_instagram`, `a_status`, `a_created`, `a_modified`) VALUES
(1, '', 2, 1, '', 'This is first Author test.<span style=\"font-weight: bold;\">Testyst</span><br> ', 'https://www.facebook.com/author', 'https://www.twitter.com/author', 'https://www.gplus.com/author', 'https://www.instagram.com/author', '1', '2019-07-17 14:37:51', '2019-07-17 14:38:26'),
(2, '', 4, 4, '', 'asdf asdfa sdfasdfasdfas this is isda lsadf<div>sadf sadf asdf asdfa sdfasd fasdf sadfasd fsadf asdfas dfdsaf</div>', '', '', '', '', '1', '2019-07-18 10:06:30', '2019-07-18 10:07:51'),
(3, '', 22, 1978, '', 'This is tesing email...... sanmdmamsndmn,asmdn', 'https://www.facebook.com/dsf', 'https://www.twitter.com/fd', 'https://www.gplus.com/dfs', 'https://www.instagram.com/sdf', '1', '2019-07-25 09:53:33', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `b_id` bigint(20) NOT NULL,
  `b_fk_of_aid` bigint(20) NOT NULL,
  `b_fk_of_uid` bigint(20) NOT NULL,
  `b_language` enum('somali','english') NOT NULL,
  `b_title` varchar(255) NOT NULL,
  `b_published` date NOT NULL,
  `b_price` double(8,2) NOT NULL,
  `b_rating` int(5) NOT NULL,
  `b_category` int(11) NOT NULL,
  `b_image` text NOT NULL,
  `b_description` text NOT NULL,
  `b_file` text NOT NULL,
  `b_downloads` bigint(20) NOT NULL,
  `b_status` enum('1','0') NOT NULL,
  `b_created` datetime NOT NULL,
  `b_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`b_id`, `b_fk_of_aid`, `b_fk_of_uid`, `b_language`, `b_title`, `b_published`, `b_price`, `b_rating`, `b_category`, `b_image`, `b_description`, `b_file`, `b_downloads`, `b_status`, `b_created`, `b_modified`) VALUES
(6, 7, 1, 'english', 'dsdas dasd asd asd asdsdafasdf', '2019-06-25', 0.00, 4, 8, '', '.pdf', 'Mail_A_Ticket.pdf', 0, '1', '2019-06-25 12:24:15', '0000-00-00 00:00:00'),
(7, 7, 1, 'english', 'vcxvxcvxcv', '2019-06-25', 0.00, 3, 5, '', '.mp4', 'What_are_you_waiting_for_-_30_seconds.mp4', 0, '1', '2019-06-25 12:25:10', '0000-00-00 00:00:00'),
(9, 7, 1, 'english', '2x323xc23v2xc32xc', '2019-06-25', 0.00, 3, 6, '', '.pdf', 'b93f896e80a23a2fa1187ab3d0b65e9d.pdf', 0, '1', '2019-06-25 13:07:12', '0000-00-00 00:00:00'),
(10, 7, 1, 'english', 'TEST ONE', '2010-05-01', 0.00, 4, 2, 'book_1.png', '.pdf', 'book.pdf', 0, '1', '2019-06-25 13:18:04', '2019-06-26 08:41:50'),
(11, 8, 7, 'english', 'vvvvvvvvvvvvws', '2003-06-02', 0.00, 1, 8, 'book_4.png', '.pdf', 'pdf-sample3.pdf', 0, '1', '2019-06-25 13:18:53', '2019-06-26 08:23:53'),
(12, 7, 1, 'english', 'ghgffgfghfhfgh', '2001-05-30', 0.00, 4, 7, '9cf582c45558e41f4c3f0ea859899f58.png', '.pdf', '8dd95ecb22e109e1c50c77159cdfd36d.pdf', 0, '1', '2019-06-25 13:22:22', '0000-00-00 00:00:00'),
(13, 11, 12, 'english', 'dsadasd asdsa', '0000-00-00', 0.00, 3, 3, 'wormhole-abstract-hd-wallpaper-iltwmt2.png', '', 'Mail_A_Ticket1.pdf', 1, '1', '2019-07-05 11:24:20', '0000-00-00 00:00:00'),
(14, 11, 12, 'english', 'B?HDSODSKdfsh', '0000-00-00', 0.00, 3, 2, 'Summer-Wallpaper-Pictures1.jpg', '.pdf', 'demoform11.pdf', 0, '1', '2019-07-05 11:27:33', '0000-00-00 00:00:00'),
(15, 25, 1, 'somali', 'TEST SOMALI1', '2019-07-10', 0.00, 3, 10, 'book_6.png', '.pdf', 'Mail_A_Ticket2.pdf', 0, '1', '2019-07-08 15:54:18', '2019-07-15 08:47:57'),
(16, 12, 1, 'somali', 'vvvvvvvvv', '2004-07-14', 0.00, 3, 10, 'book_51.png', '.pdf', 'demoform12.pdf', 0, '1', '2019-07-12 13:52:48', '0000-00-00 00:00:00'),
(17, 27, 1, 'somali', 'TESTS DOMALIDFDF', '2019-07-12', 0.00, 3, 7, '', '.pdf', '5096011b280c5ebfac82e87f6f37ab74.pdf', 0, '1', '2019-07-12 14:36:01', '0000-00-00 00:00:00'),
(18, 30, 1, 'english', 'SDFGpp', '2019-07-09', 0.00, 4, 10, 'book_41.png', '.pdf', 'pdf-sample(2).pdf', 0, '0', '2019-07-12 15:45:17', '2019-07-12 15:49:27'),
(19, 2, 1, 'english', 'Tset book', '2002-05-02', 0.00, 3, 6, 'book_52.png', '.pdf', 'demoform13.pdf', 0, '1', '2019-07-15 07:10:30', '2019-07-17 15:04:22'),
(20, 4, 1, 'english', 'This is test book', '1991-02-02', 50.00, 0, 6, 'book_53.png', '.pdf', 'demoform14.pdf', 0, '1', '2019-07-19 08:31:52', '0000-00-00 00:00:00'),
(21, 2, 1, 'english', 'rtTtest nooksd', '2010-06-30', 34.00, 0, 4, 'high-definition-download-hd-wallpaper3.jpg', 'WWWsdfs dfs dfs fsdf sdfs dfds fsdf sdf sdf<br>', 'pdf-sample7.pdf', 0, '1', '2019-07-19 15:03:04', '0000-00-00 00:00:00'),
(22, 4, 1, 'english', 'Second Testnn', '2004-07-15', 30.00, 0, 8, 'kPl0TGN1.jpg', 'This is second book description. <span style=\"font-weight: bold;\">KLK</span><br><blockquote><blockquote>szdafg asdfasd fasdf asdf asdfasd fsadf sadf asas dfaf asd<br>ksjklasdjlflsadlkf;lssdf<br></blockquote></blockquote>\r\n<hr>sdfdsaf asdf asdf sdaf asdf', 'demoform15.pdf', 0, '1', '2019-07-19 15:04:50', '2019-07-19 15:25:39');

-- --------------------------------------------------------

--
-- Table structure for table `book_access`
--

CREATE TABLE `book_access` (
  `ba_id` bigint(20) NOT NULL,
  `ba_userid` bigint(20) NOT NULL,
  `ba_bookid` bigint(20) NOT NULL,
  `ba_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `book_access`
--

INSERT INTO `book_access` (`ba_id`, `ba_userid`, `ba_bookid`, `ba_created`) VALUES
(1, 35, 14, '0000-00-00 00:00:00'),
(2, 36, 14, '0000-00-00 00:00:00'),
(3, 37, 12, '0000-00-00 00:00:00'),
(4, 38, 14, '0000-00-00 00:00:00'),
(5, 39, 15, '0000-00-00 00:00:00'),
(6, 43, 14, '0000-00-00 00:00:00'),
(7, 45, 17, '0000-00-00 00:00:00'),
(8, 53, 17, '0000-00-00 00:00:00'),
(9, 60, 17, '0000-00-00 00:00:00'),
(10, 65, 17, '0000-00-00 00:00:00'),
(11, 67, 17, '0000-00-00 00:00:00'),
(12, 69, 16, '0000-00-00 00:00:00'),
(13, 70, 17, '0000-00-00 00:00:00'),
(14, 71, 17, '0000-00-00 00:00:00'),
(15, 72, 17, '0000-00-00 00:00:00'),
(16, 73, 16, '0000-00-00 00:00:00'),
(17, 74, 16, '0000-00-00 00:00:00'),
(18, 75, 17, '0000-00-00 00:00:00'),
(19, 76, 17, '0000-00-00 00:00:00'),
(20, 77, 17, '0000-00-00 00:00:00'),
(21, 78, 17, '0000-00-00 00:00:00'),
(22, 79, 17, '0000-00-00 00:00:00'),
(23, 80, 17, '2019-07-17 10:13:51'),
(24, 81, 16, '2019-07-17 10:15:49'),
(25, 82, 17, '2019-07-17 10:18:15'),
(26, 3, 17, '2019-07-17 15:21:15'),
(27, 9, 17, '2019-07-20 11:43:54'),
(28, 10, 17, '2019-07-20 12:19:47'),
(29, 13, 17, '2019-07-23 09:30:09'),
(30, 14, 17, '2019-07-23 10:00:43'),
(31, 16, 17, '2019-07-23 11:05:51'),
(32, 17, 17, '2019-07-23 12:41:19'),
(33, 18, 17, '2019-07-23 12:43:23'),
(34, 19, 17, '2019-07-25 08:31:53'),
(35, 20, 16, '2019-07-25 09:09:08'),
(36, 21, 15, '2019-07-25 09:21:46');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `c_id` bigint(20) NOT NULL,
  `c_name` varchar(250) NOT NULL,
  `c_image` varchar(255) NOT NULL,
  `c_description` text NOT NULL,
  `c_status` enum('1','0') NOT NULL,
  `c_created` datetime NOT NULL,
  `c_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`c_id`, `c_name`, `c_image`, `c_description`, `c_status`, `c_created`, `c_modified`) VALUES
(1, 'Business & Investing', 'categories1.png', '  ', '1', '2019-06-24 00:00:00', '2019-07-17 14:56:56'),
(2, 'Arts & Photography', 'categories51.png', '', '1', '2019-06-24 00:01:00', '2019-07-06 13:19:01'),
(3, 'Cooking,Food & Wine', 'categories3.png', ' ', '1', '2019-06-24 00:02:00', '2019-07-06 12:17:47'),
(4, 'Computers & Internew', 'categories4.png', '', '1', '2019-06-24 00:03:00', '2019-07-06 12:18:22'),
(5, 'Entertainment', 'categories21.png', '', '1', '2019-06-24 00:04:00', '2019-07-06 13:19:30'),
(6, 'Businewss Management', 'categories6.png', '', '1', '2019-06-24 05:00:00', '2019-07-06 12:19:32'),
(7, 'Health, Mind & Body', 'categories7.png', '', '1', '2019-06-24 00:06:00', '2019-07-06 12:19:51'),
(8, 'Personal Development', 'categories8.png', '', '1', '2019-06-24 00:07:00', '2019-07-06 12:20:16'),
(9, 'Biographies & Memoirs', 'categories_9.png', '', '1', '2019-06-24 00:08:00', '2019-07-06 12:20:44'),
(10, 'Soft Skills', 'categories10.png', 'This is categories details...', '1', '2019-06-24 00:09:00', '2019-07-17 11:32:24');

-- --------------------------------------------------------

--
-- Table structure for table `emailSubscriber`
--

CREATE TABLE `emailSubscriber` (
  `es_id` bigint(20) NOT NULL,
  `es_firstname` varchar(255) NOT NULL,
  `es_lastname` varchar(255) NOT NULL,
  `es_email` varchar(150) NOT NULL,
  `es_planid` int(2) NOT NULL,
  `es_status` enum('1','0') NOT NULL,
  `es_created` datetime NOT NULL,
  `es_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `emailSubscriber`
--

INSERT INTO `emailSubscriber` (`es_id`, `es_firstname`, `es_lastname`, `es_email`, `es_planid`, `es_status`, `es_created`, `es_modified`) VALUES
(1, 'user', 'lastname', 'user@gmail.com', 1, '1', '2019-07-17 15:20:23', '0000-00-00 00:00:00'),
(2, 'Test', 'last test', 'last@gmail.com', 1, '1', '2019-07-20 07:58:11', '0000-00-00 00:00:00'),
(3, 'sadsa', 'asda', 'asdsad@asdsad.sd', 1, '1', '2019-07-20 09:27:23', '0000-00-00 00:00:00'),
(4, 'Test', 'testlast', 'test@gmail.com', 1, '1', '2019-07-20 09:40:09', '0000-00-00 00:00:00'),
(5, 'amit', 'rairr', 'amit@gmail.com', 1, '1', '2019-07-20 10:20:10', '0000-00-00 00:00:00'),
(6, 'v', 'tanraj', 'tan@gmail.com', 1, '1', '2019-07-20 11:37:08', '0000-00-00 00:00:00'),
(7, 'gffg', 'gffgA', 'gffgA@Df.dd', 1, '1', '2019-07-20 11:41:02', '0000-00-00 00:00:00'),
(8, 'raju', 'dd', 'rag@dsfds.fgf', 1, '1', '2019-07-20 11:42:11', '0000-00-00 00:00:00'),
(9, 'asdfname', 'asdlast', 'zx@ZX.zxc', 1, '1', '2019-07-20 11:43:18', '0000-00-00 00:00:00'),
(10, 'ajay', 'ten', 'ajay@gmailr.com', 1, '1', '2019-07-20 12:18:56', '0000-00-00 00:00:00'),
(11, 'vasu', 'tamra', 'va@gmail.com', 1, '1', '2019-07-20 12:20:38', '0000-00-00 00:00:00'),
(12, 'ss', 'sslastname', 'sslastname@gmail.com', 1, '1', '2019-07-23 07:34:57', '0000-00-00 00:00:00'),
(13, 'firstss', 'lastss', 'lastss@last.com', 1, '1', '2019-07-23 09:25:46', '0000-00-00 00:00:00'),
(14, 'Firstrr', 'Lastrr', 'lastrr@last.com', 1, '1', '2019-07-23 09:29:28', '0000-00-00 00:00:00'),
(15, 'wefirstname', 'welastname', 'we@lastname.com', 1, '1', '2019-07-23 09:59:32', '0000-00-00 00:00:00'),
(16, 'wwfirst', 'wwlast', 'ww@wwlast.com', 1, '1', '2019-07-23 10:05:50', '0000-00-00 00:00:00'),
(17, 'wwfirst', 'wwlastname', 'ww@wwlast1.com', 2, '1', '2019-07-23 11:09:45', '0000-00-00 00:00:00'),
(18, 'rtfirst', 'rtlastname', 'rt@last.com', 1, '1', '2019-07-23 12:40:33', '0000-00-00 00:00:00'),
(19, 'erfirstname', 'erlastname', 'er@lastname.fd', 3, '1', '2019-07-23 12:42:50', '0000-00-00 00:00:00'),
(20, 'Vasu', 'Tamrakar', 'vasu1tamra@gmail.com', 1, '1', '2019-07-25 08:31:07', '0000-00-00 00:00:00'),
(21, 'xcvxcvxcv', 'dgdfg', 'dfg@DFgf.ffg', 1, '1', '2019-07-26 11:26:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `membershipplan`
--

CREATE TABLE `membershipplan` (
  `mp_id` bigint(20) NOT NULL,
  `mp_name` varchar(200) NOT NULL,
  `mp_price` decimal(8,0) NOT NULL,
  `mp_validity` varchar(250) NOT NULL,
  `mp_descriptions` text NOT NULL,
  `mp_created` datetime NOT NULL,
  `mp_modified` datetime NOT NULL,
  `mp_status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `membershipplan`
--

INSERT INTO `membershipplan` (`mp_id`, `mp_name`, `mp_price`, `mp_validity`, `mp_descriptions`, `mp_created`, `mp_modified`, `mp_status`) VALUES
(1, 'Pletinum', '30', 'Monthly Access', 'Repeate predefined chunks discovered the undoubtable sourece going to use a passage of him discovered the undoubtable source repeat predefined chunks.', '2019-06-26 15:09:12', '2019-07-17 12:54:06', '1'),
(2, 'Silver Plan', '50', 'Yearly Access', 'Repeate predefined chunks discovered the undoubtable sourece going to use a passage of him discovered the undoubtable source repeat predefined chunks.', '2019-06-27 07:37:05', '2019-07-17 08:09:49', '1'),
(3, 'Golden Plans', '100', 'Lifetime Accessds', 'Repeate predefined chunks discovered the undoubtable sourece going to use a passage of him discovered the undoubtable source repeat predefined chunks.', '2019-06-27 07:37:44', '2019-07-17 08:10:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `option_table`
--

CREATE TABLE `option_table` (
  `ot_id` bigint(20) NOT NULL,
  `ot_keyname` varchar(190) NOT NULL,
  `ot_value` text NOT NULL,
  `ot_created` datetime NOT NULL,
  `ot_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `paymentDetails`
--

CREATE TABLE `paymentDetails` (
  `pd_id` bigint(20) NOT NULL,
  `pd_txnid` text NOT NULL,
  `pd_planid` bigint(20) NOT NULL,
  `pd_bookid` bigint(20) NOT NULL,
  `pd_planprice` decimal(8,0) NOT NULL,
  `pd_userid` bigint(20) NOT NULL,
  `pd_currency` varchar(20) NOT NULL,
  `pd_payby` enum('cc','evc','mpesa') NOT NULL,
  `pd_status` varchar(50) NOT NULL,
  `pd_cardholder` varchar(255) NOT NULL,
  `pd_cardnumber` varchar(20) NOT NULL,
  `pd_cvvnumber` varchar(4) NOT NULL,
  `pd_cartexpmonth` int(2) NOT NULL,
  `pd_cardexpyear` int(5) NOT NULL,
  `pd_chargeid` varchar(255) NOT NULL,
  `pd_created` datetime NOT NULL,
  `pd_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paymentDetails`
--

INSERT INTO `paymentDetails` (`pd_id`, `pd_txnid`, `pd_planid`, `pd_bookid`, `pd_planprice`, `pd_userid`, `pd_currency`, `pd_payby`, `pd_status`, `pd_cardholder`, `pd_cardnumber`, `pd_cvvnumber`, `pd_cartexpmonth`, `pd_cardexpyear`, `pd_chargeid`, `pd_created`, `pd_modified`) VALUES
(1, 'txn_1ExD4IB7viNOPLYhIq34Focp', 1, 0, '30', 3, 'usd', 'cc', 'succeeded', 'user lastname', '4242424242424242', '123', 12, 2024, 'ch_1ExD4IB7viNOPLYh9kDyEvm0', '2019-07-17 15:21:15', '0000-00-00 00:00:00'),
(2, 'txn_1EyF6bB7viNOPLYh7sV8JepT', 1, 0, '30', 9, 'usd', 'cc', 'succeeded', 'asdfname asdlast', '4242424242424242', '123', 12, 2024, 'ch_1EyF6aB7viNOPLYhE9tVj6oM', '2019-07-20 11:43:53', '0000-00-00 00:00:00'),
(3, 'txn_1EyFfLB7viNOPLYhlJG3mUan', 1, 0, '30', 10, 'usd', 'cc', 'succeeded', 'ajay ten', '4242424242424242', '123', 12, 2024, 'ch_1EyFfLB7viNOPLYhH3EXzPKw', '2019-07-20 12:19:47', '0000-00-00 00:00:00'),
(4, '230720191563866788', 1, 0, '30', 12, 'usd', 'evc', 'waiting', '', '', '', 0, 0, '', '2019-07-23 09:26:28', '0000-00-00 00:00:00'),
(5, '230720191563867009', 1, 0, '30', 13, 'usd', 'evc', 'waiting', '', '', '', 0, 0, '', '2019-07-23 09:30:09', '0000-00-00 00:00:00'),
(6, '230720191563868843', 1, 0, '30', 14, 'usd', 'evc', 'waiting', '', '', '', 0, 0, '', '2019-07-23 10:00:43', '0000-00-00 00:00:00'),
(7, '230720191563872751', 1, 0, '30', 16, 'usd', 'mpesa', 'waiting', '', '', '', 0, 0, '', '2019-07-23 11:05:51', '0000-00-00 00:00:00'),
(8, '230720191563878478', 3, 0, '100', 17, 'usd', 'evc', 'waiting', '', '', '', 0, 0, '', '2019-07-23 12:41:18', '0000-00-00 00:00:00'),
(9, '230720191563878602', 2, 0, '50', 18, 'usd', 'mpesa', 'waiting', '', '', '', 0, 0, '', '2019-07-23 12:43:22', '0000-00-00 00:00:00'),
(10, '250720191564036313', 3, 0, '100', 19, 'usd', 'evc', 'waiting', '', '', '', 0, 0, '', '2019-07-25 08:31:53', '0000-00-00 00:00:00'),
(11, '250720191564038548', 3, 0, '100', 20, 'usd', 'evc', 'waiting', '', '', '', 0, 0, '', '2019-07-25 09:09:08', '0000-00-00 00:00:00'),
(12, 'txn_1F01GnB7viNOPLYhUbK0TY14', 1, 0, '30', 21, 'usd', 'cc', 'succeeded', 'Vasu Tamrakar', '4242424242424242', '123', 12, 2024, 'ch_1F01GnB7viNOPLYhsbl86hxr', '2019-07-25 09:21:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `s_id` bigint(20) NOT NULL,
  `s_heading` varchar(255) NOT NULL,
  `s_subtitle` varchar(255) NOT NULL,
  `s_tagline` varchar(255) NOT NULL,
  `s_link` varchar(255) NOT NULL,
  `s_image` text NOT NULL,
  `s_order` int(5) NOT NULL,
  `s_status` enum('1','0') NOT NULL,
  `s_created` datetime NOT NULL,
  `s_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`s_id`, `s_heading`, `s_subtitle`, `s_tagline`, `s_link`, `s_image`, `s_order`, `s_status`, `s_created`, `s_modified`) VALUES
(1, 'The Best BookStores Online', 'Ower 3.5 Milion ebook read with on limits...', 'E-SOMA. BUUG WALWA', 'http://esomabooks.com/', 'slider_1.png', 2, '1', '0000-00-00 00:00:00', '2019-07-17 13:12:30'),
(3, 'The Best BookStores Online', 'Ower 3.5 Milion ebook read with on limits...', 'E-SOMA. BUUG WALWA', 'http://localhost/ebook/dashboard/vvv/1', 'slider_11.png', 1, '1', '2019-06-27 15:17:52', '2019-07-17 13:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `user_credentials`
--

CREATE TABLE `user_credentials` (
  `uc_id` bigint(20) NOT NULL,
  `uc_authid` varchar(150) NOT NULL,
  `uc_firstname` varchar(150) NOT NULL,
  `uc_lastname` varchar(150) NOT NULL,
  `uc_email` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `uc_mobile` varchar(15) NOT NULL,
  `uc_password` varchar(255) NOT NULL,
  `uc_image` varchar(255) NOT NULL,
  `uc_role` int(2) NOT NULL,
  `uc_secrete` varchar(40) NOT NULL,
  `uc_address` text NOT NULL,
  `uc_created` datetime NOT NULL,
  `uc_modified` datetime NOT NULL,
  `uc_status` enum('1','0') NOT NULL,
  `uc_active` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_credentials`
--

INSERT INTO `user_credentials` (`uc_id`, `uc_authid`, `uc_firstname`, `uc_lastname`, `uc_email`, `uc_mobile`, `uc_password`, `uc_image`, `uc_role`, `uc_secrete`, `uc_address`, `uc_created`, `uc_modified`, `uc_status`, `uc_active`) VALUES
(1, '', 'Vasu', 'Tamrakar', 'vasu.impetrosys@gmail.com', '+917000667679', '81dc9bdb52d04dc20036dbd8313ed055', '', 1, '', 'Indore India.', '2019-07-17 00:00:00', '2019-07-23 07:34:13', '1', '1'),
(2, '', 'Author', 'AuthorLastname', 'author@esomabooks.com', '+9172121213232', 'e8f5564586a0bbfcbfd6bcec9d8e4369', 'kPl0TGN.jpg', 4, '', '', '2019-07-17 14:37:51', '2019-07-17 14:59:32', '1', '1'),
(3, '', 'user', 'lastname', 'user@gmail.com', '+919854854768', 'e10adc3949ba59abbe56e057f20f883e', 'Ankit_edit94402.jpg', 5, '', '', '2019-07-17 15:21:09', '2019-07-18 09:23:14', '0', '0'),
(4, '', 'vasd', 'Tamrakar', 'author@gmail.com', 'asdf', 'c3c7ba886c149e908f0510915f436c8a', 'people_think2.png', 4, '', '', '2019-07-18 10:06:30', '2019-07-18 14:47:08', '1', '1'),
(5, '', 'amit', 'rairr', 'amit@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', '', '2019-07-20 11:35:23', '0000-00-00 00:00:00', '0', '0'),
(6, '', 'v', 'tanraj', 'tan@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', '', '2019-07-20 11:37:29', '0000-00-00 00:00:00', '0', '0'),
(7, '', 'gffg', 'gffgA', 'gffgA@Df.dd', '', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', '', '2019-07-20 11:41:29', '0000-00-00 00:00:00', '0', '0'),
(8, '', 'raju', 'dd', 'rag@dsfds.fgf', '', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', '', '2019-07-20 11:42:38', '0000-00-00 00:00:00', '0', '0'),
(9, '', 'asdfname', 'asdlast', 'zx@ZX.zxc', '', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', '', '2019-07-20 11:43:50', '0000-00-00 00:00:00', '0', '0'),
(10, '', 'ajay', 'ten', 'ajay@gmailr.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', '', '2019-07-20 12:19:46', '0000-00-00 00:00:00', '1', '1'),
(11, '', 'ss', 'sslastname', 'sslastname@gmail.com', '+9188546545', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', 'S2 yashbant plaza.Indore', '2019-07-23 08:55:07', '0000-00-00 00:00:00', '0', '0'),
(12, '', 'firstss', 'lastss', 'lastss@last.com', '+917777777777', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', 'Indore this address testing.', '2019-07-23 09:26:28', '0000-00-00 00:00:00', '0', '0'),
(13, '', 'Firstrr', 'Lastrr', 'lastrr@last.com', '+918877777777', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', 'Firstrr Lastrr indore', '2019-07-23 09:30:09', '0000-00-00 00:00:00', '0', '0'),
(14, '', 'wefirstname', 'welastname', 'we@lastname.com', '+9170004241111', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', 'This is for testing.Indore address.', '2019-07-23 10:00:43', '0000-00-00 00:00:00', '0', '0'),
(15, '', 'wwfirst', 'wwlast', 'ww@wwlast.com', '+918788787777', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', 'Test mpesa.', '2019-07-23 10:57:48', '0000-00-00 00:00:00', '0', '0'),
(16, '', 'wwfirst', 'wwlast', 'ww@wwlast1.com', '+915454545454', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', 'mpesa Indore1212121', '2019-07-23 11:05:50', '0000-00-00 00:00:00', '0', '0'),
(17, '', 'rtfirst', 'rtlastname', 'rt@last.com', '+954545454444', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', '45454545', '2019-07-23 12:41:18', '0000-00-00 00:00:00', '0', '0'),
(18, '', 'erfirstname', 'erlastname', 'er@lastname.fd', '+8788787546456', 'e10adc3949ba59abbe56e057f20f883e', '', 5, '', 'dfgdfgdgdfgd', '2019-07-23 12:43:22', '0000-00-00 00:00:00', '0', '0'),
(22, '', 'Basu', 'Tamrakar', 'vasu1tamra@gmail.com', '+917417417410', 'e10adc3949ba59abbe56e057f20f883e', 'book_3.png', 4, '', '', '2019-07-25 09:53:33', '0000-00-00 00:00:00', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `ur_id` int(11) NOT NULL,
  `ur_name` varchar(250) NOT NULL,
  `uc_created` datetime NOT NULL,
  `uc_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`ur_id`, `ur_name`, `uc_created`, `uc_modified`) VALUES
(1, 'Administrator', '2019-06-20 05:14:00', '0000-00-00 00:00:00'),
(2, 'Manager', '2019-06-20 05:15:00', '0000-00-00 00:00:00'),
(3, 'Contributor', '2019-06-20 05:38:00', '0000-00-00 00:00:00'),
(4, 'Author', '2019-06-20 05:40:00', '0000-00-00 00:00:00'),
(5, 'Customer', '2019-07-03 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `v_id` bigint(20) NOT NULL,
  `v_language` enum('english','somali') NOT NULL,
  `v_category` enum('Testimonial Videos','Promotional Videos','Training and Tools Videos','Premium Training Videos') NOT NULL,
  `v_title` varchar(255) NOT NULL,
  `v_premium` enum('true','false') NOT NULL,
  `v_description` text NOT NULL,
  `v_image` varchar(255) NOT NULL,
  `v_videoid` varchar(255) NOT NULL,
  `v_like` bigint(20) NOT NULL,
  `v_status` enum('1','0') NOT NULL,
  `v_created` datetime NOT NULL,
  `v_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`v_id`, `v_language`, `v_category`, `v_title`, `v_premium`, `v_description`, `v_image`, `v_videoid`, `v_like`, `v_status`, `v_created`, `v_modified`) VALUES
(1, 'somali', 'Training and Tools Videos', 'TEST First Video', 'true', '<div style=\"text-align: center;\"><span style=\"font-weight: bold;\">Hello</span><br></div>this is description field koko a sd kasdlk asd; ;lsad;l<br>   ', 'doc-pdf1.jpg', '59488722', 0, '1', '2019-07-18 12:12:44', '2019-07-18 14:30:28'),
(2, 'somali', 'Training and Tools Videos', 'Test second video', 'true', 'hiom sisadmlkjaslkd faslkdjfsk adfjksa kdlf jdslk k<br>', '', '59488722', 0, '1', '2019-07-18 12:15:37', '0000-00-00 00:00:00'),
(3, 'english', 'Promotional Videos', 'sdfdsafsad', 'true', 'sdafdsafasdfasdf', '', '59488722', 0, '1', '2019-07-18 12:24:09', '0000-00-00 00:00:00'),
(4, 'english', 'Promotional Videos', 'asdfasdfasdfa', 'true', 'sadfsadfasasdfasdf', '', '59488722', 0, '1', '2019-07-18 12:24:57', '0000-00-00 00:00:00'),
(5, 'somali', 'Training and Tools Videos', 'tese cvidkevvvvv', 'true', 'zzsd ma dfsa dsa&nbsp; dsa dsfa dsaf&nbsp; asdf asd f sadf sdaf dsfa dfsa <br>', 'full-logo.png', '59488722', 0, '1', '2019-07-18 12:26:52', '2019-07-18 14:26:48'),
(6, 'somali', 'Promotional Videos', 'zxcvzxcvzxcv', 'true', 'zxcvzxcvzxcvzxcv', 'doc-pdf.jpg', 'zxcvzxcvzxc', 0, '1', '2019-07-18 12:29:49', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `book_access`
--
ALTER TABLE `book_access`
  ADD PRIMARY KEY (`ba_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `emailSubscriber`
--
ALTER TABLE `emailSubscriber`
  ADD PRIMARY KEY (`es_id`),
  ADD UNIQUE KEY `esmail` (`es_email`);

--
-- Indexes for table `membershipplan`
--
ALTER TABLE `membershipplan`
  ADD PRIMARY KEY (`mp_id`);

--
-- Indexes for table `option_table`
--
ALTER TABLE `option_table`
  ADD PRIMARY KEY (`ot_id`),
  ADD UNIQUE KEY `otkeyname` (`ot_keyname`);

--
-- Indexes for table `paymentDetails`
--
ALTER TABLE `paymentDetails`
  ADD PRIMARY KEY (`pd_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `user_credentials`
--
ALTER TABLE `user_credentials`
  ADD PRIMARY KEY (`uc_id`),
  ADD UNIQUE KEY `ucemail` (`uc_email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`ur_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`v_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `a_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `b_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `book_access`
--
ALTER TABLE `book_access`
  MODIFY `ba_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `c_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `emailSubscriber`
--
ALTER TABLE `emailSubscriber`
  MODIFY `es_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `membershipplan`
--
ALTER TABLE `membershipplan`
  MODIFY `mp_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `option_table`
--
ALTER TABLE `option_table`
  MODIFY `ot_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paymentDetails`
--
ALTER TABLE `paymentDetails`
  MODIFY `pd_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `s_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_credentials`
--
ALTER TABLE `user_credentials`
  MODIFY `uc_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `ur_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `v_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
