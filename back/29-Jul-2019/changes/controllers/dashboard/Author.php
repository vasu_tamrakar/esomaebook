<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Author extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		
	}
	public function index()
	{
		
		$data["title"] = 'Add New Author Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		
		
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}else{
			$post = $this->input->post();
			if($post){
				print_r($post); die;
				$this->form_validation->set_rules('txt_authorname','Author Name','required');
				$this->form_validation->set_rules('txt_published','Publish Year','required');
				$this->form_validation->set_rules('txt_description','Description','required');
				if($this->form_validation->run() == TRUE){
					$fomdata["a_name"] = $post["txt_authorname"];
					$fomdata["a_publishbooks"] = $post["txt_published"];
					$fomdata["a_description"] = $post["txt_description"];
					$fomdata["a_facebook"] = $post["txt_facebook"];
					$fomdata["a_twitter"] = $post["txt_twitter"];
					$fomdata["a_gplush"] = $post["txt_gplush"];
					$fomdata["a_instagram"] = $post["txt_instagram"];
					$fomdata["a_fK_of_uc_id"] = $post["txt_byuserid"];
					$fomdata["a_status"] = '1';
					$fomdata["a_created"] = date("Y-m-d H:i:s");

					if($_FILES['txt_image']['name']){
						$Imgres = $this->do_upload('txt_image');
							
						if(isset($Imgres['upload_data'])){
							$fomdata['a_image'] = $Imgres['upload_data']['file_name'];
							
						}else{
							$this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,$Imgres['error']);
							redirect('dashboard/addnewAuthor'); exit();
						}
					}

					$insertRes = $this->user_Auth->insert('authors', $fomdata);
					if($insertRes){
						$this->session->set_flashdata('alert', 'success');
						$this->session->set_flashdata('message', 'Successfully save.');
						redirect('dashboard/addnewAuthor'); exit(); 
					}else{
						$this->session->set_flashdata('alert', 'error');
						$this->session->set_flashdata('message', 'Insert Failed.');
						$this->load->view('dashboard/common/header',$data);
						$this->load->view('dashboard/page-addauthor',$data);
						$this->load->view('dashboard/common/footer',$data);
					}
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-addauthor',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-addauthor',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
			 
		}
		
	}
	/*-------------------------------------------
		do_upload(name,folder);
	-------------------------------------------*/

	public function do_upload($fileName)
    {
        $config['upload_path']          = './uploads/users/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2048;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($fileName))
        {
                return array('error' => $this->upload->display_errors());

        }
        else
        {
                return array('upload_data' => $this->upload->data());

        }
    }

    /*----------------------------------------------
		viewallAuthors()
    ----------------------------------------------*/
    public function viewallAuthors(){
    	$data["title"] = 'View All Author Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		$data['allAuthorsids'] = $this->user_Auth->getData('user_credentials',$w=array("uc_role"=>4) ,$se='uc_id', $short='uc_id DESC');
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-viewallauthors',$data);
		$this->load->view('dashboard/common/footer',$data);
    }


	/*----------------------------------------------
		authorDetails($aid)
    ----------------------------------------------*/
    public function authorDetails($aid){
    	$data["title"] = 'View Author Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($aid){
			$data['authorDetails'] = $this->user_Auth->getData('user_credentials',$w=array('uc_id' => $aid) ,$se='', $short='');
	    	$this->load->view('dashboard/common/header',$data);
			$this->load->view('dashboard/page-viewauthorsdetails',$data);
			$this->load->view('dashboard/common/footer',$data);
		}
		
    }

    /*----------------------------------------------
		editauthorDetails($aid)
    ----------------------------------------------*/
    public function editauthorDetails($aid){
    	$data["title"] = 'Edit Author Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($aid){
			$data['authorDetails'] = $this->user_Auth->getData('user_credentials',$w=array('uc_id' => $aid) ,$se='', $short='');

			$post = $this->input->post();
			if($post){
				// print_r($post); die;
				$this->form_validation->set_rules('txt_authorname', ' Author Name','required');
				$this->form_validation->set_rules('txt_published', ' Author Publish','required');
				$this->form_validation->set_rules('txt_description', ' Author Description','required');
				if($this->form_validation->run() === TRUE){
					$formData["a_name"] = $post["txt_authorname"];
					$formData["a_publishbooks"] = $post["txt_published"];
					$formData["a_description"] = $post["txt_description"];
					$formData["a_facebook"] = $post["txt_facebook"];
					$formData["a_twitter"] = $post["txt_twitter"];
					$formData["a_gplush"] = $post["txt_gplush"];
					$formData["a_instagram"] = $post["txt_instagram"];
					
					
					$formData["a_status"] = (($post["txt_status"]==1)?'1':'0');

					if($post["txt_created"]){
						$formData["a_created"] = date('Y-m-d H:i:s',strtotime($post["txt_created"]));	
					}
					
					$formData["a_modified"] = date('Y-m-d H:i:s');
					if($_FILES['txt_image']['name']){
						if($_FILES['txt_image']['name']){
							$Imgres = $this->do_upload('txt_image');
								
							if(isset($Imgres['upload_data'])){
								$formData['a_image'] = $Imgres['upload_data']['file_name'];
								
							}else{
								$this->session->set_flashdata('alert' ,'error');
								$this->session->set_flashdata('message' ,$Imgres['error']);
								$this->load->view('dashboard/common/header', $data);
								$this->load->view('dashboard/page-editauthor', $data);
								$this->load->view('dashboard/common/footer', $data);
								exit();
							}
						}
					}
					$updated = $this->user_Auth->update('user_credentials',$formData,array('uc_id'=>$aid));
					if($updated){
						$this->session->set_flashdata('alert' ,'success');
						$this->session->set_flashdata('message' ,'Successfully Updated.');
						redirect('dashboard/editauthorDetails/'.$aid); exit();
					}else{
						$this->session->set_flashdata('alert' ,'error');
						$this->session->set_flashdata('message' ,'Author update failed.');
						$this->load->view('dashboard/common/header',$data);
						$this->load->view('dashboard/page-editauthor',$data);
						$this->load->view('dashboard/common/footer',$data);
					}
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-editauthor',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-editauthor',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
	    	
		}
		
    }
    /*----------------------------------------------
		deleteAuthor();
    ----------------------------------------------*/
    public function deleteAuthor(){
    	$post = $this->input->post();
    	if($post){
    		$aId = $post["delid"];
    		$delete = $this->user_Auth->delete('user_credentials', array('uc_id' => $aId));
    		if($delete){
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/viewallAuthors'), 'message' => 'Author Delete Successfully Done.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Process Process Failed.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid operation.'));
    	}
    }
    /*----------------------------------------------
		addAuthor();
    ----------------------------------------------*/
    public function addAuthor(){
    	$post = $this->input->post();
    	if($post){
    		$formData["uc_firstname"] = $post["txt_authorfirstname"];
    		$formData["uc_lastname"] = $post["txt_authorlastname"];
    		$formData["uc_email"] = $post["txt_authoremail"];
    		$formData["uc_mobile"] = $post["txt_authormobile"];
    		$created = date('Y-m-d H:i:s');
    		$formData["uc_created"] = $created;
    		$formData["uc_status"] = "1";
    		$formData["uc_role"] = 4;
    		$formData["uc_active"] = '1';
    		$pass = 'ebok'.mt_rand(100000, 999999);;
    		$formData["uc_password"] = md5($pass);

    		if($_FILES["txt_image"]["name"]){
    			$imageData = $this->do_upload("txt_image");
    			if(isset($imageData['upload_data'])){
					$formData['uc_image'] = $imageData['upload_data']['file_name'];
				}else{
					echo json_encode( array( "status" => "fail","message" => '<div class="alert alert-danger alert-dismissible fade in" role="alert"><strong style="text-transform: capitalize;">Error !</strong> '.$imageData["error"].'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'));
					die();
				}
    			// $formData["uc_image"] = $_FILES["txt_image"]["name"];	
    		}
    		
    		$resExistemail =  $this->user_Auth->existEmail($post["txt_authoremail"]);
			$resExistmobile =  $this->user_Auth->existMobile($post["txt_authormobile"]);
			/* Check email exist ornot */
			if($resExistemail){
				echo json_encode( array( "status" => "fail","message" => '<div class="alert alert-danger alert-dismissible fade in" role="alert"><strong style="text-transform: capitalize;">Error !</strong> '.$post["txt_authoremail"].'" Email Address Already Registered.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'));
				die();
			}elseif($resExistmobile){
				echo json_encode( array( "status" => "fail","message" => '<div class="alert alert-danger alert-dismissible fade in" role="alert"><strong style="text-transform: capitalize;">Error !</strong> '.$post["txt_authormobile"].'" Mobile Number Already Registered.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'));
				die();
			}else{
				$insertID =  $this->user_Auth->insert('user_credentials',$formData);
				if($insertID){
					$formData2["a_fK_of_uc_id"] = $insertID;
		    		$formData2["a_publishbooks"] = $post["txt_published"];
		    		$formData2["a_facebook"] = $post["txt_facebook"];
		    		$formData2["a_twitter"] = $post["txt_twitter"];
		    		$formData2["a_gplush"] = $post["txt_gplush"];
		    		$formData2["a_instagram"] = $post["txt_instagram"];
		    		$formData2["a_description"] = $post["text_message"];
		    		$formData2["a_status"] = '1';
		    		$formData2["a_created"] = $created;

					$user = $this->user_Auth->insert('authors',$formData2);
					$html = '<p>Hi '.$formData["uc_firstname"].'.</p>
					<p>Your account generated Your email address is '.$formData["uc_email"].' And password is '.$pass.'.</p>';
					
					$config['protocol'] 	= 'smtp';
			        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
			        $config['smtp_port'] 	= '465';
			        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
			        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
			        $config['mailtype'] 	= 'html';
			        $config['charset'] 		= 'utf-8';
			        $config['newline'] 		= "\r\n";
			        $config['wordwrap'] 	= TRUE;
					$this->email->initialize($config);
					$this->email->set_mailtype("html");
					$this->email->set_newline("\r\n");

					$this->email->to($formData["uc_email"]);
			        $this->email->from('pareshnagar87@gmail.com', SITENAME);

			        $this->email->subject( (isset($subject)?$subject:'Author Account Generated') );
			        $this->email->message($html);
			        $emailsend =$this->email->send();
					if($emailsend){
						echo json_encode(array("status" => "200", "data" =>$html, "message" =>'<div class="alert alert-success alert-dismissible fade in" role="alert"><strong style="text-transform: capitalize;">Success !</strong> Author registration process completed. Email will be send.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'));
						die();
					}else{
						echo json_encode(array("status" => "200", "data" =>$html, "message" =>'<div class="alert alert-info alert-dismissible fade in" role="alert"><strong style="text-transform: capitalize;">Success !</strong> Author register completed.email not send.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'));
						die();
					}
				}else{
					echo json_encode(array("status" => "fail", "message" =>'<div class="alert alert-danger alert-dismissible fade in" role="alert"><strong style="text-transform: capitalize;">Error !</strong> Registration process failed please try again after sometime.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'));
					die();
				}
			}

    	}else{
    		echo json_encode(array("status" => "fail", "message" =>'<div class="alert alert-danger alert-dismissible fade in" role="alert"><strong style="text-transform: capitalize;">Error !</strong> In-valid Method!.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'));
    		die();
    	}
    	die();
    }

 	/*-----------------------------------------------
 		editAjaxAuthor()
 	-----------------------------------------------*/   
    
    public function editAjaxAuthor(){
    	$post= $this->input->post();
    	if($post){
    		$authorID = $post["txt_authorid"];
    		$formData["a_publishbooks"] 	=  $post["txt_published"];
    		$formData["a_facebook"] =  $post["txt_facebook"];
		    $formData["a_twitter"] 	=  $post["txt_twitter"];
		    $formData["a_gplush"] 	=  $post["txt_gplush"];
		    $formData["a_instagram"] = $post["txt_instagram"];
		    $formData["a_description"] =$post["text_message"];
		    $formData["a_modified"] =date('Y-m-d H:i:s');
		    $updated = $this->user_Auth->update('authors', $formData, array('a_fK_of_uc_id'=>$authorID));
    		if($updated){
    			echo json_encode(array("status" => "200","message" => "Successfully Updated!." ));
    		}else{
    			echo json_encode(array("status" => "300","message" => "Updation Process Failed!." ));
    		}
    	}else{
    		echo json_encode(array("status" => "failed","message" => "In-valid Opration!."));
    	}
    	die();	
    }

}
