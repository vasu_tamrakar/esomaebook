<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->library('upload');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
	}
	public function index()
	{
		$data["title"] = 'Add New User Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
		}else{
			$post = $this->input->post();
			if($post){
				// print_r($post); die;
				// Array ( [txt_name] => b [txt_published] => 2014 [txt_rating] => 1 [txt_category] => 7 )
				$this->form_validation->set_rules('txt_firstname','First Name','required');
				$this->form_validation->set_rules('txt_lastname','Last Name','required');
				$this->form_validation->set_rules('txt_email','Email','required|valid_email|is_unique[user_credentials.uc_email]');
				$this->form_validation->set_rules('txt_mobile','Mobile','required|is_unique[user_credentials.uc_mobile]');
				$this->form_validation->set_rules('txt_role','User Role','required');
				$this->form_validation->set_rules('txt_password','Password','required');
				if($this->form_validation->run() === TRUE){
					$resExistemail =  $this->user_Auth->existEmail($post["txt_email"]);
					$resExistmobile =  $this->user_Auth->existMobile($post["txt_mobile"]);
					/* Check email exist ornot */
					if($resExistemail){
						$this->session->set_flashdata('alert','info');
						$this->session->set_flashdata('message',$post["txt_email"].' Email Address Already Registered.');
						redirect('dashboard/addnewUser'); exit();
					}elseif($resExistmobile){
						$this->session->set_flashdata('alert','info');
						$this->session->set_flashdata('message',$post["txt_mobile"].' Mobile Number Already Registered.');
						redirect('dashboard/addnewUser'); exit();
					}else{

						$formData["uc_firstname"] = $post["txt_firstname"];
						$formData["uc_lastname"] = $post["txt_lastname"];
						$formData["uc_email"] = $post["txt_email"];
						$formData["uc_mobile"] = $post["txt_mobile"];
						$formData["uc_password"] = md5($post["txt_password"]);
						$formData["uc_role"] = $post["txt_role"];
						$formData["uc_created"] = date("Y-m-d H:i:s");
						$formData["uc_status"] = '0';
						$formData["uc_active"] = '0';
						if($_FILES['txt_image']['name']){
							
							$img_config['upload_path']          = './uploads/users/';
					        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
					        $img_config['max_size']             = 3072;
					        // $img_config['encrypt_name'] = TRUE;
					        
					        $this->upload->initialize($img_config);
					        if ( ! $this->upload->do_upload('txt_image'))
					        {
					            $this->session->set_flashdata('alert' ,'error');
								$this->session->set_flashdata('message' ,$this->upload->display_errors());
								redirect('dashboard/addnewUser/'); exit();
					        }
					        else
					        {
					            $upload_data = $this->upload->data();
					            $formData['uc_image'] = $upload_data['file_name'];
					            
					        }
						}
						$instID =  $this->user_Auth->insert('user_credentials',$formData);
						if($instID){
							$user = $this->user_Auth->getData('user_credentials',$where=array('uc_id' =>$instID),$sel='',$sort ='');
							$emailsend = $this->signup_verification($formData["uc_email"],$subject="",$user);
							if($emailsend === 1){
								$this->session->set_flashdata('alert','success');
								$this->session->set_flashdata('message',$post["txt_email"].' Email Address Successfully Registered And Please check registered email for receiving activation link.');
								redirect('dashboard/addnewUser'); exit();	
							}else{
								$this->session->set_flashdata('alert','success');
								$this->session->set_flashdata('message',$post["txt_email"].' Email Address Successfully Registered but email sending is failed.');
								redirect('dashboard/addnewUser'); exit();
							}
							
						}else{
							$this->session->set_flashdata('alert','error');
							$this->session->set_flashdata('message','Registration process failed Please try to after some time.');
							redirect('dashboard/addnewUser'); exit();
						}
					}
				}else{
					$this->load->view('dashboard/common/header',$data);
					$this->load->view('dashboard/page-addnewuser',$data);
					$this->load->view('dashboard/common/footer',$data);
				}
				
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-addnewuser',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
			 
		}
		
	}
	

    /*----------------------------------------------
		viewallUsers()
    ----------------------------------------------*/
    public function viewallUsers(){
    	$data["title"] = 'View All Users Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		$data['allUsersids'] = $this->user_Auth->getData('user_credentials',$w='' ,$se='uc_id', $short='uc_id DESC');
    	$this->load->view('dashboard/common/header',$data);
		$this->load->view('dashboard/page-viewallusers',$data);
		$this->load->view('dashboard/common/footer',$data);
    }


	/*----------------------------------------------
		userDetails($aid)
    ----------------------------------------------*/
    public function userDetails($uid){
    	$data["title"] = 'View User Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($uid){
			$data['userDetails'] = $this->user_Auth->getData('user_credentials',$w=array('uc_id' => $uid) ,$se='', $short='');
	    	$this->load->view('dashboard/common/header',$data);
			$this->load->view('dashboard/page-viewusersdetails',$data);
			$this->load->view('dashboard/common/footer',$data);
		}
		
    }

    /*----------------------------------------------
		edituserDetails($aid)
    ----------------------------------------------*/
    public function edituserDetails($uid){
    	$data["title"] = 'Edit User Details Page | '.SITENAME;
		$data["login_userid"] = $this->is_logged_in_user;
		$data["is_logged_in_user_info"] = $this->is_logged_in_user_info;
		if($uid){
			$data['userDetails'] = $this->user_Auth->getData('user_credentials',$w=array('uc_id' => $uid) ,$se='', $short='');
			$post = $this->input->post();
			if($post){
				$this->form_validation->set_rules('txt_firstname','First Name','required');
				$this->form_validation->set_rules('txt_lastname','Last Name','required');
				$this->form_validation->set_rules('txt_email','Email','required');
				$this->form_validation->set_rules('txt_role','User Role','required');

				if($this->form_validation->run() ===TRUE){
					$form_data['uc_firstname'] = $post["txt_firstname"];
					$form_data['uc_lastname'] = $post["txt_lastname"];
					$form_data['uc_email'] = $post["txt_email"];
					$form_data['uc_mobile'] = $post["txt_mobile"];
					if($post["txt_mobile"]){
						$form_data['uc_mobile'] = $post["txt_mobile"];	
					}
					
					if($post["txt_password"]){
						$form_data['uc_password'] = md5($post["txt_password"]);	
					}
					if($post["txt_created"]){
						$form_data['uc_created'] = date('Y-m-d H:i:s',strtotime($post["txt_created"]));	
					}
					
					$form_data['uc_status'] = (($post["txt_status"] == 1)?'1':'0');
					$form_data['uc_active'] = (($post["txt_active"] == 1)?'1':'0');
					$form_data['uc_role'] = $post["txt_role"];
					$form_data['uc_modified'] = date('Y-m-d H:i:s');
					$result = $this->user_Auth->exist_emailMobile($post["txt_email"], $post["txt_mobile"], $uid);
					if($result["status"] == 'success'){

						if($_FILES['txt_image']['name']){
							
							$img_config['upload_path']          = './uploads/users/';
					        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
					        $img_config['max_size']             = 3072;
					        // $img_config['encrypt_name'] = TRUE;
					        
					        $this->upload->initialize($img_config);
					        if ( ! $this->upload->do_upload('txt_image'))
					        {
					            $this->session->set_flashdata('alert' ,'error');
								$this->session->set_flashdata('message' ,$this->upload->display_errors());
								redirect('dashboard/edituserDetails/'.$uid); exit();
					        }
					        else
					        {
					            $upload_data = $this->upload->data();
					            $form_data['uc_image'] = $upload_data['file_name'];
					            
					        }
						}
						$resultUpdate = $this->user_Auth->update('user_credentials', $form_data, $filter=array('uc_id' => $uid) );
						if($resultUpdate){
							

							$this->session->set_flashdata('alert', "success");
							$this->session->set_flashdata('message', 'Profile successfully updated..');
							redirect('dashboard/edituserDetails/'.$uid);
							exit();
						}else{
							$this->session->set_flashdata('alert' ,'error');
							$this->session->set_flashdata('message' ,'Update Process Failed.');
							$this->load->view('dashboard/common/header', $data);
							$this->load->view('dashboard/page-edituser', $data);
							$this->load->view('dashboard/common/footer', $data);
							exit();
						}
						

					}else{
						$this->session->set_flashdata('alert' ,'error');
						$this->session->set_flashdata('message' ,$result["message"]);
						$this->load->view('dashboard/common/header', $data);
						$this->load->view('dashboard/page-edituser', $data);
						$this->load->view('dashboard/common/footer', $data);
					}

				}else{
					$this->load->view('dashboard/common/header', $data);
					$this->load->view('dashboard/page-edituser', $data);
					$this->load->view('dashboard/common/footer', $data);
				}
			}else{
				$this->load->view('dashboard/common/header',$data);
				$this->load->view('dashboard/page-edituser',$data);
				$this->load->view('dashboard/common/footer',$data);
			}
	    	
		}
		
    }
    /*-----------------------------------------
		deleteuserDetail()
    -----------------------------------------*/
    public function deleteuserDetail(){
    	$post = $this->input->post();
    	if($post){
    		$uId = $post["delid"];
    		$delete = $this->user_Auth->delete('user_credentials', array('uc_id' => $uId));
    		if($delete){
    			echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/viewallUsers'), 'message' => 'User Delete Successfully Done.'));
    		}else{
				echo json_encode(array('status' => 'Fail', 'message' => 'Process Process Failed.'));
    		}
    	}else{
    		echo json_encode(array('status' => 'error', 'message' => 'In-valid operation.'));
    	}
    }

    /*------------------------------------------------
    	signup_verification($to, $subject=false, $data)
    ------------------------------------------------*/
    public function signup_verification($to, $subject=false, $data){
		$url = site_url('signIn/activated/?time='.base64_encode($data[0]->uc_id).'&slot='.base64_encode($data[0]->uc_email));
		$html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block; width:100%">
					                    <h6>Hi '.$data[0]->uc_firstname.',</h6>
					                	<p>For activation your account click  <a href="'.$url.'"> me</a></p>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		$this->email->to($to);
        $this->email->from('pareshnagar87@gmail.com', SITENAME);

        $this->email->subject( (($subject)?$subject:'Ebook SignUp Verification') );
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
        	return 1;
        }else{
    		$this->email->print_debugger();
    	}
	}
    
}
