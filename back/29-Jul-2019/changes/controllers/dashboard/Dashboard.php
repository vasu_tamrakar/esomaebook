<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	private $is_logged_in_user=FALSE;
	private $is_logged_in_user_info=FALSE;

	public function __construct (){
		parent :: __construct();
		if(!$this->session->userdata('is_logged_in_user')){
			redirect('signIn'); exit();
		}
		
		$this->is_logged_in_user = $this->session->userdata('is_logged_in_user');
		$this->is_logged_in_user_info = $this->session->userdata('is_logged_in_user_info');
		$this->load->model('user_Auth');
		$this->load->library('upload');
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
	}
	/*------------------------------------
		Dashboard index 
	------------------------------------*/
	public function index()
	{
		$data['title'] = 'Dashboard Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		if($this->is_logged_in_user_info['u_role'] > 3){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$this->load->view('dashboard/common/header', $data);
		$this->load->view('dashboard/page-dashboard', $data);
		$this->load->view('dashboard/common/footer', $data);
	}

	/*------------------------------------
		homeSlider() 
	------------------------------------*/
	public function homeSlider(){
		$data['title'] = 'Home Slider Setting Page | '.SITENAME;
		$data['login_userid'] = $this->is_logged_in_user;
		$data['is_logged_in_user_info'] = $this->is_logged_in_user_info;
		$data['sliderSlide'] = $this->user_Auth->getData('sliders',$w='',$sel='',$sh='s_id DESC'); 
		$this->load->view('dashboard/common/header', $data);
		$this->load->view('dashboard/page-homeslidersetting', $data);
		$this->load->view('dashboard/common/footer', $data);
	}
	/*------------------------------------
		addNewSlide() 
	------------------------------------*/
	public function addNewSlide(){
		if($this->is_logged_in_user_info['u_role'] > 4){
			echo '<label style="margin:0 auto; padding:5px; border:1px solid gray;border-radius:5px;"><strong>Warning: </strong>Access denide!.. <a href="'.base_url().'">Back</a></label>';
			exit();
		}
		$post = $this->input->post();
		if($post){
			$formData['s_heading'] = $post["txt_heading"];
			$formData['s_subtitle'] = $post["txt_subtitle"];
			$formData['s_tagline'] = $post["txt_tagline"];
			$formData['s_link'] = $post["txt_link"];
			$formData['s_created'] = date('Y-m-d H:i:s');
			$formData['s_status'] = '1';
			if($_FILES['txt_image']['name']){
				$img_config['upload_path']          = './uploads/sliders/';
		        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
		        $img_config['max_size']             = 2048;
		        $img_config['min_width']            = 1528;
                $img_config['min_height']           = 600;
		        // $img_config['encrypt_name'] = TRUE;
		        
		        $this->upload->initialize($img_config);
		        if ( ! $this->upload->do_upload('txt_image'))
		        {
		            echo json_encode(array('status' => 'error', 'message' => $this->upload->display_errors()));
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            $formData['s_image'] = $upload_data['file_name'];
		            $instID = $this->user_Auth->insert('sliders',$formData);
			        if($instID){
			        	echo json_encode(array('status' => 'Success', 'message' => 'Successfully Done.'));
			        }else{
			        	echo json_encode(array('status' => 'error', 'message' => 'Insert Process Failed.'));
			        }
		        }
		        
			}
			
			
		}else{
			echo json_encode(array('status' => 'error', 'message' => 'Invalid Input.'));
		}
		die();
	}
	/*------------------------------------
		deleteslideDetail() 
	------------------------------------*/
	public function deleteslideDetail(){
		$post = $this->input->post();
		if($post){
			$sId = $post['delid'];
			$delete = $this->user_Auth->delete('sliders', array('s_id' => $sId));
			if($delete){
				echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/homeSlider'), 'message' => 'Successfully Done.'));
			}else{
				echo json_encode(array('status' => 'error', 'message' => 'Operation failed.'));
			}
		}else{
			echo json_encode(array('status' => 'error', 'message' => 'Invalid Input.'));
		}
		die();
	}
	/*------------------------------------
		getSlideData() 
	------------------------------------*/
	public function getSlideData(){
		$post = $this->input->post();
		if($post){
			$sId = $post['dataid'];
			$returnData = $this->user_Auth->getData('sliders', array('s_id' => $sId), $s='',$srt='');
			if($returnData){
				$html ='<div class="form-group">
    	<input type="hidden" name="edittxt_id" id="edittxt_id" value="'.$returnData[0]->s_id.'">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edittxt_heading">Heading <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="edittxt_heading" id="edittxt_heading" value="'.$returnData[0]->s_heading.'" placeholder="Heading" class="form-control col-md-7 col-xs-12" max-length="150">
		</div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edittxt_subtitle">Sub  Title <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="edittxt_subtitle" id="edittxt_subtitle" value="'.$returnData[0]->s_subtitle.'" placeholder="Sub title" class="form-control col-md-7 col-xs-12" max-length="200">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edittxt_tagline">Slide Tagline <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="edittxt_tagline" id="edittxt_tagline" value="'.$returnData[0]->s_tagline.'" placeholder="Tag Line" class="form-control col-md-7 col-xs-12" max-length="100">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edittxt_link">Read More Link <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
       <input type="text" name="edittxt_link" id="edittxt_link" value="'.$returnData[0]->s_link.'" placeholder="https://www.example.lcom" class="form-control col-md-7 col-xs-12" max-length="200">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_image">Image <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
       <input type="file" name="edittxt_image" id="edittxt_image" class="form-control col-md-7 col-xs-12" accept="image/*">
       	
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edittxt_created"> Created Date
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
       <input type="text" name="edittxt_created" id="edittxt_created" value="" placeholder="'.(($returnData[0]->s_created >0)?date('Y-m-d H;i:s',strtotime($returnData[0]->s_created)):"").'" class="form-control col-md-7 col-xs-12" max-length="40">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edittxt_status"> Slide Status <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
      	<label for="edittxt_status1">
       		<input type="radio" name="edittxt_status" id="edittxt_status1" value="1" '.(($returnData[0]->s_status == "1")?"checked":"").'> TRUE
   		</label>
   		<label for="edittxt_status0">
       		<input type="radio" name="edittxt_status" id="edittxt_status0" value="0" '.(($returnData[0]->s_status == "0")?"checked":"").'> FALSE
   		</label>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edittxt_order"> Order Number <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
      		<input type="number" name="edittxt_order" id="edittxt_order" value="'.(($returnData[0]->s_order)?$returnData[0]->s_order:"").'" class="form-control" min="1" max="20">
   		
      </div>
    </div>
    <div class="homeshimg">
       	<img src="'.(($returnData[0]->s_id)?base_url('uploads/sliders/'.$returnData[0]->s_image):base_url('uploads/sliders/slide.png')).'" class="img-responsive">
       	</div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">    
        <button type="submit" class="btn btn-success">Update</button>
      </div>
    </div>'; 
				echo json_encode(array('status' => 'Success', 'data' => $html, 'message' => 'Successfully Done.'));
			}else{
				echo json_encode(array('status' => 'error', 'message' => 'Operation failed.'));
			}
		}else{
			echo json_encode(array('status' => 'error', 'message' => 'Invalid Input.'));
		}
		die();
	}
	/*------------------------------------------
		editSlideDetail()
	------------------------------------------*/

	public function editSlideDetail(){
		$post = $this->input->post();
		// print_r($post); die;
		if($post){

			$sId =  $post["edittxt_id"];

			$formData['s_heading'] = $post["edittxt_heading"];
			$formData['s_subtitle'] = $post["edittxt_subtitle"];
			$formData['s_tagline'] = $post["edittxt_tagline"];
			$formData['s_link'] = $post["edittxt_link"];

			if($post["edittxt_created"]){
				$formData['s_created'] = date('Y-m-d H:i:s',strtotime($post["edittxt_created"]));	
			}
			if($post["edittxt_order"]){
				$formData['s_order'] = $post["edittxt_order"];	
			}
			
			
			$formData['s_status'] = (($post["edittxt_status"])?'1':'0');
			$formData['s_modified'] = date('Y-m-d H:i:s');

			if($_FILES['edittxt_image']['name']){
				$img_config['upload_path']          = './uploads/sliders/';
		        $img_config['allowed_types']        = 'gif|jpg|png|jpeg';
		        $img_config['max_size']             = 2048;
		        // $img_config['encrypt_name'] = TRUE;
		        
		        $this->upload->initialize($img_config);
		        if ( ! $this->upload->do_upload('edittxt_image'))
		        {
		            echo json_encode(array('status' => 'error', 'message' => $this->upload->display_errors()));
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            $formData['s_image'] = $upload_data['file_name'];
		            $updateID = $this->user_Auth->update('sliders', $formData,array('s_id' => $sId));
			        if($updateID){
			        	echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/homeSlider'), 'message' => 'Successfully Done.'));
			        }else{
			        	echo json_encode(array('status' => 'error', 'message' => 'Insert Process Failed.'));
			        }
		        }
		        
			}else{
				$updateID = $this->user_Auth->update('sliders', $formData,array('s_id' => $sId));
		        if($updateID){
		        	echo json_encode(array('status' => 'Success', 'data' => site_url('dashboard/homeSlider'), 'message' => 'Successfully Done.'));
		        }else{
		        	echo json_encode(array('status' => 'error', 'message' => 'Insert Process Failed.'));
		        }
			}

		}else{
			echo json_encode(array('status' => 'error', 'message' => 'Invalid Input.'));
		}
		die();
	}
}

?>
