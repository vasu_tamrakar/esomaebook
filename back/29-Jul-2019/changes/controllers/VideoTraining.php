<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VideoTraining extends CI_Controller {
	// public function __construct (){
	// 	parent :: __construct();
		
	// }
	public function index()
	{
		$data["title"] = 'Video Training Page | '.SITENAME;
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-videotraining',$data);
		$this->load->view('front/common/footer',$data);
	}
}
