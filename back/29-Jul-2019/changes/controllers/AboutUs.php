<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AboutUs extends CI_Controller {
	// public function __construct (){
	// 	parent :: __construct();
		
	// }
	public function index()
	{
		$data["title"] = 'About Us Page | '.SITENAME;
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-aboutus',$data);
		$this->load->view('front/common/footer',$data);
	}
}
