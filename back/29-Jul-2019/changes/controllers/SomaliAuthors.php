<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SomaliAuthors extends CI_Controller {
	// public function __construct (){
	// 	parent :: __construct();
		
	// }
	public function index()
	{
		$data["title"] = 'Somali Authors page | '.SITENAME;
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-somaliauthors',$data);
		$this->load->view('front/common/footer',$data);
	}
}
