<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SignUp extends CI_Controller {
	protected $planDetails;
	public function __construct (){
	 	parent :: __construct();
		if($this->session->userdata('is_logged_in_user')){
			redirect(base_url());  exit();
		}
		$this->load->model('user_Auth');
	}
	public function txngenreate(){
			
			$mt = explode(' ',microtime());
			$round = date('dmY').$mt[1];
			return $round;
		}
	public function index()
	{
		$data["title"] = 'Sign Up Page | '.SITENAME;

		$post = $this->input->post();
		$subscriberID = $this->session->userdata('signUpdataid');
		if($subscriberID){
			$data["subscriberData"] = $this->user_Auth->getData('emailSubscriber',array('es_id' =>$subscriberID));
		
			/* Check Form from post*/
			if($post){
				// print_r($post); die;
				
				// $post["txt_firstname"];
				// $post["txt_lastname"];
				// $post["txt_email"];
				// $post["txt_mobile"];
				// $post["txt_password"];
				// $post["txt_cnfpassword"];
				// $post["select_book"];
				// $post["txt_holdername"];
				// $post["txt_cardnumber"];
				// $post["txt_cvv"];
				// $post["txt_expmonth"];
				// $post["txt_expyear"];
				// $post["txt_agree"];
				// $post["txt_userID"];
				// $post["txt_planID"];
				// $post["stripeToken"];



				$this->form_validation->set_rules('txt_firstname','First Name','required');
				$this->form_validation->set_rules('txt_lastname','Last Name','required');
				$this->form_validation->set_rules('txt_email','Email Address','required|valid_email|is_unique[user_credentials.uc_email]');
				if(isset($post["txt_mobile"]) && ($post["txt_mobile"])){
					$this->form_validation->set_rules('txt_mobile','Mobile','required|is_unique[user_credentials.uc_mobile]');
				}
				$this->form_validation->set_rules('txt_password','Password','required');
				// $this->form_validation->set_rules('txt_cnfpassword','Password','required|matches[txt_password]');
				if($post["txt_paymethod"] != 'cc'){
					$this->form_validation->set_rules('txt_address','Address','required');	
				}
				/* Check validation */
				if($this->form_validation->run() === TRUE){
					
					$resExistemail =  $this->user_Auth->existEmail($post["txt_email"]);
					if(isset($post['txt_mobile']) && ($post['txt_mobile'])){
						$resExistmobile = $this->user_Auth->existMobile($post["txt_mobile"]);
					}else{
						$resExistmobile = FALSE;
					}
					
					/* Check email exist ornot */
					if($resExistemail){
						$this->session->set_flashdata('alert','info');
						$this->session->set_flashdata('message',$post["txt_email"].' Email Address Already Registered.');
						redirect('signUp'); exit();
					}elseif($resExistmobile){
						$this->session->set_flashdata('alert','info');
						$this->session->set_flashdata('message',$post["txt_mobile"].' Mobile Number Already Registered.');
						redirect('signUp'); exit();
					}else{

						$formData["uc_firstname"] = $post["txt_firstname"];
						$formData["uc_lastname"] = $post["txt_lastname"];
						$formData["uc_email"] = $post["txt_email"];
						if(isset($post["txt_mobile"]) && ($post["txt_mobile"])){
							$formData["uc_mobile"] = $post["txt_mobile"];
						}
						$formData["uc_password"] = md5($post["txt_password"]);
						$formData["uc_role"] = 5;
						$formData["uc_created"] = date("Y-m-d H:i:s");
						$formData["uc_status"] = '0';
						$formData["uc_active"] = '0';
						if($post["txt_paymethod"] != 'cc'){
							$formData["uc_address"] = $post["txt_address"];
						}
						$instID =  $this->user_Auth->insert('user_credentials',$formData);
						
						/* Session set for goto payment page info. */
						
						if($instID){
							$user = $this->user_Auth->getData('user_credentials',$where=array('uc_id' =>$instID),$sel='',$sort ='');						
							if( $post["txt_paymethod"] === 'cc' ){
								// $post["txt_planid"];
								// $post["txt_firstname"];
								// $post["txt_lastname"];
								// $post["txt_email"];
								// $post["txt_mobile"];
								// $post["txt_password"];
								// $post["txt_cnfpassword"];
								// $post["select_book"];
								// $post["txt_holdername"];
								// $post["txt_cardnumber"];
								// $post["txt_cvv"];
								// $post["txt_expmonth"];
								// $post["txt_expyear"];
								// $post["txt_agree"];
								// $post["txt_userID"];
								// $post["txt_planID"];
								// $post["stripeToken"];
								$bookID = $post["select_book"];
								$this->stripePayment($post["txt_planID"],$instID,$bookID);		

							}else if( $post["txt_paymethod"] === 'evc' ){

								/*evc process todo */

								$planID = $post["txt_planidform"];
								
								$post["txt_paymethod"];
								$post["txt_firstname"];
								$post["txt_lastname"];
								$post["txt_email"];
								$post["txt_mobile"];
								$post["txt_password"];
								$post["select_book"];
								$post["txt_address"];
								$post["txt_agree2"];
								$planDetail = $this->user_Auth->getData('membershipplan',array('mp_id' =>$planID));
								$txtID = $this->txngenreate();
								$paymentDetails["pd_txnid"] = $txtID;
					            $paymentDetails["pd_planid"] = $planID;
					            $paymentDetails["pd_planprice"] = $planDetail[0]->mp_price;
					            $paymentDetails["pd_userid"] = $instID;
					            $paymentDetails["pd_currency"] = 'usd';
					            $paymentDetails["pd_status"] = 'waiting';
					            $paymentDetails["pd_payby"] = $post["txt_paymethod"];
					            
					            $paymentDetails["pd_created"] = date('Y-m-d H;i:s');
					            $bookID = $post["select_book"];
					            $paymentDetails["pd_bookid"] = $bookID;
					            $paydetailRES = $this->user_Auth->insert("paymentDetails", $paymentDetails);
					            $this->session->unset_userdata('signUpdataid');
					            if($paydetailRES){
					            	$access = array("ba_bookid" => $bookID,"ba_userid" => $instID,"ba_created"=> date('Y-m-d H:i:s'));
						            $user = $this->user_Auth->getData('user_credentials',array('uc_id'=> $instID));
						            $this->user_Auth->insert("book_access", $access);
						            $this->signup_verification($user[0]->uc_email,$sub='Sign Up Process Notification of '.SITENAME, $user, $paydetailRES);
						            

					            	$this->session->set_flashdata('alert','success');
					            	$this->session->set_flashdata('message','SignUp Process Successfully Done!.');
					            	
					            	redirect('thankyou/'.base64_encode($txtID)); exit();
					            }else{
					            	$this->session->set_flashdata('alert','error');
					            	$this->session->set_flashdata('message','pay process failed.');
					            	redirect('failure'); exit();
					            }
							}else{

								/* mpesage process block */
								$planID = $post["txt_planidform3"];
								$post["txt_paymethod"];
								$post["txt_firstname"];
								$post["txt_lastname"];
								$post["txt_email"];
								$post["txt_mobile"];
								$post["txt_password"];
								$post["txt_cnfpassword"];
								$post["select_book"];
								$post["txt_address"];
								$post["txt_agree3"];
								$bookID = $post["select_book"];
								$planDetail = $this->user_Auth->getData('membershipplan',array('mp_id' =>$planID));
								$txtID = $this->txngenreate();
								$paymentDetails["pd_txnid"] = $txtID;
					            $paymentDetails["pd_planid"] = $planID;
					            $paymentDetails["pd_planprice"] = $planDetail[0]->mp_price;
					            $paymentDetails["pd_userid"] = $instID;
					            $paymentDetails["pd_currency"] = 'usd';
					            $paymentDetails["pd_status"] = 'waiting';
					            $paymentDetails["pd_payby"] = $post["txt_paymethod"];
					            $paymentDetails["pd_bookid"] = $bookID;
					            $paymentDetails["pd_created"] = date('Y-m-d H;i:s');
					            
					            
					            $paydetailRES = $this->user_Auth->insert("paymentDetails", $paymentDetails);
					            $this->session->unset_userdata('signUpdataid');
					            if($paydetailRES){
					            	$access = array("ba_bookid" => $bookID,"ba_userid" => $instID,"ba_created"=> date('Y-m-d H:i:s'));
						            $user = $this->user_Auth->getData('user_credentials',array('uc_id'=> $instID));
						            $this->user_Auth->insert("book_access", $access);
						            $this->signup_verification($user[0]->uc_email,$sub='SignUp Process Notification of '.SITENAME, $user, $paydetailRES);
						            

					            	$this->session->set_flashdata('alert','success');
					            	$this->session->set_flashdata('message','SignUp Process Successfully Done!.');
					            	redirect('thankyou/'.base64_encode($txtID)); exit();
					            }else{
					            	$this->session->set_flashdata('alert','error');
					            	$this->session->set_flashdata('message','pay process failed.');
					            	redirect('failure'); exit();
					            }
							}
							
						}else{
							$this->session->set_flashdata('alert','error');
							$this->session->set_flashdata('message','Registration process failed Please try to after some time.');
							redirect('signUp'); exit();
						}
					}
				}else{	
					$this->load->view('front/common/header',$data);
					$this->load->view('front/page-signup',$data);
					$this->load->view('front/common/footer',$data);
				}
			}else{
				$this->load->view('front/common/header',$data);
				$this->load->view('front/page-signup',$data);
				$this->load->view('front/common/footer',$data);	
			}

		}else{
			/*  signUpdataid session not set */
			redirect('membership'); exit();
		}
		
	}
	/*--------------------------------------------------
		
	---------------------------------------------------*/
	public function stripePayment($planID,$userID,$bookID){

		$this->planDetails = $this->user_Auth->getData('membershipplan', array("mp_id" => $planID));


		$cardHolder     = $this->input->post("txt_holdername");
        $cardNumber     = $this->input->post("txt_cardnumber");
        $cvvNumber 		= $this->input->post("txt_cvv");
        $cardexpMonth   = $this->input->post("txt_expmonth");
        $cardexpyear    = $this->input->post("txt_expyear");
    

        require_once('application/libraries/stripe-php/init.php');

    

        \Stripe\Stripe::setApiKey($this->config->item('stripe_secret'));

     

        $charge =  \Stripe\Charge::create ([

                "amount"        => 100 * ($this->planDetails[0]->mp_price),
                "currency"      => "usd",
                "source"        => $this->input->post('stripeToken'),
                "description"   => $this->planDetails[0]->mp_name.','.$this->planDetails[0]->mp_validity.' '.SITENAME,
                "metadata"      =>  array('planID' => $this->planDetails[0]->mp_id, "customerID" => $userID)

        ]);
        $chargeJson = $charge->jsonSerialize(); 
        if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
            $transactionID      = $chargeJson['balance_transaction']; 
            $paidAmount         = $chargeJson['amount']; 
            $paidCurrency       = $chargeJson['currency']; 
            $payment_status     = $chargeJson['status'];
            
            $description        = $chargeJson["description"];
            $metadata           = $chargeJson["metadata"];
            $application        = $chargeJson["application"];
            $chargeId           = $chargeJson["id"];
            
            $paymentDetails["pd_txnid"] = $transactionID;
            $paymentDetails["pd_planid"] = $metadata["planID"];
            $paymentDetails["pd_planprice"] = ($paidAmount/100);
            $paymentDetails["pd_userid"] = $metadata["customerID"];
            $paymentDetails["pd_currency"] = $paidCurrency;
            $paymentDetails["pd_status"] = $payment_status;
            $paymentDetails["pd_payby"] = 'cc';
            
            $paymentDetails["pd_cardholder"] = $cardHolder;
            $paymentDetails["pd_cardnumber"] = $cardNumber;
            $paymentDetails["pd_cvvnumber"] = $cvvNumber;
            $paymentDetails["pd_cartexpmonth"] = $cardexpMonth;
            $paymentDetails["pd_cardexpyear"] = $cardexpyear;
            $paymentDetails["pd_chargeid"] = $chargeId;
            $paymentDetails["pd_bookid"] = $bookID;
            $paymentDetails["pd_created"] = date('Y-m-d H;i:s');
            
            
            $this->user_Auth->insert("paymentDetails", $paymentDetails);
            $this->user_Auth->update("user_credentials", array("uc_status" => '1',"uc_active" => '1'),array("uc_id" => $userID));

            //  Insert detail in
            if($payment_status == 'succeeded'){
                $this->session->unset_userdata("payerData");
                $this->session->unset_userdata("signUpdataid");

                $access = array("ba_bookid" => $bookID,"ba_userid" => $userID,"ba_created"=> date('Y-m-d H:i:s'));
                $user = $this->user_Auth->getData('user_credentials',array('uc_id'=> $userID));
                $this->signup_activated($user[0]->uc_email,$sub='', $user);
                $this->user_Auth->insert("book_access", $access);
                $this->session->set_flashdata('alert','success');
                $this->session->set_flashdata('message','Transaction Successfully completed.');
                redirect("thankyou/".base64_encode($transactionID));
                exit();
            }else{
                $this->user_Auth->delete("user_credentials", array('uc_id'=> $userID));
                $this->session->set_flashdata('alert','error');
                $this->session->set_flashdata('message',$charege);
                redirect("failure");
                exit();
            }

        }else{
        	$this->user_Auth->delete("user_credentials", array('uc_id'=> $userID));
            $this->session->set_flashdata('alert','error');
            $this->session->set_flashdata('message',$charege);
            redirect("failure");
            exit();
        }
	}


	/*------------------------------------------------------------------
        signup_verification
    ------------------------------------------------------------------*/
    public function signup_verification($to, $subject=false, $data, $payid=false){
        $url = site_url('signIn/activated/?time='.base64_encode($data[0]->uc_id).'&slot='.base64_encode($data[0]->uc_email));
        if($payid){
        	$payDetails = $this->user_Auth->getData("paymentDetails", array('pd_id' => $payid));
        
	        if($payDetails[0]->pd_payby == 'evc'){
	        	$addedhtml = '<p>Thanks for interesing in '.SITENAME.'. </p><p>Sign up process is compeleted and payment not done so pay.</p><p>OUR CUSTOMER SERVICES IN SOMALIA HAFSA HASSAN +252-61-3148376</p>';
	        }else{
	        	$addedhtml = '<p>Thanks for interesing in '.SITENAME.'. </p><p>Sign up process is compeleted and payment not done so pay.</p><p>Dear Valued Customer please send the money through Mpesa and call us to confirm. (Mpesa Number) 0725-893-222 We are still working on to the our system automatic.</p>';;
	        }
        }else{
        	$addedhtml='';
        }
        
    	$html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block; width:100%">
					                    <h6>Hi '.$data[0]->uc_firstname.',</h6>
					                        '.$addedhtml.'
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

        $this->email->to($to);
        $this->email->from('pareshnagar87@gmail.com',SITENAME);

        $this->email->subject( (($subject)?$subject:'SignUp Verification') );
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
            return 1;
        }else{
            return $this->email->print_debugger();
        }
    }
    public function signup_activated($to, $subject=false, $data){
    	$url = site_url('signIn/activated/?time='.base64_encode($data[0]->uc_id).'&slot='.base64_encode($data[0]->uc_email));
        $html = '<!DOCTYPE html>
					<html lang="en">
					    <head>
					        <title>'.SITENAME.'</title>
					        <meta charset="utf-8">
					        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
					        <link rel="stylesheet" href="'.base_url("assets/fonts/icomoon/style.css").'">
					        <link rel="stylesheet" id="wbe-gfonts-css" href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" type="text/css" media="all">
					        <link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
					        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					        <link rel="stylesheet" href="'.base_url("assets/css/style.css").'">
					        <style>
					        </style>
					    </head>
					    <body>
					        <div class="container">
					            <div class="row">
					                <div style="background: #54bfe3;width: 100%;">
					                    <h2 style="color: #fff;font-size: 40px;font-weight: bold;text-align: center;padding: 15px 0px;font-family: Arial-black;"><strong>Ebook</strong></h2>
					                </div>
					                <div style="margin: 30px;display: block; width:100%">
					                    <h6>Hi '.$data[0]->uc_firstname.',</h6>
					                    <p>Thanks for registration on '.SITENAME.'.</p>
                                    	<p>Your account activated you can signup click  <a href="'.site_url('signIn').'">here</a></p>
					                </div>
					                <div style="display: block;width: 100%;background: #54bfe3;color: floralwhite;">
					                    <p style="text-align: center;margin: 0;padding: 14px;"> For more details contact us.<a href="'.site_url("contact").'"> '.SITENAME.'</a></p>
					                </div>
					            </div>
					        </div>
					    </body>
					</html>';
        $config['protocol'] 	= 'smtp';
        $config['smtp_host'] 	= 'ssl://smtp.gmail.com';
        $config['smtp_port'] 	= '465';
        $config['smtp_user'] 	= 'pareshnagar87@gmail.com';
        $config['smtp_pass'] 	= 'yravperbiwvaxgku';
        $config['mailtype'] 	= 'html';
        $config['charset'] 		= 'utf-8';
        $config['newline'] 		= "\r\n";
        $config['wordwrap'] 	= TRUE;
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

        
        $this->email->to($to);
        $this->email->from('pareshnagar87@gmail.com',SITENAME);

        $this->email->subject( (($subject)?$subject:'SignUp Process Notification') );
        $this->email->message($html);
        $sended =$this->email->send();
        if($sended){
            return 1;
        }else{
            $this->email->print_debugger();
        }
    }
	
}
/*
351469sbi 
04
*/
/*priya sharma
5173989821110785
*/