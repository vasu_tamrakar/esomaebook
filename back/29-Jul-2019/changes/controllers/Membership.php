<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership extends CI_Controller {
	public function __construct (){
		parent :: __construct();
		if($this->session->userdata('is_logged_in_user')){
			redirect(base_url());  exit();
		}
		$this->load->model('user_Auth');
	}
	public function index()
	{
		// $this->load->model('user_Auth');
		$data["title"] = 'Membership page | '.SITENAME;
		$data['membershipDetailIDs'] = $this->user_Auth->getData('membershipplan',array('mp_status' => '1'),$se= 'mp_id',$sh='');
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-membership',$data);
		$this->load->view('front/common/footer',$data);
	}

	/*-----------------------------------------------------
	addsSbscriber()
	-----------------------------------------------------*/
	public function addsSbscriber(){
		// $this->load->model('user_Auth');
		$post = $this->input->post();
		
		if($post){
			if(empty($post["txtfirstname"])){
				echo json_encode(array("status" => "error","message" => "First Name field is required!."));	
				die();
			}
			if(empty($post["txtlastname"])){
				echo json_encode(array("status" => "error","message" => "Last name field is required!."));	
				die();
			}
			if(empty($post["txtemail"])){
				echo json_encode(array("status" => "error","message" => "Email field is required!."));	
				die();
			}

			$formData["es_firstname"] = $post["txtfirstname"];
			$formData["es_lastname"] = $post["txtlastname"];
			$formData["es_email"] = $post["txtemail"];
			$formData["es_planid"] = $post["txtplanId"];
			$formData["es_created"] = date('Y-m-d H:i:s');
			$formData["es_status"] = "1";

			$existEmail = $this->user_Auth->getData("emailSubscriber", array("es_email" => $formData["es_email"]), $s="",$sh="") ;
			if($existEmail){
				if(!empty($existEmail[0]->es_firstname)){
					$updateDAta = array(
						"es_firstname" => $formData["es_firstname"],
						"es_lastname" => $formData["es_lastname"],
						"es_planid"	   => $formData["es_planid"],
						"es_status"	   => '1'
						);
					$this->user_Auth->update('emailSubscriber',$updateDAta,array("es_id" => $existEmail[0]->es_id));
					$this->session->set_userdata('signUpdataid', $existEmail[0]->es_id);
					echo json_encode(array("status" => "200", "data" => site_url('signUp'), "message" => "Succesfully Done."));
				}else{
					$updateDAta = array(
						"es_firstname" => $formData["es_firstname"],
						"es_lastname" => $formData["es_lastname"],
						"es_planid"	   => $formData["es_planid"],
						"es_status"	   => '1'
						);
					$this->user_Auth->update('emailSubscriber',$updateDAta,array("es_id" => $existEmail[0]->es_id));
					$this->session->set_userdata('signUpdataid', $existEmail[0]->es_id);
					echo json_encode(array("status" => "200", "data" => site_url('signUp'), "message" => "Succesfully Done."));
				}
			}else{
				$insertID = $this->user_Auth->insert('emailSubscriber',$formData);
				if($insertID){
					$this->session->set_userdata('signUpdataid', $insertID);
					echo json_encode(array("status" => "200", "data" => site_url('signUp'), "message" => "Succesfully Done."));	
				}else{
					echo json_encode(array("status" => "fail","message" => "Insert Process failed!."));
				}
			}
		}else{
			echo json_encode(array("status" => "fail","message" => "Invalid operation!."));
		}
		die();
	}
}
