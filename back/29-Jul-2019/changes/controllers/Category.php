<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
	public function __construct (){
		parent :: __construct();
		$this->load->model('user_Auth');
	}
	public function index()
	{
		$data["title"] = 'Category Page | '.SITENAME;
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-category',$data);
		$this->load->view('front/common/footer',$data);
	}
	public function category($catname, $page=false)
	{
		$catname =urldecode($catname);
		$data["title"] = $catname.' Category Page | '.SITENAME;
		$data["categoryName"] = $catname;
		if($page){
			
		}else{

		}
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-archivecategory',$data);
		$this->load->view('front/common/footer',$data);
	}
	public function categories($catname, $cat){
		
		$catname =urldecode($catname);
		$cat =urldecode($cat);
		$data["title"] = $cat.' Book Page | '.SITENAME;
		$data["categoryName"] = $cat;
		$data["bookName"] = $cat;
		
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page-singlebook',$data);
		$this->load->view('front/common/footer',$data);
	}
}
