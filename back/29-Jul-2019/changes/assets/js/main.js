 AOS.init({
 	duration: 800,
 	easing: 'slide',
 	once: true
 });

jQuery(document).ready(function($) {

	"use strict";

	

	var siteMenuClone = function() {

		$('.js-clone-nav').each(function() {
			var $this = $(this);
			$this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
		});


		setTimeout(function() {
			
			var counter = 0;
      $('.site-mobile-menu .has-children').each(function(){
        var $this = $(this);
        
        $this.prepend('<span class="arrow-collapse collapsed">');

        $this.find('.arrow-collapse').attr({
          'data-toggle' : 'collapse',
          'data-target' : '#collapseItem' + counter,
        });

        $this.find('> ul').attr({
          'class' : 'collapse',
          'id' : 'collapseItem' + counter,
        });

        counter++;

      });

    }, 1000);

		$('body').on('click', '.arrow-collapse', function(e) {
      var $this = $(this);
      if ( $this.closest('li').find('.collapse').hasClass('show') ) {
        $this.removeClass('active');
      } else {
        $this.addClass('active');
      }
      e.preventDefault();  
      
    });

		$(window).resize(function() {
			var $this = $(this),
				w = $this.width();

			if ( w > 768 ) {
				if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
			}
		})

		$('body').on('click', '.js-menu-toggle', function(e) {
			var $this = $(this);
			e.preventDefault();

			if ( $('body').hasClass('offcanvas-menu') ) {
				$('body').removeClass('offcanvas-menu');
				$this.removeClass('active');
			} else {
				$('body').addClass('offcanvas-menu');
				$this.addClass('active');
			}
		}) 

		// click outisde offcanvas
		$(document).mouseup(function(e) {
	    var container = $(".site-mobile-menu");
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	      if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
	    }
		});
	}; 
	siteMenuClone();


	var sitePlusMinus = function() {
		$('.js-btn-minus').on('click', function(e){
			e.preventDefault();
			if ( $(this).closest('.input-group').find('.form-control').val() != 0  ) {
				$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) - 1);
			} else {
				$(this).closest('.input-group').find('.form-control').val(parseInt(0));
			}
		});
		$('.js-btn-plus').on('click', function(e){
			e.preventDefault();
			$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) + 1);
		});
	};
	// sitePlusMinus();


	var siteSliderRange = function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 300 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
	};
	// siteSliderRange();


	
	var siteCarousel = function () {
		if ( $('.nonloop-block-13').length > 0 ) {
			$('.nonloop-block-13').owlCarousel({
		    center: false,
		    items: 1,
		    loop: true,
				stagePadding: 0,
		    margin: 0,
		    autoplay: true,
		    dots: true,
		    nav: true,
				navText: ['<span class="img_back">', '<span class="img_forward">'],
		    responsive:{
	        600:{
	        	margin: 0,
	        	nav: true,
	          items: 2
	        },
	        1000:{
	        	margin: 0,
	        	stagePadding: 0,
	        	nav: true,
	          items: 3
	        },
	        1200:{
	        	margin: 0,
	        	stagePadding: 0,
	        	nav: true,
	          items: 6
	        }
		    }
			});
		}

		$('.slide-one-item').owlCarousel({
	    center: false,
	    items: 1,
	    loop: true,
			stagePadding: 0,
	    margin: 0,
	    autoplay: true,
	    pauseOnHover: false,
	    nav: true,
	    navText: ['<span class="icon-keyboard_arrow_left">', '<span class="icon-keyboard_arrow_right">']
	  });

	  $('.slide-one-item-alt').owlCarousel({
	    center: false,
	    items: 1,
	    loop: true,
			stagePadding: 0,
			smartSpeed: 700,
	    margin: 0,
	    autoplay: true,
	    pauseOnHover: false,
      responsive:{
    600:{
      margin: 0,
      items: 1
    },
    1000:{
      margin: 0,
      stagePadding: 0,
      items: 1
    },
    1200:{
      margin: 0,
      stagePadding: 0,
      items: 1
    }
    }
	  });

	  
	  

	  $('.custom-next').click(function(e) {
	  	e.preventDefault();
	  	$('.slide-one-item-alt').trigger('next.owl.carousel');
	  });
	  $('.custom-prev').click(function(e) {
	  	e.preventDefault();
	  	$('.slide-one-item-alt').trigger('prev.owl.carousel');
	  });
	  
	};
	siteCarousel();

	var siteStellar = function() {
		$(window).stellar({
	    responsive: false,
	    parallaxBackgrounds: true,
	    parallaxElements: true,
	    horizontalScrolling: false,
	    hideDistantElements: false,
	    scrollProperty: 'scroll'
	  });
	};
	siteStellar();

	var siteCountDown = function() {

		$('#date-countdown').countdown('2020/10/10', function(event) {
		  var $this = $(this).html(event.strftime(''
		    + '<span class="countdown-block"><span class="label">%w</span> weeks </span>'
		    + '<span class="countdown-block"><span class="label">%d</span> days </span>'
		    + '<span class="countdown-block"><span class="label">%H</span> hr </span>'
		    + '<span class="countdown-block"><span class="label">%M</span> min </span>'
		    + '<span class="countdown-block"><span class="label">%S</span> sec</span>'));
		});
				
	};
	siteCountDown();

	var siteDatePicker = function() {

		if ( $('.datepicker').length > 0 ) {
			$('.datepicker').datepicker();
		}

	};
	siteDatePicker();

	var siteSticky = function() {
		$(".js-sticky-header").sticky({topSpacing:0});
	};
	siteSticky();

	// navigation
  var OnePageNavigation = function() {
    var navToggler = $('.site-menu-toggle');
   	$("body").on("click", ".main-menu li a[href^='#'], .smoothscroll[href^='#'], .site-mobile-menu .site-nav-wrap li a", function(e) {
      e.preventDefault();

      var hash = this.hash;

      $('html, body').animate({
        'scrollTop': $(hash).offset().top
      }, 600, 'easeInOutCirc', function(){
        window.location.hash = hash;
      });

    });
  };
  OnePageNavigation();

  var siteScroll = function() {

  	

  	$(window).scroll(function() {

  		var st = $(this).scrollTop();

  		if (st > 100) {
  			$('.js-sticky-header').addClass('shrink');
  		} else {
  			$('.js-sticky-header').removeClass('shrink');
  		}

  	}) 

  };
  siteScroll();

});

 $('#featureBooks').owlCarousel({
    center: false,
    items: 1,
    loop: false,
		stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
		navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
    	margin: 0,
    	nav: true,
      items: 2
    },
    1000:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 3
    },
    1200:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 6
    }
    }
	});
 $('#recentlyreleasedBooks').owlCarousel({
    center: false,
    items: 1,
    loop: false,
		stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
		navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
    	margin: 0,
    	nav: true,
      items: 2
    },
    1000:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 3
    },
    1200:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 6
    }
    }
	});
 $('#mostPopularbooks').owlCarousel({
    center: false,
    items: 1,
    loop: false,
		stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
		navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
    	margin: 0,
    	nav: true,
      items: 2
    },
    1000:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 3
    },
    1200:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 6
    }
    }
	});
$('#PopularAuthors').owlCarousel({
    center: false,
    items: 1,
    loop: false,
		stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
		navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
    	margin: 0,
    	nav: true,
      items: 2
    },
    1000:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 3
    },
    1200:{
    	margin: 0,
    	stagePadding: 0,
    	nav: true,
      items: 6
    }
    }
	});
$('.authors').owlCarousel({
    center: false,
    items: 1,
    loop: false,
    stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
    navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
      margin: 0,
      nav: true,
      items: 2
    },
    1000:{
      margin: 0,
      stagePadding: 0,
      nav: true,
      items: 2
    },
    1200:{
      margin: 0,
      stagePadding: 0,
      nav: true,
      items: 4
    }
    }
  });
$('.commingSoon').owlCarousel({
    center: false,
    items: 1,
    loop: false,
    stagePadding: 0,
    margin: 0,
    autoplay: true,
    dots: true,
    nav: true,
    navText: ['<span class="img_back">', '<span class="img_forward">'],
    responsive:{
    600:{
      margin: 0,
      nav: true,
      items: 2
    },
    1000:{
      margin: 0,
      stagePadding: 0,
      nav: true,
      items: 4
    },
    1200:{
      margin: 0,
      stagePadding: 0,
      nav: true,
      items: 6
    }
    }
  });
$('#frontSlider').owlCarousel({
    center: false,
    items: 1,
    loop: false,
    stagePadding: 0,
    margin: 0,
    autoplay: true,
    responsive:{
    600:{
      margin: 0,
      items: 1
    },
    1000:{
      margin: 0,
      stagePadding: 0,
      items: 1
    },
    1200:{
      margin: 0,
      stagePadding: 0,
      items: 1
    }
    }
  });

$(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        

});

$(document).ready(function () {         
$(function(){
    var current_page_URL = location.href;

    $( ".leftsidemenu a" ).each(function() {

        if ($(this).attr("href") !== "#") {

            var target_URL = $(this).prop("href");
                console.log(target_URL+"  = =  "+current_page_URL);
                if (target_URL == current_page_URL) {
                    $('a').parents('li, ul').removeClass('active');
                    $(this).parent('li').addClass('active');
                    return false;
                }
        }
    }); }); });


$('.datepicker').datepicker();



function viewBookDetais(bid){
    $.ajax({
        type: 'POST',
        url: SITEURL+'user/viewBookDetails',
        data: {'view_ID': bid},
        dataType: 'json',
        success:function(responce){
          if(responce.status == 'Success'){
            //$.alert(responce.message);
              $("#myModalview").modal("show");
              
              $("#myModalview .modal-title").html("View details hahah");
              $("#myModalview .modal-body").html(responce.data);
          }else{
            $.alert(responce.message);
          }
        }
      });
}
function deleteBookDetais(bid){
    $.confirm({
    content: 'Are you sure want to delete?',
    title: 'Confirm!',
    buttons: {
        specialKey: {
            text: 'Yes',
            keys: ['ctrl', 'enter'],
            btnClass: 'bg-danger',
            action: function(){
              $.ajax({
                type: 'POST',
                url: SITEURL+'user/deleteBookDetails',
                data: {'del_ID': bid},
                dataType: 'json',
                success:function(responce){
                  if(responce.status == 'Success'){
                    $.alert(responce.message);
                     window.location.href= responce.data;
                  }else{
                    $.alert(responce.message);
                  }
                }
              });
                
            }
        },
        alphabet: {
            text: 'No',
            keys: ['shift','N'],
            btnClass: 'bg-success',
            action: function(){
                // $.alert('Delete Process failed.'+ids);
            }
        }
    }
});
}

 $(document).ready(function() {
  if($('#example').length > 0){
      var table = $('#example').DataTable( {
          responsive: true
      } );
      new $.fn.dataTable.FixedHeader( table );
    }
} );


$('body').on('submit', '#subscribe_Form', function(e){
    e.preventDefault();
    var textEmail = $("#subscribe_Formtxt_email").val();
    if(textEmail){
        $.ajax({
            type: 'POST',
            url: SITEURL+'front/emailSubscribe',
            data: {'edata': textEmail},
            dataType: 'json',
            success: function(responce){
                if(responce.status == '200'){
                  $.alert(responce.message);
                  $("#subscribe_Form")[0].reset();
                }else{
                  $.alert(responce.message);
                }
            }
        });
    }
});


function openmembershipForm(ids){
    $("#membershipModal").modal("show");
    // alert(ids);
    if(ids){
      var htmldata= '<div class="row justify-content-md-center"><div class="col-md-10"><div class="form-group"><input type="hidden" name="txtplanId" id="txtplanId" value = "'+ids+'" class="form-control"></div><div class="form-group"><label for="txtfirstname"><b>Your First Name</b></label><input type="text" name="txtfirstname" id="txtfirstname" value = "" placeholder="Enter First Name" class="form-control"></div><div class="form-group"><label for="txtlastname"><b>Your Last Name</b></label><input type="text" name="txtlastname" id="txtlastname" value = "" placeholder="Enter Last Name" class="form-control"></div><div class="form-group"><label for="txtemail"><b>Your Email Address</b></label><input type="text" name="txtemail" id="txtemail" value = "" placeholder="Enter Email Address" class="form-control"></div><div class="form-group text-center"><button type="submit" class="btn btn-info smbtbtn" style="border-radius: 25px !important;">Save</button></div></div></div>';
      $("body #memberinfoForm").html(htmldata);
    }
}

jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid Email Address.");


$("body #memberinfoForm").validate({
  rules: {
      txtfirstname: {"required":true},
      txtlastname: {"required":true},
      txtemail: {"required":true, "validate_email":true},
  },messages: {
      txtfirstname: {"required":"First Name field is required."},
      txtlastname: {"required":"Last Name field is required."},
      txtemail: {"required":"Email field is required."},
  }
});
$("body #memberinfoForm").submit(function(evt){
    evt.preventDefault();
    var fDATa = new FormData(this);
    if($("#memberinfoForm").valid()){
      $("body .smbtbtn").addClass('loader');
        $.ajax({
            type: 'POST',
            url: SITEURL+'membership/addsSbscriber',
            data: fDATa,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(responce){
              $("body .smbtbtn").removeClass('loader');
                if(responce.status == 200){
                    window.location.href =responce.data;
                }else{
                  $("#responce").html(responce.message);
                }
            }
        });

    }
});

$("body #payment_form").validate({
  rules: {
      txt_firstname: {"required":true},
      txt_lastname: {"required":true},
      txt_email: {"required":true, "validate_email":true},
      // txt_mobile: {"required":true},
      txt_password: {"required":true,"minlength":6, "maxlength":20},
      // txt_cnfpassword: {"required":true,"equalTo": "#txt_password"},
      select_book: {"required":true},
      txt_paymethod: {"required":true},
      txt_holdername: {"required":true},
      txt_cardnumber: {"required":true,"minlength":16,"number":true},
      txt_cvv: {"required":true,"minlength":3,"number":true},
      txt_expmonth: {"required":true},
      txt_expyear: {"required":true},
      txt_agree: {"required":true},
  },messages: {
      txt_firstname: {"required":"First Name field is required."},
      txt_lastname: {"required":"Last Name field is required."},
      txt_email: {"required":"Email field is required."},
      // txt_mobile: {"required":"Mobile field is required."},
      txt_password: {"required":"Password field is required."},
      // txt_cnfpassword: {"required":"Enter Confirm Password Same as Password."},
      select_book: {"required":"Select Book field is required."},
      txt_paymethod: {"required":"Payment Method field is required."},
      txt_holdername: {"required":"Card holder name field is required."},
      txt_cardnumber: {"required":"Card number field is required."},
      txt_cvv: {"required":"Cvv number field is required."},
      txt_expmonth: {"required":"Select month field is required."},
      txt_expyear: {"required":"Select year field is required."},
      txt_agree: {"required": "I agree to condition is required."}
  }
});
$('#printMe').click(function(){
    var divToPrint = document.getElementById('outprint');
    var htmlToPrint = '' +
        '<style type="text/css">' +
        'table{margin:0 auto;} table th, table td {' +
        'border:1px solid #000;' +
        'padding;0.5em;' +
        '}' +
        '</style>';
    htmlToPrint += divToPrint.outerHTML;
    newWin = window.open("");
    newWin.document.write("<h3 align='center'>Bill Payment Details</h3>");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
    

});

/*----------Stripe ------------------------------*/

$(function() {

    var $form         = $("#payment_form");

  $('form#payment_form').bind('submit', function(e) {

    var $form         = $("#payment_form"),

        inputSelector = ['input[type=email]', 'input[type=password]',

                         'input[type=text]', 'input[type=file]',

                         'textarea'].join(', '),

        $inputs       = $form.find('.required').find(inputSelector),

        $errorMessage = $form.find('div.error'),

        valid         = true;

        $errorMessage.addClass('hide');

 

        $('.has-error').removeClass('has-error');

    $inputs.each(function(i, el) {

      var $input = $(el);

      if ($input.val() === '') {

        $input.parent().addClass('has-error');

        $errorMessage.removeClass('hide');

        e.preventDefault();

      }

    });

     

    if (!$form.data('cc-on-file')) {

      e.preventDefault();
      // alert($form.data('stripe-publishable-key'));
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));

      Stripe.createToken({

        number: $('.card-number').val(),

        cvc: $('.card-cvc').val(),

        exp_month: $('.card-expiry-month').val(),

        exp_year: $('.card-expiry-year').val()

      }, stripeResponseHandler);

    }

    

  });

      

  function stripeResponseHandler(status, response) {
        
        if (response.error) {

            $('.error')

                .removeClass('hide')

                .find('.alert')

                .text(response.error.message);

        } else {

            var token = response['id'];

            $form.find('input[type=text]').empty();
            
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            if($("body #payment_form").valid()){
                $("body #payment_form")[0].submit(); 
            }

        }

    }

     

});
/*----------End stripe-----------------------*/


$("body #payment_formEVC").validate({
  rules: {
      txt_planidform: {"required":true},
      txt_firstname: {"required":true},
      txt_lastname: {"required":true},
      txt_email: {"required":true, "validate_email":true},
      txt_mobile: {"required":true,"minlength":10},
      txt_password: {"required":true,"minlength":6, "maxlength":20},
      select_book: {"required":true},
      txt_address: {"required":true},
      txt_agree2: {"required":true},
  },messages: {
      txt_planidform: {"required":"Select plan is required."},
      txt_firstname: {"required":"First Name field is required."},
      txt_lastname: {"required":"Last Name field is required."},
      txt_email: {"required":"Email field is required."},
      txt_mobile: {"required":"Mobile field is required."},
      txt_password: {"required":"Password field is required."},
      txt_cnfpassword: {"required":"Enter Confirm Password Same as Password."},
      select_book: {"required":"Select Book field is required."},
      txt_paymethod: {"required":"Payment Method field is required."},
      txt_address: {"required":"Address field is required."},
      txt_agree2: {"required": "Check the condition is required."}
  }
});
 $("body #payment_formMPESA").validate({
  rules: {
      txt_planidform3: {"required":true},
      txt_firstname: {"required":true},
      txt_lastname: {"required":true},
      txt_email: {"required":true, "validate_email":true},
      txt_mobile: {"required":true,"minlength":10},
      txt_password: {"required":true,"minlength":6, "maxlength":20},
      select_book: {"required":true},
      txt_address: {"required":true},
      txt_agree3: {"required":true},
  },messages: {
      txt_planidform3: {"required": "Select plan field is required."},
      txt_firstname: {"required":"First Name field is required."},
      txt_lastname: {"required":"Last Name field is required."},
      txt_email: {"required":"Email field is required."},
      txt_mobile: {"required":"Mobile field is required."},
      txt_password: {"required":"Password field is required."},
      select_book: {"required":"Select Book field is required."},
      txt_paymethod: {"required":"Payment Method field is required."},
      txt_address: {"required":"Address field is required."},
      txt_agree3: {"required": "Check the condition is required."}
  }
});

 $("body #subscribe_Form").validate({
  rules: {
    subscribe_Formtxt_email: {required:true,validate_email:true}
  },messages: {
    subscribe_Formtxt_email: {required:"Email field is required.",validate_email:"Enter valid email addrss."}
  }
 });

 jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid Email Address.");

function changeCheckPlan(){
    var id = document.getElementById('txt_planidform').value;
    if(id){
      var elems = document.getElementsByClassName('pdetails');
      for (var i=0;i<elems.length;i+=1){
        elems[i].style.display = 'none';
      }
      document.getElementsByClassName('pdetails_'+id)[0].style.display='block';
    }

}
$('body').on('click', '.imgvidoebox', function(){
    $('body #videoModal').modal('show');
    var link = $(this).attr('data');
    $('body #videoModal .modal-title').html($(this).attr('title'));
    $('body #videoModalData iframe').attr('src', '');
    $('body #videoModalData iframe').attr('src', link);
});
function stop_Video(){
    $('body #videoModalData iframe').attr('src', '');
}




$(window).scroll(function(){
    var t = window.location.href;
    if(t !== SITEURL){
        if($('.sticky-wrapper').hasClass('is-sticky')){
            $('.site-logo img').attr('src',SITEURL+'assets/images/white.png');
        }else{
            // $('.site-logo img').attr('src',SITEURL+'assets/images/logo.png');
        }  
    }
});
    
