<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2" style="background: #54bfe3;">
            <?php include 'frontleftmenu.php'; ?>
        </div>
        <div class="col-md-10 col-lg-10">
            <div class="row">
                <div class="col-md-12">
                <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                  <div class="card">
                    <?php 
                    if($authorAdded){ ?>
                      <div class="card-header bg-info"><span style="color: #fff;">Add new book form</span></div>
                      <div class="card-body">
                          <form name="addbook_Form" id="addbook_Form" action="" method="post" enctype="multipart/form-data">
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="txt_bookname">Book Name:</label>
                                          <input type="text" name="txt_bookname" id="txt_bookname" value="<?php echo set_value('txt_bookname'); ?>" class="form-control" placeholder="Enter Book Name" max-length="150">
                                          <?php echo form_error('txt_bookname', '<small class="error">', '</small>'); ?>
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="txt_published">Book Published Date:</label>
                                          <input type="text" name="txt_published" id="txt_published" value="<?php echo set_value('txt_published'); ?>" class="form-control datepicker">
                                          <?php echo form_error('txt_published', '<small class="error">', '</small>'); ?>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="txt_category">Book Category :</label>
                                          <?php 
                                          $secat = set_value('txt_category');
                                          $category = $this->user_Auth->getData('categories',$w = array('c_status' => '1'),$se = 'c_id,c_name',$sh='c_id ASC');
                                          if($category){ ?>
                                          <select name="txt_category" id="txt_category" class="form-control">
                                              <option value=""> Select Category</option>
                                              <?php
                                              foreach ($category as $value) { ?>
                                                  <option value="<?php echo $value->c_id; ?>" <?php if($value->c_id == $secat){ echo 'selected'; } ?>><?php echo $value->c_name; ?></option>
                                                  <?php
                                              }
                                              ?>
                                          </select>
                                          <?php
                                          } 
                                          ?>
                                          <?php echo form_error('txt_category', '<small class="error">', '</small>'); ?>
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="txt_frontimage">Book Front Image :</label>
                                          <input type="file" name="txt_frontimage" id="txt_frontimage" class="form-control-file" accept="image/*" required>
                                          <?php echo form_error('txt_frontimage', '<small class="error">', '</small>'); ?>
                                          <small class="text-info">Image format accept only gif,jpg and png.</small>
                                      </div>
                                  </div>
                              </div>

                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="txt_rating">Book rating :</label>
                                          <div class="form-control">
                                            <?php $selrate = set_value('txt_rating'); ?>
                                          <label for="text_rating1">
                                            <input type="radio" name="txt_rating" id="text_rating1" value="1" <?php if($selrate == 1){ echo 'checked'; } ?>> 1
                                          </label>
                                          <label for="text_rating2">
                                            <input type="radio" name="txt_rating" id="text_rating2" value="2" <?php if($selrate == 2){ echo 'checked'; } ?>> 2
                                          </label>
                                          <label for="text_rating3">
                                            <input type="radio" name="txt_rating" id="text_rating3" value="3" <?php if($selrate == 3){ echo 'checked'; } ?>> 3
                                          </label>
                                          <label for="text_rating4">
                                            <input type="radio" name="txt_rating" id="text_rating4" value="4" <?php if($selrate == 4){ echo 'checked'; } ?>> 4
                                          </label>
                                          <label for="text_rating5">
                                            <input type="radio" name="txt_rating" id="text_rating5" value="5" <?php if($selrate == 5){ echo 'checked'; } ?>> 5
                                          </label>
                                        </div>
                                          <?php echo form_error('txt_rating', '<small class="error">', '</small>'); ?>
                                      </div>      
                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="txt_bookfile">Book File :</label>
                                          <input type="file" name="txt_bookfile" id="txt_bookfile" class="form-control-file" accept="application/msword, application/vnd.ms-powerpoint,
text/plain, application/pdf" required>
                                          <?php echo form_error('txt_bookfile', '<small class="error">', '</small>'); ?>
                                          <small class="text-info">File format accept only pdf,docx,txt and pptx.</small>
                                      </div>
                                  </div>
                              </div>
                               <button type="submit" class="btn btn-info">Save</button>
                        </form>
                      </div>
                      <?php
                            }
                            ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
body {
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 102px;
height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.site-section {
    padding: 0;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

a.site_title:hover{
    color: #fff;
}
.navbar-nav .nav-link {
    padding: 0.3rem 1rem;
}
.fa::after {
  margin-right: 2px;
  content: ;
  content: "";
}
@media only screen and (max-width: 992px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 167px !important;
        height: 21vh;
    }
} 


</style>
  