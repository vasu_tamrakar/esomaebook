<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">
</div>
    <section class="site-section bg-white" id="contact-section">
      <div class="container">
        <div class="row mb-4">
          <div class="col-md-12 text-center">
            <br/>
            <br/>
            <h2 class="section-title mb-3">Published Book</h2>
            <div class="text-center">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Category</a></li>
                <li class="breadcrumb-item active"><?php echo $categoryName; ?></li>
              </ol>
            </nav>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="book-img">
                                <img src="<?php echo base_url('assets/images/categories5.png'); ?>" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <h3 style="margin-bottom: 0.3rem;"><strong>People of the Lakes </strong></h3>
                            <label>by <span style="color: #57C6E6;">Katheleen O neal </span></label>
                            <div class="rating">
                                <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                <span>1886 Ratings</span>
                                <span>886 reviews</span>
                            </div>
                            <div class="description">
                                <p>In a route, the array key contains the URI to be matched, while the array value contains the destination it should be re-routed to. In the above example, if the literal word “product” is found in the first segment of the URL, and a number is found in the second segment, the “catalog” class and the “product_lookup” method are instead used.</p></div>
                            <div class="details">
                                <h5 style="color: #57C6E6;margin-bottom:0.2rem;"><strong>Book Details</strong></h5>
                                <p>Published: September ,2019   </p>
                                <p>pages: 450 </p>
                                <p>ISBN: as32d1f32d1s</p>
                                <p>Publisher: Books Stall library</p>
                                <p>Downloads: 8971</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-5"></div>


            <div id="accordion" class="accordion">
        <div class="card mb-0">
            <div class="card-header " data-toggle="collapse" href="#collapseOne">
                <a class="card-title">
                    Chapter 1
                </a>
            </div>
            <div id="collapseOne" class="card-body collapse show" data-parent="#accordion" >
                <div class="pages">
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </p>
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                    craft beer farm-to-table.
                </p>
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                    craft beer farm-to-table.
                </p>
                    <div class="navPage">
                        <span class="prevnav"></span>
                        <span class="nxtnav"></span>
                    </div>
                </div>
                
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                <a class="card-title">
                  Chapter 2
                </a>
            </div>
            <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </p>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                <a class="card-title">
                  Chapter 3
                </a>
            </div>
            <div id="collapseThree" class="collapse" data-parent="#accordion" >
                <div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS.
                </div>
            </div>
        </div>
    </div>



          </div>
          <div class="col-md-3">
            <div class="sidebar">
                <div class="sidebarboxheading">More Books For This Author</div>
                <div class="sidebarbox">
                    <div class="imgbox">
                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                    </div>
                    <div class="desc">
                        <h6><strong>War And Peace</strong></h6>
                        <div class="rating">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <p>884 Reviews</p>
                        <p>September ,2019</p>
                    </div>
                </div>
                <div class="sidebarbox">
                    <div class="imgbox">
                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                    </div>
                    <div class="desc">
                        <h6><strong>War And Peace</strong></h6>
                        <div class="rating">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <p>884 Reviews</p>
                        <p>September ,2019</p>
                    </div>
                </div>
                <div class="sidebarbox">
                    <div class="imgbox">
                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                    </div>
                    <div class="desc">
                        <h6><strong>War And Peace</strong></h6>
                        <div class="rating">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <p>884 Reviews</p>
                        <p>September ,2019</p>
                    </div>
                </div>
                <div class="sidebarbox">
                    <div class="imgbox">
                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                    </div>
                    <div class="desc">
                        <h6><strong>War And Peace</strong></h6>
                        <div class="rating">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <p>884 Reviews</p>
                        <p>September ,2019</p>
                    </div>
                </div>
                <div class="AuthorbookMore">
                    <button type="button">see More</button>
                </div>


            </div>
            <div class="pt-5"></div>
            <div class="sidebar">
                <div class="sidebarboxheading">You may also like</div>
                <div class="sidebarbox">
                    <div class="imgbox">
                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                    </div>
                    <div class="desc">
                        <h6><strong>War And Peace</strong></h6>
                        <div class="rating">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <p>884 Reviews</p>
                        <p>September ,2019</p>
                    </div>
                </div>
                <div class="sidebarbox">
                    <div class="imgbox">
                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                    </div>
                    <div class="desc">
                        <h6><strong>War And Peace</strong></h6>
                        <div class="rating">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <p>884 Reviews</p>
                        <p>September ,2019</p>
                    </div>
                </div>
                <div class="sidebarbox">
                    <div class="imgbox">
                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                    </div>
                    <div class="desc">
                        <h6><strong>War And Peace</strong></h6>
                        <div class="rating">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <p>884 Reviews</p>
                        <p>September ,2019</p>
                    </div>
                </div>
                <div class="sidebarbox">
                    <div class="imgbox">
                        <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                    </div>
                    <div class="desc">
                        <h6><strong>War And Peace</strong></h6>
                        <div class="rating">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                            <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <p>884 Reviews</p>
                        <p>September ,2019</p>
                    </div>
                </div>

                <div class="likebookMore">
                    <button type="button">see More</button>
                </div>
            </div>
          </div>
            <div class="col-sm-12 col-md-12 col-lg-12 pt-5">
                <div class="row" style="border-bottom: 1px solid #80808059;padding-bottom: 40px;">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div style="background-color: #54bfe3; height: 350px; width: 350px;">
                            <div style="height: 350px; width: 350px;">
                                <img src="http://bionovacalidad.es/comerbien/wp-content/uploads/2014/12/Pogo-SpaceBunny-350x350.jpg" class="img-fluid" style="margin: 36px;">
                                <div style="height: 350px; width: 350px; border: 5px solid gray; margin: -353px 0 0 77px; position: absolute;"></div>
                            </div>
                            
                            
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="heading">
                            <div class="title">About the Author</div>
                            <div class="subtitle">Author of 858 books</div>
                        </div>
                        <div style="border-bottom: 1px solid #80808059; margin: 28px 0px;"></div>
                        <div class="body">
                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                        </div>
                        <div class="socialicons">
                            Follow &nbsp;&nbsp; <i class="fa fa-long-arrow-right" aria-hidden="true"></i>&nbsp;&nbsp;

                            <a href="" target="_blank" ><i class="fa fa-facebook" aria-hidden="true"></i>
</a>
                            <a href="" target="_blank" ><i class="fa fa-instagram" aria-hidden="true"></i>
</a>
                            <a href="" target="_blank" ><i class="fa fa-twitter" aria-hidden="true"></i>
</a>
                            <a href="" target="_blank" ><i class="fa fa-linkedin" aria-hidden="true"></i>
</i>
</a>
                            <a href="" target="_blank" ><i class="fa fa-pinterest-p" aria-hidden="true"></i>
</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

      </div>
    </section>
<style type="text/css">
body{
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 83px;
    height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
div.plans button:hover div.plans{
    box-shadow: 1px 2px 8px #54bfe3 !important;
}
.plans {
    background: #fff;
    padding: 0px 0px 32px 0;
    border-top-left-radius: 60px;
    width: 86%;
    margin: 15px auto;
    box-shadow: 1px 2px 8px black;
    background-image: url(./assets/images/membership_plans_forma_tag.png);
    background-repeat: no-repeat;
    background-position-y: bottom;
}

.plans h1 {
    font-weight: bold;
    border-bottom: 1px solid gray;
    text-align: center;
    margin: 25px;
}
.plans h6 {
    font-size: 20px;
    text-align: center;
    font-family: monospace;
    font-weight: 600;
}
.plans p {
    text-align: center;
    text-decoration: underline;
    letter-spacing: 1px;
    padding: 13px;
}
.plans button {panel-collapse in collapse show
    background-color: #656161;
    margin: 0 auto;
    border: 1px solid gray;
    color: #fff;
    font-weight: bold;
    text-transform: capitalize;
    padding: 7px 34px;
    cursor: pointer;
}
.plans button:hover{
    background-color: #54bfe3;
    color: #fff;
}

.site-section {
    padding: 0em 0 4em 0;
}

.rating span {
    display: inline-block;
    margin: 0px 9px;
    font-size: 15px;
    cursor: pointer;
}
.description {
    margin: 4px 0;
    font-size: 15px;
    font-family: Lato,sans-serif;
}
.description p {
    margin-bottom: 0px;
}
.details p {
    margin-bottom: 2px;
    font-size: 14px;
}
label {
    display: inline-block;
    margin-bottom: 0rem;
}





.sidebar {
    background: #a9dcdb2e;
    padding: 0 0 2px 0px;
}
.sidebarboxheading {
    background: #fff;
    box-shadow: 0px 0px 6px #00bcd459;
    margin-bottom: 1.3rem;
    padding: 6px;
}
.AuthorbookMore, .likebookMore {
    margin: 0 0 12px 0px;
    text-align: center;
}

.AuthorbookMore button, .likebookMore button{
    background: transparent;
    border-bottom: 2px solid #00BCD4;
    color: #00BCD4;
    border-top: 0;
    border-left: 0;
    border-right: 0;
    text-transform: capitalize;
    font-size: 14px;
    cursor: pointer;
    padding: 0px 0px;
}
.sidebarbox {
    margin: 5px;
    padding: 8px;
}
.imgbox {
    display: block;
    float: left;
    width: 72px;
}
.desc {
    display: inline-block;
    margin: 0px 11px;
}
.desc p {
    margin: 3px 0px;
    font-size: 15px;
}
.desc h6 {
    margin-bottom: 2px;
    margin-top: 2px;
}
.sidebarbox:hover {
    box-shadow: 0 0 4px #7ff1f1c9;    border-bottom: 1px solid #80808059;
    margin: 28px 0px;
    background: #fff;
    cursor: pointer;
}
.breadcrumb {
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-inline-box;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    padding: 0.4rem 1rem;
    margin: 0 auto;
    list-style: none;
    background-color: #e9ecef0a;
    border-radius: 0.25rem;
}
.breadcrumb-item+.breadcrumb-item {
    padding-left: 0.3rem;
}
.breadcrumb-item a {
    color: #54bfe3;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #6c757d;
    content: ".";
}
.accordion .card-header.collapsed:after {
    content: "\f0d7";
}
.accordion .card-header:after {
    font-family: 'FontAwesome';
    content: "\f0d8";
    margin: 0px 0px 0px 7px;
    font-size: 24px;
    
}<div class="navPage">
                    <span class="prevnav"></span>
                    <span class="nxtnav"></span>
                </div>
.card-header {
    cursor: pointer;
}
.card {
    border: 0;
}
.card-header {
    background-color: #fff;
    font-size: 19px;
    font-weight: bold;
    color: #00BCD4;
}
.card-header.collapsed {
    color: gray;
}
.title {
    color: #55C0E1;
    font-weight: bold;
    font-size: 26px;
    background: 2px;
    font-family: Lato,sans-serif;
}
.subtitle {
    font-size: 15px;
    font-family: Lato,sans-serif;
}
.body p{
    font-size: 14px;
    font-family: Lato,sans-serif;
}
.socialicons{
    color: gray;
    font-size: 15px;
    font-family: Lato,sans-serif;
}
.socialicons a {
    color: gray;
    font-size: 17px;
    margin-left: 12px;
}
.socialicons a:hover {
    color: #00BCD4;
}


@media only screen and (max-width: 992px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 168px !important;
        height: 22vh;
    }
    .breadcrumb {
        display: flex;
    }
}
</style>
  