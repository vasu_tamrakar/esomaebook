<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style type="text/css">
        .panel-title {
        display: inline;
        font-weight: bold;
        }
        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }
    </style>
</head>
<body>
<?php $payerData = $this->session->userdata("payerData"); 
?>
<div class="container">
    <h1><?php echo SITENAME; ?> </h1>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default credit-card-box">
                <div class="panel-heading display-table" >
                    <div class="row display-tr" >
                        <h3 class="panel-title display-td" >Payment Details</h3>
                        <div class="display-td" >                            
                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                        </div>
                    </div>                    
                </div>
                <div class="panel-body">
                    <?php if($this->session->flashdata('success')){ ?>
                    <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p><?php echo $this->session->flashdata('success'); ?></p>
                        </div>
                    <?php } ?>
                    <form role="form" action="<?php echo site_url('signUp'); ?>" method="post" class="require-validation"

                                                     data-cc-on-file="false"

                                                    data-stripe-publishable-key="<?php echo $this->config->item('stripe_key') ?>"

                                                    id="payment-form">

     

                        <div class='form-row row'>

                            <div class='col-xs-12 form-group required'>

                                <label class='control-label'>Name on Card</label> <input

                                    class='form-control' size='4' type='text' name='txt_holdername' maxlength="180">

                            </div>

                        </div>

     

                        <div class='form-row row'>

                            <div class='col-xs-12 form-group card required'>

                                <label class='control-label'>Card Number</label> <input

                                    autocomplete='off' class='form-control card-number' size='20'

                                    type='text' name='txt_cardnumber' maxlength="20">

                            </div>

                        </div>

      

                        <div class='form-row row'>

                            <div class='col-xs-12 col-md-4 form-group cvc required'>

                                <label class='control-label'>CVC</label> <input autocomplete='off'

                                    class='form-control card-cvc' placeholder='ex. 311' size='4'

                                    type='text' name='txt_cvv' maxlength="4">

                            </div>

                            <div class='col-xs-12 col-md-4 form-group expiration required'>

                                <label class='control-label'>Expiration Month</label> <select

                                    class='form-control card-expiry-month' placeholder='Month'

                                    name='txt_expmonth'>
                                    <option vlaue="">Month</option>
                                    <option vlaue="01">01</option>
                                    <option vlaue="02">02</option>
                                    <option vlaue="03">03</option>
                                    <option vlaue="04">04</option>
                                    <option vlaue="05">05</option>
                                    <option vlaue="06">06</option>
                                    <option vlaue="07">07</option>
                                    <option vlaue="08">08</option>
                                    <option vlaue="09">09</option>
                                    <option vlaue="10">10</option>
                                    <option vlaue="11">11</option>
                                    <option vlaue="12">12</option>
                                </select>

                            </div>

                            <div class='col-xs-12 col-md-4 form-group expiration required'>

                                <label class='control-label'>Expiration Year</label> <select

                                    class='form-control card-expiry-year'

                                    name='txt_expyear'>
                                    <option vlaue="">Month</option>
                                    <script>
                                    
                                    var d= new Date(); 
                                    var x= d.getFullYear();
                                    var y= x+50
                                        for(;x < y ; x++){
                                        document.write('<option value="'+x+'">'+x+'</option>');
                                        }
                                    </script>
                                </select>

                            </div>

                        </div>

      

                        <div class='form-row row'>

                            <div class='col-md-12 error form-group hide'>

                                <div class='alert-danger alert'>Please correct the errors and try

                                    again.<a href="#" class="close" data-dismiss="alert">&times;</a></div>

                            </div>

                        </div>

                    <?php
                    if(isset($payerData["planID"])){
                        $planDetails = $this->user_Auth->getData('membershipplan', array("mp_id" => $payerData["planID"]));
                        // $planDetails[0]->mp_name;
                        // $planDetails[0]->mp_validity;
                        
                    }
                    ?>

                        <div class="row">

                            <div class="col-xs-12">

                                <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now ($ <?php echo $planDetails[0]->mp_price; ?>)</button>

                            </div>

                        </div>
                        
                        
                        <input type="hidden" name="txt_userID" id="txt_userID" value="<?php echo $payerData['userID']; ?>">
                        <input type="hidden" name="txt_planID" id="txt_planID" value="<?php echo $payerData['planID']; ?>">
                        <input type="hidden" name="txt_bookID" id="txt_bookID" value="<?php echo $payerData['bookID']; ?>">
                             

                    </form>

                </div>

            </div>        

        </div>

    </div>

         

</div>

     

</body>  

     

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

     

<script type="text/javascript">

$(function() {

    var $form         = $(".require-validation");

  $('form.require-validation').bind('submit', function(e) {

    var $form         = $(".require-validation"),

        inputSelector = ['input[type=email]', 'input[type=password]',

                         'input[type=text]', 'input[type=file]',

                         'textarea'].join(', '),

        $inputs       = $form.find('.required').find(inputSelector),

        $errorMessage = $form.find('div.error'),

        valid         = true;

        $errorMessage.addClass('hide');

 

        $('.has-error').removeClass('has-error');

    $inputs.each(function(i, el) {

      var $input = $(el);

      if ($input.val() === '') {

        $input.parent().addClass('has-error');

        $errorMessage.removeClass('hide');

        e.preventDefault();

      }

    });

     

    if (!$form.data('cc-on-file')) {

      e.preventDefault();
      // alert($form.data('stripe-publishable-key'));
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));

      Stripe.createToken({

        number: $('.card-number').val(),

        cvc: $('.card-cvc').val(),

        exp_month: $('.card-expiry-month').val(),

        exp_year: $('.card-expiry-year').val()

      }, stripeResponseHandler);

    }

    

  });

      

  function stripeResponseHandler(status, response) {

        if (response.error) {

            $('.error')

                .removeClass('hide')

                .find('.alert')

                .text(response.error.message);

        } else {

            var token = response['id'];

            $form.find('input[type=text]').empty();
            
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");

            $form.get(0).submit();

        }

    }

     

});

</script>

</html>