<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/biogrphy_bg.png'); ?>);">
    <div class="container">
        <div class="row">
        <div class="bannerheading"><h1><?php echo $categoryName; ?></h1></div>
        </div>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Category</a></li>
            <li class="breadcrumb-item active"><?php echo $categoryName; ?></li>
          </ol>
        </nav>

    </div>
    
</div>


<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="sidebar">
                    <div class="sidebar-boxx">
                      <form name="short_Form" id="short_Form" type="get" action="" class="inline-form">
                        <div class="formsearch">
                          <label class="txt_Short">Short By:</label>
                            <select name="txt_Short" id="txt_Short" class="selectfilter">
                                <option value="">Select Shorting</option>
                                <option value="">Recently Released</option>
                                <option value="">Most Popular</option>
                                <option value="">Title</option>
                                <option value="">Author</option>
                            </select>
                        </div>
                      </form>
                    </div>
                    <div class="sidebar-boxx">
                      <div class="categories">
                        <h3>Categories</h3>
                            <form name="cat_Form" id="cat_Form">
                                <li><label><input type="checkbox" name="" value=""> Creatives </label><span>(12)</span></li>
                                <li><label><input type="checkbox" name="" value=""> News </label><span>(22)</span></li>
                                <li><label><input type="checkbox" name="" value=""> Design </label><span>(37)</span></li>
                                <li><label><input type="checkbox" name="" value=""> HTML </label><span>(42)</span></li>
                                <li><label><input type="checkbox" name="" value=""> Web Development </label><span>(14)</span></li>
                            </form>
                      </div>
                    </div>

                   </div>
                   <div class="sidebar2">
                        
                        <div class="sidebarheading">Popular Authors</div>
                        <div class="box">
                            <div class="sidebarbox">
                                <div class="sidebarimg"><img src="<?php echo base_url('assets/images/popular_authors_6.png'); ?>" class="img-fluid"></div>
                                <div class="sidebartitle">Joshep. p</div>
                                <div class="sidebartext">1285 Published books</div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="sidebarbox">
                                <div class="sidebarimg"><img src="<?php echo base_url('assets/images/popular_authors_6.png'); ?>" class="img-fluid"></div>
                                <div class="sidebartitle">Joshep. p</div>
                                <div class="sidebartext">1285 Published books</div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="sidebarbox">
                                <div class="sidebarimg"><img src="<?php echo base_url('assets/images/popular_authors_6.png'); ?>" class="img-fluid"></div>
                                <div class="sidebartitle">Joshep. p</div>
                                <div class="sidebartext">1285 Published books</div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="sidebarbox">
                                <div class="sidebarimg"><img src="<?php echo base_url('assets/images/popular_authors_6.png'); ?>" class="img-fluid"></div>
                                <div class="sidebartitle">Joshep. p</div>
                                <div class="sidebartext">1285 Published books</div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="sidebarbox">
                                <div class="sidebarimg"><img src="<?php echo base_url('assets/images/popular_authors_6.png'); ?>" class="img-fluid"></div>
                                <div class="sidebartitle">Joshep. p</div>
                                <div class="sidebartext">1285 Published books</div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="sidebarbox">
                                <div class="sidebarimg"><img src="<?php echo base_url('assets/images/popular_authors_6.png'); ?>" class="img-fluid"></div>
                                <div class="sidebartitle">Joshep. p</div>
                                <div class="sidebartext">1285 Published books</div>
                            </div>
                        </div>

                   </div>
                </div>

                <div class="col-md-8">
                    <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div>
                            <div class="float-right">Showing 1-8 of 30 results</div>
                            <h3 class="heading">Most Popular</h3>
                        </div>
                        <div class="border-top pt-4"></div>
                        <div class="Mostppular row">
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <button type="button" id="mostpopularBook">see All</button> 
                        </div>
                    </div>

                    <div style="margin:30px;"></div>

                    <div class="col-md-12">
                        <div>
                            <div class="float-right">Showing 1-8 of 30 results</div>
                            <h3 class="heading">All about Business & Investing</h3>
                        </div>
                        <div class="border-top pt-4"></div>
                        <div class="Mostppular row">
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" class="img-fluid">
                                    <div class="rating">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                        <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                    </div>
                                    <h5>sdf saf asddf </h5>
                                    <p>;lsdkl sd</p>
                                </div>
                            </div>
                            <button type="button" id="mostpopularBook">see All</button> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="site-section" style="background-size: 100%;background-position: center center;background-image: url(<?php echo base_url('assets/images/become_a_member_bg.png'); ?>);background-repeat: no-repeat;">
        <div class="container">
            <div>
              <center><h3 class="heading">Do you wanna get unlimited access...? </h3>
                
              <div class="link"><a href="<?php echo site_url(''); ?>" class="memberbtn">Become a Menber</a></div></center>
            </div>        
        </div>
    </section>

<section style="margin: 70px 0px;">
    <div class="container">

    <div class="col-lg-12">
        <div>
            
            <h3 class="heading">Comming Soon</h3>
        </div>
        <div class="border-top pt-4"></div>
            <div id ="commingSoon" class="commingSoon owl-carousel owl-theme">
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                <div class="item">
                    <div class="commingSoonbooks">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid" style="box-shadow: 0px 0px 7px #0000007a;">
                        </a>
                        <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                      </div>
                      <div class="describe">
                        <div class="rates">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                          <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                        </div>
                        <label>Abandoned </label>
                        <span> 24 April 2018 </span>
                      </div>
                      <div class="readdiv"><a href="<?php echo site_url('1'); ?>">READ NOW</a></div>
                </div>
                
                
            </div>
        </div>
      </div>
  </section>

<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 328px;
    height: calc(40vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar {
    background: #54bfe3 !important;
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}


.bannerheading {
    margin: 0 auto;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    top: 130px;
    font-size: 55px;
}
.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}


.categories li, .sidelink li {
    position: relative;
    margin-bottom: 0px; 
    padding-bottom: 10px;
    border-bottom: 1px dotted #dee2e6;
    list-style: none;
}
h3.heading {
    font-weight: bold;
    font-size: 22px;
    text-transform: uppercase;
}
label {
    display: inline-block;
     margin-bottom: 0px; 
}
.categories h3 {
    color: #0009;
    font-weight: bold;
    font-size: 20px;
}
.sidebar {
    background: #e4fbfb4a;
    padding: 10px;
    box-shadow: 0 0 3px 0px #00000042;
    margin-bottom: 22px;
}
.sidebar-boxx {
    margin-bottom: 1px;
    padding: 25px;
    font-size: 15px;
    width: 100%;
    background: #e4fbfb4a;
}
li span {
    float: right;
}
.txt_Short {
    color: #0009;
    font-weight: bold;
    font-size: 14px;
    padding: 0px 1px;
}

#txt_Short {
    display: inline;
    width: 190px;
    border: none;
    background: #fff;
    color: gray;
}
.rating {
    margin: 4px 0px;
}
.box h5 {
    font-size: 17px;
    font-weight: bold;
    margin-top: 0.3rem;
    margin-bottom: 0.2rem;
}
.box p {
    font-size: 15px;
}
.formsearch {
    background: #fff;
    border: 1px solid #c8c1c1;
    padding: 3px;
}
.sidebarheading {
    background: #fff;
    box-shadow: 0px 2px 4px #00000029;
    font-weight: bold;
    font-size: 15px;
    padding: 3px;
    margin-bottom: 18px;
}
.sidebarbox {
    padding: 10px;
    display: block;
}
.sidebarimg {
    display: inline-block;
    width: 90px;
    border: 3px solid #8080806e;
    float: left;
}
.sidebartitle {
    display: inline-block;
    margin: 11px 0px 2px 13px;
    padding: 5px;
    font-weight: bold;
    font-size: 15px;
}
.sidebartext {
    margin: 5px 0px 13px 110px;
    font-size: 15px;
}
.sidebar2{
    background: #e4fbfb4a;
}
.sidebarbox:hover {
    box-shadow: 0 3px 8px #31ceceb3;
    cursor: pointer;
    background: #fff;
}
button#mostpopularBook {
    margin: 0 auto;
    border: 0px;
    background: #fff;
    border-bottom: 1px solid;
    text-transform: capitalize;
    font-weight: 500;
    color: #00BCD4;
    cursor: pointer;
}

/* Extra small devices (phones, 600px and down) */
@media only screen and (min-width: 600px) {
    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 130px;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -72px 24%;
        background: transparent;
    }
}
input[type=checkbox], input[type=radio] {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0;
    margin: 0px 9px;
}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
}
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {


} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

}

@media only screen and (max-width: 992px){
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 279px !important;
        height: 22vh;
    }
}
</style>
  