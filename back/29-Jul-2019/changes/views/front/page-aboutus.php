<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="site-blocks-cover overlay" data-aos="fade" id="home-section" style="background-image: url(<?php echo base_url('assets/images/about_bg.png'); ?>);">
    <div class="container">
        <div class="row">
        <div class="bannerheading"><h1>About Us</h1>
            <p>The best bookstore online</p>
        </div>
        </div>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">About Us</li>
          </ol>
        </nav>

    </div>
    
</div>


<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="col-md-12">
            <!-- <div class="heading"><h1>Leave us a message</h1></div> -->
            <div class="row">
                <div class="col-md-6">
                    <h3 class="heading">Who are we....  </h3>
                    <p>Create horizontal forms with the grid by adding the .row class to form groups and using the .col-*-* classes to specify the width of your labels and controls. Be sure to add .col-form-label to your s as well so they’re vertically centered with their associated form controls.</p>
<p>
At times, you maybe need to use margin or padding utilities to create that perfect alignment you need. For example, we’ve removed the padding-top on our stacked radio inputs label to better align the text baseline.</p>
                </div>

                <div class="col-md-6">
                   <img src="<?php echo base_url('assets/images/who_we_are.png'); ?>" class="img-fluid">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                   <img src="<?php echo base_url('assets/images/who_we_are.png'); ?>" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <h3 class="heading">Who are we....  </h3>
                    <p>Create horizontal forms with the grid by adding the .row class to form groups and using the .col-*-* classes to specify the width of your labels and controls. Be sure to add .col-form-label to your s as well so they’re vertically centered with their associated form controls.</p>
<p>
At times, you maybe need to use margin or padding utilities to create that perfect alignment you need. For example, we’ve removed the padding-top on our stacked radio inputs label to better align the text baseline.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="site-section" style="background-size: cover;background-position: center;background-image: url(./assets/images/become_a_member_bg.png);">
    <div class="container">
        <div>
          <center><h3 class="heading">Do you wanna get unlimited access...? </h3>
            
          <div class="link"><a href="<?php echo site_url('membership'); ?>" class="memberbtn">Become a Member</a></div></center>
        </div>        
        </div>
</section>
<section class="site-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div><h4 class="heading">RECENTLY RELEASED BOOKS</h4></div>
                    <div class="border-top pt-4"></div>

                    <div id ="recentlyreleasedBooks" class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                              </div>
                              <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                              </div>
                              <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                        <div class="item">
                          <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                            </div>
                            <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                        <div class="item">
                          <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_3.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                            </div>
                            <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                        <div class="item">
                          <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_4.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                            </div>
                            <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                        <div class="item">
                          <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_5.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                            </div>
                            <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                        <div class="item">
                          <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_6.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                            </div>
                            <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                        <div class="item">
                          <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_6.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                            </div>
                            <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                        <div class="item">
                          <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_5.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                            </div>
                            <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                        <div class="item">
                          <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_4.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                            </div>
                            <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                        <div class="item">
                          <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_3.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                            </div>
                            <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                        <div class="item">
                          <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_2.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                            </div>
                            <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                        <div class="item">
                            <div class="featurebooks">
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php echo base_url('assets/images/book_1.png'); ?>" alt="Image" class="img-fluid">
                                </a>
                                <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                            </div>
                            <div class="describe">
                                <div class="rates">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_a.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                  <img src="<?php echo base_url('assets/images/rating_b.png'); ?>" class="img-fluid">
                                </div>
                                <label>ASS a sd </label>
                                <span> Mr sdf sd </span>
                            </div>
                            <div class="readdiv"><a href="<?php echo site_url(); ?>">READ NOW</a></div>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
    </section>
<section class="site-section bg-white" id="contact-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 text-center">
                <br/>
                <br/>
                <h2 class="section-title mb-3">Enrich your knowladge with our tutorials</h2>
                <!-- <h3 class="section-sub-title">over 3.5 millions ebooks Read with no limit...</h3> -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
              <ul class="timeline">
        <!-- <li>
          <div class="timeline-badge success"><i class="glyphicon glyphicon-check"></i></div>
          <div class="timeline-panel">
            
            <div class="timeline-body">
              <div class="imageleft"></div>
              <div class="timeline-heading">
                <h4 class="timeline-title">Mussum ipsum cacilds</h4>
              </div>
              <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis.</p>
            </div>
          </div>
        </li> -->
        <li class="timeline-inverted">
          <div class="timeline-badge warning"><i class="glyphicon glyphicon-credit-card"></i></div>
          <div class="timeline-panel">
            
            <div class="timeline-body">
              <div class="imageright"><img src="<?php echo base_url('assets/images/people_think2.png'); ?>" class="img-fluid"></div>
              <div class="timeline-heading">
                <h4 class="timeline-title">Samita</h4>
              </div>
              <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis.Mussum ipsum cacilds, vidis litro abertis.</p>
            </div>
          </div>
        </li>
        <li>
          <div class="timeline-badge danger"><i class="glyphicon glyphicon-credit-card"></i></div>
          <div class="timeline-panel">
            
            <div class="timeline-body">
              <div class="imageleft"><img src="<?php echo base_url('assets/images/people_think3.png'); ?>" class="img-fluid"></div>
              <div class="timeline-heading">
                <h4 class="timeline-title">John Doe</h4>
              </div>
              <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis.Mussum ipsum cacilds, vidis litro abertis.</p>
            </div>
          </div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-badge warning"><i class="glyphicon glyphicon-credit-card"></i></div>
          <div class="timeline-panel">
            
            <div class="timeline-body">
              <div class="imageright"><img src="<?php echo base_url('assets/images/people_think1.png'); ?>" class="img-fluid"></div>
              <div class="timeline-heading">
                <h4 class="timeline-title">Andrew</h4>
              </div>
              <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis.Mussum ipsum cacilds, vidis litro abertis.</p>
            </div>
          </div>
        </li>
        <div class="loadmoreComment">View all comments</div>
    </ul>
            </div>
        </div>
    </div>
</section>



<style type="text/css">
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 365px;
    height: calc(47vh);
    background-size: cover;
    background-position: center;
}
.sticky-wrapper.is-sticky .site-navbar {
    background: #54bfe3 !important;
}
/*.sticky-wrapper.is-sticky .site-navbar ul li a {
    color: #505048c4 !important
}*/
.sticky-wrapper a.nav-link.dropdown-toggle {
    color: #fff !important;
}


.sticky-wrapper .is-sticky #headercatmenu a.nav-link.dropdown-toggle {
    color: #505048c4 !important
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target.shrink {
    background: #54bfe3;
}




.bannerheading {
    margin: auto;
    display: block;
}
.bannerheading h1 {
    position: relative;
    color: #fff;
    font-size: 55px;
    text-align: center;
}
.bannerheading p {
    color: #fff;
    top: 121px;
    font-size: 37px;
    font-weight: 500;
}
.breadcrumb {
    position: absolute;
    margin: -50px 30%;
    background: transparent;
}
li.breadcrumb-item a {
    color: aliceblue;
}
.breadcrumb-item+.breadcrumb-item:before {
    display: inline-block;
    padding-right: 0.5rem;
    color: #e4e8e8;
    content: ".";
}
.breadcrumb-item.active {
    color: #e4e8e8;
}

.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}

h2.heading {
    color: #757575bd;
    display: block;
    font-weight: bold;
    font-size: 31px;
}
form#contact_form input {
    font-size: 14px;
}
form#contact_form {
    font-size: 14px;
    margin: 22px 0px;
}
.loadmore {
    background: #54bfe3 !important;
    border-radius: 1px !important;
    color: #fff !important;
    font-weight: bold;
    font-size: 15px;
    margin-top: 20px;
    border: 1px solid #54bfe3 !important;
    padding: 12px 80px;
}
.address {
    background: #00BCD4;
    padding: 50px;
}
.address p {
    text-align: center;
    color: #fff;
}
.address label {
    text-align: center !important;
    margin: 0 auto !important;
    color: #fff;
    font-size: large;
}
.socialicon{
  text-align: center;
}
.socialicon a {
    padding: 10px;
    background-repeat: no-repeat;
    background-size: cover;
    display: inline-block;
    width: 30px;
    height: 30px;
    margin: 4px;
}
.socialicon a:hover {
    opacity: 0.6;
}
.form-control:active, .form-control:focus {

    border-color: #00BCD4;

}
a.facebook {
    background: url(http://localhost/ebook/assets/images/follow_us_fb.png);
}
a.twitter {
    background: url(http://localhost/ebook/assets/images/follow_us_tw.png);
}
a.google {
    background: url(http://localhost/ebook/assets/images/follow_us_g.png);
}
a.instagram {
    background: url(http://localhost/ebook/assets/images/follow_us_insta.png);
}
a.linkedin {
    background: url(http://localhost/ebook/assets/images/follow_us_link.png);
}
#recentlyreleasedBooks .owl-nav {

    position: absolute;
    top: 50%;
    width: 100%;

}
#recentlyreleasedBooks .item {
    margin: 10px;
}
#recentlyreleasedBooks .owl-prev {

    height: 24px;
    width: 24px;
    left: -2em;
    background-image: url(./assets/images/button_previous_2.png);
    background-size: cover;
    background-position: center;
}
#recentlyreleasedBooks .owl-next {

    height: 24px;
    width: 24px;
    right: -2em;
    background-image: url(./assets/images/button_next_2.png);
    background-size: cover;
    background-position: center;

}
.heading {
    text-transform: uppercase;
}
.link {
    margin-top: 20px;
}
.memberbtn {
    text-transform: uppercase;
    background: #1aaada;
    color: #fff;
    padding: 11px 15px;
    border-radius: 8px;
    font-size: 13px;
    font-weight: bold;
}
.memberbtn:hover {

    background: #86acb1cc;
    color: #fff;
    border: 1px solid #1aaada;
    cursor: pointer;

}



.imageleft {
    height: 147px;
    width: 120px;
    float: left;
    margin-right: 17px;
    display: block;
}
.imageright {
    height: 147px;
    width: 120px;
    float: right;
    margin-left: 17px;
    display: block;
}
.imageleft img {
  width: 100%;
  height: 100%;
}
.imageright img {
  width: 100%;
  height: 100%;
}
.loadmoreComment {
  font-size: 18px;
  text-transform: capitalize;
  border-bottom: 2px solid;
  display: inline-block;
}
/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {

    .bannerheading h1 {
        position: relative;
        color: #fff;
        top: 130px;
        font-size: 55px;
        text-align: center;
        font-family: unset;
    }
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
}
.timeline {
    list-style: none;
    padding: 20px 0 20px;
    position: relative;
}

    .timeline:before {
        top: 0;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: #eeeeee;
        left: 50%;
        margin-left: -1.5px;
    }

    .timeline > li {
        margin-bottom: 20px;
        position: relative;
    }

        .timeline > li:before,
        .timeline > li:after {
            content: " ";
            display: table;
        }

        .timeline > li:after {
            clear: both;
        }

        .timeline > li:before,
        .timeline > li:after {
            content: " ";
            display: table;
        }

        .timeline > li:after {
            clear: both;
        }

        .timeline > li > .timeline-panel {
            width: 46%;
            float: left;
            border: 1px solid #d4d4d4;
            border-radius: 2px;
            padding: 20px;
            position: relative;
            -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
            box-shadow: 0 0px 3px #00BCD4;
          }


            /*.timeline > li > .timeline-panel:before {
                position: absolute;
                top: 26px;
                right: -15px;
                display: inline-block;
                border-top: 15px solid transparent;
                border-left: 15px solid #ccc;
                border-right: 0 solid #ccc;
                border-bottom: 15px solid transparent;
                content: " ";
            }

            .timeline > li > .timeline-panel:after {
                position: absolute;
                top: 27px;
                right: -14px;
                display: inline-block;
                border-top: 14px solid transparent;
                border-left: 14px solid #fff;
                border-right: 0 solid #fff;
                border-bottom: 14px solid transparent;
                content: " ";
            }*/
            .timeline > li > .timeline-panel::before {
                position: absolute;
                top: -1px;
                right: -23px;
                display: inline-block;
                border-top: 0px solid transparent;
                border-left: 23px solid #ccc;
                    border-left-width: 23px;
                border-right: 0px solid #ccc;
                    border-right-width: 0px;
                border-bottom: 19px solid transparent;
                content: " ";
            }
            .timeline > li > .timeline-panel::after {
                position: absolute;
                top: 0px;
                right: -19px;
                display: inline-block;
                border-top: 0px solid transparent;
                border-left: 19px solid #fff;
                border-right: 0px solid #fff;
                border-bottom: 15px solid transparent;
                content: " ";
            }

        .timeline > li > .timeline-badge {
            /*color: #fff;
            width: 50px;
            height: 50px;
            line-height: 50px;
            font-size: 1.4em;
            text-align: center;
            position: absolute;
            top: 16px;
            left: 50%;
            margin-left: -25px;
            background-color: #999999;
            z-index: 100;
            border-top-right-radius: 50%;
            border-top-left-radius: 50%;
            border-bottom-right-radius: 50%;
            border-bottom-left-radius: 50%;
            */
            width: 10px;
            height: 10px;
            line-height: 50px;
            font-size: 1.4em;
            text-align: center;
            position: absolute;
            top: -10px;
            left: 50%;
            margin-left: -13px;
            background-color: #774c4c;
            z-index: 100;
            border-radius: 10%;
            border: 6px solid #fff;
            padding: 7px;
            box-shadow: 0 0 14px 1px #00BCD4;
        }

        .timeline > li.timeline-inverted > .timeline-panel {
            float: right;
        }

            .timeline > li.timeline-inverted > .timeline-panel:before {
                border-left-width: 0;
                border-right-width: 15px;
                left: -15px;
                right: auto;
            }

            .timeline > li.timeline-inverted > .timeline-panel:after {
                border-left-width: 0;
                border-right-width: 14px;
                left: -14px;
                right: auto;
            }

.timeline-badge.primary {
    background-color: #2e6da4 !important;
}

.timeline-badge.success {
    background-color: #3f903f !important;
}

.timeline-badge.warning {
    background-color: #f0ad4e !important;
}

.timeline-badge.danger {
    background-color: #d9534f !important;
}

.timeline-badge.info {
    background-color: #5bc0de !important;
}

.timeline-title {
    margin-top: 0;
    color: #54bfe3;
    font-size: 21px;
    font-family: Lato,sans-serif;
    font-weight: bold;
}

.timeline-body > p,
.timeline-body > ul {
    margin-bottom: 0;
    font-family: Lato,sans-serif;
}

    .timeline-body > p + p {
        margin-top: 5px;
    }

@media (max-width: 767px) {
    ul.timeline:before {
        left: 40px;
    }

    ul.timeline > li > .timeline-panel {
        width: calc(100% - 90px);
        width: -moz-calc(100% - 90px);
        width: -webkit-calc(100% - 90px);
    }

    ul.timeline > li > .timeline-badge {
        left: 15px;
        margin-left: 0;
        top: 16px;
    }

    ul.timeline > li > .timeline-panel {
        float: right;
    }

        ul.timeline > li > .timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        ul.timeline > li > .timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {
    .breadcrumb {
        position: absolute;
        margin: -91px 30%;
        background: transparent;
    }
}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
}
} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {

} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

}
</style>
