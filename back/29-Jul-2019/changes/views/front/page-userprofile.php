<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="site-blocks-cover overlay" data-aos="fade" id="home-section">
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2" style="background: #54bfe3;">
            <?php include 'frontleftmenu.php'; ?>
        </div>
        <div class="col-md-10 col-lg-10">
            <div class="row">
                <div class="col-md-12">
                <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade show" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                  <div class="row mb-1">
                      <div class="col-12 text-center">
                        <br/>
                        <h3 class="section-sub-title">Profile Form</h3>
                      </div>
                    </div>
                   <div class="row justify-content-md-center">
            <div class="col-md-10">
                <?php if($is_logged_in_user_info){ ?>
                <div style="margin: 0 auto;width:70px;height:70px;">
                    <img src="<?php echo (($is_logged_in_user_info['u_image'])?base_url('uploads/users/'.$is_logged_in_user_info['u_image']):base_url('uploads/users/user.png')); ?>" class="img-fluid" style="border-radius: 50%;">
                </div>
                <form name="userprofile_Form" id="userprofile_Form" action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_firstname">First Name:</label>
                                <input type="text" name="txt_firstname" id="txt_firstname" value="<?php echo (($is_logged_in_user_info['u_firstname'])?$is_logged_in_user_info['u_firstname']:''); ?>" class="form-control" placeholder="Enter First Name" max-length="150">
                                <?php echo form_error('txt_firstname', '<small class="error">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_lastname">Last Name:</label>
                                <input type="text" name="txt_lastname" id="txt_lastname" value="<?php echo (($is_logged_in_user_info['u_lastname'])?$is_logged_in_user_info['u_lastname']:''); ?>" class="form-control" placeholder="Enter Last Name" max-length="150">
                                <?php echo form_error('txt_lastname', '<small class="error">', '</small>'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_email">Email:</label>
                                <input type="text" name="txt_email" id="txt_email" value="<?php echo (($is_logged_in_user_info['u_email'])?$is_logged_in_user_info['u_email']:''); ?>" class="form-control" placeholder="Enter Email Address" max-length="150">
                                <?php echo form_error('txt_email', '<small class="error">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_mobile">Mobile:</label>
                                <input type="text" name="txt_mobile" id="txt_mobile" value="<?php echo (($is_logged_in_user_info['u_mobile'])?$is_logged_in_user_info['u_mobile']:''); ?>" class="form-control" placeholder="Enter Mobile Number" max-length="15">
                                <?php echo form_error('txt_mobile', '<small class="error">', '</small>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_role">User Role:</label>
                                <?php $role = $this->user_Auth->getData('user_roles',$w='',$se="ur_id,ur_name",$so='ur_id ASC');
                                $selrole = $is_logged_in_user_info['u_role'];
                                ?>
                                <select name="txt_role" id="txt_role" class="form-control" disabled>
                                    <option value="" >Select the role</option>
                                    <?php 
                                    if($role){
                                        foreach ($role as $value) {
                                            ?>
                                            <option value="<?php echo $value->ur_id; ?>" <?php if($value->ur_id === $selrole){ echo 'selected'; } ?>><?php echo $value->ur_name; ?></option>
                                            <?php
                                        }

                                    } ?>
                                </select>
                                <?php echo form_error('txt_role', '<small class="error">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_password">Change Password:</label>
                                <input type="password" name="txt_password" id="txt_password" value="" class="form-control" placeholder="*********" max-length="20">
                                <?php echo form_error('txt_password', '<small class="error">', '</small>'); ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_image">Profile Image :</label>
                                <input type="file" name="txt_image" id="txt_image" class="form-control-file" accept="image/*">
                                <small class="text-info">Image format accept only gif,jpg and png.</small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_status">User Status:</label>
                                <label for="txt_statustrue">
                                    <input type="radio" name="txt_status" id="txt_statustrue" value="1" <?php if ($is_logged_in_user_info['u_status'] == '1'){ echo 'checked';} ?>> TRUE
                                </label>
                                <label for="txt_statusfalse">
                                <input type="radio" name="txt_status" id="txt_statusfalse" value="0" <?php if ($is_logged_in_user_info['u_status'] == '0'){ echo 'checked'; } ?>> FALSE
                                </label>
                                <?php echo form_error('txt_status', '<small class="error">', '</small>'); ?>
                                <br/><small>Note: If you select false then your account will be happen freeze then will be contact ot administrator.</small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control"> Created Date : 
                                    <small class="text-info"><?php echo (($is_logged_in_user_info['u_created'] > 0)?date('d-M-Y H:i:s',strtotime($is_logged_in_user_info['u_created'])):''); ?>
                                    </small>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="form-control"> Last Modified Date : 
                                    <small class="text-info"><?php echo (($is_logged_in_user_info['u_modified'] > 0)?date('d-M-Y H:i:s',strtotime($is_logged_in_user_info['u_modified'])):''); ?>
                                    </small>
                                </label>
                            </div>
                        </div>
                    </div>
                      <button type="submit" class="btn btn-info">Update</button>
                </form>
                <?php
                }else{ ?>
                    <label class="form-control">In-valid Data.</label>
                <?php
                } ?>
            </div>
            <div style="padding-bottom:75px;"></div>
        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
body {
    font-family: Lato,sans-serif;
}
.site-blocks-cover, .site-blocks-cover > .container > .row {
    min-height: 102px;
height: calc(12vh);
}
header.site-navbar.py-4.js-sticky-header.site-navbar-target {
    background-color: #54BFE3;
}
/*.pb-4, .py-4 {
    padding-bottom: 0rem !important;
    padding-top: 0rem !important;
}*/
.site-section {
    padding: 0;
}

#headercatmenu a.nav-link.dropdown-toggle {
    color: #fff !important;
}
.section-title {
    color: #9B9B9B;
    font-size: 40px;
    font-weight: 900;
    font-family: unset;
}
.section-sub-title {
    font-size: 13px;
    color: #6d6064;
    letter-spacing: .2em;
    text-transform: uppercase;
}
a.site_title:hover{
    color: #fff;
}
.navbar-nav .nav-link {
    padding: 0.3rem 1rem;
}
.fa::after {
  margin-right: 2px;
  content: ;
  content: "";
}


@media only screen and (max-width: 992px) {
    .site-blocks-cover, .site-blocks-cover > .container > .row {
        min-height: 167px !important;
        height: 21vh;
    }
} 


</style>

  