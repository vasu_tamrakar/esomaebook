<div class="leftsidebar">
                <div class="nav_title" style="border: 0;">
                    <a href="<?php echo base_url(); ?>" class="site_title"><i class="fa fa-paw"></i> <span><?php echo SITENAME; ?></span></a>
                </div>

                <div class="clearfix"></div>
                <?php
                if($is_logged_in_user_info){ ?>

                
                <!-- menu profile quick info -->
                <div class="profile clearfix">
                  <div class="profile_pic">
                    <img src="<?php echo (isset($is_logged_in_user_info['u_image'])?base_url('uploads/users/').$is_logged_in_user_info['u_image']:base_url('uploads/users/').$is_logged_in_user_info['u_image']); ?>" alt="..." class="img-circle profile_img">
                  </div>
                  <div class="profile_info">
                    <span>Welcome,</span>
                    <h2><?php echo (isset($is_logged_in_user_info['u_firstname'])?$is_logged_in_user_info['u_firstname']:''). ' '.(isset($is_logged_in_user_info['u_lastname'])?$is_logged_in_user_info['u_lastname']:''); ?></h2>
                  </div>
                </div>
                <!-- /menu profile quick info -->
                <?php } ?>

                <div id="leftsidemenu">
                  <ul class="leftsidemenu navbar-nav">
                    <li class="nav-item">

                      <a class="nav-link" href="<?php echo site_url('user'); ?>"><i class="fa fa-home"></i> Dashboard</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('user/profile/'.$is_logged_in_user_info['u_id']); ?>"><i class="fa fa-user"></i> Profile</a>
                    </li>
                    <?php 
                    if($is_logged_in_user_info['u_role'] == 4){


                        if(!$authorAdded){ ?>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('user/addAuthor'); ?>"><i class="fa fa-edit"></i> Add New Author</a>
                    </li>
                      <?php
                        }else{
                      ?>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('user/editAuthor/'.$authorAdded[0]->a_id); ?>"><i class="fa fa-edit"></i> View And Edit Author</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('user/addbook'); ?>"><i class="fa fa-book"></i> Add New Books</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('user/viewBooks'); ?>"><i class="fa fa-book"></i> View Books</a>
                    </li>
                    <?php
                        }
                    } ?>
                  </ul>
                </div>
            </div>