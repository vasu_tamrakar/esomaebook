<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>View User Details</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
     
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>View User<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                  <div class="x_content">
                    <br />
                    
                      <?php if($userDetails){ ?>
                      <div class="col-md-8 col-md-offset-2">
                      <div class="row borderbot">
                          <div class="col-md-6">
                              <label class="control-label">User ID </label>
                          </div>
                          <div class="col-md-6">
                              <label class="control-label text-info"><?php echo $userDetails[0]->uc_id; ?> </label>
                          </div>
                        </div>
                        <div class="row borderbot">
                        <div class="col-md-6">
                            <label class="control-label">User Name </label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label text-info"><?php echo (($userDetails[0]->uc_firstname)?$userDetails[0]->uc_firstname:'').' '.(($userDetails[0]->uc_lastname)?$userDetails[0]->uc_lastname:''); ?> </label>
                        </div>
                        </div>
                        <div class="row borderbot">
                        <div class="col-md-6">
                            <label class="control-label">Email </label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label text-info">
                              <?php echo (($userDetails[0]->uc_email)?$userDetails[0]->uc_email:''); ?>
                            </label>
                        </div>
                        </div>
                        <div class="row borderbot">
                        <div class="col-md-6">
                            <label class="control-label">Mobile Number </label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label text-info">
                              <?php echo (($userDetails[0]->uc_mobile)?$userDetails[0]->uc_mobile:''); ?>
                              </label>
                        </div>
                        </div>
                        
                        <div class="row borderbot">
                        <div class="col-md-6">
                            <label class="control-label">Book Created </label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label text-info"><?php echo (($userDetails[0]->uc_created > 0)?date('d M-Y H:i:s', strtotime($userDetails[0]->uc_created)):""); ?> </label>
                        </div>
                        </div>
                        <div class="row borderbot">
                        <div class="col-md-6">
                            <label class="control-label">Book Modified </label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label text-info"><?php echo (($userDetails[0]->uc_modified > 0)?date('d M-Y H:i:s', strtotime($userDetails[0]->uc_modified)):""); ?> </label>
                        </div>
                        </div>
                        <div class="row borderbot">
                        <div class="col-md-6">
                            <label class="control-label">User Status </label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label text-info"><?php echo (($userDetails[0]->uc_status == '1')?"TRUE":"FALSE"); ?> </label>
                        </div>
                        </div>
                        <div class="row borderbot">
                        <div class="col-md-6">
                            <label class="control-label">User Active </label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label text-info">
                              <?php echo (($userDetails[0]->uc_active == '1')?'TRUE':'FALSE'); ?>
                              </label>
                        </div>
                        </div>
                        <div class="row borderbot">
                        <div class="col-md-6">
                            <label class="control-label">User Role </label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label text-info">
                              <?php
                              $uRole = $this->user_Auth->getData('user_roles', array('ur_id' => $userDetails[0]->uc_role), $se='ur_id, ur_name',$so='');
                               echo (($uRole[0]->ur_name)?$uRole[0]->ur_name:''); ?>
                              </label>
                        </div>
                        </div>
                        
                        <div class="row borderbot">
                        <div class="col-md-6">
                            <label class="control-label">User Picture </label>
                        </div>
                        <div class="col-md-6">
                            <div style="width: 100px;">
                              <img src="<?php echo (($userDetails[0]->uc_image)?base_url('uploads/users/'.$userDetails[0]->uc_image):base_url('uploads/users/user.png')); ?>" class="img-responsive">
                            </div>
                        </div>
                        </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-md-12" style="margin-top: 10px;">
                          <center>
                            <a href="<?php echo site_url('dashboard/viewallUsers'); ?>" class="btn btn-primary">Back</a>
                            <a href="<?php echo site_url('dashboard/addnewUser'); ?>" class="btn btn-info">Add New Book</a>
                            <a href="<?php echo site_url('dashboard/edituserDetails/'.$userDetails[0]->uc_id); ?>" class="btn btn-success">Edit</a>
                            <button type="button" onclick="deleteUserDetail(<?php echo $userDetails[0]->uc_id; ?>)" class="btn btn-danger">Delete</button>
                          </center>
                          </div>
                        </div>
                      <?php }else{
                        ?>
                        <div class="col-md-8 col-md-offset-2">
                          <label class="control-label"> Invalid Users.</label>
                        </div><?php
                      } ?>
                    </div>
                  </div>
                </div>

                <?php if((isset($userDetails[0]->uc_role)) && ($userDetails[0]->uc_role == 5)){ ?>

                <div class="x_panel">
                  <div class="x_title">

                    <h2>View Details<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                      
                     
                  <div class="x_content">
                    <?php 
                      
                      $payment = $this->user_Auth->getData('paymentDetails', array('pd_userid'=>$userDetails[0]->uc_id),$se='', $sh='pd_id ASC');
                          if($payment){ ?>
                          <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <th>pd_txnid</th>
                                  <th>pd_planid</th>
                                  <th>pd_planprice</th>
                                  <th>pd_userid</th>
                                  <th>pd_currency</th>
                                  <th>pd_payby</th>
                                  <th>pd_status</th>
                                  <th>pd_cardholder</th>
                                  <th>pd_cardnumber</th>
                                  <th>pd_created</th>
                                  <th>pd_modified</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                              foreach ($payment as $valueDet) { ?>
                                 
                                <tr>
                                    <td><?php echo $valueDet->pd_txnid; ?></td>
                                    <td><?php echo $valueDet->pd_planid; ?></td>
                                    <td><?php echo $valueDet->pd_planprice; ?></td>
                                    <td><?php echo $valueDet->pd_userid; ?></td>
                                    <td><?php echo $valueDet->pd_currency; ?></td>
                                    <td><?php echo $valueDet->pd_payby; ?></td>
                                    <td><?php echo $valueDet->pd_status; ?></td>
                                    <td><?php echo $valueDet->pd_cardholder; ?></td>
                                    <td><?php echo $valueDet->pd_cardnumber; ?></td>
                                    <!-- <td><?php echo $valueDet->pd_cvvnumber; ?></td>
                                    <td><?php echo $valueDet->pd_cartexpmonth; ?></td>
                                    <td><?php echo $valueDet->pd_cardexpyear; ?></td>
                                    <td><?php echo $valueDet->pd_chargeid; ?></td> -->
                                    <td><?php echo $valueDet->pd_created; ?></td>
                                    <td><?php echo $valueDet->pd_modified; ?></td>
                                </tr><?php
                              }
                              ?>
                            </tbody>
                          </table><?php
                          }    
                    
                    ?>
                  </div>
                  </div>
              </div>
              <?php } ?>
            </div>

            
          </div>
        </div>
        <!-- /page content -->





<style>
.borderbot {
    border-bottom: 1px solid;
}
</style>