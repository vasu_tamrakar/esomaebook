<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">     
    <div class="">
        <div class="page-title">
            <div class="title_left">
              <h3>Add New Author</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Add New Author Form<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                          <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div id="responcesResult"></div>
                    <div class="x_content">
                        <br />
                        <form name="author_Form" id="author_Form" action="<?php echo site_url('dashboard/addnewAuthor'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_authorfirstname">First Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_authorfirstname" id="txt_authorfirstname" value="<?php echo set_value('txt_authorfirstname'); ?>" placeholder="First Name" class="form-control col-md-7 col-xs-12">
                                            <?php echo form_error('txt_authorfirstname','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_authorlastname">Last Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_authorlastname" id="txt_authorlastname" value="<?php echo set_value('txt_authorlastname') ?>" placeholder="Last Name" class="form-control col-md-7 col-xs-12">
                                            <?php echo form_error('txt_authorlastname','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_authoremail">Email <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_authoremail" id="txt_authoremail" value="<?php echo set_value('txt_authoremail'); ?>" placeholder="Email Address" class="form-control col-md-7 col-xs-12">
                                            <?php echo form_error('txt_authoremail','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_authormobile">Mobile <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_authormobile" id="txt_authormobile" value="<?php echo set_value('txt_authormobile') ?>" placeholder="+91xxxxxxxxxx" class="form-control col-md-7 col-xs-12">
                                            <?php echo form_error('txt_authormobile','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_published">Published books <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="txt_published" id="txt_published" value="<?php echo set_value('txt_published') ?>" placeholder="2018" class="form-control col-md-7 col-xs-12">
                                            <?php echo form_error('txt_published','<span class="text-danger">','</span>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_image">Image <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="file" name="txt_image" id="txt_image"  class="form-control col-md-7 col-xs-12" accept="image/*">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_social">Facebook <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" name="txt_facebook" id="txt_facebook" value="<?php echo set_value('txt_facebook') ?>" placeholder="Facebook Link" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_social">Twitter <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" name="txt_twitter" id="txt_twitter" value="<?php echo set_value('txt_twitter') ?>" placeholder="Twitter link" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_social">Google+ <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" name="txt_gplush" id="txt_gplush" value="<?php echo set_value('txt_gplush') ?>" placeholder="Google plus link" class="form-control col-md-7 col-xs-12">
                                              </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_social">Instagram <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" name="txt_instagram" id="txt_instagram" value="<?php echo set_value('txt_instagram') ?>" placeholder="Instagram Link" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                   <textarea name="txt_message" id="txtEditor"></textarea>
                                   <label id="txt_message-error" class="error" for="txt_message">Description field is required.</label>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary" type="button">Cancel</a>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

        </div> 
    </div>
</div>
        <!-- /page content -->