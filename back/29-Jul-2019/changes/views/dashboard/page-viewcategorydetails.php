<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Categories Details</h3>
              </div>

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2><?php echo (($categoryDetails)?$categoryDetails[0]->c_name:''); ?> <small> Category Details.</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_content">
                    <br />
                    
                     
                        
                    <?php
                    if($categoryDetails){
                        $categoryDetails[0]->c_id; ?>
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>Name</th>
                              <th>Details</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><label> Category Name: </label></td>
                              <td><label> <?php echo $categoryDetails[0]->c_name; ?> </label></td>
                            </tr>
                            
                            <tr>
                              <td><label> Status:</label></td>
                              <td><label> <?php echo (($categoryDetails[0]->c_status)?"TRUE":"FALSE"); ?></label></td>
                            </tr>
                            <tr>
                              <td><label> Created Date:</label></td>
                              <td><label> <?php echo (($categoryDetails[0]->c_created > 0)?date('d-M-Y H:i:s',strtotime($categoryDetails[0]->c_created)):''); ?></label></td>
                            </tr>
                            <tr>
                              <td><label> Modified Date:</label></td>
                              <td><label> <?php echo (($categoryDetails[0]->c_modified > 0)?date('d-M-Y H:i:s',strtotime($categoryDetails[0]->c_modified)):''); ?></label></td>
                            </tr>
                            <tr>
                              <td>
                                <label> Category Description:</label>
                              </td>
                              <td><p><?php echo $categoryDetails[0]->c_description; ?></p></td>
                            </tr>
                            <tr>
                              <td>
                                <label> Category Image :</label>
                              </td>
                              <td>
                                <div style="width:150px;">
                                    <img src="<?php echo (($categoryDetails[0]->c_image)?base_url('uploads/category/'.$categoryDetails[0]->c_image):base_url('uploads/category/category.png')); ?>" class="img-responsive">
                                </div>
                              </td>
                            </tr>
                            <tbody>
                     </table>
                     <center>
                     <a href="<?php echo site_url('dashboard/editcategoryDetails/'.$categoryDetails[0]->c_id); ?>" class="btn btn-info">Edit Info</a>
                     
                     <button name="deleteCategory" id="deleteCategory" class="btn btn-info" onclick="deleteCategory(<?php echo $categoryDetails[0]->c_id; ?>)">Delete</button>
                   </center>
                    <?php }else{
                      echo "No details.";
                    }
                    
                     ?>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

