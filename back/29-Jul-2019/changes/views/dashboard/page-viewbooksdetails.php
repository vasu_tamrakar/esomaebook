<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>View Books Details</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>View Book<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <?php 
                    $alert = $this->session->flashdata('alert');
                    if($alert){
                        ?>
                        <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                          <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="x_content">
                        <br />
                        <div class="col-md-8 col-md-offset-2">
                        <?php if($bookDetails){ ?>
                            <div class="row bg-danger">
                                <div class="col-md-6 ">
                                    <label class="control-label">Book ID</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookDetails[0]->b_id; ?> </label>
                                </div>
                            </div>
                            <div class="row bg">
                                <div class="col-md-6">
                                    <label class="control-label">Book Language</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookDetails[0]->b_language; ?> </label>
                                </div>
                            </div>
                            <div class="row bg-danger">
                                <div class="col-md-6">
                                    <label class="control-label">Book Title</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookDetails[0]->b_title; ?></label>
                                </div>
                            </div>
                            <div class="row bg">
                                <div class="col-md-6">
                                    <label class="control-label">Author Name</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">
                                      <?php
                                      $author = $this->user_Auth->getData('user_credentials', $w=array('uc_id' => $bookDetails[0]->b_fk_of_aid), $se='uc_id, uc_firstname, uc_lastname, uc_email',$sh='');
                                      if(isset($author[0]->uc_id)){
                                          echo '<a href="'.site_url('dashboard/authorDetails/'.$author[0]->uc_id).'">'.(isset($author[0]->uc_firstname)?$author[0]->uc_firstname:'').' '.(isset($author[0]->uc_lastname)?$author[0]->uc_lastname:'').'</a>';
                                      }
                                      ?>
                                    </label>
                                </div>
                            </div>
                            <div class="row bg-danger">
                                <div class="col-md-6">
                                    <label class="control-label">Book Published</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo (($bookDetails[0]->b_published > 0)?date('M d Y', strtotime($bookDetails[0]->b_published)):""); ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Book Rating</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo $bookDetails[0]->b_rating; ?></label>
                                </div>
                            </div>
                            <div class="row bg-danger">
                                <div class="col-md-6">
                                    <label class="control-label">Book Category</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">
                                      <?php
                                      $category = $this->user_Auth->getData('categories', $w=array('c_id' => $bookDetails[0]->b_category), $se='c_id, c_name',$su='');
                                      echo (isset($category[0]->c_name)?$category[0]->c_name:'');
                                      ?></label>
                                </div>
                            </div>
                            <?php if($bookDetails[0]->b_file){ ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Book Picture</label>
                                </div>
                                <div class="col-md-6">
                                    <div style="width: 100px;">
                                      <a target="_blank" href="<?php echo base_url('uploads/books/'.$bookDetails[0]->b_file);?>" title=""><img src="<?php echo (($bookDetails[0]->b_image)?base_url('uploads/books/'.$bookDetails[0]->b_image):base_url('uploads/books/book.png')); ?>" class="img-responsive"></a>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="row bg-danger">
                                <div class="col-md-6">
                                    <label class="control-label">Book Status</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo (($bookDetails[0]->b_status)?"TRUE":"FALSE"); ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Book Created</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo (($bookDetails[0]->b_created > 0)?date('M d Y H:i:s',strtotime($bookDetails[0]->b_created)):""); ?></label>
                                </div>
                            </div>
                            <div class="row bg">
                                <div class="col-md-6">
                                    <label class="control-label">Book Modified</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo (($bookDetails[0]->b_modified > 0)?date('M d Y H:i:s',strtotime($bookDetails[0]->b_modified)):""); ?> </label>
                                </div>
                            </div>
                            <div class="row">
                                  <div class="col-md-12">
                                      <center>
                                          <a href="<?php echo site_url('dashboard/editbookDetails/'.$bookDetails[0]->b_id); ?>" class="btn btn-success">Edit</a>
                                          <a href="<?php echo site_url('dashboard/viewallBooks'); ?>" class="btn btn-warning">Back</a>
                                          <a href="<?php echo site_url('dashboard/addnewBook'); ?>" class="btn btn-primary">Add New Book</a>
                                          <button type="button" onclick="deletebookDetail(<?php echo $bookDetails[0]->b_id; ?>)" class="btn btn-info">Delete</button>
                                      </center>
                                  </div>
                            </div>
                    
                        <?php
                        }else{
                        ?><label class="control-label"> Invalid Books.</label><?php
                        } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->