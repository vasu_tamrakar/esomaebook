 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Datatables -->
    <link href="<?php echo base_url('themes/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('themes/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3> Veiw All Categories List<small></small></h3>
      </div>

      <!-- <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
      </div> -->
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>View All<small>Categories</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <?php 
              $alert = $this->session->flashdata('alert');
              if($alert){
                  ?>
                  <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                    <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <?php
              }
              ?>
  
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>C id</th>
                  <th>C Name</th>
                  <th>C Image</th>
                  <th>C Desc.</th>
                  <th>C status</th>
                  <th>C Created</th>
                  <th>C Action</th>
                </tr>
              </thead>
              <?php
              if($allcategoryids) { ?>
              
              <tbody>
                <?php
                  foreach ($allcategoryids as $cID) {
                  $category = $this->user_Auth->getData('categories', $w=array('c_id' => $cID->c_id),$se='',$sh='');
                   ?>
                <tr>
                  <td><?php echo $category[0]->c_id; ?></td>
                  <td><?php echo $category[0]->c_name; ?></td>
                  
                  <td>
                      <div style="width:50px;">
                          <img src="<?php echo (($category[0]->c_image)?base_url('uploads/category/'.$category[0]->c_image):base_url('uploads/category/category.png')); ?>" class="img-responsive">
                      </div>
                  </td>
                  <td>
                    <?php
                    if(strlen($category[0]->c_description) > 50){
                        echo substr($category[0]->c_description,0,50).'...';
                    }else{
                        echo ($category[0]->c_description);  
                    }
                     ?>
                  </td>
                  <td><?php echo (($category[0]->c_status)?"TRUE":"FALSE"); ?></td>
                  <td><?php echo (($category[0]->c_created > 0)?date('d-M-Y H:i:s',strtotime($category[0]->c_created)):""); ?></td>
                  <td>

                      
                      <div class="action-menu">
                          <a title="View Details" class="btn btn-info" href="<?php echo site_url('dashboard/categoryDetails/'.$cID->c_id); ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                          <a title="Edit" class="btn btn-info" href="<?php echo site_url('dashboard/editcategoryDetails/'.$cID->c_id); ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                          <a title="Delete" data-title="Goto twitter?" class="btn btn-info" href="javascript:void(0)" onclick="deletecategoryDetail(<?php echo $cID->c_id; ?>)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                      </div>
                      <div class="clearfix"></div>
                    
                    <?php //echo $author[0]->a_id; ?>
                  </td>
                </tr>
                <?php
                  }
                ?>
              </tbody>
              <?php 
              }else{ ?>
              
              No More Data..
              <?php
              } ?>
            </table>
  
  
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->