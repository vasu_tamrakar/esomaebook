<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Category</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
     
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>Edit Category Form<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <?php 
                  $alert = $this->session->flashdata('alert');
                  if($alert){
                      ?>
                      <div class="alert alert-<?php print_r($alert); ?> alert-dismissible fade in" role="alert">
                        <strong style="text-transform: capitalize;"><?php print_r($alert); ?>!</strong> <?php print_r($this->session->flashdata('message')); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php
                  }
                  ?>
                  <div class="x_content">
                    <br />
                    <?php if($categoryDetails){ ?>
                    <form name="editcategory_Form" id="editcategory_Form" action="<?php echo site_url('dashboard/editcategoryDetails/'.$categoryDetails[0]->c_id); ?>" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
                      <div style="width: 150px !important; display: block; margin: 0 auto 10px;">
                          <a href="<?php echo (($categoryDetails[0]->c_image)?base_url('uploads/category/'.$categoryDetails[0]->c_image):base_url('uploads/category/category.png')); ?>">
                              <img src="<?php echo (($categoryDetails[0]->c_image)?base_url('uploads/category/'.$categoryDetails[0]->c_image):base_url('uploads/category/category.png')); ?>" class="img-responsive">
                          </a>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_categoryname">Category Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_categoryname" id="txt_categoryname" value="<?php echo $categoryDetails[0]->c_name; ?>" placeholder="Category Name" class="form-control col-md-7 col-xs-12">
                          <?php echo form_error('txt_categoryname','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_catdescription">Category Descriptions  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea name="txt_catdescription" id="txt_catdescription" rows="5" placeholder="Descriptions..." class="form-control col-md-7 col-xs-12"><?php echo $categoryDetails[0]->c_description; ?></textarea>
                          <?php echo form_error('txt_catdescription','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_categorystatus">Category Stastus <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <label for="txt_categorystatus1">
                              <input type="radio" name="txt_categorystatus" id="txt_categorystatus1" value="1" <?php if($categoryDetails[0]->c_status === '1'){echo 'checked'; } ?>> TURE
                          </label>
                          <label for="txt_categorystatus0">
                              <input type="radio" name="txt_categorystatus" id="txt_categorystatus0" value="0" <?php if($categoryDetails[0]->c_status === '0'){echo 'checked'; } ?>>FALSE
                          </label>
                          <?php echo form_error('txt_categorystatus','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_createddate">Created Date
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="txt_createddate" id="txt_createddate" value="" placeholder="<?php echo $categoryDetails[0]->c_created; ?>" class="form-control col-md-7 col-xs-12">
                          <?php echo form_error('txt_createddate','<span class="text-danger">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txt_image">Image 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="file" name="txt_image" id="txt_image"  class="form-control col-md-7 col-xs-12" accept="image/*">
                          <small class="text-info">Image format accept only png,jpeg,jpg,gif 261 x 184-398 px. </small>
                        </div>
                      </div>
                      

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">Update</button>
                          <a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary" type="button">Cancel</a>
                        </div>
                      </div>

                    </form>
                    <?php }else{ ?>
                    In-valid data. 
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>

            
          </div>
        </div>
        <!-- /page content -->
        